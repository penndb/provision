#!/usr/bin/python

# Add provenance tracking available for each trimming function

#------------------------------------------------------------------------------------------
# INITIALIZATIONS
#------------------------------------------------------------------------------------------

import sys, os, argparse
#from asyncore import read
#from operator import pos
#from matplotlib.backends.backend_ps import seq_allequal


VERSION = '0.7.2'
PAIRED = False

# indecies for the read set
HEADER = 'header'
SEQUENCE = 'seq'
QUALS = 'quals'
LENGTH = 'length'
TRIMMED = 'trimmed'
FLAGS = 'flags'
DISCARDED = 'discarded'
TRIMMED_LOCATION = 'trimmed_location'
POSITION = 'position'

DEFAULT_WORKERS = 4
DEFAULT_END = 3
DEFAULT_MAX = 0
DEFAULT_METHOD = 2
DEFAULT_PERCENT_IDENTITY = .9
DEFAULT_SIZE_KMER = 7
DEFAULT_TOTAL_IDENTITY = 16
DEFAULT_WINDOWS_NUMBER = 6


# used by removePolyAT()
POLY_A = 'AAAAAAAAAA'
POLY_T = 'TTTTTTTTTT'

def computeKMers(seq, kmerSize, numWindows):
    seqLength = len(seq)

    # the last window is bounded by the length of the sequence and the size of a k-mer
    lastWindow = seqLength - kmerSize
    if numWindows > 1:
        windowStep = lastWindow / (numWindows - 1)
    else:
        # make sure we only get one k-mer
        windowStep = seqLength

    kmerList = []
    # slice up the sequence based on the window step size and the k-mer size
    for index in range(0, lastWindow + 1, windowStep):
        # store k-mer and the k-mer position offset for a 3'
        # mapping. Offset is the 5' position of the k-mer.
        kmerList.append((seq[index:index+kmerSize], index))

    return kmerList


def parseContaminants(contaminantsFileName):
    contaminantList = []
    try: 
        contFile = open(contaminantsFileName, 'r')
        # need to retain order so using tuple instead of set
       
        count = 0
        for line in contFile:
            if line.startswith('>'):
                header = line[1:].strip()
                # need to process header arguments
                args = header.split()
        
                # convert options string into set. Initialize values to
                # defaults. The initialized set doesn't include the name
                # as name is required.
                options = { 'method':DEFAULT_METHOD, 
                        'size':DEFAULT_SIZE_KMER, 
                        'windows':DEFAULT_WINDOWS_NUMBER,
                        'percentIdentity':DEFAULT_PERCENT_IDENTITY, 
                        'totalIdentity':DEFAULT_TOTAL_IDENTITY }
                for option in args:
                    key, value = option.split(':')
        
                    if key == 'method':
                        value = int(value)
                        if value != 0 and value != 1 and value != 2:
                            msg = 'Invalid "method" option (%d) for contaminant. Must be 0, 1, or 2.' % value
                            quitOnError(msg)
                    elif key == 'size':
                        value = int(value)
                    elif key == 'windows':
                        value = int(value)
                    elif key == 'totalIdentity':
                        value = int(value)
                    elif key == 'percentIdentity':
                        value = float(value)
                        if value <= 0.0 or value > 1.0:
                            msg = 'Invalid "percentIdentity" option (%f) for contaminant. Percent identity must be greater than 0 and less than or equal to 1.' % value
                            quitOnError(msg)
                    # at this point we've accounted for all options but
                    # the name. If we don't have a name then the option
                    # doesn't exist.
                    elif key != 'name':
                        msg = 'Invalid contamination option "%s".' % option
                        quitOnError(msg)
        
                    # save value in options
                    options[key] = value
        
                # make sure we loaded a name.
                if 'name' not in options:
                    quitOnError('Contaminant does not have a name.')
        
                # use name to initialize contaminant trimming counts. We
                # initialize the counts for N and poly A/T removal below
                # (ie outside of the if/then statement) as we might not
                # always have a contaminants file.
            
            else:
                seq = line.strip().upper()
    
                # compute set of k-mers
                kmerList = computeKMers(seq, options['size'], options['windows'])
    
                if len(seq) < options['size']:
                    msg = 'The k-mer size (%d) must be smaller than the length of the sequence (%s).' % (options['size'], seq)
                    quitOnError(msg)
    
                    # save contaminant and related values in tuple
                contaminantList.append([options, seq, kmerList])
    
                count += 1
    
        contFile.close()
    except IOError: print 'ERROR: No such file', clArgs.contaminants_fa
    return contaminantList

# cut nCut from the 3' end of the seq
def cut3(read, nCut):
    seq = read[SEQUENCE]
    quals = read[QUALS]
    length = len(read[SEQUENCE])
    #trimmedLocation = read[TRIMMED_LOCATION]
    position = read[POSITION]
    
    seq = seq[:(length - nCut)]
    #trimmedLocation.extend([i for i in range(length - nCut, length)])
    position = position[:(length - nCut)]
    quals = quals[:(length - nCut)]
    
    read[SEQUENCE] = seq
    read[QUALS] = quals
    read[LENGTH] = len(seq)
    #read[TRIMMED_LOCATION] = trimmedLocation
    read[POSITION] = position
    return read

# cut nCut from the 5' end of the seq
def cut5(read, nCut):
    seq = read[SEQUENCE]
    quals = read[QUALS]    
    length = len(read[SEQUENCE])
    #trimmedLocation = read[TRIMMED_LOCATION]
    position = read[POSITION]
    
    seq = seq[nCut:]
   #trimmedLocation.extend([i for i in range(length - nCut, length)])
    position = position[nCut:]
    quals = quals[nCut:]
    
    read[SEQUENCE] = seq
    read[QUALS] = quals
   #read[LENGTH] = len(seq)
   #read[TRIMMED_LOCATION] = trimmedLocation
    read[POSITION] = position
    return read


# Replace base with N if quality score is below the user-specified
# phredThreshold. Low quality bases at the ends of the read will be
# removed if -rN flag is set. We do not change the quality score, just
# the base.
def qualityScoreThreshold(read, phredThreshold):
    # Replace low quality bases with N
    seq = read[SEQUENCE]
    quals = read[QUALS]
    length = len(read[SEQUENCE])
    #trimmedLocation = read[TRIMMED_LOCATION]
    position = read[POSITION]
    
    wasTrimmed = False
    i = 0
    newSeq = ""
    newPos = []
    for qualityScore in quals:
        if ord(qualityScore) < phredThreshold:
            newSeq += 'N'
            #trimmedLocation.add(i)
            newPos.append(i)
            wasTrimmed = True
        else:
            newSeq += seq[i]
            newPos.append(i)
        i += 1
        
    if wasTrimmed:
            read[SEQUENCE] = newSeq
            read[POSITION] = newPos
    return read


# remove N's on either end of the seq
def removeNs(read):
    # Remove any N's from the 3' and 5' ends of a seq
    seq = read[SEQUENCE]
    quals = read[QUALS]
    length = len(read[SEQUENCE])
    #trimmedLocation = read[TRIMMED_LOCATION]
    position = read[POSITION]
      
    wasTrimmed = False
    
    if seq.startswith('N'):
        seq = seq.lstrip('N')
        #trimmedLocation.extend(range(0, len(quals) - len(seq)))
        position = position[len(position) - len(seq):]
        quals = quals[len(quals) - len(seq):]
        wasTrimmed = True
        
    if seq.endswith('N'):
        seq = seq.rstrip('N')
        position = position[:len(seq)]
        quals = quals[:len(seq)]
        wasTrimmed = True
        
    if wasTrimmed:
        read[SEQUENCE] = seq
        read[QUALS] = quals
        #read[LENGTH] = len(seq)
        #read[TRIMMED_LOCATION] = trimmedLocation
        read[POSITION] = position
        
    return read

    
# trim the entire contaminant based on single k-mer mapping from 5' end to 3' end
# @ contaminant is a tuple (options{}, seq, k-merlist[])
def fullContaminantTrimming(read, contaminant):
    seq = read[SEQUENCE]
    quals = read[QUALS]
    #length = len(read[SEQUENCE])
    position = read[POSITION]
    
    kmers = contaminant[2]
    options = contaminant[0]
    name = option['name']
    
    # 5'end is the right side
    pos = -1
    for kmer, offset in kmers:
        index = seq.rfind(kmer)
        # if found
        if index > -1:
            pos = index - offset
            break
        
    if pos == -1:
        return read
    # once found matching, trim entire right side (right side is 5' end)
    seq = seq[:pos]
    quals = quals[:pos]
    position = position[:pos]
    
    read[SEQUENCE] = seq
    read[QUALS] = quals
    read[POSITION] = position
    return read


# trim the portion that maps from the k-mer region to the end of seq
# @ contaminant (options{}, cSeq, k-merList{})
def mappedContaminantTrimming(read, contaminant):
    seq = read[SEQUENCE]
    quals = read[QUALS]
    #length = len(read[SEQUENCE])
    position = read[POSITION]
    
    # contaminatn sequence
    cSeq = contaminant[1]
    #cLength = len(cSeq)
    
    kmers = contaminant[2]
    options = contaminant[0]
    name = options['name']
    
    pos = -1
    for kmer, offset in kmers:
        if pos > -1:
                break
        
        index = seq.rfind(kmer)
        if index > -1: #found
            pos = 0
            while offset > 0 and index > 0:
                if seq[index-1] != cSeq[offset - 1]:
                    pos = index
                    break
                else:
                    index -= 1
                    offset -= 1
                    
    if pos == -1: # not found
        return read
    
    seq = seq[:pos]
    quals = quals[:pos]
    position = position[:pos]
    
    read[SEQUENCE] = seq
    read[QUALS] = quals
    read[POSITION] = position
    return read

"""
# @ contaminant (options{}, cSeq, k-merList{})
# if the options contains oercentIdentity
def identityTrimming(read, contaminant):
    seq = read[SEQUENCE]
    quals = read[QUALS]
    position = read[POSITION]
    length = len(seq)
    
    cSeq = contaminant[1]
    cLength = len(seq)
    kmers = contaminant[2]
    options = contaminant[0]
    name = options['name']
    percIdentityThreshold = options['percentIdentity']
    totIdentityThreshold = options['totalIdentity']
    
    percIdentity = 0
    totIdentity = 0
    for kmer, offset in kmers:
        index = seq.rfind(kmer)
        if index > -1:
            pos = index - offset
            # cPos is the position in contaminant
            cPos = 0
            if pos < 0:
                cPos = pos * -1
                pos = 0
            rPos = pos
            totIdentity = 0
            count = 0
            while cPos < cLength and rPos < length:
                # count the number of matching bases
                if seq[rPos] == cSeq[cPos] :
                    totIdentity += 1
                cPos += 1
                rPos += 1
                count += 1
                
            percIdentity = float(totIdentity)/float(count)
            break
    if index == -1:
        return read
    if (percIdentity < percIdentityThreshold) or (totIdentity < totIdentityThreshold):
        return read
    
    seq = seq[:pos]
    quals = quals[:pos]
    position = position[:pos]
    
    read[SEQUENCE] = seq
    read[QUALS] = quals
    read[POSITION] = position
    return read
"""

# remove poly A from 3' end and poly T from 5' end
def removePolyAT(read, trimLength):
    seq = read[SEQUENCE]
    quals = read[QUALS]
    position = read[POSITION]
    wasTrimmed = False
    
    if seq.startswith(POLY_A):
        n_mer = seq[:trimLength]
        n_mer = n_mer.lstrip('T')
        seq = n_mer + seq[trimLength:]
        quals = quals[len(quals) - len(seq):]
        position = position[len(position)-len(seq):]
        wasTrimmed = True
        
    if seq.endswith(POLY_A):
        idx = len(seq) - trimLength
        n_mer = seq[idx:]
        n_mer = n_mer.rstrip('A')
        seq = seq[:idx] + n_mer
        quals = quals[:len(seq)]
        position = position[:len(seq)]
        wasTrimmed = True
    
    if wasTrimmed:
        read[SEQUENCE] = seq
        read[QUALS] = quals
        read[POSITION] = position
        
    return read
    
    

def identityTrimming(read, contaminant):
    seq = read[SEQUENCE]
    quals = read[QUALS]
    length = len(seq)
    position = read[POSITION]

    # contaminant sequence
    cSeq = contaminant[1]
    cLength = len(cSeq)
    # list of contaminant k-mers to look for in read
    kmers = contaminant[2]
    # load contaminant's options
    options = contaminant[0]
    # contaminant name
    name = options['name']
    # percent identity threshold
    percIdentityThreshold = options['percentIdentity']
    # total identity threshold
    totIdentityThreshold = options['totalIdentity']

    # we attempt to perfectly align each k-mer against the
    # sequence. If any alignment succeeds, then we compute the percent
    # identity between the contaminant and read. If above our
    # threshold then trim the entire contaminant, otherwise do not
    # trim anything.
    percIdentity = 0
    totIdentity = 0
    for kmer, offset in kmers:
        # look for k-mer in read sequence. Index is the position from
        # the 5' end of the read where the k-mer mapped to the read.
        # If the k-mer maps multiple times in the read, then the
        # mapping closest to the 3' end is used.
        index = seq.rfind(kmer)
        if index > -1:
            # Pos is the position where the 5' end of contaminant
            # overlaps the read, based in k-mer mapping. Offset is
            # the position of the 5' end of the k-mer within the
            # contaminant.
            pos = index - offset

            # cPos is the position in the contaminant
            cPos = 0

            # if the contaminant hangs off the 5' end of the read then
            # we need to adjust the search to begin from the 5' end of
            # the read
            if pos < 0:
                # shift where we begin in the contaminant based on how
                # many bases are hanging off the 5' end of the read
                cPos = pos * -1
                # start at 5' end of read
                pos = 0

            # rPos is the position in the read
            rPos = pos

            totIdentity = 0
            count = 0
            while cPos < cLength and rPos < length:
                # count the number of matching bases
                if seq[rPos] == cSeq[cPos]: totIdentity += 1

                # increment positions so we check next base
                cPos += 1
                rPos += 1
                # count the number of bases checked
                count += 1

            # we've now checked all overlapping bases, so we can
            # compute the percent identity.
            percIdentity = float(totIdentity) / float(count)

            # break out of loop since we found a match
            break

    # if k-mers not found, then no trimming
    if index == -1: return read

    # if the k-mers were not found or the contaminant didn't map
    # sufficiently to the read then we don't trim.
    if (percIdentity < percIdentityThreshold) or (totIdentity < totIdentityThreshold): return read

    seq = seq[:pos]
    quals = quals[:pos]
    position = position[:pos]


    read[SEQUENCE] = seq
    read[QUALS] = quals
    read[POSITION] = position
    return read



# Not considering actions based on pairs
def trimSingleFile(cmd, readHeader, readString, baseQualityString):
    sys.argv = cmd.split()
    argParser = argparse.ArgumentParser(version=VERSION, 
                    description='Trim NGS reads (requires Python 2.7 or later).',
                    formatter_class=argparse.RawDescriptionHelpFormatter)
    argParser.add_argument( '-p', '--padPaired', dest='padPaired', action='store_true', default=False,
                help='Pad paired reads so that they are the same length after all trimming has occured. N\'s will be added to the 3\' end with \'#\' added to the quality score for each N that is added. This will not do anything for single-end reads. (default: no)' )
    argParser.add_argument( '-m', '--minLen', dest='min_len', action='store', default=0, type=int,
                help='Minimum size of trimmed read. If trimmed beyond minLen, then read is discarded. If read is paired then read is replaced with N\'s, unless both reads in pair are smaller than minLen in which case the pair is discarded. (default: no minimum length)' )
    argParser.add_argument( '-c3', '--cut3', dest='num_cut_3', action='store', default=0, type=int, 
                help='number of bases to remove from 3\' end of read. TRUNCATING READS DOES NOT COUNT TOWARD TRIMMING TOTALS. This happens prior to the removing of N\'s and hence prior to contaminant trimming. (default: 0)' )
    argParser.add_argument( '-c5', '--cut5', dest='num_cut_5', action='store', default=0, type=int, 
                help='number of bases to remove from 5\' end of read. TRUNCATING READS DOES NOT COUNT TOWARD TRIMMING TOTALS. This happens prior to the removing of N\'s and hence prior to contaminant trimming. (default: 0)' )
    argParser.add_argument( '-q', dest='phred_threshold', action='store', default=0, type=int, 
                help='threshold for Phred score below which the base will be changed to an N. This happens prior to the removing of N\'s and hence low quality bases at the ends of the read will be removed if this is used in conjunction with the -rN flag. QUALITY SCORES ARE NOT SCALED BASED ON ENCODING SCHEME. For example, use a threshold of 53 to filter Illumina 1.8 fastq files (Phred+33) based on a Phred score of 20. REPLACING BASES WITH N DOES NOT COUNT TOWARD TRIMMING TOTAL. (default: 0)' )
    argParser.add_argument( '-rN', '--removeNs', dest='removeN', action='store_true', default=False,
                help='remove N\'s from both ends of the read. This trimming happens before contaminant trimming. (default: no)' )
    argParser.add_argument( '-rAT', '--removePolyAT', dest='remove_AT', action='store', default=-1, type=int,
                help='length of 3\' poly-A and 5\' poly-T to remove from the respective ends of the read. If all poly A/T is to be removed then the value should be equal to or greater than the length of the read. A minimum of ten A\'s or ten T\'s must exist in order for this trimming to happen, regardless of the trimming length; that is, poly-A and poly-T fragments are defined as being at least 10 nt in length. A sequences of A\'s or T\'s are ignored. This trimming happens after contaminant trimming. (default: no trimming)' )
    argParser.add_argument( '-c', dest='contaminants_fa', action='store', default=None, 
                help='fasta-like file containing list of contaminants to trim from the 3\' end of the read' )
    """
    argParser.add_argument( '-f', dest='first_fq', action='store', required=True,
                help='fastq file with reads to be trimmed' )
    argParser.add_argument( '-r', dest='second_fq', 
                help='second read with paired-end reads. This file is not present for single-end reads.' )
    argParser.add_argument( '-o', dest='output_prefix', action='store', required=True,
                help='prefix for output file(s). A \'_1\' will be appended to the first reads output file and if paired reads then a \'_2\' will be appended to the second reads file. Output files similarly named and with the suffix \'.disc.txt\' will be created to store the fastq headers for reads shorter than the minimum length threshold after trimming.' )            
    argParser.add_argument( '-t', dest='threads', type=int, default=4,
        help='Number of worker processes. Set to 1 for serial execution in a single process. Increasing above 5 is unlikely to produce any speedup. (default: 4)' )
    """
    argParser.add_argument( '-C', '--chunk-size', dest='chunk_size', type=int, default=200,
                help='Number of reads passed to each worker thread at a time. For 200 read chunks and 5 threads, blocks of 1000 reads would be read in at a time. The "sweet spot" of low memory usage and high cpu utilization appears to be fairly low, somewhere around 100 to 1000 on test data.' )
    
    global clArgs, contaminantsList
    clArgs = argParser.parse_args()
    if clArgs.contaminants_fa:
        contaminantsList = parseContaminants(clArgs.contaminants_fa)
    read = {}
    read[HEADER] = readHeader
    read[SEQUENCE] = readString
    read[QUALS] = baseQualityString
    read[POSITION] = range(len(readString))
    return doTrimming(read)

    
def doTrimming(fread):
    #fread[LENGTH] = len(fread[SEQUENCE])
    #fread[TRIMMED] = False
    #fread[FLAGS] = []
    #fread[DISCARDED] =False
    #fread[TRIMMED_LOCATION] = []
    #originLen = fread[LENGTH]
    provLocation = fread[POSITION]
    
    if clArgs.num_cut_3 > 0:
        fread = cut3(fread, clArgs.num_cut_3)
        
    if clArgs.num_cut_5 > 0:
        fread = cut5(fread, clArgs.num_cut_5)
        
    # threhold quality scores 
    if clArgs.phred_threshold > 0:
        fread = qualityScoreThreshold(fread, clArgs.phred_threshold)
    
    if clArgs.removeN:
        fread = removeNs(fread)
        
    if clArgs.contaminants_fa:
        for contaminant in contaminantsList:
            method = contaminant[0]['method']
            if method == 0:
                fread = fullContaminantTrimming(fread, contaminant)
            elif method == 1:
                fread = mappedContaminantTrimming(fread, contaminant)
            else:
                fread = identityTrimming(fread, contaminant)
                
    if clArgs.remove_AT > 0:
        fread = removePolyAT(fread, clArgs.remove_AT)
        
    #finalTrimmedLoc = list(set(fread[TRIMMED_LOCATION]))    
    #provLocation = [i for i in provLocation if i not in finalTrimmedLoc]
    provLocation = fread[POSITION]
    header = fread[HEADER]
    seq = fread[SEQUENCE]
    quals = fread[QUALS]
        
    location = dict([('field', 'readString'),('position',provLocation)])
    #value = dict([('readHeader', header),('readString',seq), ('baseQualityString', quals)])
    value = dict([('readString_trimmed',seq), ('baseQualityString_trimmed', quals)])
    row = dict([('location', location),('value',value)])
    result = [row]
    return result



def test(readHeader, readString, baseQualityString):
    read = {}
    read[HEADER] = readHeader
    read[SEQUENCE] = readString
    read[QUALS] = baseQualityString
    #read[LENGTH] = len(read[SEQUENCE])
    read[POSITION] = range(len(readString))
    read = cut3(read, 1)
    read = cut5(read, 1)
    read = qualityScoreThreshold(read, 51)
    read = removeNs(read)
    provLocation = read[POSITION]
    header = read[HEADER]
    seq = read[SEQUENCE]
    quals = read[QUALS]
        
    location = dict([('field', 'readString'),('position',provLocation)])
    #value = dict([('readHeader', header),('readString',seq), ('baseQualityString', quals)])
    value = dict([('readString_trimmed',seq), ('baseQualityString_trimmed', quals)])
    row = dict([('location', location),('value',value)])
    result = [row]
    return result
    
def nothing():
    location = dict([('field', 'readString'),('position',[0])])
    #value = dict([('readHeader', header),('readString',seq), ('baseQualityString', quals)])
    value = dict([('readString_trimmed','abc'), ('baseQualityString_trimmed', '123')])
    row = dict([('location', location),('value',value)])
    result = [row]
    return result

  
if __name__ == '__main__':
    #print parseContaminants('contaminants.fa')
    #readHeader='header'
    #readString = 'abcdef'
    #baseQualityString = '123456'
    #cmd = 'trimSingleFile.py -rN -rAT 1 -c3 1 -c contaminants.fa '
    #result = trimSingleFile(cmd, readHeader, readString, baseQualityString)
    #result = test(readHeader, readString, baseQualityString)
    result = nothing()
    print result
    
     
     

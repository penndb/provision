/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.applications;

import static edu.upenn.cis.db.habitat.test.Util.getResourcePath;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mapdb.DB;

import de.hpi.fgis.dude.similarityfunction.contentbased.impl.simmetrics.LevenshteinDistanceFunction;
import de.hpi.fgis.dude.util.csv.CSVReader;
import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.api.mocks.NullProvAPI;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTupleExtractorWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.Schema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.CartesianOperator;
import edu.upenn.cis.db.habitat.engine.operators.FieldExtractionOperator;
import edu.upenn.cis.db.habitat.engine.operators.extract.CSVExtractor;
import edu.upenn.cis.db.habitat.provenance.expressions.ComposeProvenance;
import edu.upenn.cis.db.habitat.provenance.expressions.JoinProvenance;
import edu.upenn.cis.db.habitat.repository.type.StructuredCSVWriter;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;


public class TestDuDe {
	final static Logger logger = LogManager.getLogger(TestDuDe.class);
	static ProvenanceApi provApi;
	static ProvenanceWrapperApi provWrapperApi;
	
	static boolean didInit = false;
	static DB db;
	
	@Before
	public void setUp() throws Exception {
		if(!didInit) {
//			db = DBMaker.fileDB("DuDe_cd.db")
//					.fileMmapEnable()
//					.checksumHeaderBypass()
//					.closeOnJvmShutdown()
//					.cleanerHackEnable()
//					.make();
//			provApi = new MapDBProvAPI(db);
			provApi = new NullProvAPI();
			provWrapperApi = new ProvenanceWrapperApi(provApi, null, "");
			didInit = true;
		}	
	}
	
	@After
	public void tearDown() throws Exception {
		System.out.println("Created " + Schema.getAllSchemas().size() + " schema objects");
		
		for (Schema s: Schema.getAllSchemas())
			System.out.println(s.getName() + ": " + s.getTuplesProduced() + " tuples");
	}
	
	@Test
	public void testLoadCSV() throws IOException, HabitatServiceException {
		String filename = getResourcePath(TestDuDe.class.getClassLoader(),
				"dude")
				+ "/cd.csv";
		logger.debug(filename);
		
		StructuredCSVWriter result = new StructuredCSVWriter("output_load_cd_csv.txt");
		File fil = Paths.get(filename).toFile();
		
		CSVExtractor<TableWithVariableSchema> csve = new CSVExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		csve.configure(CSVReader.DEFAULT_SEPARATOR, 
				CSVReader.DEFAULT_QUOTE_CHARACTER, 
				null, true);
		
		TableWithVariableSchema answer = csve.execute();
		logger.debug(answer.size());
		assertTrue(answer.size() == 9763);
		result.addAll(answer);
		result.close();	
	}
	
	@Test
	public void testCartesian() throws IOException, HabitatServiceException {
		String filename =
				getResourcePath(TestDuDe.class.getClassLoader(),"dude")
				+ "/cd_sample.csv";
		
        StructuredCSVWriter result = new StructuredCSVWriter("output_cart_cd_csv_sample.txt");
		
		File fil = Paths.get(filename).toFile();
		
		
		CSVExtractor<TableWithVariableSchema> csve = new CSVExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		CSVExtractor<TableWithVariableSchema> csve2 = new CSVExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		csve.configure(CSVReader.DEFAULT_SEPARATOR, 
				CSVReader.DEFAULT_QUOTE_CHARACTER, 
				null, true);
		csve2.configure(CSVReader.DEFAULT_SEPARATOR, 
				CSVReader.DEFAULT_QUOTE_CHARACTER, 
				null, true);
		
		CartesianOperator<TableWithVariableSchema> cartes = new 
				CartesianOperator<>(csve, csve2, new JoinProvenance(), provWrapperApi);
		
		// Warning: this builds an in-memory structure; don't use it for large data
		// and instead use the iterator interface
		//TableWithVariableSchema answer = cartes.execute();
		
		List<MutableTupleWithSchema<String>> outputs = new ArrayList<>();
		int count = 0;
		if (cartes.initialize()) {
			while (cartes.getNextTuple(outputs)) {
				count += outputs.size();
				
				// Warning: this takes huge amounts of space
				result.addAll(outputs);
				
				cartes.recycle(outputs);
				
				System.out.println(count);
			}
		}
		cartes.close();
		
		assertTrue(count == 100 * 100);//9763 * 9763);
		
		result.close();	
	}
	
	@Test
	public void testCDExec() throws IOException, HabitatServiceException {
		String filename =
				getResourcePath(TestDuDe.class.getClassLoader(), "dude")
				+ "/cd_sample.csv";
		
        StructuredCSVWriter result = new StructuredCSVWriter("output_DuDe_cd_csv_sample.txt");
		
		File fil = Paths.get(filename).toFile();
		System.out.println(fil.getName());
		
		CSVExtractor<TableWithVariableSchema> csve = new CSVExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		CSVExtractor<TableWithVariableSchema> csve2 = new CSVExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		csve.configure(CSVReader.DEFAULT_SEPARATOR, 
				CSVReader.DEFAULT_QUOTE_CHARACTER, 
				null, true);
		csve2.configure(CSVReader.DEFAULT_SEPARATOR, 
				CSVReader.DEFAULT_QUOTE_CHARACTER, 
				null, true);
		
		CartesianOperator<TableWithVariableSchema> cartes = new 
				CartesianOperator<>(csve, csve2, new JoinProvenance(), provWrapperApi);
		FieldExtractionOperator<TableWithVariableSchema,String,String> extract = new FieldExtractionOperator<>
		(cartes, 
				"similarity_title",
				String.class,
				new UnaryTupleExtractorWithLookup<String,String>() {

					@Override
					public List<String> apply(TupleWithSchema<String> t) {
						LevenshteinDistanceFunction simFunc = new LevenshteinDistanceFunction();
						String title1 = (String) this.getValue("title");
						String title2 = (String) this.getValue("title_2");
						double similarity = simFunc.getSimilarity(title1, title2);
						
						List<String> ret = new ArrayList<>();
						ret.add(Double.toString(similarity));
						return ret;
					}
					
					@Override
					public String getName() {
						return "levenshtein";
					}
				}, 
				new ComposeProvenance(),
				provWrapperApi);
		
//		TableWithVariableSchema answer = extract.execute();
		List<MutableTupleWithSchema<String>> outputs = new ArrayList<>();
		int count = 0;
		if (extract.initialize()) {
			while (extract.getNextTuple(outputs)) {
				count += outputs.size();

				// Warning: this takes huge amounts of space
				result.addAll(outputs);
				
				extract.recycle(outputs);
				
				System.out.println(count);
			}
		}
		extract.close();
		
//		result.addAll(answer);
		result.close();	
		
	}
}
/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.applications;

import static edu.upenn.cis.db.habitat.test.Util.getResourcePath;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.python.core.PySystemState;

import com.google.common.collect.Sets;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionVariable;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.FieldExtractionOperator;
import edu.upenn.cis.db.habitat.engine.operators.JoinOperator;
import edu.upenn.cis.db.habitat.engine.operators.ProjectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.extract.FastQExtractor;
import edu.upenn.cis.db.habitat.jython.JythonUnaryTupleExtractor;
import edu.upenn.cis.db.habitat.provenance.MapDBProvAPI;
import edu.upenn.cis.db.habitat.provenance.expressions.ComposeProvenance;
import edu.upenn.cis.db.habitat.provenance.expressions.JoinProvenance;
import edu.upenn.cis.db.habitat.repository.type.StructuredCSVWriter;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

public class TestTrimCases {
	final static Logger logger = LogManager.getLogger(TestTrimCases.class);

	
	static ProvenanceApi provApi;// = new InMemoryProvAPI();
	static ProvenanceWrapperApi provWrapperApi;
	static SamReaderFactory factory;
	
	static boolean didInit = false;
	
	static DB db;
	
	//static String thePath = TestTrimCases.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	static String thePath = getResourcePath(TestTrimCases.class.getClassLoader(), "genome");
	
	@Before
	public void setUp() throws Exception {
		if (!didInit) {
			factory =
			          SamReaderFactory.makeDefault()
		              .enable(SamReaderFactory.Option.INCLUDE_SOURCE_IN_RECORDS, SamReaderFactory.Option.VALIDATE_CRC_CHECKSUMS)
		              .validationStringency(ValidationStringency.SILENT);
			
			db = DBMaker.fileDB("trim.db")
					.fileMmapEnable()
					.checksumHeaderBypass()
					.closeOnJvmShutdown()
//					.cleanerHackEnable()
					.make();
			provApi = new MapDBProvAPI(db);
			provWrapperApi = new ProvenanceWrapperApi(provApi, null, "");
		
			Properties props = PySystemState.getBaseProperties();
			String pythonPathProp = props.getProperty("python.path");
			String ourPath;
			if(pythonPathProp == null) {
				ourPath = System.getProperty("user.dir") +java.io.File.pathSeparator ;
			} else {
				ourPath = pythonPathProp +java.io.File.pathSeparator 
						+ System.getProperty("user.dir") 
						+ java.io.File.pathSeparator;
			}
			props.setProperty("python.path", ourPath + thePath + "/trimPython");
			PySystemState.initialize(System.getProperties(), props, null);
			didInit = true;
//			System.out.println(this.getClass().getResource("test_1.fq"));
//			System.out.println(this.getClass().getClassLoader().getResource("test_1.fq"));
//			//URL classURL = getClass().getProtectionDomain().getCodeSource().getLocation();
//			System.out.println(thePath);
		}
	}
	
	@Test
	public void testTrimCut3() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_trim_cut3_1.txt");
		File fil = new File(Paths.get(thePath,"test_1.fq").toString());
		FastQExtractor<TableWithVariableSchema> fqe = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		
		List<String> newFields = new ArrayList<String>();
		List<Class<? extends Object>> types = new ArrayList<>();
		newFields.add(ProvenanceApi.Provenance);
		types.add(ProvLocation.class);
		newFields.add("readString_trimmed");
		types.add(String.class);
		newFields.add("baseQualityString_trimmed");
		types.add(String.class);
		BasicSchema inputParameters = new BasicSchema("parameters");
		inputParameters.addField("readHeader", String.class);
		inputParameters.addField("readString", String.class);
		inputParameters.addField("baseQualityString", String.class);
		List<ArithmeticExpressionOperation<?>> expressions = new ArrayList<>();
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readHeader"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readString"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "baseQualityString"));
		
		
		BasicSchema pythonSchema = new BasicSchema("TestTrimCut3", newFields, types);
		
		FieldExtractionOperator<TableWithVariableSchema,TupleWithSchema<String>,String> extract = 
				new FieldExtractionOperator<TableWithVariableSchema, TupleWithSchema<String>,String>
						(fqe, newFields, types,
						new JythonUnaryTupleExtractor<String>(pythonSchema, "cut3",
								"from cut3 import cut3\nresult=cut3(readHeader, readString, baseQualityString, 1)",
								inputParameters, expressions),
						new ComposeProvenance(),
						provWrapperApi);
		TableWithVariableSchema answer = extract.execute();
		
		result.addAll(answer);
		result.close();
	}
	
	@Test
	public void testTrimSingleFile() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_trim_single.txt");
		
		File fil = new File(Paths.get(thePath,"test_1.fq").toString());
		FastQExtractor<TableWithVariableSchema> fqe = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		
		List<String> newFields = new ArrayList<String>();
		List<Class<? extends Object>> types = new ArrayList<>();
		newFields.add(ProvenanceApi.Provenance);
		types.add(ProvLocation.class);
		newFields.add("readString_trimmed");
		types.add(String.class);
		newFields.add("baseQualityString_trimmed");
		types.add(String.class);
		
		BasicSchema pythonSchema = new BasicSchema("TestTrimSingleFile", newFields, types);
		
		//String pyCmd = "\"trimSingleFile.py -c3 1 -c src/test/resources/contaminants.fa \"";
		String pyCmd = "\"trimSingleFile.py -c3 1 -rN -rAT 26 -c '" + thePath + "/contaminants.fa' \"";
		String pyScript1 = "from trimSingleFile import trimSingleFile\n"
				+ "result=trimSingleFile(" 
				+ pyCmd 
				+ ", readHeader, readString, baseQualityString)";
		String pyScript2 = "from trimWithProv import trimSingleFile\n"
				+ "result=trimSingleFile(" 
				+ pyCmd 
				+ ", readHeader, readString, baseQualityString)";
		BasicSchema inputParameters = new BasicSchema("parameters");
		inputParameters.addField("readHeader", String.class);
		inputParameters.addField("readString", String.class);
		inputParameters.addField("baseQualityString", String.class);
		List<ArithmeticExpressionOperation<?>> expressions = new ArrayList<>();
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readHeader"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readString"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "baseQualityString"));

		FieldExtractionOperator<TableWithVariableSchema,TupleWithSchema<String>,String> extract = 
				new FieldExtractionOperator<TableWithVariableSchema, TupleWithSchema<String>,String>
						(fqe, newFields, types, 
						new JythonUnaryTupleExtractor<String>(pythonSchema, "trim",
								pyScript2, inputParameters, expressions),
						new ComposeProvenance(),
						provWrapperApi);
		TableWithVariableSchema answer = extract.execute();
		
		result.addAll(answer);
		result.close();
	}
	
	@Test
	public void TestJoin() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_join_f1_f2.txt");
		
		File fil1 = new File(Paths.get(thePath,"test_1.fq").toString());
		File fil2 = new File(Paths.get(thePath,"test_2.fq").toString());
		FastQExtractor<TableWithVariableSchema> fqe1 = new FastQExtractor<TableWithVariableSchema>(fil1, provWrapperApi);
		FastQExtractor<TableWithVariableSchema> fqe2 = new FastQExtractor<TableWithVariableSchema>(fil2, provWrapperApi);

		JoinOperator<TableWithVariableSchema> join = new 
				JoinOperator<>(fqe1, fqe2,
						new BinaryTuplePredicateWithLookup() {

							@Override
							public boolean test(TupleWithSchema<String> t, TupleWithSchema<String> u) {
								return t.getValue("lineNumber").equals(u.getValue("lineNumber"));
							}

							@Override
							public Set<String> getSymbolsReferencedLeft() {
								return Sets.newHashSet("lineNumber");
							}

							@Override
							public Set<String> getSymbolsReferencedRight() {
								return Sets.newHashSet("lineNumber");
							}

							@Override
							public void swapLeftAndRight() {
								// TODO Auto-generated method stub
								
							}
				},
						new JoinProvenance(), provWrapperApi);

		TableWithVariableSchema answer = join.execute();
		
		result.addAll(answer);
		result.close();
	
	}
	
	
	@Test
	public void TestJoinTrim() throws IOException, HabitatServiceException {
		StructuredCSVWriter result1 = new StructuredCSVWriter("output_join_trim_proj_1.txt");
		StructuredCSVWriter result2 = new StructuredCSVWriter("output_join_trim_proj_2.txt");
		
		File fil1 = new File(Paths.get(thePath,"test_1.fq").toString());
		File fil2 = new File(Paths.get(thePath,"test_2.fq").toString());
		FastQExtractor<TableWithVariableSchema> fqe1 = new FastQExtractor<TableWithVariableSchema>(fil1, provWrapperApi);
		FastQExtractor<TableWithVariableSchema> fqe2 = new FastQExtractor<TableWithVariableSchema>(fil2, provWrapperApi);

		JoinOperator<TableWithVariableSchema> join = new 
				JoinOperator<>(fqe1, fqe2,
						new BinaryTuplePredicateWithLookup() {

							@Override
							public boolean test(TupleWithSchema<String> t, TupleWithSchema<String> u) {
								return t.getValue("lineNumber").equals(u.getValue("lineNumber"));
							}

							@Override
							public Set<String> getSymbolsReferencedLeft() {
								return Sets.newHashSet("lineNumber");
							}

							@Override
							public Set<String> getSymbolsReferencedRight() {
								return Sets.newHashSet("lineNumber");
							}

							@Override
							public void swapLeftAndRight() {
								// TODO Auto-generated method stub
								
							}

				},
						new JoinProvenance(), provWrapperApi);
		
		String pyCmd = "\"trimWithProv.py -p -c3 1 -m 20 -rN -rAT 26 -c '" + thePath + "/contaminants.fa' \"";
		
		List<String> newFields1 = new ArrayList<String>();
		List<Class<? extends Object>> types1 = new ArrayList<>();
		newFields1.add(ProvenanceApi.Provenance);
		types1.add(ProvLocation.class);
		newFields1.add("readString_trimmed");
		types1.add(String.class);
		newFields1.add("baseQualityString_trimmed");
		types1.add(String.class);	
		BasicSchema pythonSchema1 = new BasicSchema("TestTrimSingleFile1", newFields1, types1);
		String pyScript1 = "from trimWithProv import trimFirst\n"
				+ "result = trimFirst("
				+ pyCmd
				+ ", readHeader, readString, baseQualityString"
				+ ", readHeader_2, readString_2, baseQualityString_2)";
		BasicSchema inputParameters = new BasicSchema("parameters");
		inputParameters.addField("readHeader", String.class);
		inputParameters.addField("readString", String.class);
		inputParameters.addField("baseQualityString", String.class);
		inputParameters.addField("readHeader_2", String.class);
		inputParameters.addField("readString_2", String.class);
		inputParameters.addField("baseQualityString_2", String.class);
		List<ArithmeticExpressionOperation<?>> expressions = new ArrayList<>();
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readHeader"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readString"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "baseQualityString"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readHeader_2"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readString_2"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "baseQualityString_2"));
		FieldExtractionOperator<TableWithVariableSchema,TupleWithSchema<String>,String> extract1 = 
				new FieldExtractionOperator<TableWithVariableSchema, TupleWithSchema<String>,String>
						(join, newFields1, types1, 
						new JythonUnaryTupleExtractor<String>(pythonSchema1, "trim1", 
								pyScript1, inputParameters, expressions),
						new ComposeProvenance(),
						provWrapperApi);
		List<String> projFields1 = new ArrayList<>();
		projFields1.add("lineNumber");
		projFields1.add("readHeader");
		projFields1.add("readString_trimmed");
		projFields1.add("baseQualityString_trimmed");
		ProjectionOperator<TableWithVariableSchema> proj1 =
				new ProjectionOperator<TableWithVariableSchema>(extract1, projFields1, provWrapperApi);
		TableWithVariableSchema answer1 = proj1.execute();

		
		List<String> newFields2 = new ArrayList<String>();
		List<Class<? extends Object>> types2 = new ArrayList<>();
		newFields2.add(ProvenanceApi.Provenance);
		types2.add(ProvLocation.class);
		newFields2.add("readString_2_trimmed");
		types2.add(String.class);
		newFields2.add("baseQualityString_2_trimmed");
		types2.add(String.class);	
		BasicSchema pythonSchema2 = new BasicSchema("TestTrimSingleFile2", newFields2, types2);
		String pyScript2 = "from trimWithProv import trimSecond\n"
				+ "result = trimSecond("
				+ pyCmd
				+ ", readHeader, readString, baseQualityString"
				+ ", readHeader_2, readString_2, baseQualityString_2)";

		FieldExtractionOperator<TableWithVariableSchema,TupleWithSchema<String>,String> extract2 = 
				new FieldExtractionOperator<TableWithVariableSchema, TupleWithSchema<String>,String>
						(join, newFields2, types2, 
						new JythonUnaryTupleExtractor<String>(pythonSchema2, "trim2", 
								pyScript2, inputParameters, expressions),
						new ComposeProvenance(),
						provWrapperApi);
		List<String> projFields2 = new ArrayList<>();
		projFields2.add("lineNumber");
		projFields2.add("readHeader");
		projFields2.add("readString_2_trimmed");
		projFields2.add("baseQualityString_2_trimmed");
		ProjectionOperator<TableWithVariableSchema> proj2 =
				new ProjectionOperator<TableWithVariableSchema>(extract2, projFields2, provWrapperApi);
		TableWithVariableSchema answer2 = proj2.execute();
		
		result1.addAll(answer1);
		result2.addAll(answer2);
		
		result1.close();
		result2.close();
		
	
	}

	@AfterClass
	public static void shutdown() {
		db.close();
	}
}

/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.descriptors;

import static edu.upenn.cis.db.habitat.test.Util.getResourcePath;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.python.core.PySystemState;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.provenance.MapDBProvAPI;
import edu.upenn.cis.db.habitat.querygen.ModuleDescriptor;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.querygen.QueryGen;
import edu.upenn.cis.db.habitat.querygen.exceptions.QueryParseException;
import edu.upenn.cis.db.habitat.repository.type.StructuredCSVWriter;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

public class TestModuleDescriptor {
	
	static boolean didInit = false;
	static DB db;
	static String thePath = getResourcePath(TestModuleDescriptor.class.getClassLoader(), "genome");

	@Before
	public void setUp() throws Exception {
		if (!didInit) {
			Properties props = PySystemState.getBaseProperties();
			String pythonPathProp = props.getProperty("python.path");
		    String ourPath;
		    
		    if (pythonPathProp==null) {
		        ourPath  = System.getProperty("user.dir") + java.io.File.pathSeparator;
		    } else {
		        ourPath = pythonPathProp +java.io.File.pathSeparator + 
		        		System.getProperty("user.dir") + java.io.File.pathSeparator;
		    }
		    props.setProperty("python.path", ourPath + thePath + "/trimPython");
		    
			PySystemState.initialize(System.getProperties(), props, null);
			PlanGen.setPythonPath(thePath);
			
			QueryGen.samReader =
			          SamReaderFactory.makeDefault()
		              .enable(SamReaderFactory.Option.INCLUDE_SOURCE_IN_RECORDS, SamReaderFactory.Option.VALIDATE_CRC_CHECKSUMS)
		              .validationStringency(ValidationStringency.SILENT);
			
			db = DBMaker.fileDB("file.db")
					.fileMmapEnable()
					.checksumHeaderBypass()
					.closeOnJvmShutdown()
//					.cleanerHackEnable()
					.make();
			QueryGen.provAPI = new MapDBProvAPI(db);
			
			didInit = true;
		}
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDescriptor() throws JsonSyntaxException, JsonIOException, FileNotFoundException {
		ModuleDescriptor desc = new ModuleDescriptor(Paths.get(thePath, "trim_py.yaml").toString(), false);
		
		System.out.println("Dependencies: " + desc.getDependencies());
		System.out.println("Scripts: " + desc.getScripts());
		System.out.println("Plans: " + desc.getPlans());
		
		PlanGen.loadDependencies(desc.getDependencies());
		PlanGen.runScripts(thePath, desc.getScripts());
		
		for (String planFile: desc.getPlans())
			System.out.println(PlanGen.getQueryPlan(thePath, planFile));
	}

	@Test
	public void testDescriptorToPlan() throws JsonSyntaxException, JsonIOException, IOException, QueryParseException {
		ModuleDescriptor desc = new ModuleDescriptor(
				Paths.get(thePath, "trim_py.yaml").toString(), false);
		
		System.out.println("Dependencies: " + desc.getDependencies());
		System.out.println("Scripts: " + desc.getScripts());
		System.out.println("Plans: " + desc.getPlans());
		
		PlanGen.loadDependencies(desc.getDependencies());
		PlanGen.runScripts(thePath, desc.getScripts());
		
		StructuredCSVWriter result = new StructuredCSVWriter("output_module.txt");

		for (String planFile: desc.getPlans()) {
			QueryOperator<TableWithVariableSchema> root = 
					PlanGen.getQueryOperator(PlanGen.getQueryPlan(thePath, planFile));
			
			TableWithVariableSchema answer = null;
//			try {
//				answer = root.execute();
//			} catch (HabitatServiceException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			if (answer != null)
				result.addAll(answer);
		}
		result.close();
	}

	@AfterClass
	public static void shutdown() {
		db.close();
	}
}

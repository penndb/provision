/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_PATH_PARAM;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Sets;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionVariable;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.FieldExtractionOperator;
import edu.upenn.cis.db.habitat.engine.operators.JoinOperator;
import edu.upenn.cis.db.habitat.engine.operators.ProjectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.extract.FastQExtractor;
import edu.upenn.cis.db.habitat.jython.JythonUnaryTupleExtractor;
import edu.upenn.cis.db.habitat.provenance.expressions.ComposeProvenance;
import edu.upenn.cis.db.habitat.provenance.expressions.JoinProvenance;
import edu.upenn.cis.db.habitat.repository.type.StructuredCSVWriter;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import spark.Request;
import spark.Response;
import spark.Service;

/**
 * @author John Frommeyer
 *
 */
public class TrimExec extends TrimExecWsMethod
		implements
		RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvenanceGraphApi backend;
	private final ProvenanceWrapperApi provWrapperApi;
	private final Service spark;

	public TrimExec(ProvenanceGraphApi backend,
			ProvDmApi dm,
			Service spark) {
		super();
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
		this.provWrapperApi = new ProvenanceWrapperApi(backend, dm, Config.getProvGraph());
	}

	@Override
	public Object handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";

		final String resource = getPathParam(spark, request,
				RESOURCE_PATH_PARAM);
		logger.debug("{}: Got resource from path {}", m, resource);

		execTrim(resource);
		response.status(HttpServletResponse.SC_NO_CONTENT);

		return null;

	}

	@Override
	public void execTrim(
			String resource)
			throws Exception {
		final String m = "execTrim(...)";
//		backend.createProvenanceGraph(resource);
		TestJoinTrim(resource);
	}

	static String thePath = "src/main/resources/descriptors/genome";//

	public void TestJoinTrim(String resource) throws IOException, HabitatServiceException {
		logger.info("At test case, " + thePath);
		StructuredCSVWriter result1 = new StructuredCSVWriter("output_join_trim_proj_1.txt");
		StructuredCSVWriter result2 = new StructuredCSVWriter("output_join_trim_proj_2.txt");
		
		File fil1 = new File(Paths.get(thePath, resource + "_1.fq").toString());
		File fil2 = new File(Paths.get(thePath, resource + "_2.fq").toString());
		FastQExtractor<TableWithVariableSchema> fqe1 = new FastQExtractor<TableWithVariableSchema>(fil1, provWrapperApi);
		FastQExtractor<TableWithVariableSchema> fqe2 = new FastQExtractor<TableWithVariableSchema>(fil2, provWrapperApi);

		JoinOperator<TableWithVariableSchema> join = new 
				JoinOperator<>(fqe1, fqe2,
						new BinaryTuplePredicateWithLookup() {

							@Override
							public boolean test(TupleWithSchema<String> t, TupleWithSchema<String> u) {
								return t.getValue("lineNumber").equals(u.getValue("lineNumber"));
							}

							@Override
							public Set<String> getSymbolsReferencedLeft() {
								return Sets.newHashSet("lineNumber");
							}

							@Override
							public Set<String> getSymbolsReferencedRight() {
								return Sets.newHashSet("lineNumber");
							}

							@Override
							public void swapLeftAndRight() {
								// TODO Auto-generated method stub
								
							}

				},
						new JoinProvenance(), provWrapperApi);
		
		String pyCmd = "\"trimWithProv.py -p -c3 1 -m 20 -rN -rAT 26 -c '" + thePath + "/contaminants.fa' \"";
		
		List<String> newFields1 = new ArrayList<String>();
		List<Class<? extends Object>> types1 = new ArrayList<>();
		newFields1.add(ProvenanceApi.Provenance);
		types1.add(ProvLocation.class);
		newFields1.add("readString_trimmed");
		types1.add(String.class);
		newFields1.add("baseQualityString_trimmed");
		types1.add(String.class);	
		BasicSchema pythonSchema1 = new BasicSchema("TestTrimSingleFile1", newFields1, types1);
		
		String pyScript1 = "from trimWithProv import trimFirst\n"
				+ "result = trimFirst("
				+ pyCmd
				+ ", readHeader, readString, baseQualityString"
				+ ", readHeader_2, readString_2, baseQualityString_2)";
		BasicSchema inputParameters = new BasicSchema("parameters");
		inputParameters.addField("readHeader", String.class);
		inputParameters.addField("readString", String.class);
		inputParameters.addField("baseQualityString", String.class);
		inputParameters.addField("readHeader_2", String.class);
		inputParameters.addField("readString_2", String.class);
		inputParameters.addField("baseQualityString_2", String.class);
		List<ArithmeticExpressionOperation<?>> expressions = new ArrayList<>();
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readHeader"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readString"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "baseQualityString"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readHeader_2"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readString_2"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "baseQualityString_2"));
		FieldExtractionOperator<TableWithVariableSchema,TupleWithSchema<String>,String> extract1 = 
				new FieldExtractionOperator<>
						(join, newFields1, types1, 
						new JythonUnaryTupleExtractor<String>(pythonSchema1, "trim1", 
								pyScript1, inputParameters, expressions),
						new ComposeProvenance(),
						provWrapperApi);
		List<String> projFields1 = new ArrayList<>();
		projFields1.add("lineNumber");
		projFields1.add("readHeader");
		projFields1.add("readString_trimmed");
		projFields1.add("baseQualityString_trimmed");
		ProjectionOperator<TableWithVariableSchema> proj1 =
				new ProjectionOperator<TableWithVariableSchema>(extract1, projFields1, provWrapperApi);
		TableWithVariableSchema answer1 = proj1.execute();

		
		List<String> newFields2 = new ArrayList<String>();
		List<Class<? extends Object>> types2 = new ArrayList<>();
		newFields2.add(ProvenanceApi.Provenance);
		types2.add(ProvLocation.class);
		newFields2.add("readString_2_trimmed");
		types2.add(String.class);
		newFields2.add("baseQualityString_2_trimmed");
		types2.add(String.class);	
		BasicSchema pythonSchema2 = new BasicSchema("TestTrimSingleFile2", newFields2, types2);
		String pyScript2 = "from trimWithProv import trimSecond\n"
				+ "result = trimSecond("
				+ pyCmd
				+ ", readHeader, readString, baseQualityString"
				+ ", readHeader_2, readString_2, baseQualityString_2)";

		FieldExtractionOperator<TableWithVariableSchema,BasicTuple,String> extract2 = 
				new FieldExtractionOperator<>
						(join, newFields2, types2, 
						new JythonUnaryTupleExtractor(pythonSchema2, "trim2", 
								pyScript2, inputParameters, expressions),
						new ComposeProvenance(),
						provWrapperApi);
		List<String> projFields2 = new ArrayList<>();
		projFields2.add("lineNumber");
		projFields2.add("readHeader");
		projFields2.add("readString_2_trimmed");
		projFields2.add("baseQualityString_2_trimmed");
		ProjectionOperator<TableWithVariableSchema> proj2 =
				new ProjectionOperator<TableWithVariableSchema>(extract2, projFields2, provWrapperApi);
		TableWithVariableSchema answer2 = proj2.execute();
		
		result1.addAll(answer1);
		result2.addAll(answer2);
		
		result1.close();
		result2.close();
		
		System.out.println("*** Finished execTRIM ***");
	}
}

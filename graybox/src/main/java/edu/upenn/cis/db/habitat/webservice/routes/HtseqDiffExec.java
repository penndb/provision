/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.PRUNE_PATH_PARAM;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_PATH_PARAM;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_PATH_PARAM_2;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.querygen.QueryGen;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import spark.Request;
import spark.Response;
import spark.Service;

/**
 * @author John Frommeyer
 *
 */
public class HtseqDiffExec extends HtseqDiffExecWsMethod
		implements
		RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvenanceGraphApi backend;
	private final ProvDmApi dm;
	private final Service spark;

	public HtseqDiffExec(ProvenanceGraphApi backend,
			ProvDmApi dm,
			Service spark) {
		super();
		this.dm = dm;
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
	}

	@Override
	public Object handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";

		final String resource = getPathParam(spark, request,
				RESOURCE_PATH_PARAM);
		logger.debug("{}: Got resource from path {}", m, resource);
		final String resource2 = getPathParam(spark, request,
				RESOURCE_PATH_PARAM_2);
		final String prune = getPathParam(spark, request,
				PRUNE_PATH_PARAM);

		TableWithVariableSchema result = execHtseq(resource, resource2, prune);
		response.status(HttpServletResponse.SC_OK);
		
		return result.toString();

	}

	static String thePath = "src/main/resources/descriptors/genome";//

	@Override
	public TableWithVariableSchema execHtseq(
			String input1, String input2,
			String prune)
			throws Exception {
		final String m = "execHtseq(...)";

		// TODO: load this, replace files including prune
		QueryGen.provAPI = backend;
		QueryGen.provWrapperAPI = new ProvenanceWrapperApi(backend, dm, Config.getProvGraph());
		
	    QueryOperator<TableWithVariableSchema> root =
			   PlanGen.getQueryOperator(PlanGen.getQueryPlan(thePath, "htseq_algebra_tree_semijoin.json"));
	    
		TableWithVariableSchema result = root.execute();
	    root.close();
	    
	    return result;
	}

}

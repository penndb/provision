/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.querygen;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

public class ModuleDescriptor {
	Map<String, ?> config = new HashMap<>();
	
	public ModuleDescriptor(String path, boolean isInClassPath) {
		if (isInClassPath)
			config = getConfigFromStream(path);
		else {
			config = getConfigFromPath(path);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,?> loadFromStream(String path) throws FileNotFoundException {
		InputStream input = ClassLoader.class.getResourceAsStream(path);
	    Yaml yaml = new Yaml();
	    return (Map<String,?>) yaml.load(input);
	}
	
	/**
	 * Parse the config YAML file.
	 * 
	 * @param path
	 * @return
	 */
	public Map<String,?> getConfigFromStream(String path) {
			try {
				config = loadFromStream(path);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				config = new HashMap<String,String>();
			}
		
		return config;
	}

	@SuppressWarnings("unchecked")
	public Map<String,?> loadFromPath(String path) throws FileNotFoundException {
		InputStream input = new BufferedInputStream(new FileInputStream(path));
	    Yaml yaml = new Yaml();
	    return (Map<String,?>) yaml.load(input);
	}
	
	/**
	 * Parse the module YAML file.
	 * 
	 * @param path
	 * @return
	 */
	public Map<String,?> getConfigFromPath(String path) {
			try {
				config = loadFromPath(path);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				config = new HashMap<String,String>();
			}
		
		return config;
	}

	@Override
	public String toString() {
		return config.toString();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getDependencies() {
		return (List<String>) config.get("download");
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getScripts() {
		return (List<String>) config.get("scripts");
	}

	@SuppressWarnings("unchecked")
	public List<String> getPlans() {
		return (List<String>) config.get("plans");
	}
}

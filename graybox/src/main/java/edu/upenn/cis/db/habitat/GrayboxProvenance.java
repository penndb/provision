package edu.upenn.cis.db.habitat;
/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import static spark.Spark.port;
import static spark.Spark.threadPool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import edu.upenn.cis.db.habitat.auth.AuthServiceModule;
import edu.upenn.cis.db.habitat.auth.AuthStorageModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.AuthModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.PostgresAuthModule;
import edu.upenn.cis.db.habitat.permissions.PermissionServiceModule;
import edu.upenn.cis.db.habitat.provenance.ProvServiceModule;
import edu.upenn.cis.db.habitat.provenance.ProvStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.StorageServiceModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.NeoStorageModule;
import edu.upenn.cis.db.habitat.webservice.RestService;

/**
 * Mainline module:  launch the main Web service interfaces and host environment
 * in the Spark framework.  Pull in all plugins and register them, adding appropriate
 * authorization checks.
 * 
 * Deprecated, use LogProvenance instead
 * 
 * @author zives
 *
 */
@Deprecated
public class GrayboxProvenance {

	final static Logger logger = LogManager.getLogger(GrayboxProvenance.class);
	
	public static void main(String[] args) {
		port(Config.getServerPort());
		threadPool(Config.getMaxThreads(), 2, Config.getTimeoutMsec());
		
		Injector injector = Guice.createInjector(
				// Generic authorization
				new AuthModule(),
				// Permissions
				new PermissionServiceModule(),
				// Authorization using Postgres backend
				new PostgresAuthModule(),
				//
				new NeoStorageModule(),
				new StorageServiceModule(),
				// Provenance services
				new ProvStorageModule(),
				new ProvServiceModule(),
				// User auth services
				new AuthServiceModule(),
				// Storage services
				new AuthStorageModule());
		
		System.out.println("Habitat Graybox Provenance Engine launching on port " + Config.getServerPort() + "...");
		RestService web = injector.getInstance(RestService.class);
		web.call();
	}
	
}

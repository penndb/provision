/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.catalog.SystemCatalog;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.optimizer.OptimizeExpression;
import edu.upenn.cis.db.habitat.optimizer.RelAlgebraHelper;
import edu.upenn.cis.db.habitat.optimizer.graph.QueryGraph;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.querygen.QueryTypes;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import spark.Request;
import spark.Response;
import spark.Route;

@Api
@Path("/graybox/execute/{username}/{queryName}")
@Produces("application/json")
public class CreateQueryFromPlan implements Route {
	final static Logger logger = LogManager.getLogger(CreateQueryFromPlan.class);
	
	final UserApi backend;
	final AuthServices caller;
	final PlanGen generator;
	
	public CreateQueryFromPlan(UserApi backend, AuthServices services, PlanGen generator) {
		this.backend = backend;
		caller = services;
		this.generator = generator;
	}
	
	@POST
	@ApiOperation(value = "Processes a new query plan", nickname="ExecuteAlgebra")
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="username", paramType = "path"), //
		@ApiImplicitParam(required = true, dataType="string", name="queryName", paramType = "path"), //
		@ApiImplicitParam(
				required = true, 
				dataTypeClass=QueryTypes.QueryPlan.class, 
				name="planDescriptor",
				paramType = "body",
				examples= @Example(value = {
						@ExampleProperty(mediaType="application/json",
								value="{tree:'thetree'}")})), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=QueryTypes.ProvResult.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	String username = request.params("username");
    	String query = request.params("queryName");

    	logger.debug("Request execute " + query + " for user " + username);
    	logger.debug(request.body());
    	Map<String,Object> map = caller.getProfileMap(request, response);
		
		request.session(true);
		map.put("sessionId", request.session().id());

		if (!request.contentType().equals("application/json")) {
			throw new UnsupportedOperationException();
		}
		QueryTypes.QueryPlan plan = new Gson().fromJson(request.body(), QueryTypes.QueryPlan.class);
		
		// The given query plan
		QueryOperator<TableWithVariableSchema> root = PlanGen.getQueryOperator(plan);
		
		// Let's rewrite it using the query optimizer
		OptimizeExpression<Serializable,TableWithVariableSchema> optimizer = new OptimizeExpression<>(
				new SystemCatalog("."));
		
		// What are the nodes designating subplans?
		List<QueryOperator<TableWithVariableSchema>> leaves = RelAlgebraHelper.getLeaves(root, true);
		
		QueryGraph<TableWithVariableSchema> queryGraph = optimizer.getQueryGraph(root, leaves);

		List<QueryOperator<TableWithVariableSchema>> boundaries = optimizer.getSubBlocks(root);
		
		return boundaries.get(0).execute();
		//return root.execute();
	}

}

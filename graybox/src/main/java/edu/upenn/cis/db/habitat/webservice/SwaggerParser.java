/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice;

import io.swagger.annotations.Api;
import io.swagger.jaxrs.Reader;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.models.Swagger;
import io.swagger.models.auth.ApiKeyAuthDefinition;
import io.swagger.models.auth.In;

import java.util.Set;

import org.reflections.Reflections;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author serol, https://serol.ro/posts/2016/swagger_sparkjava/
 *
 */
public class SwaggerParser {

	public static String getSwaggerJson(String packageName) throws JsonProcessingException {
		Swagger swagger = getSwagger(packageName);
		
//		Map<String,Object> map = new HashMap<>();
//		map.put("scheme", "bearer");
//		map.put("bearerFormat", "JWT");
		
		swagger.securityDefinition("jwt", 
				new ApiKeyAuthDefinition("api_key", In.HEADER));
		
		String json = swaggerToJson(swagger);
		return json;
	}

	public static Swagger getSwagger(String packageName) {
		Reflections reflections = new Reflections(packageName);
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setResourcePackage(packageName);
		beanConfig.setScan(true);
		beanConfig.scanAndRead();
		Swagger swagger = beanConfig.getSwagger();

		Reader reader = new Reader(swagger);

		Set<Class<?>> apiClasses = reflections.getTypesAnnotatedWith(Api.class);
		return reader.read(apiClasses);
	}

	public static String swaggerToJson(Swagger swagger) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setSerializationInclusion(Include.NON_EMPTY);
		String json = objectMapper.writeValueAsString(swagger);
		return json;
	}

}
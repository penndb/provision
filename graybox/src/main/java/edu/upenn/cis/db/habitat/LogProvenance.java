package edu.upenn.cis.db.habitat;
/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.test.Util.getResourcePath;
import static spark.Spark.get;
import static spark.Spark.port;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.python.core.PySystemState;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.ProvisionException;

import edu.upenn.cis.db.habitat.auth.AuthBrowserModule;
import edu.upenn.cis.db.habitat.auth.AuthServiceModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.AuthModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.PostgresAuthModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.ProtectedStorageModule;
import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.core.webservice.RestPlugin;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier.RequestType;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.FieldExtractionOperator;
import edu.upenn.cis.db.habitat.engine.operators.JoinOperator;
import edu.upenn.cis.db.habitat.engine.operators.ProjectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.extract.FastQExtractor;
import edu.upenn.cis.db.habitat.jython.JythonUnaryTupleExtractor;
import edu.upenn.cis.db.habitat.permissions.PermissionServiceModule;
import edu.upenn.cis.db.habitat.provenance.MapDBProvAPI;
import edu.upenn.cis.db.habitat.provenance.ProvServiceModule;
import edu.upenn.cis.db.habitat.provenance.expressions.ComposeProvenance;
import edu.upenn.cis.db.habitat.provenance.expressions.JoinProvenance;
import edu.upenn.cis.db.habitat.provenance.provdm.ProvDmServiceModule;
import edu.upenn.cis.db.habitat.querygen.QueryGen;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.ConfigurableNeoStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.Neo4JStore;
import edu.upenn.cis.db.habitat.repository.type.StructuredCSVWriter;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import edu.upenn.cis.db.habitat.webservice.RestService;
import edu.upenn.cis.db.habitat.webservice.SparkModule;
import edu.upenn.cis.db.habitat.webservice.WorkflowServiceModule;
import edu.upenn.cis.db.habitat.webservice.WorkflowServices.GsonFactory;
import edu.upenn.cis.db.habitat.webservice.routes.ExceptionHandlingRoute;
import edu.upenn.cis.db.habitat.webservice.routes.RouteWithInfo;
import edu.upenn.cis.db.habitat.webservice.routes.TrimExec;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import spark.Request;
import spark.Response;
import spark.Service;

/**
 * Mainline module:  launch the main Web service interfaces and host environment
 * in the Spark framework.  Pull in all plugins and register them, adding appropriate
 * authorization checks.
 * 
 * @author zives
 *
 */
public class LogProvenance {

	final static Logger logger = LogManager.getLogger(LogProvenance.class);
	
	static ProvenanceGraphApi provApi;
	static ProvDmApi provDmApi;
	
	public static void main(String[] args) throws Exception {
		final Service spark = Service.ignite();
		spark.port(Config.getServerPort());
		spark.threadPool(Config.getMaxThreads(), 2, Config.getTimeoutMsec());

		Injector injector = Guice.createInjector(
//				// Storage services
//				new AuthStorageModule());
				new HabitatRequestScopeModule(),
				// Generic authorization
				new AuthModule(),
				// Web app authorization
				new AuthBrowserModule(),
				// Permissions
				new PermissionServiceModule(),
				// Authorization using Postgres backend
				new PostgresAuthModule(),
				// Provenance REST services
				new ProvServiceModule(),
				new ProvDmServiceModule(),
				// User auth services
				new AuthServiceModule(),
				// Storage services
				new ConfigurableNeoStorageModule(),
				new ProtectedStorageModule(Neo4JStore.class),
				// Web server, required for REST
				new SparkModule(spark),
				new WorkflowServiceModule()
				);
		setUpPython();
		
		provDmApi = injector.getInstance(ProvDmApi.class);
		provApi = injector.getInstance(ProvenanceGraphApi.class);
		
		PermissionApi perms = injector.getInstance(PermissionApi.class);
		
		UserApi users = injector.getInstance(UserApi.class);
		QueryGen.provAPI = provApi;
		QueryGen.provWrapperAPI = new ProvenanceWrapperApi(provApi, provDmApi, Config.getProvGraph());
		
		if (!users.isUserValid("default_user", "local")) {
			users.addCredential("default_user", "local", "http", "default_password");
		
			perms.grantUserPermissionOn("", PermissionApi.WritePermission, "default_user");
			perms.grantUserPermissionOn(Config.getProvGraph(), PermissionApi.WritePermission, "default_user");
		}
		
		System.out.println("PROVision Provenance Engine launching on port " + Config.getServerPort() + "...");
		System.out.println("For DEMO ONLY purposes, use default_user, default_password...");
		
		RestService web = injector.getInstance(RestService.class);

		// Launch web server and services
		web.call();
		
	}
	
	static SamReaderFactory factory;
	
	static boolean didInit = false;
	
	// Use the test case data
	static String thePath = "descriptors/genome";
	
	/**
	 * Initialize Python environment
	 * 
	 * @throws Exception
	 */
	public static void setUpPython() throws Exception {
		if (!didInit) {
			factory =
			          SamReaderFactory.makeDefault()
		              .enable(SamReaderFactory.Option.INCLUDE_SOURCE_IN_RECORDS, SamReaderFactory.Option.VALIDATE_CRC_CHECKSUMS)
		              .validationStringency(ValidationStringency.SILENT);
			
			Properties props = PySystemState.getBaseProperties();
			String pythonPathProp = props.getProperty("python.path");
			String ourPath;
			if(pythonPathProp == null) {
				ourPath = System.getProperty("user.dir") +java.io.File.pathSeparator ;
			} else {
				ourPath = pythonPathProp +java.io.File.pathSeparator 
						+ System.getProperty("user.dir") 
						+ java.io.File.pathSeparator;
			}
			props.setProperty("python.path", ourPath + thePath + "/trimPython");
			PySystemState.initialize(System.getProperties(), props, null);
			didInit = true;
		}
	}
	
}

# Provenance Selective Reconstruction & Difference

## Basics

The key capabilities provided by PROVision involve (1) reproducing **fine-grained** provenance, and (2) computing differences.

The fine-grained provenance requires that we create a JSON **semantic descriptor** for the computation.

An example is `trim_algebra_tree.json`:

```
{
	"operator": {
		"name": "FieldExtract",
		"scriptName": "trimContaminants",
		"script": "import trimContaminants.py",
		"fields": ["value1", "value2"],
		"types": ["String", "Integer"],
		"parameter": "contaminants.fa",
		"child": {
			"operator": {
				"name": "FieldExtract",
				"fields": ["value1", "value2"],
				"types": ["String", "Integer"],
				"script": "import removeNs.py",
				"child": {
					"operator": {
						"name": "FieldExtract",
						"fields": ["value1", "value2"],
						"types": ["String", "Integer"],
						"script": "from cut3 import cut3\ncut3({'seq': readString, 'quals': '1234', },2)",
						"child": {
							"operator": {
								"name": "FastQExtractor",
								"filename": "src/test/resources/genome/test_1.fq"
							}
						}
					}
				}
			}
		}
	}
}
```

You'll see that it consists of a series of relational algebra operators, such as field extraction, which call Python or Java code.  Here, we are importing the `cut3` Python package, running it over `test_1.fq`, and so on.

The **main/resources/descriptors** directory includes some sample descriptors, as well as updated code for the `trim` tool.

## Using PROVision for a Sequencing Workflow

This use case targets the workflow used in the Kim Lab, which runs TRIM, followed by STAR, followed by HTSEQ.

### Comparing Two Runs

Suppose we want to compare the outputs of two versions of STAR, within the workflow.

For our example, let's assume we are comparing a pair of unaligned gene sequences in FASTQ format: `unaligned_1.fq` and `unaligned_2.fq`. 

The first step is to do your standard runs, using your standard workflow.

Suppose the final output files after the alignment of the two runs are `v1_Sample_CM434.htseq.exons.cnts.txt` and    `v2_Sample_CM434.htseq.exons.cnts.txt`.
   
### Setup: Acquire Packages for PROVision 

We now want to **selectively recompute** provenance for where the two runs differ.

Download the source code of TRIM (modified slightly to include provenance tracking of strings) from
* [https://bitbucket.org/penndb/provision/src/master/graybox/src/test/resources/genome/trimPython/trimWithProv.py](https://bitbucket.org/penndb/provision/src/master/graybox/src/test/resources/genome/trimPython/trimWithProv.py)

Download STAR source code from
* [https://github.com/alexdobin/STAR/releases](https://github.com/alexdobin/STAR/releases)

Download HTSEQ source code (also modified slightly to include provenance tracking) from
* [https://bitbucket.org/penndb/provision/src/provision/graybox/src/test/resources/genome/htseq/](https://bitbucket.org/penndb/provision/src/provision/graybox/src/test/resources/genome/htseq/)

### Tracing Provenance

1.  Compare the final output with the “diff” command.
   `diff v1_Sample_CM434.htseq.exons.cnts.txt v2_Sample_CM434.htseq.exons.cnts.txt`

2.  Out of the elements of v1 that don't appear in v2, select a subset and write to file `diff_v1.txt`.

3.  Run the `HtseqDiffExec` option from the GUI, specifying the appropriate files.

5.  Select subset of output of v2 that are different from v1 from the diff command and write to file `diff_v2.txt`. Then repeat Step 3 onwards. 


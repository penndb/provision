/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.querygen.operators;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.FileExtractOperator;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import htsjdk.samtools.reference.FastaSequenceFile;
import htsjdk.samtools.reference.ReferenceSequence;

public class ManyFastAExtractor<T extends TableWithVariableSchema> extends FileExtractOperator<T> {
	File file;
	FastaSequenceFile fasta;
	int lineNo = 0;
	ReferenceSequence firstSeq = null;
	final BasicSchema project;
	boolean split = false;
	int repeats;
	int dupeCount;

	// Pre-coded positions
	int tokenPos = 0;
	int locationPos = 0;
	int numberPos = 0;
	int namePos = 0;
	int baseStringPos = 0;
	int contigIndexPos = 0;
	int lengthPos = 0;

	public ManyFastAExtractor(String filename, int repeats, ProvenanceWrapperApi provApi) throws FileNotFoundException {
		super(provApi);

		project = new BasicSchema(filename);
		project.addField(ProvenanceApi.Token, ProvToken.class);
		project.addField(ProvenanceApi.Provenance, ProvLocation.class);
		project.addField("lineNumber", Integer.class);
		project.addField("name", String.class);
		project.addField("baseString", String.class);
		project.addField("contigIndex", Integer.class);
		project.addField("length", Integer.class);
		
		projectThese.add(project);
		
		this.repeats = repeats;
		dupeCount = repeats;

		file = new File(filename);
	}

	public ManyFastAExtractor(File fil, int repeats, ProvenanceWrapperApi provApi) throws FileNotFoundException {
		super(provApi);

		project = new BasicSchema(fil.getName());
		project.addField(ProvenanceApi.Token, ProvToken.class);
		project.addField(ProvenanceApi.Provenance, ProvLocation.class);
		project.addField("lineNumber", Integer.class);
		project.addField("name", String.class);
		project.addField("baseString", String.class);
		project.addField("contigIndex", Integer.class);
		project.addField("length", Integer.class);
		
		project.addLookupKey("lineNumber");
		
		projectThese.add(project);

		file = fil;
		this.repeats = repeats;
		dupeCount = repeats;
	}

	@Override
	public boolean initialize() throws HabitatServiceException {
		if (!super.initialize())
			return false;
		
		fasta = new FastaSequenceFile(file, false);

		firstSeq = fasta.nextSequence();

		if (firstSeq == null)
			return false;
		
		if (firstSeq.getName().contains("name:")) {
			split = true;
			String[] fields = firstSeq.getName().split(" ");
			
			for (String field: fields) {
				String[] kv = field.split(":");
				
				if (isDouble(kv[1])) {
					project.addField(kv[0], Double.class);
				} else if (isInteger(kv[1])) {
					project.addField(kv[0], Integer.class);
				} else
					project.addField(kv[0], String.class);
			}
		}
		
		// Schema
		tokenPos = project.indexOf(ProvenanceApi.Token);
		locationPos = project.indexOf(ProvenanceApi.Provenance);
		numberPos = project.indexOf("lineNumber");
		namePos = project.indexOf("name");
		baseStringPos = project.indexOf("baseString");
		contigIndexPos = project.indexOf("contigIndex");
		lengthPos = project.indexOf("length");

		provApi.writeInputFile(file.getName());
		return true;
	}
	
	@Override
	public boolean close() throws HabitatServiceException {
		fasta.close();
		
		return super.close();
	}

	/**
	 * We only have a single tuple schema
	 * 
	 * @return
	 */
	public BasicSchema getSchema() {
		return projectThese.iterator().next();
	}

	ReferenceSequence seq;

	@Override
	public boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		
		if (firstSeq != null) {
			seq = firstSeq;
			if (dupeCount-- == 0) {
				firstSeq = null;
				dupeCount = repeats;
				seq = fasta.nextSequence();
			}
		} else {
			if (dupeCount-- == 0) {
				firstSeq = null;
				dupeCount = repeats;
				seq = fasta.nextSequence();
			}
		}

		if (seq == null)
			return false;

		MutableTupleWithSchema<String> tup = (!recycled.isEmpty()) ? recycled.remove(0) : getSchema().createTuple();
		ProvToken us = PlanGen.getToken(this);
		ProvLocation here = getInputPosition();
		
//		provApi.storeProvenanceNode("", us, here);
		
		tup.setValueAt(//SimpleProvAPI.Token, 
				tokenPos, us);
		tup.setValueAt(//SimpleProvAPI.Provenance, 
				locationPos, here);
		tup.setValueAt(//"lineNumber", 
				numberPos, lineNo);

		if (!split) {
			tup.setValue("name", seq.getName());
		} else {
			String[] fields = seq.getName().split(" ");
			
			for (String field: fields) {
				String[] kv = field.split(":");
				
				if (isInteger(kv[1])) {
					tup.setValue(kv[0], Integer.valueOf(kv[1]));
				} else if (isDouble(kv[1])) {
					tup.setValue(kv[0], Double.valueOf(kv[1]));
				} else
					tup.setValue(kv[0], kv[1]);
			}
		}
		tup.setValueAt(//"baseString", 
				baseStringPos, seq.getBaseString());
		tup.setValueAt(//"contigIndex", 
				contigIndexPos, seq.getContigIndex());
		tup.setValueAt(//"length", 
				lengthPos, seq.length());
		
		provApi.writeInputRecord(us, here, file.getName(), getId().toString(), tup);
		outputs.add(tup);

		lineNo++;

		return true;
	}

	@Override
	public ProvLocation getInputPosition() {
		Set<Integer> range = new HashSet<>();
		range.add(lineNo);
		return new ProvLocation(getId(), getSchema().getName(), 
				range);
	}

	@Override
	protected String getArguments() {
		return file.getName();
	}

	@Override
	public String getLogicalDescriptor() {
		return getClass().getSimpleName() + ": " + getArguments();
	}
}

/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.querygen.operators;

import static edu.upenn.cis.db.habitat.test.Util.getResourcePath;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.python.core.PySystemState;

import com.google.common.collect.Sets;
import com.google.inject.Guice;
import com.google.inject.Injector;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionVariable;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTupleExtractorWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ProvExtraction;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.CartesianOperator;
import edu.upenn.cis.db.habitat.engine.operators.FieldExtractionOperator;
import edu.upenn.cis.db.habitat.engine.operators.GroupByOperator;
import edu.upenn.cis.db.habitat.engine.operators.ProjectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.SelectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.extract.FastQExtractor;
import edu.upenn.cis.db.habitat.jython.JythonTuplePredicate;
import edu.upenn.cis.db.habitat.jython.JythonUnaryTupleExtractor;
import edu.upenn.cis.db.habitat.provenance.ProvStorageModule;
import edu.upenn.cis.db.habitat.provenance.expressions.AggregateProvenance;
import edu.upenn.cis.db.habitat.provenance.expressions.ComposeProvenance;
import edu.upenn.cis.db.habitat.provenance.expressions.JoinProvenance;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.NeoStorageModule;
import edu.upenn.cis.db.habitat.repository.type.StructuredCSVWriter;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

public class TestQueryProvStorage {
	final static Logger logger = LogManager.getLogger(TestQueryProvStorage.class);

	static String thePath = getResourcePath(TestQueryProvStorage.class.getClassLoader(), "genome");
	
	static ProvenanceApi provApi = null;
	static ProvenanceWrapperApi provWrapperApi = null;
	static SamReaderFactory factory;
	
	static boolean didInit = false;
	
//	static DB db;
	
	@Before
	public void setUp() throws Exception {
		if (!didInit) {
			if (File.separator.equals("\\") && thePath.charAt(2) == ':')
				thePath = thePath.substring(3);
			
			factory =
			          SamReaderFactory.makeDefault()
		              .enable(SamReaderFactory.Option.INCLUDE_SOURCE_IN_RECORDS, SamReaderFactory.Option.VALIDATE_CRC_CHECKSUMS)
		              .validationStringency(ValidationStringency.SILENT);

			Properties props = PySystemState.getBaseProperties();
			String pythonPathProp = props.getProperty("python.path");
		    String ourPath;
		    
		    if (pythonPathProp==null) {
		        ourPath  = System.getProperty("user.dir") + java.io.File.pathSeparator;
		    } else {
		        ourPath = pythonPathProp +java.io.File.pathSeparator + 
		        		System.getProperty("user.dir") + java.io.File.pathSeparator;
		    }
		    props.setProperty("python.path", ourPath + thePath + File.pathSeparator + thePath + "/trimPython");
		    
			PySystemState.initialize(System.getProperties(), props, null);

			Injector injector = Guice.createInjector(
					// Storage / repository services (by default PostgreSQL and Neo4J)
					new NeoStorageModule(),
					// Provenance services
					new ProvStorageModule()
					);
			
			provApi = injector.getInstance(ProvenanceApi.class);
			provWrapperApi = new ProvenanceWrapperApi(provApi, null, "");
			didInit = true;
		}
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFastQ() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_fastq.txt");
		
		File fil = Paths.get(thePath, "test_1.fq").toFile();
		FastQExtractor<TableWithVariableSchema> fqe = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		
		TableWithVariableSchema answer = fqe.execute();
		
		assertTrue(answer.size() == 25);
		
		result.addAll(answer);
		result.close();
	}

	@Test
	public void testFastQGrouping() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_scan_cartes_group.txt");
		
		File fil = Paths.get(thePath, "test_1.fq").toFile();
		FastQExtractor<TableWithVariableSchema> fqe = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		FastQExtractor<TableWithVariableSchema> fqe2 = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);

		CartesianOperator<TableWithVariableSchema> cartes = new 
				CartesianOperator<>(fqe, fqe2, new JoinProvenance(), provWrapperApi);
		
		List<String> groupFields = new ArrayList<>();
		groupFields.add("lineNumber");
		BasicSchema aggSchema = new BasicSchema("Agg");
		aggSchema.addField("count", Integer.class);
		GroupByOperator<TableWithVariableSchema> gby = new
				GroupByOperator<>(cartes, groupFields, new Function<TableWithVariableSchema,BasicTuple>() {

					@Override
					public BasicTuple apply(TableWithVariableSchema t) {
						BasicTuple tup = aggSchema.createTuple();
						
						tup.setValue("count", t.size());
						return tup;
					}
					
				}, aggSchema, 
						new AggregateProvenance(), provWrapperApi);
		
		TableWithVariableSchema answer = gby.execute();
		
		result.addAll(answer);
		result.close();
		assertTrue(answer.size() == 25);
		
	}

	@Test
	public void testPySelectFromFastQ() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_py_sel.txt");
		
		File fil = Paths.get(thePath, "test_1.fq").toFile();
		FastQExtractor<TableWithVariableSchema> fqe = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		
		SelectionOperator<TableWithVariableSchema> sel = 
				new SelectionOperator<TableWithVariableSchema>(
				fqe, new JythonTuplePredicate("result = lineNumber < 14", Sets.newHashSet("lineNumber")), provWrapperApi); 
		
		TableWithVariableSchema answer = sel.execute();
		
//		System.out.println(answer.size());
		
		result.addAll(answer);
		result.close();
	}

	@Test
	public void testProjectPySelectFromFastQ() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_proj_py_sel.txt");
		
		File fil = Paths.get(thePath, "test_1.fq").toFile();
		FastQExtractor<TableWithVariableSchema> fqe = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		
		List<String> projFields = new ArrayList<>();
		projFields.add("lineNumber");
		projFields.add("readString");
		ProjectionOperator<TableWithVariableSchema> proj =
				new ProjectionOperator<TableWithVariableSchema>(fqe, projFields, provWrapperApi);
		
		SelectionOperator<TableWithVariableSchema> sel = 
				new SelectionOperator<TableWithVariableSchema>(
				proj, new JythonTuplePredicate("result = lineNumber < 14", Sets.newHashSet("lineNumber")), provWrapperApi); 
		
		TableWithVariableSchema answer = sel.execute();
		
		System.out.println(answer.size());
		
		System.out.println(proj.getOutputSchemas());
		
		result.addAll(answer);
		result.close();
	}

	@Test
	public void testFastQExtraction() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_scan_extract.txt");
		
		File fil = Paths.get(thePath, "test_1.fq").toFile();
		FastQExtractor<TableWithVariableSchema> fqe = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		
		List<String> fnSignature = new ArrayList<>();
		fnSignature.add("2mer");
		fnSignature.add("readString");
		FieldExtractionOperator<TableWithVariableSchema,ProvExtraction,String> extract = new FieldExtractionOperator<>
			(fqe, 
					"2mer",
					String.class,
					new UnaryTupleExtractorWithLookup<ProvExtraction,String>() {

				// Extract all 2-grams / 2-mers
				@Override
				public List<ProvExtraction> apply(TupleWithSchema<String> t) {
					String seq = (String) this.getValue("readString");
					
					List<ProvExtraction> ret = new ArrayList<>();
					
					int k = 2;
					for (int i = 0; i < seq.length() - k + 1; i++) {
						String mer = seq.substring(i, i+k);
						
						List<Integer> span = new ArrayList<>();
						span.add(i);
						span.add(i+k);
						ret.add(new ProvExtraction(
								new ProvLocation("readString", span), 
								mer, () -> PlanGen.getToken(fqe)));
					}
					
					return ret;
				}
				
				@Override
				public String getName() {
					return "2-mer";
				}
			}, 
					new ComposeProvenance(),
					provWrapperApi);
		
		TableWithVariableSchema answer = extract.execute();
		
		result.addAll(answer);
		result.close();
	}

	@Test
	public void testFastQListExtraction() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_scan_extract_list.txt");
		
		File fil = Paths.get(thePath, "test_1.fq").toFile();
		FastQExtractor<TableWithVariableSchema> fqe = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);

		List<String> fields = new ArrayList<>();
		List<Class<? extends Object>> types = new ArrayList<>();
		fields.add("2mer");
		types.add(String.class);
		fields.add("2inv");
		types.add(String.class);
		FieldExtractionOperator<TableWithVariableSchema,ProvExtraction,String> extract = new FieldExtractionOperator<>
			(fqe, 
					fields,
					types,
					new UnaryTupleExtractorWithLookup<ProvExtraction,String>() {

				// Extract all 2-grams / 2-mers and their inverses
				@Override
				public List<ProvExtraction> apply(TupleWithSchema<String> t) {
					String seq = (String) this.getValue("readString");
					
					List<ProvExtraction> ret = new ArrayList<>();
					
					int k = 2;
					for (int i = 0; i < seq.length() - k + 1; i++) {
						String mer = seq.substring(i, i+k);
						
						List<String> items = new ArrayList<String>();
						items.add(mer);
						StringBuilder mer2 = new StringBuilder();
						mer2.append(mer.charAt(1));
						mer2.append(mer.charAt(0));
						items.add(mer2.toString());
						
						List<Integer> span = new ArrayList<>();
						span.add(i);
						span.add(i+k);
						ret.add(new ProvExtraction(
								new ProvLocation("readString", span), 
								items, () -> PlanGen.getToken(fqe)));
					}
					
					return ret;
				}
				
				@Override
				public String getName() {
					return "2-mer";
				}
			}, 
					new ComposeProvenance(),
					provWrapperApi);
		
		TableWithVariableSchema answer = extract.execute();
		
		result.addAll(answer);
		result.close();
	}

	/**
	 * This is a test case that calls a very simple bit of Python code.
	 * Python code is assumed to return a List of dictionaries.
	 * 
	 * The result should always be of the form [{location: {field: 'f', position: [123, ...] },
	 * value: {value1: 'x', value2: 3, ...}}, {location: {field: 'f', position: [123, ...]},
	 * value: {value1: 'x', value2: 3, ...}} ,...] 
	 * 
	 * You need to create the basic schema for these tuples, which MUST include
	 * (1) a Provenance field from which the location dictionary's field name and
	 * position will be used, (2) any additional fields listed in the value dictionary.
	 * 
	 * 
	 * @throws IOException
	 * @throws HabitatServiceException
	 */
	@Test
	public void testFastQPyExtraction() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_scan_py_extract.txt");
		
		File fil = Paths.get(thePath, "test_1.fq").toFile();
		FastQExtractor<TableWithVariableSchema> fqe = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);

		// The Python code will return a provenance location plus two fields,
		// value1 (String) and value2 (Integer)
		List<String> fields = new ArrayList<String>();
		List<Class<? extends Object>> types = new ArrayList<>();
		fields.add(ProvenanceApi.Provenance);
		types.add(ProvLocation.class);
		fields.add("value1");
		types.add(String.class);
		fields.add("value2");
		types.add(Integer.class);
		BasicSchema pythonSchema = new BasicSchema("TestBlock", fields, types);
		BasicSchema inputParameters = new BasicSchema("parameters");
		inputParameters.addField("lineNumber", Integer.class);
		inputParameters.addField("readString", String.class);
		List<ArithmeticExpressionOperation<?>> expressions = new ArrayList<>();
		expressions.add(new ArithmeticExpressionVariable<Integer>(Integer.class, "lineNumber"));
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readString"));
		
		FieldExtractionOperator<TableWithVariableSchema,TupleWithSchema<String>,Object> extract = 
				new FieldExtractionOperator<>
			(fqe, 
					fields,
					types,
					new JythonUnaryTupleExtractor<Object>(pythonSchema, "extract1",
							"location = dict([('field', 'pyop'),('position',[lineNumber,lineNumber+1])])\n"
							 + "value = dict([('value1', readString),('value2',22)])\n"
							 + "row = dict([('location', location),('value',value)])\n"
							 + "result = [row,row]\n",
							inputParameters,
							expressions),
					
					new ComposeProvenance(),
					provWrapperApi);
		
		TableWithVariableSchema answer = extract.execute();
		
		result.addAll(answer);
		result.close();
	}

	/**
	 * Cartesian product
	 * 
	 * @throws IOException
	 * @throws HabitatServiceException
	 */
	@Test
	public void testFastQCartesian() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_scan_cartes.txt");
		
		File fil = Paths.get(thePath, "test_1.fq").toFile();
		FastQExtractor<TableWithVariableSchema> fqe = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);
		FastQExtractor<TableWithVariableSchema> fqe2 = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);

		CartesianOperator<TableWithVariableSchema> cartes = new 
				CartesianOperator<>(fqe, fqe2, new JoinProvenance(), provWrapperApi);
		
		TableWithVariableSchema answer = cartes.execute();
		
		assertTrue(answer.size() == 625);
		
		result.addAll(answer);
		result.close();
	}


}

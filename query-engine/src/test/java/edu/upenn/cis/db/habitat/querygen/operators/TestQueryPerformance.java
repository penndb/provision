/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.querygen.operators;

import static edu.upenn.cis.db.habitat.test.Util.getResourcePath;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.api.mocks.InMemoryProvAPI;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.provenance.MapDBProvAPI;
import edu.upenn.cis.db.habitat.repository.type.InMemoryVariableSchemaTable;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

public class TestQueryPerformance {
	final static Logger logger = LogManager.getLogger(TestQueryPerformance.class);

	
	static ProvenanceApi provApi;// = new InMemoryProvAPI();
	static SamReaderFactory factory;
	
	static boolean didInit = false;
	
	static DB db;
	
	static String thePath = getResourcePath(TestQueryOperators.class.getClassLoader(), "genome");

	@Before
	public void setUp() throws Exception {
		if (!didInit) {
			if (File.separator.equals("\\") && thePath.charAt(2) == ':')
				thePath = thePath.substring(3);
			factory =
			          SamReaderFactory.makeDefault()
		              .enable(SamReaderFactory.Option.INCLUDE_SOURCE_IN_RECORDS, SamReaderFactory.Option.VALIDATE_CRC_CHECKSUMS)
		              .validationStringency(ValidationStringency.SILENT);
			
			db = DBMaker.fileDB("file.db")
					.fileMmapEnable()
					.checksumHeaderBypass()
					.closeOnJvmShutdown()
//					.cleanerHackEnable()
					.make();
			provApi = new MapDBProvAPI(db);
			didInit = true;
		}
	}

	@After
	public void tearDown() throws Exception {
//		db.close();
	}


	@Test
	public void testFastManyA() throws IOException, HabitatServiceException {
		long time = System.currentTimeMillis();
		File fil = new File(Paths.get(thePath,"contaminants.fa").toString());

		ManyFastAExtractor<TableWithVariableSchema> fqe = 
				new ManyFastAExtractor<TableWithVariableSchema>(fil, 1000000, 
						new ProvenanceWrapperApi(new InMemoryProvAPI(), null, ""));
		
		int count = 0;
		fqe.initialize();
		TableWithVariableSchema ret = new InMemoryVariableSchemaTable(fqe.getSchema());
		while (fqe.getNextTuple(ret)) {
			fqe.recycle(ret);
			count++;
			if (count > 100000 && count % 100000 == 0)
				System.out.println("To " + count);
		}
		
		fqe.close();
		
		System.out.println(count + " in " + (System.currentTimeMillis() - time));
	}
	
	@AfterClass
	public static void shutdown() {
		db.close();
	}
}

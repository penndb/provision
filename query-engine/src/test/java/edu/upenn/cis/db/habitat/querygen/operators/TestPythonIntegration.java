/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.querygen.operators;

import static edu.upenn.cis.db.habitat.test.Util.getResourcePath;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.api.mocks.InMemoryProvAPI;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionVariable;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.FieldExtractionOperator;
import edu.upenn.cis.db.habitat.engine.operators.extract.FastQExtractor;
import edu.upenn.cis.db.habitat.jython.JythonGen;
import edu.upenn.cis.db.habitat.jython.JythonUnaryTupleExtractor;
import edu.upenn.cis.db.habitat.provenance.expressions.ComposeProvenance;
import edu.upenn.cis.db.habitat.repository.type.StructuredCSVWriter;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class TestPythonIntegration {
	final static Logger logger = LogManager.getLogger(TestPythonIntegration.class);

	
	static String thePath = getResourcePath(TestPythonIntegration.class.getClassLoader(), "genome");
	static ProvenanceApi provApi = new InMemoryProvAPI();
	static ProvenanceWrapperApi provWrapperApi = new ProvenanceWrapperApi(provApi, null, "");
	
	static boolean didInit = false;
	
	@Before
	public void setUp() throws Exception {
		if (!didInit) {
			if (File.separator.equals("\\") && thePath.charAt(2) == ':')
				thePath = thePath.substring(3);
			Properties props = PySystemState.getBaseProperties();
			String pythonPathProp = props.getProperty("python.path");
		    String ourPath;
		    
		    if (pythonPathProp==null) {
		        ourPath  = System.getProperty("user.dir") + java.io.File.pathSeparator;
		    } else {
		        ourPath = pythonPathProp +java.io.File.pathSeparator + 
		        		System.getProperty("user.dir") + java.io.File.pathSeparator;
		    }
		    props.setProperty("python.path", ourPath + thePath + "/trimPython");
		    
			PySystemState.initialize(System.getProperties(), props, null);
			didInit = true;
		}
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testHelloWorld() {
		PythonInterpreter interp = JythonGen.getPythonInterpreter("tst1");
		
		interp.exec("print 'Hello world'");
	}

	@Test
	public void testImport() {
		PythonInterpreter interp = JythonGen.getPythonInterpreter("tst1");
		
		interp.exec("import cut3");
	}

//	@Test
	public void testCut3() {
		PythonInterpreter interp = JythonGen.getPythonInterpreter("tst1");
		
		interp.exec("from cut3 import cut3\nprint cut3({'seq':'abcd', 'quals':'1234', }, 1)");
	}

	/**
	 * This is a test case that calls a very simple bit of Python code.
	 * Python code is assumed to return a List of dictionaries.
	 * 
	 * The result should always be of the form [{location: {field: 'f', position: [123, ...] },
	 * value: {value1: 'x', value2: 3, ...}}, {location: {field: 'f', position: [123, ...]},
	 * value: {value1: 'x', value2: 3, ...}} ,...] 
	 * 
	 * You need to create the basic schema for these tuples, which MUST include
	 * (1) a Provenance field from which the location dictionary's field name and
	 * position will be used, (2) any additional fields listed in the value dictionary.
	 * 
	 * 
	 * @throws IOException
	 * @throws HabitatServiceException
	 */
//	@Test
	public void testFastQCut3() throws IOException, HabitatServiceException {
		StructuredCSVWriter result = new StructuredCSVWriter("output_scan_py_extract.txt");
		
		File fil = new File(Paths.get(thePath, "test_1.fq").toString());
		FastQExtractor<TableWithVariableSchema> fqe = new FastQExtractor<TableWithVariableSchema>(fil, provWrapperApi);

		// The Python code will return a provenance location plus three fields,
		// quals (string), length (int), seq (string)
		List<String> fields = new ArrayList<String>();
		List<Class<? extends Object>> types = new ArrayList<Class<? extends Object>>();
		fields.add(ProvenanceApi.Provenance);
		types.add(ProvLocation.class);
		fields.add("quals");
		types.add(String.class);
		fields.add("seq");
		types.add(String.class);
		fields.add("length");
		types.add(Integer.class);
		BasicSchema pythonSchema = new BasicSchema("TestBlock", fields, types);
		BasicSchema inputParameters = new BasicSchema("parameters");
		inputParameters.addField("readString", String.class);
		List<ArithmeticExpressionOperation<?>> expressions = new ArrayList<>();
		expressions.add(new ArithmeticExpressionVariable<String>(String.class, "readString"));
		
		FieldExtractionOperator<TableWithVariableSchema,BasicTuple,Object> extract = 
				new FieldExtractionOperator<TableWithVariableSchema,BasicTuple,Object>
			(fqe, 
					fields,
					types,
					new JythonUnaryTupleExtractor(pythonSchema, "cut3",
							"from cut3 import cut3\ncut3({'seq': readString, 'quals': '1234', },2)\n",
							inputParameters, expressions),
					
					new ComposeProvenance(),
					provWrapperApi);
		
		TableWithVariableSchema answer = extract.execute();
		
		result.addAll(answer);
		result.close();
	}
}

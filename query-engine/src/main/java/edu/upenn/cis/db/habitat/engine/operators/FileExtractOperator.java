/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public abstract class FileExtractOperator<T extends TableWithVariableSchema> 
extends NullaryQueryOperator<T> {
	protected Set<BasicSchema> projectThese;
	
	public FileExtractOperator(BasicSchema projectThese, ProvenanceWrapperApi provApi) {
		super(provApi);
		this.projectThese = new HashSet<>();
		this.projectThese.add(projectThese);
	}

	public FileExtractOperator(ProvenanceWrapperApi provApi) {
		super(provApi);
		this.projectThese = new HashSet<>();
	}

	public FileExtractOperator(Set<BasicSchema> projectThese, ProvenanceWrapperApi provApi) {
		super(provApi);
		this.projectThese = projectThese;
	}

	@Override
	public Set<BasicSchema> getOutputSchemas() {
		return projectThese;
	}
	
	public abstract ProvSpecifier getInputPosition();

	public static boolean isNumeric(String str)
	{
	  return str.matches("-?\\d*(\\.\\d+)?");  //match a number with optional '-' and decimal.
	}

	static Pattern intPatt = Pattern.compile("-?\\d+");
	public static boolean isInteger(String str)
	{
		return intPatt.matcher(str).matches();
//	  return str.matches("-?\\d+");  //match a number with optional '-' and decimal.
	}

	static Pattern doublePatt = Pattern.compile("-?\\d*\\.\\d+");
	public static boolean isDouble(String str)
	{
		return doublePatt.matcher(str).matches();
//	  return str.matches("-?\\d*\\.\\d+");  //match a number with optional '-' and decimal.
	}
}

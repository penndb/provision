/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import edu.upenn.cis.db.habitat.core.expressions.UnaryTupleExtractorWithLookup;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * A blocking operator has a function that is basically an extractor, but returns
 * an integer.
 * 
 * @author zives
 *
 * @param <T>
 */
public class BlockingOperator<T extends TableWithVariableSchema,S> extends FieldExtractionOperator<T,Integer,S> {

	public BlockingOperator(QueryOperator<T> child, String newField, 
			UnaryTupleExtractorWithLookup<Integer,S> blockingFn, 			
			ProvenanceWrapperApi provApi) {
		super(child, newField, Integer.class,  
				blockingFn, (val1, val2) -> val1, provApi);
	}

}

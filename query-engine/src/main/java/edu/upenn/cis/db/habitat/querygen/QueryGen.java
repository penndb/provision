/************************************************
 * Copyright 2017-20 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.querygen;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import com.google.common.collect.Sets;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionLiteral;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.expressions.BinaryFieldComparisonPredicate;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTupleBooleanExpression;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations;
import edu.upenn.cis.db.habitat.core.expressions.UnaryFieldComparisonPredicate;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTupleBooleanExpression;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.CartesianOperator;
import edu.upenn.cis.db.habitat.engine.operators.FieldExtractionOperator;
import edu.upenn.cis.db.habitat.engine.operators.GroupByOperator;
import edu.upenn.cis.db.habitat.engine.operators.JoinOperator;
import edu.upenn.cis.db.habitat.engine.operators.ProjectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.engine.operators.RenameOperator;
import edu.upenn.cis.db.habitat.engine.operators.SelectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.extract.BamExtractor;
import edu.upenn.cis.db.habitat.engine.operators.extract.FastAExtractor;
import edu.upenn.cis.db.habitat.engine.operators.extract.FastQExtractor;
import edu.upenn.cis.db.habitat.engine.operators.extract.GtfExtractor;
import edu.upenn.cis.db.habitat.jython.JythonUnaryTupleAggregate;
import edu.upenn.cis.db.habitat.jython.JythonUnaryTupleExtractor;
import edu.upenn.cis.db.habitat.provenance.expressions.AggregateProvenance;
import edu.upenn.cis.db.habitat.provenance.expressions.ComposeProvenance;
import edu.upenn.cis.db.habitat.provenance.expressions.ProvenanceCombiner;
import edu.upenn.cis.db.habitat.querygen.exceptions.QueryParseException;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import htsjdk.samtools.SamReaderFactory;

public class QueryGen {
	
	public static ProvenanceApi provAPI = null;
	public static ProvenanceWrapperApi provWrapperAPI = null;
	public static SamReaderFactory samReader = null;
	
	/**
	 * Converts from a parse tree into a query plan
	 * 
	 * @param parseTree
	 * @return
	 * @throws FileNotFoundException
	 * @throws QueryParseException
	 */
	@SuppressWarnings("unchecked")
	public static QueryOperator<TableWithVariableSchema> getQueryOperator(Map<String, Object> parseTree) throws FileNotFoundException, QueryParseException {
		
		parseTree = (Map<String, Object>) parseTree.get("operator");
		// Unary
		if (parseTree.containsKey("child")) {
			return getUnaryOperator(parseTree, 
					getQueryOperator((Map<String,Object>)parseTree.get("child")));
			
		} else if (parseTree.containsKey("left-child") && parseTree.containsKey("right-child")) {
			// Binary
			return getBinaryOperator(parseTree, 
					getQueryOperator((Map<String,Object>)parseTree.get("left-child")),
					getQueryOperator((Map<String,Object>)parseTree.get("right-child")));
			
		} else {
			// Nullary
			
			return getNullaryOperator(parseTree);
		}
	}
	
	public static String getOpType(Map<String,Object> info) {
		return info.get("name").toString();
	}

//	private static ComposeProvenance cp = new ComposeProvenance();
	private static ProvenanceCombiner cp = new ComposeProvenance();
	private static AtomicInteger schemaId = new AtomicInteger(0);
	
	
	/**
	 * Creates a predicate over a single tuple, ie a selection condition
	 *  
	 * @param parseTree
	 * @return
	 * @throws QueryParseException
	 */
	private static UnaryTuplePredicateWithLookup getPredicateForUnaryExpression(Map<String, Object> parseTree) throws QueryParseException {
		List<UnaryTuplePredicateWithLookup> predicates = new ArrayList<>();
		
		List<List<Object>> lists = (List<List<Object>>)parseTree.get("predicates");
		for (List<Object> predicate: lists) {
			if (predicate.size() < 2 || predicate.size() > 3)
				throw new QueryParseException("Illegal predicate: " + predicate.toString());

			String fieldName = (String)predicate.get(0);
			PredicateOperations.ComparisonOperation comparison;
			try {
				comparison = PredicateOperations.getComparison((String)predicate.get(1));
			} catch (Exception e) {
				throw new QueryParseException("Illegal predicate: " + predicate.get(1));
			}
			
			if (predicate.size() == 2)
				predicates.add(new UnaryFieldComparisonPredicate(fieldName, comparison));
			else if (predicate.get(2) instanceof String)
				try {
					predicates.add(new UnaryFieldComparisonPredicate(fieldName, comparison, (String)predicate.get(2)));
				} catch (UnsupportedEncodingException e) {
					throw new QueryParseException(e.getMessage());
				}
			else
				predicates.add(new UnaryFieldComparisonPredicate(fieldName, comparison, predicate.get(2)));
		}
		UnaryTuplePredicateWithLookup pred = null;
		if (predicates.size() == 1)
			pred = predicates.get(0);
		else
			for (int i = 0; i < predicates.size(); i++)
				if (i == 0)
					pred = predicates.get(0);
				else
					pred = new UnaryTupleBooleanExpression(PredicateOperations.BooleanOperation.And, pred, predicates.get(i));
		
		if (pred == null)
			return new UnaryTuplePredicateWithLookup() {

				@Override
				public boolean test(TupleWithSchema<String> tuple) {
					return true;
				}
			
				@Override
				public Set<String> getSymbolsReferenced() {
					return Sets.newHashSet();
				}

				@Override
				public void rename(Map<String, String> symbolTable) {
					// TODO Auto-generated method stub
					
				}
			};
		return pred;
	}
	
	/**
	 * Creates a binary predicate, eg an equality-based join condition
	 * 
	 * @param parseTree
	 * @return
	 * @throws QueryParseException
	 */
	private static BinaryTuplePredicateWithLookup getPredicateForBinaryExpression(Map<String, Object> parseTree) throws QueryParseException {
		List<BinaryTuplePredicateWithLookup> predicates = new ArrayList<>();
		
		List<List<Object>> lists = (List<List<Object>>)parseTree.get("predicates");
		for (List<Object> predicate: lists) {
			if (predicate.size() != 3)
				throw new QueryParseException("Illegal predicate: " + predicate.toString());

			String fieldName = (String)predicate.get(0);
			PredicateOperations.ComparisonOperation comparison;
			try {
				comparison = PredicateOperations.getComparison((String)predicate.get(1));
			} catch (Exception e) {
				throw new QueryParseException("Illegal predicate: " + predicate.get(1));
			}
			
			try {
				predicates.add(new BinaryFieldComparisonPredicate(fieldName, comparison, 
						(String)predicate.get(2)));
			} catch (UnsupportedEncodingException e) {
				throw new QueryParseException(e.getMessage());
			}
		}
		BinaryTuplePredicateWithLookup pred = null;
		if (predicates.size() == 1)
			pred = predicates.get(0);
		else
			for (int i = 0; i < predicates.size(); i++)
				if (i == 0)
					pred = predicates.get(0);
				else
					pred = new BinaryTupleBooleanExpression(PredicateOperations.BooleanOperation.And, pred, predicates.get(i));
		
		if (pred == null)
			return new BinaryTuplePredicateWithLookup() {

				@Override
				public boolean test(TupleWithSchema<String> left, TupleWithSchema<String> right) {
					return true;
				}
			
				@Override
				public Set<String> getSymbolsReferencedLeft() {
					return Sets.newHashSet();
				}

				@Override
				public Set<String> getSymbolsReferencedRight() {
					return Sets.newHashSet();
				}

				@Override
				public void swapLeftAndRight() {
					// TODO Auto-generated method stub
					
				}
			};
		return pred;
	}

	/**
	 * Defines a schema based on parsed info
	 * 
	 * @param fields
	 * @param typeNames
	 * @return
	 */
	public static BasicSchema getSchemaFromFieldsAndTypes(List<String> fields, List<String> typeNames) {
		BasicSchema newSchema = new BasicSchema("Gen_" + schemaId.getAndIncrement());
		newSchema.addField(ProvenanceApi.Provenance, ProvLocation.class);

		if (fields == null || fields.isEmpty())
			throw new IllegalArgumentException("Missing fields specifier for field extractor");
		if (typeNames == null || typeNames.isEmpty())
			throw new IllegalArgumentException("Missing types specifier for field extractor");
		for (int i = 0; i < fields.size(); i++) {
			Class<? extends Object> typ = null;
			if (typeNames.get(i).equals("Integer"))
				typ = Integer.class;
			else if (typeNames.get(i).equals("Boolean"))
				typ = Boolean.class;
			else if (typeNames.get(i).equals("Double"))
				typ = Double.class;
			else if (typeNames.get(i).equals("Long"))
				typ = Long.class;
			else if (typeNames.get(i).equals("Short"))
				typ = Short.class;
			else if (typeNames.get(i).equals("String"))
				typ = String.class;
			else if (typeNames.get(i).equals("Date"))
				typ = Date.class;
			else 
				throw new UnsupportedOperationException("Don't support type " + typeNames.get(i));
			
			newSchema.addField(fields.get(i), typ);
		}
		return newSchema;
	}
	
	/**
	 * Generate from the parse tree a unary operator, such as select, project,
	 * field extraction, rename, group-by
	 * 
	 * @param parseTree
	 * @param child
	 * @return
	 * @throws QueryParseException 
	 */
	@SuppressWarnings("unchecked")
	private static QueryOperator<TableWithVariableSchema> getUnaryOperator(Map<String, Object> parseTree,
			QueryOperator<TableWithVariableSchema> child) throws QueryParseException {
		
		if (getOpType(parseTree).equals("FieldExtract")) {
			List<String> fields = (List<String>)parseTree.get("fields");
			List<String> typeNames = (List<String>)parseTree.get("types");
			
			BasicSchema newSchema = getSchemaFromFieldsAndTypes(fields, typeNames);
			if (!parseTree.containsKey("script"))
				throw new IllegalArgumentException("Missing Python script specifier for field extractor");

			List<String> paramNames = (List<String>)parseTree.get("parameters");
			
			BasicSchema inputParameters = new BasicSchema("input");
			List<ArithmeticExpressionOperation<?>> expressions = new ArrayList<>();
			for (String parms: paramNames) {
				inputParameters.addField(parms, String.class);
				expressions.add(new ArithmeticExpressionLiteral<String>(String.class, parms));
			}
			
			return new FieldExtractionOperator<TableWithVariableSchema,TupleWithSchema<String>,String>(
					child, newSchema, 
					new JythonUnaryTupleExtractor<String>(newSchema, (String)parseTree.get("scriptName"), 
							(String)parseTree.get("script"), inputParameters, expressions),
					cp, provWrapperAPI);
		} else if (getOpType(parseTree).equals("Select")) {
			UnaryTuplePredicateWithLookup pred = getPredicateForUnaryExpression(parseTree);
			
			return new SelectionOperator<>(child, pred, provWrapperAPI);
		} else if (getOpType(parseTree).equals("Project")) {
			List<String> fields = (List<String>)parseTree.get("fields");

			return new ProjectionOperator<>(child, fields, provWrapperAPI);
		} else if (getOpType(parseTree).equals("Rename")) {
			Map<String,String> fieldMap = (Map<String,String>)parseTree.get("map");
			
			return new RenameOperator<>(child, fieldMap, 
					provWrapperAPI);
		} else if (getOpType(parseTree).equals("Groupby")) {
			List<String> groupingFields = (List<String>)parseTree.get("group-fields");
			List<String> fields = (List<String>)parseTree.get("output-fields");
			List<String> typeNames = (List<String>)parseTree.get("output-types");
			
			BasicSchema newSchema = getSchemaFromFieldsAndTypes(fields, typeNames);
			
			Function<TableWithVariableSchema,BasicTuple> aggFunction = 
					new JythonUnaryTupleAggregate(newSchema, 
							(String)parseTree.get("scriptName"), (String)parseTree.get("script"));

			return new GroupByOperator<>(child, groupingFields, aggFunction, newSchema, 
					new AggregateProvenance(), provWrapperAPI);
		} else
			throw new UnsupportedOperationException("Not supported: op type " + getOpType(parseTree));
	}

	/**
	 * Generate, from the parse tree, a join or Cartesian product
	 * 
	 * @param parseTree
	 * @param queryOperator
	 * @param queryOperator2
	 * @return
	 * @throws QueryParseException 
	 */
	private static QueryOperator<TableWithVariableSchema> getBinaryOperator(Map<String, Object> parseTree,
			QueryOperator<TableWithVariableSchema> left, QueryOperator<TableWithVariableSchema> right) throws QueryParseException {
		if (getOpType(parseTree).equals("Join")) {
			BinaryTuplePredicateWithLookup pred = getPredicateForBinaryExpression(parseTree);
			List<String> leftFields = (List<String>)parseTree.get("left-fields");
			List<String> rightFields = (List<String>)parseTree.get("right-fields");

			if (leftFields != null && rightFields != null)
				return new JoinOperator<>(left, right, leftFields, rightFields, pred, provWrapperAPI);
			else
				return new JoinOperator<>(left, right, pred, provWrapperAPI);
		} else if (getOpType(parseTree).equals("Cartesian")) {
			return new CartesianOperator<>(left, right, provWrapperAPI);
		} else
			throw new UnsupportedOperationException("Not supported: op type " + getOpType(parseTree));
	}

	/**
	 * Generate, from the parse tree, a scan-style nullary operator
	 * 
	 * @param parseTree
	 * @return
	 * @throws FileNotFoundException
	 */
	private static QueryOperator<TableWithVariableSchema> getNullaryOperator(Map<String, Object> parseTree) throws FileNotFoundException {
		if (getOpType(parseTree).equals("FastQExtractor")) {
			return new FastQExtractor<>((String)parseTree.get("filename"), (String)parseTree.get("filename"), provWrapperAPI);
		} else if (getOpType(parseTree).equals("FastAExtractor")) {
				return new FastAExtractor<>((String)parseTree.get("filename"), provWrapperAPI);
		} else if (getOpType(parseTree).equals("BamExtractor")) {
			return new BamExtractor<>((String)parseTree.get("filename"), samReader, provWrapperAPI);
		} else if (getOpType(parseTree).equals("GtfExtractor")) {
			return new GtfExtractor<>((String)parseTree.get("filename"), provWrapperAPI);
		} else
			throw new UnsupportedOperationException("Not supported: op type " + getOpType(parseTree));
		
	}

}

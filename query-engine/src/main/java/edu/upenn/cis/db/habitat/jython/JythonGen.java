/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.jython;

import java.util.HashMap;
import java.util.Map;

import org.python.util.PythonInterpreter;

public class JythonGen {
	static Map<String,PythonInterpreter> interpreter = new HashMap<>();
	
	public static synchronized PythonInterpreter getPythonInterpreter(String key) {
		if (interpreter.get(key) == null)
			interpreter.put(key, new PythonInterpreter());
		
		return interpreter.get(key);
	}
}

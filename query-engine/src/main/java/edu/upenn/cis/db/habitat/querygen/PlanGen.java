/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.querygen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.python.util.PythonInterpreter;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import edu.upenn.cis.db.habitat.core.type.ID;
import edu.upenn.cis.db.habitat.core.type.PlanBasedID;
import edu.upenn.cis.db.habitat.core.type.ProvIntToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.jython.JythonGen;
import edu.upenn.cis.db.habitat.querygen.exceptions.QueryParseException;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class PlanGen {
	static AtomicInteger opCount = new AtomicInteger(0);
	static AtomicInteger tupCount = new AtomicInteger(0);
	
	static String pythonPath = "";

	public static QueryOperator<TableWithVariableSchema> getQueryOperator(QueryTypes.QueryPlan plan) throws FileNotFoundException, QueryParseException {
		QueryOperator<TableWithVariableSchema> ret = QueryGen.getQueryOperator(plan.parseTree);
		
		return ret;
	}
	
	public static ProvToken getToken(QueryOperator<? extends TableWithVariableSchema> op) {
		return new ProvIntToken(tupCount.getAndIncrement());
	}
	
	public static ID getID(QueryOperator<? extends TableWithVariableSchema> op) {
//		return new UniqueID();
		return new PlanBasedID(op.getClass().getSimpleName(), opCount.getAndIncrement());
	}
	
	public static ProvToken getTokenFromString(String str) {
		return new ProvIntToken(Integer.valueOf(str));
	}
	
	public static void setPythonPath(String path) {
		if (pythonPath.isEmpty())
			pythonPath = path;
		else
			pythonPath = pythonPath + File.pathSeparator + path;
	}
	
	/**
     * Unzip it
     * 
     * @author https://www.mkyong.com/java/how-to-decompress-files-from-a-zip-file/
     * 
     * @param zipFile input zip file
     * @param output zip file output folder
     */
	public static void unzip(String zipFile, String outputFolder){

		byte[] buffer = new byte[1024];

		try {

			//create output directory is not exists
			File folder = new File(outputFolder);
			if (!folder.exists()) {
				folder.mkdir();
			}

			//get the zip file content
			ZipInputStream zis =
					new ZipInputStream(new FileInputStream(zipFile));
			//get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			while (ze!=null) {

				String fileName = ze.getName();
				File newFile = Paths.get(outputFolder, fileName).toFile();

				System.out.println("file unzip : " + newFile.getAbsoluteFile());

				//create all non exists folders
				//else you will hit FileNotFoundException for compressed folder
				new File(newFile.getParent()).mkdirs();

				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
			}

			zis.closeEntry();
			zis.close();

			System.out.println("Done");

		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void loadDependencies(List<String> dependencies) {
		dependencies.forEach(dep -> {
			URL source;
			try {
				source = new URL(dep);
				Path p = Paths.get(source.getFile());
				Path p2 = Paths.get(pythonPath, p.getFileName().toString());
				ReadableByteChannel rbc = Channels.newChannel(source.openStream());
				try (FileOutputStream fos = new FileOutputStream(p2.toString())) {
					fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
	
					if (p2.getFileName().toString().endsWith(".zip")) {
						Path p3 = Paths.get(pythonPath, p.getFileName().toString().substring(0, p.getFileName().toString().lastIndexOf('.')));
				    	unzip(p2.toString(), p3.toString());
					}
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		});
	}

	static void execScript(String script) {
		PythonInterpreter interp = JythonGen.getPythonInterpreter("init");
		
		interp.execfile(script);
	}
	

	public static void runScripts(String basePath, List<String> scripts) {
		for (String str: scripts) {
			Path p = Paths.get(basePath, str);
			execScript(p.toString());
		}
	}
	
	/**
	 * Get a query plan from a JSON file
	 * 
	 * @param basePath
	 * @param planFile
	 * @return
	 * @throws JsonSyntaxException
	 * @throws JsonIOException
	 * @throws FileNotFoundException
	 */
	public static QueryTypes.QueryPlan getQueryPlan(String basePath, String planFile) throws JsonSyntaxException, JsonIOException, FileNotFoundException {
		Path p = Paths.get(basePath, planFile);
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		map = new Gson().fromJson(new FileReader(p.toString()), map.getClass());
		
		QueryTypes.QueryPlan ret = new QueryTypes.QueryPlan();
		ret.name = planFile;
		ret.parseTree = map;
		
		return ret;
	}
	
	/**
	 * Get a query plan specified as a JSON string
	 * 
	 * @param basePath
	 * @param plan
	 * @param name
	 * @return
	 * @throws JsonSyntaxException
	 * @throws JsonIOException
	 * @throws FileNotFoundException
	 */
	public static QueryTypes.QueryPlan getQueryPlanFromString(String basePath, String plan, String name) throws JsonSyntaxException, JsonIOException, FileNotFoundException {
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		map = new Gson().fromJson(plan, map.getClass());
		
		QueryTypes.QueryPlan ret = new QueryTypes.QueryPlan();
		ret.name = name;
		ret.parseTree = map;
		
		return ret;
	}
}

/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BinaryOperator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.provenance.expressions.JoinProvenance;
import edu.upenn.cis.db.habitat.provenance.expressions.ProvenanceCombiner;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.type.InMemoryVariableSchemaTable;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * A Cartesian product operator, which also serves as the basis for the join operator.
 * 
 * @author zives
 *
 * @param <T>
 */
public class CartesianOperator<T extends TableWithVariableSchema> extends BinaryQueryOperator<T> {
	Map<BasicSchema,Map<BasicSchema,BasicSchema>> schemaMap = new HashMap<>();
	Map<BasicSchema,Map<BasicSchema,BinaryOperator<BasicSchema>>> schemaFn = new HashMap<>();
	Map<BasicSchema,Map<BasicSchema,BinaryOperator<MutableTupleWithSchema<String>>>> tupleFn = new HashMap<>();

	Logger logger = LogManager.getLogger(CartesianOperator.class);
	
	/**
	 * Combines the provenance of the left + right tuples
	 */
	ProvenanceCombiner combiner;

	/**
	 * Cartesian product operator
	 * 
	 * @param left Left operator subtree
	 * @param right Right operator subtree
	 * @param combiner How to compose the left + right provenance
	 * @param provApi Provenance API
	 */
	public CartesianOperator(QueryOperator<T> left, QueryOperator<T> right,
			ProvenanceCombiner combiner,
			ProvenanceWrapperApi provApi) {
		super(left, right, provApi);
		this.combiner = combiner;
	}

	/**
	 * Cartesian product operator, defaults to JoinProvenance combiner
	 * 
	 * @param left Left operator subtree
	 * @param right Right operator subtree
	 * @param provApi Provenance API
	 */
	public CartesianOperator(QueryOperator<T> left, QueryOperator<T> right,
			ProvenanceWrapperApi provApi) {
		super(left, right, provApi);
		this.combiner = new JoinProvenance();
	}

	@Override
	public boolean initialize() throws HabitatServiceException {
		if (!super.initialize())
			return false;
		
		count = 0;
		Set<BasicSchema> leftInputs = left.getOutputSchemas();
		Set<BasicSchema> rightInputs = right.getOutputSchemas();
		
		// register Cartesian product functions across all schemas
		leftInputs.parallelStream().forEach(leftSchema -> 
		{
			rightInputs.parallelStream().forEach(rightSchema -> {
				registerMappings(leftSchema, rightSchema,
						// Schema map
						(ls,rs) -> ls.concat(ls.getName() + "&" + rs.getName() + "_" + getId(), rs),
						// Tuple map
						(lt,rt) -> lt.concat(rt, 
								schemaMap.get(leftSchema).get(rightSchema), combiner,
								(!recycled.isEmpty()) ? recycled.remove(0) : schemaMap.get(leftSchema).get(rightSchema).createTuple()
								)); 
						});
		});

		leftInputs.parallelStream().forEach(lSchema ->
			rightInputs.parallelStream().forEach(rSchema -> {
				if (schemaMap.get(lSchema) == null)
					schemaMap.put(lSchema, new HashMap<>());
				schemaMap.get(lSchema).put(rSchema, 
						schemaFn.get(lSchema).get(rSchema).apply(lSchema, rSchema)); 
			}));
		
		outputSchemas = new HashSet<>();
		for (Map<BasicSchema, BasicSchema> secondPart : schemaMap.values())
			outputSchemas.addAll(secondPart.values());
		// every time excute, empty rightBuffer so that a new one can be created
		rightBuffer = null;
		return true;
	}

	/**
	 * Register the schemas to be produced by pairwise combinations of input schemas.
	 * 
	 * @param schemas
	 * @param schemaFn
	 * @param tupleFn
	 */
	public void registerMappings(BasicSchema left, BasicSchema right,
			BinaryOperator<BasicSchema> schemaFn,
			BinaryOperator<MutableTupleWithSchema<String>> tupleFn) {
		if (this.schemaFn.get(left) == null)
			this.schemaFn.put(left, new HashMap<>());
		if (this.tupleFn.get(left) == null)
			this.tupleFn.put(left, new HashMap<>());
		this.schemaFn.get(left).put(right, schemaFn);
		this.tupleFn.get(left).put(right, tupleFn);
	}

	public void rebind(TupleWithSchema<String> left, TupleWithSchema<String> right) {

	}

	protected boolean needToRebind(TupleWithSchema<String> left, TupleWithSchema<String> right) {
		if (left == lastLeft && right == lastRight)
			return false;
		
		lastLeft = left;
		lastRight = right;
		
		return true;
	}
	
	/**
	 * This will serve as the basis for determining whether to join a pair of tuples.
	 * For Cartesian product it always returns true.
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public boolean isValid(TupleWithSchema<String> left, TupleWithSchema<String> right) {
		return true;
	}
	
	public boolean processTuple(MutableTupleWithSchema<String> left, MutableTupleWithSchema<String> right, Collection<MutableTupleWithSchema<String>> outputs) 
	throws HabitatServiceException {
		MutableTupleWithSchema<String> output = 
				tupleFn.get(left.getSchema()).get(right.getSchema()).apply(left, right);
		
		if (output != null) {
			output.setValue(ProvenanceApi.Token, PlanGen.getToken(this));
			outputs.add(output);
			
			count++;
			
			provApi.writeBinaryDerivation((ProvToken)output.getValue(ProvenanceApi.Token), 
					(ProvSpecifier)output.getValue(ProvenanceApi.Provenance), 
					(ProvToken)left.getValue(ProvenanceApi.Token), 
					(ProvToken)right.getValue(ProvenanceApi.Token), 
					getId().toString(), output);
			return true;
		} else
			return false;
	}

	T rightBuffer = null;

	T leftOutputs;// = (T) new InMemoryVariableSchemaTable(getOutputSchemas());

	@SuppressWarnings("unchecked")
	@Override
	public boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {

		if (leftOutputs == null)
			leftOutputs = (T) new InMemoryVariableSchemaTable(getOutputSchemas());
		else {
			leftOutputs.clear();
		}
		
		boolean ok = left.getNextTuple(leftOutputs);
		if (rightBuffer == null) {
			rightBuffer = (T) new InMemoryVariableSchemaTable(getOutputSchemas());

			if (ok)
				ok &= right.getNextTuple(rightBuffer);

			if (ok) {
				while (right.getNextTuple(rightBuffer))
					;
			}
		}
		
		if (ok) {
			for (MutableTupleWithSchema<String> lt: leftOutputs) {
				for (MutableTupleWithSchema<String> rt: rightBuffer) {
					if (needToRebind(lt, rt))
						rebind(lt, rt);
					if (isValid(lt, rt))
						processTuple(lt, rt, outputs);
				}
				left.recycle(lt);
			}
		}
		return ok;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T execute() throws HabitatServiceException {
		initialize();
		T ret = (T) new InMemoryVariableSchemaTable(getOutputSchemas());

		T rightOutputs = (T) new InMemoryVariableSchemaTable(getOutputSchemas());
		leftOutputs = (T) new InMemoryVariableSchemaTable(getOutputSchemas());

		boolean ok = left.getNextTuple(leftOutputs);
		ok &= right.getNextTuple(rightOutputs);
		if (ok) {
			while (right.getNextTuple(rightOutputs))
				;
		}
		
		logger.debug("Right input: " + rightOutputs.size());

		int count = 0;
		while (ok) {
			for (MutableTupleWithSchema<String> lt: leftOutputs) {
				for (MutableTupleWithSchema<String> rt: rightOutputs) {
					if (needToRebind(lt, rt))
						rebind(lt, rt);
					if (isValid(lt, rt))
						processTuple(lt, rt, ret);
				}
				left.recycle(lt);
			}
			count += leftOutputs.size();
			System.out.print('.');
			leftOutputs.clear();
			ok = left.getNextTuple(leftOutputs);
		}
		
		close();
		return ret;
	}
	
	@Override
	protected String getArguments() {
		return "";
	}

	@Override
	public String getLogicalDescriptor() {
		return "X: " + getArguments();
	}
}

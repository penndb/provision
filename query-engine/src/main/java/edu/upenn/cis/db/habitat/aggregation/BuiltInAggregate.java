/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.aggregation;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * Built in SQL-style aggregate functions, return a single field from a tabular input
 * 
 * @author ZacharyIves
 *
 * @param <T>
 */
public class BuiltInAggregate<T extends Comparable<T>> extends AggregateWithLookup<T> {
	public static enum AggFunction {Sum, Avg, Count, Max, Min};
	
	AggFunction function;
	String aggInput;
	BasicSchema outputSchema;
	T value;
	Class<T> theClass;
	
	public BuiltInAggregate(AggFunction function, Class<T> theClass, BasicSchema baseSchema, String aggInput, ProvenanceApi provenance) {
		this.function = function;
		this.theClass = theClass;
	}

	@Override
	public String getName() {
		switch (function) {
		case Sum:
			return "sum";
		case Avg:
			return "Avg";
		case Count:
			return "Count";
		case Max:
			return "Max";
		case Min:
			return "Min";
		}
		return null;
	}
	
	static class AggInfo {
		public double ret = 0;
		public int count = 0;
	}

	@Override
	public T apply(TableWithVariableSchema table) {
		final AggInfo agg = new AggInfo();
		
		table.forEach(tuple -> {
			switch (function) {
			case Sum:
			case Avg:
			case Count:
				agg.ret += Double.valueOf(tuple.getValue(aggInput).toString());
				agg.count++;
				break;
			case Max:
				if (value == null)
					value = (T)tuple.getValue(aggInput);
				else if (value.compareTo((T)tuple.getValue(aggInput)) < 0)
					value = (T)tuple.getValue(aggInput);
				break;
			case Min:
				if (value == null)
					value = (T)tuple.getValue(aggInput);
				else if (value.compareTo((T)tuple.getValue(aggInput)) > 0)
					value = (T)tuple.getValue(aggInput);
				break;
			}
		});

		switch (function) {
		case Sum:
			if (theClass == Integer.class)
				return (T)Integer.valueOf((int)agg.ret);
			else 
				return (T)Double.valueOf(agg.ret);
		case Avg:
			if (theClass == Integer.class)
				return (T)Integer.valueOf((int)(agg.ret/agg.count));
			else 
				return (T)Double.valueOf(agg.ret/agg.count);
		case Count:
			if (theClass == Integer.class)
				return (T)Integer.valueOf((int)agg.count);
			else 
				return (T)Double.valueOf(agg.count);
		case Max:
		case Min:
			return value;
		}
		return null;
	}
}

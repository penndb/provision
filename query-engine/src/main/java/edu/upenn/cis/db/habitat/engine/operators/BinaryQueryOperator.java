/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ID;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.type.InMemoryVariableSchemaTable;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * A binary operator takes a left + right input.  Its output schema is the Cartesian product
 * of the input schemas.
 * 
 * @author zives
 *
 * @param <T>
 */
public abstract class BinaryQueryOperator<T extends TableWithVariableSchema> implements QueryOperator<T>, BinaryOperator<T> {
	final QueryOperator<T> left;
	final QueryOperator<T> right;
	final ID id;
	final ProvenanceWrapperApi provApi;
	
	int count = 0;
	
	protected static final BasicSchema blank = new BasicSchema("");
	Set<BasicSchema> outputSchemas;
	
	List<QueryOperator<T>> list = new ArrayList<>();
	List<MutableTupleWithSchema<String>> recycled = new ArrayList<>();
	
	TupleWithSchema<String> lastLeft = null;
	TupleWithSchema<String> lastRight = null;
	
	public BinaryQueryOperator(QueryOperator<T> left, QueryOperator<T> right, ProvenanceWrapperApi provApi) {
		this.left = left;
		this.right = right;
		this.provApi = provApi;
		id = PlanGen.getID(this);
		list.add(left);
		list.add(right);
	}
	@Override
	public ProvenanceWrapperApi getProvApi() {
		return provApi;
	}

	@Override
	public ID getId() {
		return id;
	}

	/**
	 * Propagate initialization to children
	 * 
	 */
	@Override
	public boolean initialize() throws HabitatServiceException {
		if (left.initialize() && right.initialize())
			return true;
		else
			return false;
	}
	
	@Override
	public Set<BasicSchema> getOutputSchemas() {
		return Collections.unmodifiableSet(outputSchemas);
	}

	@Override
	public boolean close() throws HabitatServiceException {
		System.out.println(getId() + " closed with " + count + " tuples");
		return left.close() && right.close();
	}

	/**
	 * Apply treats the operation as a relation x relation -> relation function
	 * and calls execute().
	 * 
	 */
	@Override
	public T apply(T lInput, T rInput) {
		@SuppressWarnings("unchecked")
		T ret = (T) new InMemoryVariableSchemaTable(getOutputSchemas());
		
		try {
			ret = execute();
		} catch (HabitatServiceException he) {
			he.printStackTrace();
		}
		return ret;
	}
	
	/**
	 * Adds these tuples to be reused.
	 */
	public void recycle(Collection<MutableTupleWithSchema<String>> prevOutputs) {
		if (recycled.isEmpty()) {
			recycled.addAll(prevOutputs);
			prevOutputs.clear();
		}
	}

	public void recycle(MutableTupleWithSchema<String> prevOutput) {
		recycled.add(prevOutput);
	}

	public List<QueryOperator<T>> getChildren() {
		return Collections.unmodifiableList(list);
	}

	public Stream<QueryOperator<T>> flatten() {
		return Stream.concat(Stream.of(this),
				getChildren().stream().flatMap(QueryOperator::flatten));
	}

	public Stream<QueryOperator<T>> flattenTo(Collection<QueryOperator<T>> stopAt) {
		
		if (stopAt.contains(this))
			return Stream.of(this);
		else
			return Stream.concat(Stream.of(this),
					getChildren().stream().flatMap(QueryOperator::flatten));
	}

	protected abstract String getArguments();

	@Override
	public String toString() {
		return toString(0);

	}
	
	public String toString(int indent) {
		StringBuffer spaces = new StringBuffer();
		for (int i = 0; i < indent; i++)
			spaces.append(' ');
		
		return spaces.toString() +  getClass().getSimpleName() + "[" + getId() + "," + getArguments() + "] {\n" +
				left.toString(indent+1) + ",\n" +
				right.toString(indent+1) + "\n" + spaces.toString() + "}\n";
	}

	public void setBasePath(Path path) {
		left.setBasePath(path);
		right.setBasePath(path);
	}

	@Override
	public BasicSchema getAdditionalSchema() {
		return blank;
	}
}

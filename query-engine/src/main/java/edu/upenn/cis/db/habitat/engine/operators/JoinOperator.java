/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.provenance.expressions.ProvenanceCombiner;
import edu.upenn.cis.db.habitat.repository.type.InMemoryVariableSchemaTable;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * Join operator with two parts:  a blocking function, which segments the data
 * into blocks based on a set of blocking specifier fields, and a predicate, which
 * compares items within the Cartesian product of the tuples in the same block.
 * 
 * Note that if the blocking function is 1:m as opposed to a hashing function, we
 * expect that the blocking specifier fields were precomputed. 
 *   
 * @author zives
 *
 * @param <T>
 */
public class JoinOperator<T extends TableWithVariableSchema> extends CartesianOperator<T> {

	/**
	 * Optional: a theta-join predicate.  If left as null, we will rely on
	 * any equijoin fields
	 */
	final BinaryTuplePredicateWithLookup predicate;
	
	/**
	 * Generally: a hash-map from join key (list of fields) to sets of tuples 
	 */
	final MultiValuedMap<List<Object>,MutableTupleWithSchema<String>> blockedRightBuffer = new ArrayListValuedHashMap<>();

	/**
	 * Left equijoin keys
	 */
	final List<String> leftBlockFields;
	
	/**
	 * Right equijoin keys
	 */
	final List<String> rightBlockFields;
	
	/**
	 * Left equijoin column indices
	 */
	int[] leftColumns = null;
	
	/**
	 * Right equijoin column indices
	 */
	int[] rightColumns = null;

	/**
	 * Create an equijoin operator 
	 * @param left left subexpression / iterator
	 * @param right right subexpression / iterator
	 * @param leftBlockFields join keys for left, by name
	 * @param rightBlockFields join keys for right, by name
	 * @param predicate any additional predicates after the equijoin
	 * @param combiner provenance combiner op
	 * @param provApi provenance storage API
	 */
	public JoinOperator(
			QueryOperator<T> left, 
			QueryOperator<T> right, 
			List<String> leftBlockFields,
			List<String> rightBlockFields,
			BinaryTuplePredicateWithLookup predicate, 
			ProvenanceCombiner combiner,
			ProvenanceWrapperApi provApi) {
		super(left, right, combiner, provApi);
		
		this.predicate = predicate;
		this.leftBlockFields = leftBlockFields;
		this.rightBlockFields = rightBlockFields;
	}
	
	/**
	 * Create a theta-join operator 
	 * @param left left subexpression / iterator
	 * @param right right subexpression / iterator
	 * @param predicate arbitrary predicates
	 * @param combiner provenance combiner op
	 * @param provApi provenance storage API
	 */
	public JoinOperator(
			QueryOperator<T> left, 
			QueryOperator<T> right, 
			BinaryTuplePredicateWithLookup predicate, 
			ProvenanceCombiner combiner,
			ProvenanceWrapperApi provApi) {
		super(left, right, combiner, provApi);
		
		this.predicate = predicate;
		this.leftBlockFields = new ArrayList<>();
		this.rightBlockFields = new ArrayList<>();
	}

	/**
	 * Create an equijoin operator without a provenance combiner
	 *  
	 * @param left left subexpression / iterator
	 * @param right right subexpression / iterator
	 * @param leftBlockFields join keys for left, by name
	 * @param rightBlockFields join keys for right, by name
	 * @param predicate any additional predicates after the equijoin
	 * @param provApi provenance storage API
	 */
	public JoinOperator(
			QueryOperator<T> left, 
			QueryOperator<T> right, 
			List<String> leftBlockFields,
			List<String> rightBlockFields,
			BinaryTuplePredicateWithLookup predicate, 
			ProvenanceWrapperApi provApi) {
		super(left, right, provApi);
		
		this.predicate = predicate;
		this.leftBlockFields = leftBlockFields;
		this.rightBlockFields = rightBlockFields;
	}
	
	public JoinOperator(
			QueryOperator<T> left, 
			QueryOperator<T> right, 
			BinaryTuplePredicateWithLookup predicate, 
			ProvenanceWrapperApi provApi) {
		super(left, right, provApi);
		
		this.predicate = predicate;
		this.leftBlockFields = new ArrayList<>();
		this.rightBlockFields = new ArrayList<>();
	}

	/**
	 * Fetch the values of the named fields
	 * 
	 * @param tuple
	 * @param fields
	 * @return
	 */
	public List<Object> getBlock(TupleWithSchema<String> tuple, List<String> fields) {
		List<Object> ret = new ArrayList<>();
		
		if (fields.isEmpty())
			ret.add("o");
		else
			for (String field: fields)
				ret.add(tuple.getValue(field));
		
		return ret;
	}
	
	/**
	 * Fetch the values of the indexed fields
	 * 
	 * @param tuple
	 * @param fields
	 * @return
	 */
	public List<Object> getBlock(TupleWithSchema<String> tuple, int[] fields) {
		List<Object> ret = new ArrayList<>();
		
		if (fields.length == 0)
			ret.add("o");
		else
			for (int i = 0; i < fields.length; i++)
				ret.add(tuple.getValueAt(fields[i]));
		
		return ret;
	}

	/**
	 * If the left/right tuple objects have changed, we need to link the predicate
	 * to them 
	 */
	@Override
	public void rebind(TupleWithSchema<String> left, TupleWithSchema<String> right) {
		if (predicate != null)
			predicate.bind(left, right);
	}

	/**
	 * Does the left-right combination match the predicate?
	 */
	@Override
	public boolean isValid(TupleWithSchema<String> left, TupleWithSchema<String> right) {
		if (predicate != null)
			return predicate.test(left, right);
		else
			return true;
	}

	/**
	 * Temp buffer for left tuples
	 */
	T leftOutputs = null;
	
	/**
	 * First: read a batch of tuples from the left input (there can be > 1)
	 * Then read the right input and "build" a table
	 * Finally: probe the left input against the "build" table
	 * And repeat this for each call
	 */
	@SuppressWarnings("unchecked")
	@Override
	public synchronized boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		
		// Make sure we have a "container" for the inputs from the left child
		if (leftOutputs == null)
			leftOutputs = (T) new InMemoryVariableSchemaTable(left.getOutputSchemas());

		// Get >= 1 tuples from the left child (ok == false if that doesn't happen and
		// we can short-circuit)
		boolean ok = left.getNextTuple(leftOutputs);
		
		// Now we read and "build" a table for the right-child
		
		// Again, create the buffer
		if (rightBuffer == null) {
			rightBuffer = (T) new InMemoryVariableSchemaTable(right.getOutputSchemas());

			// Loop and build while we have something
			if (ok)
				ok &= right.getNextTuple(rightBuffer);
			while (ok) {
				// If need be, index the columns against the schema (should only
				// be called 1x)
				if (rightColumns == null && !rightBuffer.isEmpty()) {
					rightColumns = new int[rightBlockFields.size()];
					MutableTupleWithSchema<String> right = rightBuffer.iterator().next(); 
					for (int i = 0; i < rightColumns.length; i++)
						rightColumns[i] = right.getKeys().indexOf(rightBlockFields.get(i));
				}
				
				// Move from right buffer to the blocked buffer, which is a map
				// from join key to tuples
				for (MutableTupleWithSchema<String> tup: rightBuffer)
					blockedRightBuffer.put(getBlock(tup, rightColumns), tup);
				
				// Prepare the right buffer for another batch of inputs
				rightBuffer.clear();
	
				// Try to read and loop, or break out if there's nothing
				// (We leave ok the same because it will tell us if we have at least
				// left + right inputs)
				if (!right.getNextTuple(rightBuffer))
					break;
			}
			logger.debug("Operator {}: {} tuples in right buffer", getId().toString(), blockedRightBuffer.size());
		}
		
		// We have successfully read at least one result from the left
		// and built a map over the right.  Now we can do a probe from left to map
		if (ok) {
			// Initialize schema info on the first call
			if (leftColumns == null && !leftOutputs.isEmpty()) {
				leftColumns = new int[leftBlockFields.size()];
				MutableTupleWithSchema<String> left = leftOutputs.iterator().next(); 
				for (int i = 0; i < leftColumns.length; i++)
					leftColumns[i] = left.getKeys().indexOf(leftBlockFields.get(i));
			}

			// Get the hash entry from the leftColumns, then look up the matches from
			// the blockedRightBuffer.  Try to process left/right tuples and queue
			// them up in outputs.  R
			for (MutableTupleWithSchema<String> lt: leftOutputs) {
				List<Object> blockId = getBlock(lt, leftColumns);//leftBlockFields);
				for (MutableTupleWithSchema<String> rt: blockedRightBuffer.get(blockId)) {
					if (needToRebind(lt, rt))
						rebind(lt, rt);
					if (isValid(lt, rt))
						processTuple(lt, rt, outputs);
					
					// Can get rid of the left tuple "container" each time
					left.recycle(lt);
				}
			}
			leftOutputs.clear();
		}
		return ok;
	}

	/**
	 * execute() does a full-blown join, producing a table.  It's generally only
	 * used by something other than a parent operator; parent operators typically
	 * call getNextTuple instead.
	 * 
	 * For a standard join, execute() fetches one left-child tuple and one right-child
	 * tuple, just to get both pipelines going.  Thereafter it fetches the remainder of
	 * the right child (the "build" relation), and takes the left tuple and 
	 * joins ("probes") against everything.  In subsequent calls it fetches more tuples 
	 * from the left and probes the right.
	 * 
	 * Depending on the blocks / hashed fields, the predicate may play a role or
	 * it may not.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T execute() throws HabitatServiceException {
		initialize();
		T ret = (T) new InMemoryVariableSchemaTable(getOutputSchemas());
		T leftOutputs = (T) new InMemoryVariableSchemaTable(getOutputSchemas());
		T rightOutputs = (T) new InMemoryVariableSchemaTable(getOutputSchemas());

		boolean ok = left.getNextTuple(leftOutputs);
		ok &= right.getNextTuple(rightOutputs);
		if (ok) {
			while (right.getNextTuple(rightOutputs))
				;
			// Move from right buffer to the blocked buffer 
			for (MutableTupleWithSchema<String> tup: rightOutputs)
				blockedRightBuffer.put(getBlock(tup, rightBlockFields), tup);
			
			rightOutputs.clear();
		}
		
		while (ok) {
			for (MutableTupleWithSchema<String> lt: leftOutputs) {
				List<Object> blockId = getBlock(lt, leftBlockFields);
				for (MutableTupleWithSchema<String> rt: blockedRightBuffer.get(blockId)) {
					if (needToRebind(lt, rt))
						rebind(lt, rt);
					if (isValid(lt, rt))
						processTuple(lt, rt, ret);
				}
			}
			leftOutputs.clear();
			ok = left.getNextTuple(leftOutputs);
		}
		
		close();
		return ret;
	}
	
	protected String getArguments() {
		StringBuilder equijoins = new StringBuilder();
		
		if (leftBlockFields != null && !leftBlockFields.isEmpty()) {
			for (int i = 0; i < leftBlockFields.size() && i < rightBlockFields.size(); i++) {
				if (i > 0)
					equijoins.append(",");
				equijoins.append(leftBlockFields.get(i) + " = " + rightBlockFields.get(i));
			}
			return "(" + equijoins.toString() + ")" + 
				((predicate != null) ? " && " + predicate.toString() : "");
		} else if (predicate == null)
			return "";
		else
			return predicate.toString();
	}
	
	public List<String> getLeftJoinKeys() {
		return Collections.unmodifiableList(leftBlockFields);
	}

	public List<String> getRightJoinKeys() {
		return Collections.unmodifiableList(rightBlockFields);
	}

	public BinaryTuplePredicateWithLookup getPredicate() {
		return predicate;
	}
}

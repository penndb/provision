/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.jython;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.python.core.PyCode;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

import edu.upenn.cis.db.habitat.core.expressions.BinaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

/**
 * Takes the fields in the tuple and sets them as Python variables.
 * If there are clashing field names in both the left and right inputs, we append
 * a _2 to the right side.
 * 
 * Executes the python script.  Expects the returned output to be in the
 * variable result, which should evaluate to a Boolean.
 * 
 * @author zives
 *
 */
public class JythonBinaryTuplePredicate extends BinaryTuplePredicateWithLookup {
	
	PyCode pythonScript = null;
//	InputStream pythonStream = null;
	PythonInterpreter interp;
	final Set<String> leftSymbols;
	final Set<String> rightSymbols;
	
	public JythonBinaryTuplePredicate(String pythonScript, Set<String> leftSymbols, Set<String> rightSymbols) {
		String code = pythonScript.replaceAll("\\n", "\n");;
		String uniqueName = UUID.randomUUID().toString();
		interp = JythonGen.getPythonInterpreter(uniqueName);
		this.pythonScript = interp.compile(code);
		this.leftSymbols = leftSymbols;
		this.rightSymbols = rightSymbols;
	}

	public JythonBinaryTuplePredicate(InputStream stream, Set<String> leftSymbols, Set<String> rightSymbols) {
//		this.pythonStream = stream;
		String uniqueName = UUID.randomUUID().toString();
		interp = JythonGen.getPythonInterpreter(uniqueName);
		this.pythonScript = interp.compile(new InputStreamReader(stream));
		
		this.leftSymbols = leftSymbols;
		this.rightSymbols = rightSymbols;
	}

	@Override
	public boolean test(TupleWithSchema<String> left, TupleWithSchema<String> right) {

//		if (pythonScript != null)
			interp.exec(pythonScript);
//		else
//			interp.execfile(pythonStream);
		
		PyObject ret = interp.get("result");
		
		if (ret == null)
			throw new RuntimeException("Python result not found");
		
		return ret.__nonzero__();
	}

	@Override
	public void bind(TupleWithSchema<String> left, TupleWithSchema<String> right) {
		super.bind(left, right);
		for (String key: left.getSchema().getKeys()) {
			Object o = left.getValue(key);
			
			if (o == null)
				interp.set(key, null);
			else 
				interp.set(key, o);
		}
		for (String key: right.getSchema().getKeys()) {
			Object o = right.getValue(key);
			
			// Override collisions
			if (left.getSchema().getKeys().contains(key))
				key = key + "_2";
			
			if (o == null)
				interp.set(key, null);
			else 
				interp.set(key, o);
		}
	}

	@Override
	public Set<String> getSymbolsReferencedLeft() {
		return Collections.unmodifiableSet(leftSymbols);
	}

	@Override
	public Set<String> getSymbolsReferencedRight() {
		return Collections.unmodifiableSet(rightSymbols);
	}

	@Override
	public void swapLeftAndRight() {
		Set<String> tempSymbols = new HashSet<>();
		tempSymbols.addAll(leftSymbols);
		leftSymbols.clear();
		leftSymbols.addAll(rightSymbols);
		rightSymbols.clear();
		rightSymbols.addAll(tempSymbols);
	}
}

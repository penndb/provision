/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.provenance;

import static com.google.common.collect.Maps.transformValues;

import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.Serializer;
import org.mapdb.serializer.SerializerArrayTuple;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.querygen.PlanGen;

public class MapDBProvAPI implements ProvenanceApi {
	final static Logger logger = LogManager.getLogger(MapDBProvAPI.class);

	final Map<String, ProvSpecifier> nodes;
	NavigableMap<String,Object[]> edgesFrom;
	NavigableMap<String,Object[]> edgesTo;
	DB database;
	
	public MapDBProvAPI(DB database) {
		this.database = database;
		nodes = (Map<String, ProvSpecifier>) database.hashMap("nodes")
				.keySerializer(Serializer.STRING)
				.createOrOpen();
		
		edgesFrom = (BTreeMap<String,Object[]>)database.treeMap("eFrom")
				.keySerializer(Serializer.STRING)
				.valueSerializer(new SerializerArrayTuple(Serializer.STRING, Serializer.STRING))
				.createOrOpen();

		edgesTo = (BTreeMap<String,Object[]>)database.treeMap("eTo")
				.keySerializer(Serializer.STRING)
				.valueSerializer(new SerializerArrayTuple(Serializer.STRING, Serializer.STRING))
				.createOrOpen();
				
	}

	@Override
	public void storeProvenanceNode(String resource, ProvToken token, ProvSpecifier location) {
//		logger.debug("Node " + location + " @" + token);
		nodes.put(token.toString(), location);
	}

	@Override
	public ProvSpecifier getProvenanceLocation(String resource, ProvToken token) {
		return nodes.get(token.toString());
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken from, String label, ProvToken to) {
//		logger.debug("Edge (@" + from + "," + label + ", @" + to + ")");
//		edgesFrom.put(from, new ImmutablePair<ProvToken,String>(to, label));
		
		edgesFrom.put(from.toString(), new Object[]{to.toString(), label});
		edgesTo.put(to.toString(), new Object[]{from.toString(), label});
		
	}

	@Override
	public Set<ProvToken> getConnectedTo(String resource, ProvToken to) {
		Map<String,Object[]> edges = edgesTo.subMap(to.toString(), true, to.toString(), true);
		
		Set<ProvToken> ret = 
				edges.values().parallelStream()//.filter(item -> item[1].toString().equals(label))
				.map(pair -> PlanGen.getTokenFromString((String)pair[0])).collect(Collectors.toSet());
		return ret;
	}

	@Override
	public Set<ProvToken> getConnectedFrom(String resource, ProvToken from) {
		Map<String,Object[]> edges = edgesFrom.subMap(from.toString(), true, from.toString(), true);
		
		Set<ProvToken> ret = 
				edges.values().parallelStream()//.filter(item -> item[1].toString().equals(label))
				.map(pair -> PlanGen.getTokenFromString((String)pair[0])).collect(Collectors.toSet());
		return ret;
	}

	@Override
	public Set<ProvToken> getConnectedTo(String resource, ProvToken to, String label) {
		Map<String,Object[]> edges = edgesTo.subMap(to.toString(), true, to.toString(), true);
		
		Set<ProvToken> ret = 
				edges.values().parallelStream().filter(item -> item[1].toString().equals(label))
				.map(pair -> PlanGen.getTokenFromString((String)pair[0])).collect(Collectors.toSet());
		return ret;
	}

	@Override
	public Set<ProvToken> getConnectedFrom(String resource, ProvToken from, String label) {
		Map<String,Object[]> edges = edgesFrom.subMap(from.toString(), true, from.toString(), true);
		
		Set<ProvToken> ret = 
				edges.values().parallelStream().filter(item -> item[1].toString().equals(label))
				.map(pair -> PlanGen.getTokenFromString((String)pair[0])).collect(Collectors.toSet());
		return ret;
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken fromLeft, ProvToken fromRight, String label, ProvToken to) {
		storeProvenanceLink(resource, fromLeft, label, to);
		storeProvenanceLink(resource, fromRight, label, to);
		database.commit();
	}

	@Override
	public void storeProvenanceLink(String resource, List<ProvToken> inputProv, String label, ProvToken to) {
		for (ProvToken input: inputProv) {
			storeProvenanceLink(resource, input, label, to);
		}
		
		database.commit();
	}

	@Override
	public void storeProvenanceNode(String resource, ProvToken token, TupleWithSchema<String> tuple, ProvSpecifier location) {
		throw new UnsupportedOperationException("Don't yet support tuples");
	}

	@Override
	public TupleWithSchema<String> getProvenanceData(String resource, ProvToken token) {
		throw new UnsupportedOperationException("Don't yet support tuples");
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken from, String label,
			ProvToken to, TupleWithSchema<String> tuple) {
		throw new UnsupportedOperationException("Don't yet support tuples");
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken fromLeft, ProvToken fromRight, String label, ProvToken to, 
			TupleWithSchema<String> tuple) {
		throw new UnsupportedOperationException("Don't yet support tuples");
	}

	@Override
	public void storeProvenanceLink(String resource, List<ProvToken> inputProv, String label, ProvToken to, 
			TupleWithSchema<String> tuple) {
		throw new UnsupportedOperationException("Don't yet support tuples");
	}

	@Override
	public Set<TupleWithSchema<String>> getEdgesTo(String resource, ProvToken to) {
		throw new UnsupportedOperationException("Don't yet support tuples");
	}

	@Override
	public Set<TupleWithSchema<String>> getEdgesFrom(String resource, ProvToken from) {
		throw new UnsupportedOperationException("Don't yet support tuples");
	}

	@Override
	public Map<String, StructuredData<String, Object>> getProvenanceNodes(String resoure)
			throws HabitatServiceException {
		return transformValues(this.nodes, (ProvSpecifier s) -> {
			final StructuredData<String, Object> data = new StructuredValue<>();
			data.put("location", s);
			return data;
		});
	}

	@Override
	public Map<String, List<TupleWithSchema<String>>> getEdgeMapFrom(String resource, ProvToken from) throws HabitatServiceException {
		throw new UnsupportedOperationException("Don't yet support tuples");		
	}

	@Override
	public Map<String, List<TupleWithSchema<String>>> getEdgeMapTo(String resource, ProvToken to) throws HabitatServiceException {
		throw new UnsupportedOperationException("Don't yet support tuples");		
	}

}

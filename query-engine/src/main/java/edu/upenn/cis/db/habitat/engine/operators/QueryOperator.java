/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ID;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * Generic query operator
 * 
 * @author zives
 *
 */
public interface QueryOperator<T extends TableWithVariableSchema> {
	public ID getId();
	
	ProvenanceWrapperApi getProvApi();
	
	public boolean initialize() throws HabitatServiceException;
	
	boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException;
	
	public Set<BasicSchema> getOutputSchemas();
	
	/**
	 * What elements did this operator introduce to the schema,
	 * e.g., by joining, renaming, adding agg functions, evaluating a function, etc.
	 * 
	 * @return
	 */
	public BasicSchema getAdditionalSchema();
	
	public boolean close() throws HabitatServiceException;

	/**
	 * execute() does a full-blown join, producing a table.  It's generally only
	 * used by something other than a parent operator; parent operators typically
	 * call getNextTuple() instead.
	 * 
	 * @return
	 * @throws HabitatServiceException
	 */
	public T execute() throws HabitatServiceException;
	
	/**
	 * Reuse an old output tuple set that's no longer needed
	 * @param prevOutput
	 */
	public void recycle(Collection<MutableTupleWithSchema<String>> prevOutputs);

	/**
	 * Reuse an old output tuple that's no longer needed
	 * @param prevOutput
	 */
	public void recycle(MutableTupleWithSchema<String> prevOutput);
	
	public List<QueryOperator<T>> getChildren();
	
	/**
	 * Turns the tree into a flat list
	 * 
	 * @return
	 */
	public Stream<QueryOperator<T>> flatten();
	
	public Stream<QueryOperator<T>> flattenTo(Collection<QueryOperator<T>> stopAt);

	public String toString(int indent);
	
	/**
	 * Logical descriptor is used to create a signature for each operator
	 * 
	 * @return
	 */
	public String getLogicalDescriptor();
	
	/**
	 * Set the path for all file I/O operations
	 * 
	 * @param path
	 */
	public void setBasePath(Path path);
}

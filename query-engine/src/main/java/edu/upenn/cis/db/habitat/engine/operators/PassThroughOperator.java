/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.util.Collection;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public abstract class PassThroughOperator<T extends TableWithVariableSchema> 
extends UnaryQueryOperator<T> {

	public PassThroughOperator(QueryOperator<T> child, ProvenanceWrapperApi provApi) {
		super(child, provApi);
	}

	@Override
	public Set<BasicSchema> getOutputSchemas() {
		return child.getOutputSchemas();
	}
	
	public abstract boolean isPassable(TupleWithSchema<String> tuple);
	
	@Override
	public boolean processTuple(MutableTupleWithSchema<String> tuple, Collection<MutableTupleWithSchema<String>> outputs) 
	throws HabitatServiceException {
		if (isPassable(tuple)) {
			outputs.add(tuple);
			return true;
		} else
			return false;
	}

}

/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators.extract;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.FileExtractOperator;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import htsjdk.samtools.fastq.FastqReader;
import htsjdk.samtools.fastq.FastqRecord;

public class FastQExtractor<T extends TableWithVariableSchema> extends FileExtractOperator<T> {
	BufferedReader reader = null;
	FastqReader fastq;
	int lineNo = 0;
	File fil = null;
	String filename = null;
	
	public FastQExtractor(String filename, String schemaName, ProvenanceWrapperApi provApi) {
		super(provApi);

		BasicSchema project = new BasicSchema(schemaName);//filename);
		project.addField(ProvenanceApi.Token, ProvToken.class);
		project.addField(ProvenanceApi.Provenance, ProvLocation.class);
		project.addField("lineNumber", Integer.class);
		project.addField("readHeader", String.class);
		project.addField("readString", String.class);
		project.addField("baseQualityHeader", String.class);
		project.addField("baseQualityString", String.class);
		
		projectThese.add(project);

		this.filename = filename;
	}

	public FastQExtractor(File fil, ProvenanceWrapperApi provApi) {
		super(provApi);

		BasicSchema project = new BasicSchema(fil.getName());
		project.addField(ProvenanceApi.Token, ProvToken.class);
		project.addField(ProvenanceApi.Provenance, ProvLocation.class);
		project.addField("lineNumber", Integer.class);
		project.addField("readHeader", String.class);
		project.addField("readString", String.class);
		project.addField("baseQualityHeader", String.class);
		project.addField("baseQualityString", String.class);
		
		project.addLookupKey("lineNumber");
		
		projectThese.add(project);

		this.fil = fil;
	}

	@Override
	public boolean initialize() throws HabitatServiceException {
		if (!super.initialize())
			return false;
		
		try {
			if (fil != null) {
				reader = new BufferedReader(new FileReader(fil));
				filename = fil.getName();
			} else {
				if (basePath != null && !filename.startsWith(basePath.toString()))
					filename = Paths.get(basePath.toString(), this.filename).toString();
				reader = new BufferedReader(new FileReader(filename));
			}
		} catch (FileNotFoundException fnf) {
			throw new HabitatServiceException(fnf.getMessage());
		}
		
		//fastq = new FastqReader(reader);
		try {
			if (reader == null) {
				fastq = new FastqReader(new BufferedReader(new FileReader(fil)));
				filename = fil.getName();
			} else
				fastq = new FastqReader(reader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		provApi.writeInputFile(filename);

		return true;
	}
	
	@Override
	public boolean close() throws HabitatServiceException {
		fastq.close();
		
		return super.close();
	}

	/**
	 * We only have a single tuple schema
	 * 
	 * @return
	 */
	public BasicSchema getSchema() {
		return projectThese.iterator().next();
	}

	@Override
	public boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		if (!fastq.hasNext())
			return false;
		
		FastqRecord record = fastq.iterator().next();
		
		MutableTupleWithSchema<String> tup = (!recycled.isEmpty()) ? recycled.remove(0) : getSchema().createTuple();
		ProvToken us = PlanGen.getToken(this);
		ProvLocation here = getInputPosition();
		
		//provApi.storeProvenanceNode("", us, here);
		
		tup.setValue(ProvenanceApi.Token, us);
		tup.setValue(ProvenanceApi.Provenance, here);
		tup.setValue("lineNumber", lineNo);
		tup.setValue("readHeader", record.getReadHeader());
		tup.setValue("readString", record.getReadString());
		tup.setValue("baseQualityHeader", record.getBaseQualityHeader());
		tup.setValue("baseQualityString", record.getBaseQualityString());
		
		provApi.writeInputRecord(us, here, filename, getId().toString(), tup);
		
		outputs.add(tup);
		lineNo++;
		
		return true;
	}

	@Override
	public ProvLocation getInputPosition() {
		Set<Integer> range = new HashSet<>();
		range.add(lineNo);
		return new ProvLocation(getId(), getSchema().getName(), 
				range);
	}
	
	@Override
	protected String getArguments() {
		if (fil != null)
			return fil.getName();
		else
			return filename;
	}

	@Override
	public String getLogicalDescriptor() {
		return //getClass().getSimpleName() + ": " + 
				getArguments();
	}
}

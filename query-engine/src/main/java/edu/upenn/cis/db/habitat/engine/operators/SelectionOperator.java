/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.util.Collection;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.repository.type.InMemoryVariableSchemaTable;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class SelectionOperator<T extends TableWithVariableSchema> 
extends PassThroughOperator<T> {
	
	UnaryTuplePredicateWithLookup predicate;

	public SelectionOperator(QueryOperator<T> child, UnaryTuplePredicateWithLookup pred, ProvenanceWrapperApi provApi) {
		super(child, provApi);
		
		predicate = pred;
	}
	
	T childOutputs = null;

	@Override
	public synchronized boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		//T childOutputs = (T) new InMemoryVariableSchemaTable(getOutputSchemas());
		if (childOutputs == null)
			childOutputs = (T) new InMemoryVariableSchemaTable(getOutputSchemas());
		else
			childOutputs.clear();

		boolean haveResult = false;
		boolean ok = child.getNextTuple(childOutputs);
		while (ok && !haveResult) {
			for (MutableTupleWithSchema<String> tup: childOutputs) {
				if (needToRebind(tup))
					rebind(tup);
				if (processTuple(tup, outputs))
					haveResult = true;
			}
			if (!haveResult) {
				childOutputs.clear();
				ok = child.getNextTuple(childOutputs);
			}
		}
		return haveResult;
	}

	@Override
	public boolean isPassable(TupleWithSchema<String> tuple) {
		return predicate.test(tuple);
	}

	@Override
	public void rebind(TupleWithSchema<String> childTuple) {
		predicate.bind(childTuple);
	}

	@Override
	protected String getArguments() {
		return predicate.toString();
	}

	@Override
	public String getLogicalDescriptor() {
		return "sigma: " + getArguments();
	}
		
	public UnaryTuplePredicateWithLookup getPredicate() {
		return predicate;
	}
}

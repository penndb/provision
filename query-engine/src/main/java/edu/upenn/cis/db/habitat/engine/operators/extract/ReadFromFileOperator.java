/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators.extract;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.FileExtractOperator;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class ReadFromFileOperator<T extends TableWithVariableSchema> extends FileExtractOperator<T> {
	ObjectInputStream in;
	BasicTuple firstRec = null;
	int lineNo = 0;
	BasicSchema project;
	
	String filename;

	public ReadFromFileOperator(String filename, ProvenanceWrapperApi provApi) throws IOException {
		super(provApi);
		
		this.filename = filename;
		in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(filename)));

		project = new BasicSchema(filename);
	}
	
	public boolean initialize() throws HabitatServiceException {
		try {
			firstRec = (BasicTuple)in.readObject();
			project = firstRec.getSchema();
		} catch (IOException | ClassNotFoundException except) {
			throw new HabitatServiceException(except.getMessage());
		}
		return true;
	}

	@Override
	public boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		if (firstRec != null) {
			outputs.add(firstRec);
			firstRec = null;
			return true;
		}

		BasicTuple tup;
		try {
			tup = (BasicTuple) in.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		outputs.add(tup);
		lineNo++;
		
		return true;
	}

	@Override
	public ProvLocation getInputPosition() {
		Set<Integer> range = new HashSet<>();
		range.add(lineNo);
		return new ProvLocation(getId(), project.getName(), 
				range);
	}

	@Override
	protected String getArguments() {
		return filename;
	}

	@Override
	public String getLogicalDescriptor() {
		return getClass().getSimpleName() + ": " + getArguments();
	}
}

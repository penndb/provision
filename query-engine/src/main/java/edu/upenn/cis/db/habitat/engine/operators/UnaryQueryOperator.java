/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ID;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.type.InMemoryVariableSchemaTable;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public abstract class UnaryQueryOperator<T extends TableWithVariableSchema> implements QueryOperator<T>, UnaryOperator<T> {
	Logger logger = LogManager.getLogger(CartesianOperator.class);
	final protected QueryOperator<T> child;
	
	protected TupleWithSchema<String> lastTuple = null;
	
	protected static final BasicSchema blank = new BasicSchema("");
	
	final ID id;
	final ProvenanceWrapperApi provApi;
	
	List<MutableTupleWithSchema<String>> recycled = new ArrayList<>();
	List<QueryOperator<T>> list = new ArrayList<>();
	
	public UnaryQueryOperator(QueryOperator<T> child, ProvenanceWrapperApi provApi) {
		this.child = child;
		id = PlanGen.getID(this);
		this.provApi = provApi;
		list.add(child);
	}
	
	@Override
	public ProvenanceWrapperApi getProvApi() {
		return provApi;
	}

	@Override
	public ID getId() {
		return id;
	}

	@Override
	public boolean initialize() throws HabitatServiceException {
		if (!child.initialize())
			return false;
		
		return true;
	}

	@Override
	public boolean close() throws HabitatServiceException {
		return child.close();
	}
	
	abstract boolean processTuple(MutableTupleWithSchema<String> input, Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException;
	
	@Override
	public T apply(T input) {
		T ret = null;
		
		try {
			ret = execute();
		} catch (HabitatServiceException he) {
			he.printStackTrace();
		}
		return ret;
	}
	
	@Override
	public T execute() throws HabitatServiceException {
		initialize();
		T ret = (T) new InMemoryVariableSchemaTable(getOutputSchemas());

		while (getNextTuple(ret))
			;
		
		close();
		return ret;
	}
	
	
	protected boolean needToRebind(TupleWithSchema<String> input) {
		if (input == lastTuple)
			return false;
		
		lastTuple = input;
		
		return true;
	}
	
	public abstract void rebind(TupleWithSchema<String> childTuple);

	public void recycle(Collection<MutableTupleWithSchema<String>> prevOutputs) {
		if (recycled.isEmpty()) {
			recycled.addAll(prevOutputs);
			prevOutputs.clear();
		}
	}

	public void recycle(MutableTupleWithSchema prevOutput) {
		recycled.add(prevOutput);
	}
	
	public List<QueryOperator<T>> getChildren() {
		return Collections.unmodifiableList(list);
	}

	public Stream<QueryOperator<T>> flatten() {
		return Stream.concat(Stream.of(this),
				getChildren().stream().flatMap(QueryOperator::flatten));
	}
	
	public Stream<QueryOperator<T>> flattenTo(Collection<QueryOperator<T>> stopAt) {
		if (stopAt.contains(this))
			return Stream.of(this);
		else
			return Stream.concat(Stream.of(this),
					getChildren().stream().flatMap(QueryOperator::flatten));
	}
	
	protected abstract String getArguments();

	@Override
	public String toString() {
		return toString(0);

	}
	public String toString(int indent) {
		StringBuffer spaces = new StringBuffer();
		for (int i = 0; i < indent; i++)
			spaces.append(' ');
		
		return spaces.toString() + getClass().getSimpleName() + "[" + getId() + "," + getArguments() + "] {\n" +
				child.toString(indent + 1) + "\n" + spaces.toString() + "}\n";
	}

	public void setBasePath(Path path) {
		child.setBasePath(path);
	}

	@Override
	public BasicSchema getAdditionalSchema() {
		return blank;
	}
}

/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators.materialize;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.MaterializeOperator;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class MaterializeToFileOperator<T extends TableWithVariableSchema> extends MaterializeOperator<T> {
	
	ObjectOutputStream stream;
	String filename;

	public MaterializeToFileOperator(QueryOperator<T> child,
			String filename,
			ProvenanceWrapperApi provApi) throws IOException {
		super(child, new HashMap<>(), provApi);
		
		this.filename = filename;
		
		for (String str: new String[] {
				"baseQualityHeader", "baseQualityString", "readHeader", "readString"}) {
			if (!fields.containsValue(str))
				throw new IllegalArgumentException("Must have base quality header + string and read header + string.  Missing " + str + " from " + fields.keySet());
		}
		
		stream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(filename)));
	}

	@Override
	public void recordWriter(BasicTuple tup) throws HabitatServiceException {
		try {
			stream.writeObject(tup);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
	}

	@Override
	public boolean close() throws HabitatServiceException {
		try {
			stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return super.close();
	}

	@Override
	protected String getArguments() {
		return filename;
	}

	@Override
	public String getLogicalDescriptor() {
		return getClass().getSimpleName() + ": " + getArguments();
	}
}

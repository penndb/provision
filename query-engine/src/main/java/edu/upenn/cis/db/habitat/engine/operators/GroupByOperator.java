/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.UnaryOperator;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.provenance.expressions.ProvenanceAggregator;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.type.InMemoryVariableSchemaTable;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class GroupByOperator<T extends TableWithVariableSchema> extends SchemaModifyingOperator<T> {
	List<String> groupingFields;
	
	List<Function<T,BasicTuple>> aggFns = new ArrayList<>();
	List<BasicSchema> aggOutputs = new ArrayList<>();
	ProvenanceAggregator provAgg;
	
	MultiValuedMap<List<Object>,MutableTupleWithSchema<String>> groupedInput = new ArrayListValuedHashMap<>();
	
	Iterator<List<Object>> groupIter = null;

	public GroupByOperator(QueryOperator<T> child, List<String> groupingFields,
			Function<T,BasicTuple> aggFunction,
			BasicSchema aggOutput,
			ProvenanceAggregator provAgg,
			ProvenanceWrapperApi provApi) {
		super(child, provApi);
		
		aggFns.add(aggFunction);
		aggOutputs.add(aggOutput);
		this.provAgg = provAgg;
		
		this.groupingFields = groupingFields;
	}
	
	public GroupByOperator(QueryOperator<T> child, List<String> groupingFields,
			List<Function<T,BasicTuple>> aggFunctions,
			List<BasicSchema> aggOutputs,
			ProvenanceAggregator provAgg,
			ProvenanceWrapperApi provApi) {
		super(child, provApi);
		
		aggFns = aggFunctions;
		this.aggOutputs = aggOutputs;
		this.provAgg = provAgg;
		
		this.groupingFields = groupingFields;
	}
	
	@Override
	public void createMappings(Set<BasicSchema> inputs) {
		List<String> outputFields = new ArrayList<String>();
		outputFields.addAll(this.groupingFields);
		
		if (!outputFields.contains(ProvenanceApi.Token))
			outputFields.add(ProvenanceApi.Token);
		if (!outputFields.contains(ProvenanceApi.Provenance))
			outputFields.add(ProvenanceApi.Provenance);
		inputs.parallelStream().forEach(schema -> 
		{
			for (BasicSchema aggOutput: aggOutputs)
				registerMappings(schema,
						// Schema map
						s -> s.project(schema.getName() + "_" + getId(), 
								outputFields).concat(schema.getName() + "_" + getId(), 
								aggOutput));
		});
	}

	public void registerMappings(BasicSchema inputSchema,
			UnaryOperator<BasicSchema> schemaFn) {
		this.schemaFn.put(inputSchema, schemaFn);
	}
	
	@Override
	public boolean processTuple(MutableTupleWithSchema<String> tuple, Collection<MutableTupleWithSchema<String>> outputs) 
	throws HabitatServiceException {
		return true;
	}

	public boolean groupTuples(Collection<MutableTupleWithSchema<String>> tuples, Collection<MutableTupleWithSchema<String>> outputs) 
	throws HabitatServiceException {
		if (!tuples.isEmpty()) {
			TupleWithSchema<String> first = tuples.iterator().next();
			BasicTuple groupTuple = schemaMap.get(first.getSchema()).createTuple();
			
			List<ProvSpecifier> inputProv = new ArrayList<>();
			List<ProvToken> inputTokens = new ArrayList<>();
			for (TupleWithSchema<String> tup: tuples) {
				inputTokens.add((ProvToken)tup.getValue(ProvenanceApi.Token));
				inputProv.add((ProvSpecifier)tup.getValue(ProvenanceApi.Provenance));
			}
			
			groupTuple.setValue(ProvenanceApi.Token, PlanGen.getToken(this));
			groupTuple.setValue(ProvenanceApi.Provenance, provAgg.apply(inputProv));
			
			for (String field: groupingFields)
				groupTuple.setValue(field, first.getValue(field));
			
			@SuppressWarnings("unchecked")
			T toAgg = (T) new InMemoryVariableSchemaTable((BasicSchema) first.getSchema());
			for (MutableTupleWithSchema<String> tup: tuples)
				toAgg.add(tup);
			
			for (Function<T,BasicTuple> aggFn: aggFns) {
				BasicTuple aggResult = aggFn.apply(toAgg);
				
				for (String aggField: aggResult.getKeys())
					if (!aggField.equals(ProvenanceApi.Provenance) && !aggField.equals(ProvenanceApi.Token))
						groupTuple.setValue(aggField, aggResult.getValue(aggField));
			}			
			outputs.add(groupTuple);
			
			provApi.writeGroupDerivation((ProvToken)groupTuple.getValue(ProvenanceApi.Token), 
					(ProvSpecifier)groupTuple.getValue(ProvenanceApi.Provenance), 
					inputTokens, 
					getId().toString(),
					groupTuple);
			return true;
		} else
			return false;
	}

	public List<Object> getBlock(TupleWithSchema<String> tuple, List<String> fields) {
		List<Object> ret = new ArrayList<>();
		
		if (fields.isEmpty())
			ret.add("one_group");
		else
			for (String field: fields)
				ret.add(tuple.getValue(field));
		
		return ret;
	}
	
	public List<Object> getBlock(TupleWithSchema<String> tuple, int[] fields) {
		List<Object> ret = new ArrayList<>();
		
		if (fields.length == 0)
			ret.add("one_group");
		else
			for (int i = 0; i < fields.length; i++)
				ret.add(tuple.getValueAt(fields[i]));
		
		return ret;
	}

	int[] tableColumns = null;
	
	@Override
	public boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		@SuppressWarnings("unchecked")
		T childOutputs = (T) new InMemoryVariableSchemaTable(getOutputSchemas());

		int count = 0;
		if (groupIter == null) {
			groupedInput = new ArrayListValuedHashMap<>();
			boolean ok = child.getNextTuple(childOutputs);
			
			while (ok) {
				if (tableColumns == null && !childOutputs.isEmpty()) {
					tableColumns = new int[groupingFields.size()];
					MutableTupleWithSchema<String> child = childOutputs.iterator().next(); 
					for (int i = 0; i < tableColumns.length; i++)
						tableColumns[i] = child.getKeys().indexOf(groupingFields.get(i));
				}
				
				for (MutableTupleWithSchema<String> tup: childOutputs) {
					count++;
					groupedInput.put(getBlock(tup, tableColumns), tup);
					
					child.recycle(tup);
				}
				childOutputs.clear();
				ok = child.getNextTuple(childOutputs);
				
//				if (count++ % 100 == 0)
//					logger.debug("Groupby read {} tuples", count);
			}
			
			Set<List<Object>> keySet = groupedInput.keySet();
			
			groupIter = keySet.iterator();
		}
		
		logger.debug("Group-by {} read {} tuples", getId(), count);
		
		if (!groupIter.hasNext())
			return false;
		
		Collection<MutableTupleWithSchema<String>> group = groupedInput.get(groupIter.next());
		
		return groupTuples(group, outputs);
	}

	@Override
	public void rebind(TupleWithSchema<String> childTuple) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected String getArguments() {
		StringBuilder builder = new StringBuilder();
		
		for (int i = 0; i < groupingFields.size(); i++) {
			if (i > 0)
				builder.append(",");
			builder.append(groupingFields.get(i).toString());
		}
		
//		for (BasicSchema aggOutput: aggOutputs) {
//			builder.append("; ");
//			builder.append(aggOutput.toString());
//		}
		for (Function udf: this.aggFns) {
			builder.append("; ");
			builder.append(udf.toString());
		}
		
		return builder.toString();
	}

	@Override
	public String getLogicalDescriptor() {
		return getClass().getSimpleName() + ": " + getArguments();
	}

	@Override
	public BasicSchema getAdditionalSchema() {
		BasicSchema aggOutput = new BasicSchema("output");
		for (BasicSchema agg: aggOutputs)
			for (int i = 0; i < agg.getArity(); i++)
				aggOutput.addField(agg.getKeyAt(i), agg.getTypeAt(i));
		
		return aggOutput;
	}

	public List<String> getGroupingFields() {
		return groupingFields;
	}
	
	public List<Function<T,BasicTuple>> getAggFunctions() {
		return aggFns;
	}
	
	public List<BasicSchema> getAggOutput() {
		return aggOutputs;
	}
	
	public ProvenanceAggregator getProvenanceAggregator() {
		return provAgg;
	}

}

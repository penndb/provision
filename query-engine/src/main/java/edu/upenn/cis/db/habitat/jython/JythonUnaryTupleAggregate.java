/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.jython;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.python.core.PyCode;
import org.python.core.PyDictionary;
import org.python.core.PyList;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

import edu.upenn.cis.db.habitat.aggregation.AggregateWithLookup;
import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * Takes the fields in the tuple and sets them as Python variables.
 * Executes the python script.  Expects the returned output to be in the
 * variable result, which should evaluate to a Boolean.
 * 
 * @author zives
 *
 */
public class JythonUnaryTupleAggregate extends AggregateWithLookup<BasicTuple> {
	
	PyCode pythonScript = null;
	PythonInterpreter interp;
	BasicSchema outputSchema;
	String script;
	
	public JythonUnaryTupleAggregate(BasicSchema schema, String scriptName, String pythonScript) {
		String script = pythonScript.replace("\\n", "\n");
		String uniqueName = UUID.randomUUID().toString();
		interp = JythonGen.getPythonInterpreter(uniqueName);
		
		this.pythonScript = interp.compile(script);
		this.script = scriptName;
		
		outputSchema = schema;
	}

	public JythonUnaryTupleAggregate(BasicSchema schema, String scriptName, InputStream stream) {
//		this.pythonStream = stream;
		String uniqueName = UUID.randomUUID().toString();
		interp = JythonGen.getPythonInterpreter(uniqueName);
		this.script = scriptName;
		
		pythonScript = interp.compile(new InputStreamReader(stream));
		
		outputSchema = schema;
	}

	@Override
	public void bind(BasicTuple tuple) {
		super.bind(tuple);
		for (String key: tuple.getSchema().getKeys()) {
			interp.set(key, tuple.getValue(key));
		}
	}
	
	/**
	 * Take the position dictionary, and pull out (1) the field name and
	 * (2) the collection (list) of int indices.  Convert this into a ProvLocation.
	 * 
	 * @param loc Python dictionary
	 * @return
	 */
	ProvLocation getLocation(PyDictionary loc) {
		if (loc == null)
			return null;

		PyList list = (PyList) loc.get("position");

		if (list == null)
			return null;


		// Read the list of int items
		List<Integer> intList = new ArrayList<>();
		for (Object obj: list.asIterable())
			intList.add(((PyObject)obj).asInt());

		return new ProvLocation((String)loc.get("field"),
				intList);
	}
	
	/**
	 * Converts each result object (dictionary) from the Python code, into a provenance
	 * location and a tuple (which will ultimately have the provenance location embedded).
	 * 
	 * @param tup
	 * @param item
	 * @return
	 */
	boolean mapObject(BasicTuple tup, PyObject item) {
	     if (item.isMappingType()) {
	    	 PyDictionary dict = (PyDictionary)item;
	    	 
	    	 ProvLocation location = getLocation((PyDictionary)dict.get("location"));
	    	 if (location != null)
		    	 tup.setValue(ProvenanceApi.Provenance, location);

	    	 PyDictionary value = (PyDictionary)dict.get("value");
			 if (value != null) {
		    	 for (Object f: value.keySet()) {
		    		 String field = f.toString();
		    		 if (!outputSchema.getKeys().contains(field)) {
		    			 throw new RuntimeException("Cannot output " + field);
		    		 }
		    		 tup.setValue(field, value.get(field));
		    	 }
			 }
	     } else {
	    	 throw new RuntimeException("Unexpected Python result: scalar instead of dictionary");
	     }
	     return true;
	}

	@Override
	public BasicTuple apply(TableWithVariableSchema t) {
//		if (pythonScript != null)
		interp.set("table", t);
		
		interp.exec(pythonScript);
//		else
//			interp.execfile(pythonStream);
		
		PyObject result = interp.get("result");

		if (result == null)
			throw new RuntimeException("Python result not found");
		
		if (result.isSequenceType()) {
			for (PyObject item : result.asIterable()) {
				BasicTuple tup = outputSchema.createTuple();
				if (mapObject(tup, item)) {
					return tup;
				}
			 }
		} else {
			BasicTuple tup = outputSchema.createTuple();
			if (mapObject(tup, result)) {
				return tup;
			}
		 }
		
		return null;
	}

	@Override
	public String getName() {
		return script;
	}

}

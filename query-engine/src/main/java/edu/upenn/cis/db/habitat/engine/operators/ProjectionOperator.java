/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.BasicTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.Schema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class ProjectionOperator<T extends TableWithVariableSchema> extends SchemaModifyingOperator<T> {
	List<String> fields;
	Map<BasicSchema,UnaryOperator<MutableTupleWithSchema<String>>> tupleFn = new HashMap<>();
	
	Function<Schema,BasicTupleWithSchema<String>> newTuple = new Function<Schema,BasicTupleWithSchema<String>>() {

		@Override
		public BasicTupleWithSchema<String> apply(Schema schema) {
			return (!recycled.isEmpty()) ? (BasicTupleWithSchema<String>)recycled.remove(0) : schema.createTuple();
		}
		
	};

	public ProjectionOperator(QueryOperator<T> child, List<String> fields, ProvenanceWrapperApi provApi) {
		super(child, provApi);
		
		this.fields = fields;
		if (!fields.contains("token"))
			fields.add("token");
		if (!fields.contains("prov"))
			fields.add("prov");
	}
	
	@Override
	public void createMappings(Set<BasicSchema> inputs) {
		inputs.parallelStream().forEach(schema -> 
		{
			registerMappings(schema,
					// Schema map
					s -> s.project(schema.getName() + "_" + getId(), fields),
					// Tuple map
					t -> t.project((Schema)super.schemaMap.get(schema), newTuple));
		});
	}

	public void registerMappings(BasicSchema inputSchema,
			UnaryOperator<BasicSchema> schemaFn,
			UnaryOperator<MutableTupleWithSchema<String>> tupleFn) {
		this.schemaFn.put(inputSchema, schemaFn);
		this.tupleFn.put(inputSchema, tupleFn);
	}
	
	@Override
	public boolean processTuple(MutableTupleWithSchema<String> tuple, Collection<MutableTupleWithSchema<String>> outputs) 
	throws HabitatServiceException {
		MutableTupleWithSchema<String> output = tupleFn.get(tuple.getSchema()).apply(tuple);
		
		if (output != null) {
			outputs.add(output);
			return true;
		} else
			return false;
	}

	@Override
	public void rebind(TupleWithSchema<String> childTuple) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected String getArguments() {
		StringBuilder builder = new StringBuilder();
		
		for (int i = 0; i < fields.size(); i++) {
			if (i > 0)
				builder.append(",");
			builder.append(fields.get(i).toString());
		}

		return builder.toString();
	}

	@Override
	public String getLogicalDescriptor() {
		return  "pi: " + getArguments();
	}
}

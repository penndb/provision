/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators.extract;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.FileExtractOperator;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecord.SAMTagAndValue;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;

public class BamExtractor<T extends TableWithVariableSchema> extends FileExtractOperator<T> {
	String filename;
	SamReader sam;
	SAMRecordIterator iter;
	final SamReaderFactory factory;
	SAMRecord first;
	File file;
	int lineNo = 0;
	final BasicSchema project;
	
	public BamExtractor(String filename, SamReaderFactory bamFactory, ProvenanceWrapperApi provApi) throws FileNotFoundException {
		this(new File(filename), bamFactory, provApi);
	}
	
	public BamExtractor(File file, SamReaderFactory bamFactory, ProvenanceWrapperApi provApi) throws FileNotFoundException {
		super(provApi);

		this.filename = file.getName();

		project = new BasicSchema(file.getName());
		project.addField(ProvenanceApi.Token, ProvToken.class);
		project.addField(ProvenanceApi.Provenance, ProvLocation.class);
		project.addField("lineNumber", Integer.class);
		project.addField("name", String.class);
		project.addField("baseString", String.class);
		project.addField("contigIndex", Integer.class);
		project.addField("length", Integer.class);
		
		project.addLookupKey("lineNumber");
		
		projectThese.add(project);

		this.factory = bamFactory;
		this.file = file;
	}

	@Override
	public boolean initialize() throws HabitatServiceException {
		if (!super.initialize())
			return false;
		
		sam = factory.open(file);
		
		if (sam != null)
			iter = sam.iterator();
		
		first = iter.next();
		
		project.addField("alignmentStart", Integer.class);
		project.addField("alignmentEnd", Integer.class);
		project.addField("baseQuality", String.class);
		project.addField("cigar", String.class);
		project.addField("cigarLength", Integer.class);
		project.addField("duplicateRead", Boolean.class);
//		project.addField("fileSource", String.class);
		project.addField("firstOfPair", Boolean.class);
		project.addField("inferredInsertSize", Integer.class);
		project.addField("mappingQuality", Integer.class);
		project.addField("mateAlignmentStart", Integer.class);
		project.addField("mateNegativeStrand", Boolean.class);
		project.addField("mateReferenceIndex", Integer.class);
		project.addField("mateReferenceName", String.class);
		project.addField("mateUnmapped", Boolean.class);
		project.addField("notPrimaryAlignment", Boolean.class);
		project.addField("pairedReadName", String.class);
		project.addField("properPair", Boolean.class);
		project.addField("read", String.class);
		project.addField("readFailsVendorQualityCheck", Boolean.class);
		project.addField("readGroup", Integer.class);
		project.addField("readLength", Integer.class);
		project.addField("readName", String.class);
		project.addField("readNegativeStrand", Boolean.class);
		project.addField("readPaired", Boolean.class);
		project.addField("readUnmapped", Boolean.class);
		project.addField("referenceIndex", Integer.class);
		project.addField("referenceName", String.class);
//		project.addField("samString", String.class);
		project.addField("secondPairOf", Boolean.class);
		project.addField("supplementaryAlignment", Boolean.class);
		project.addField("unclippedStart", Integer.class);
		project.addField("unclippedEnd", Integer.class);
		project.addField("validationStringency", String.class);

		for (SAMTagAndValue kv: first.getAttributes()) {
			project.addField(kv.tag, kv.value.getClass());
			System.out.println(kv.tag + ":" + kv.value.getClass().getName());
		}
		
		provApi.writeInputFile(filename);
		return sam != null && iter != null;
	}
	
	@Override
	public boolean close() throws HabitatServiceException {
		try {
			sam.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return super.close();
	}
	

	@Override
	public boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		SAMRecord rec;
		if (first != null) {
			rec = first;
			first = null;
		} else {
			if (!iter.hasNext())
				return false;
			
			rec = iter.next();
		}
		
		MutableTupleWithSchema<String> tup = (!recycled.isEmpty()) ? recycled.remove(0) : getSchema().createTuple();
		ProvToken us = PlanGen.getToken(this);
		ProvLocation here = getInputPosition();
		
//		provApi.storeProvenanceNode("", us, here);
		
		tup.setValue(ProvenanceApi.Token, us);
		tup.setValue(ProvenanceApi.Provenance, here);
		tup.setValue("lineNumber", lineNo);

		for (SAMTagAndValue kv: rec.getAttributes()) {
			tup.setValue(kv.tag, kv.value);
		}

		tup.setValue("alignmentStart", rec.getAlignmentStart());
		tup.setValue("alignmentEnd", rec.getAlignmentEnd());
		tup.setValue("baseQuality", rec.getBaseQualityString());
		tup.setValue("cigar", rec.getCigarString());
		tup.setValue("cigarLength", rec.getCigarLength());
		tup.setValue("duplicateRead", rec.getDuplicateReadFlag());
//		tup.setValue("fileSource", rec.getFileSource().);
		tup.setValue("firstOfPair", rec.getFirstOfPairFlag());
		tup.setValue("inferredInsertSize", rec.getInferredInsertSize());
		tup.setValue("mappingQuality", rec.getMappingQuality());
		tup.setValue("mateAlignmentStart", rec.getMateAlignmentStart());
		tup.setValue("mateNegativeStrand", rec.getMateNegativeStrandFlag());
		tup.setValue("mateReferenceIndex", rec.getMateReferenceIndex());
		tup.setValue("mateReferenceName", rec.getMateReferenceName());
		tup.setValue("mateUnmapped", rec.getMateUnmappedFlag());
		tup.setValue("notPrimaryAlignment", rec.getNotPrimaryAlignmentFlag());
		tup.setValue("pairedReadName", rec.getPairedReadName());
		tup.setValue("properPair", rec.getProperPairFlag());
		tup.setValue("read", rec.getReadString().toString());
		tup.setValue("readFailsVendorQualityCheck", rec.getReadFailsVendorQualityCheckFlag());
		tup.setValue("readGroup", rec.getReadGroup() == null ? null : rec.getReadGroup().getId());
		tup.setValue("readLength", rec.getReadLength());
		tup.setValue("readName", rec.getReadName());
		tup.setValue("readNegativeStrand", rec.getReadNegativeStrandFlag());
		tup.setValue("readPaired", rec.getReadPairedFlag());
		tup.setValue("readUnmapped", rec.getReadUnmappedFlag());
		tup.setValue("referenceIndex", rec.getReferenceIndex());
		tup.setValue("referenceName", rec.getReferenceName());
//		tup.setValue("samString", rec.getSAMString());
		tup.setValue("secondPairOf", rec.getSecondOfPairFlag());
		tup.setValue("supplementaryAlignment", rec.getSupplementaryAlignmentFlag());
		tup.setValue("unclippedStart", rec.getUnclippedStart());
		tup.setValue("unclippedEnd", rec.getUnclippedEnd());
		tup.setValue("validationStringency", rec.getValidationStringency().name());
		
		provApi.writeInputRecord(us, here, filename, getId().toString(), tup);
		outputs.add(tup);
						
		lineNo++;
		return false;
	}

	/**
	 * Return our 2nd schema
	 * @return
	 */
	public BasicSchema getSchema() {
		projectThese.iterator().next();
		return projectThese.iterator().next();
	}

	@Override
	public ProvLocation getInputPosition() {
		Set<Integer> range = new HashSet<>();
		range.add(lineNo);
		return new ProvLocation(getId(), getSchema().getName(), 
				range);
	}

	@Override
	protected String getArguments() {
		return filename;
	}
	
	@Override
	public String getLogicalDescriptor() {
		return getClass().getSimpleName() + ": " + getArguments();
	}
}

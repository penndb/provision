/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators.extract;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.biojava.nbio.genome.parsers.gff.FeatureI;
import org.biojava.nbio.genome.parsers.gff.FeatureList;
import org.biojava.nbio.genome.parsers.gff.GFF3Reader;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.FileExtractOperator;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class GtfExtractor<T extends TableWithVariableSchema> extends FileExtractOperator<T> {
	String filename;

	int lineNo = 0;
	FeatureList records;
	final BasicSchema project;
	
	public GtfExtractor(String fil, ProvenanceWrapperApi provApi) throws FileNotFoundException {
		super(provApi);

		project = new BasicSchema(fil);
		project.addField(ProvenanceApi.Token, ProvToken.class);
		project.addField(ProvenanceApi.Provenance, ProvLocation.class);
		project.addField("lineNumber", Integer.class);
		project.addField("seqname", String.class);
		project.addField("locationBioStart", Integer.class);
		project.addField("locationBioEnd", Integer.class);
		project.addField("locationBioStrand", String.class);
		project.addField("group", String.class);
		project.addField("type", String.class);
		
		project.addLookupKey("lineNumber");
		
		projectThese.add(project);

		filename = fil;
	}

	@Override
	public boolean initialize() throws HabitatServiceException {
		if (!super.initialize())
			return false;
		
		try {
			records = GFF3Reader.read(filename);
		} catch (IOException e) {
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}

		if (records == null)
			return false;
		
		FeatureI firstSeq = records.get(lineNo);
		
		for (String key: firstSeq.getAttributes().keySet())
			project.addField(key, String.class);
		
		provApi.writeInputFile(filename);
		return true;
	}
	
	@Override
	public boolean close() throws HabitatServiceException {
		return super.close();
	}

	/**
	 * We only have a single tuple schema
	 * 
	 * @return
	 */
	public BasicSchema getSchema() {
		return projectThese.iterator().next();
	}

	@Override
	public boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		if (lineNo >= records.size())
			return false;
		
		FeatureI seq = records.get(lineNo);

		MutableTupleWithSchema<String> tup = (!recycled.isEmpty()) ? recycled.remove(0) : getSchema().createTuple();
		ProvToken us = PlanGen.getToken(this);
		ProvLocation here = getInputPosition();
		
//		provApi.storeProvenanceNode("", us, here);
		
		tup.setValue(ProvenanceApi.Token, us);
		tup.setValue(ProvenanceApi.Provenance, here);
		tup.setValue("lineNumber", lineNo);

		tup.setValue("seqname", seq.seqname());
		tup.setValue("locationBioStart", seq.location().bioStart());
		tup.setValue("locationBioEnd", seq.location().bioEnd());
		tup.setValue("locationBioStrand", String.valueOf(seq.location().bioStrand()));
		tup.setValue("group", seq.group());
		tup.setValue("type", seq.type());

		for (String key: seq.getAttributes().keySet())
			tup.setValue(key, seq.getAttribute(key));

		provApi.writeInputRecord(us, here, filename, getId().toString(), tup);

		outputs.add(tup);
		lineNo++;
		
		return true;
	}

	@Override
	public ProvLocation getInputPosition() {
		Set<Integer> range = new HashSet<>();
		range.add(lineNo);
		return new ProvLocation(getId(), getSchema().getName(), 
				range);
	}

	@Override
	protected String getArguments() {
		return filename;
	}

	@Override
	public String getLogicalDescriptor() {
		return getClass().getSimpleName() + ": " + getArguments();
	}
}

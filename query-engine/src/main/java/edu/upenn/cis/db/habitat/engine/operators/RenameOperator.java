/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.UnaryOperator;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class RenameOperator<T extends TableWithVariableSchema> extends SchemaModifyingOperator<T> {
	Map<String,String> fieldMap;
	Map<BasicSchema,UnaryOperator<BasicTuple>> tupleFn = new HashMap<>();
	Map<BasicSchema,BasicSchema> reMap = new HashMap<>();
	BasicSchema newItems = new BasicSchema("Proj" + getId());
	
	public RenameOperator(QueryOperator<T> child, Map<String,String> fieldMap, ProvenanceWrapperApi provApi) {
		super(child, provApi);
		
		this.fieldMap = fieldMap;
		if (!fieldMap.containsKey(ProvenanceApi.Token))
			fieldMap.put(ProvenanceApi.Token, ProvenanceApi.Token);
		if (!fieldMap.containsKey(ProvenanceApi.Provenance))
			fieldMap.put(ProvenanceApi.Provenance,ProvenanceApi.Provenance);
	}
	
	@Override
	public void createMappings(Set<BasicSchema> inputs) {
		BasicSchema any = inputs.iterator().next();
		
		newItems = any.project(any.getName() + "_" + getId(), 
			new ArrayList<>(fieldMap.keySet())).rename(fieldMap); 

		inputs.parallelStream().forEach(schema -> 
		{
			registerMappings(schema, 
					// Schema map
					s -> { 
						//BasicSchema ret = s.project(schema.getName() + "_" + getId(), 
						//	new ArrayList<>(fieldMap.keySet()));
						
						BasicSchema ret = schema.rename(fieldMap); 
						reMap.put(schema,//ret, 
								ret);
						
						return ret;
					},
					// Tuple map
					t -> t.project(super.schemaMap.get(schema)));
		});
	}

	public void registerMappings(BasicSchema inputSchema,
			UnaryOperator<BasicSchema> schemaFn,
			UnaryOperator<BasicTuple> tupleFn) {
		this.schemaFn.put(inputSchema, schemaFn);
		this.tupleFn.put(inputSchema, tupleFn);
	}
	
	int [] columnMap = null;
	
	@Override
	public boolean processTuple(MutableTupleWithSchema<String> tuple, Collection<MutableTupleWithSchema<String>> outputs) 
	throws HabitatServiceException {
//		BasicTuple output = tupleFn.get(tuple.getSchema()).apply(tuple);
		
		if (tuple != null) {
			BasicSchema schema = reMap.get(tuple.getSchema());
			MutableTupleWithSchema<String> remapped = (!recycled.isEmpty()) ? recycled.remove(0) : schema.createTuple();
			remapped.setSchema(schema);
			
			if (columnMap == null) {
				columnMap = new int[tuple.getKeys().size()];
				for (int i = 0; i < tuple.getKeys().size(); i++) {
					String k = tuple.getKeys().get(i);
					String newKey = fieldMap.get(k);
					if (newKey == null)
						newKey = k;
					columnMap[i] = remapped.getKeys().indexOf(newKey);
				}
			}
			
//			for (String k: tuple.getKeys()) {
			for (int i = 0; i < columnMap.length; i++) {
//				String newKey = fieldMap.get(k);
//				if (newKey == null)
//					newKey = k;
				//remapped.setValue(newKey, tuple.getValue(k));
				remapped.setValueAt(columnMap[i], tuple.getValueAt(i));
			}
			outputs.add(remapped);
//			outputs.add(output);
			return true;
		} else
			return false;
	}

	@Override
	public void rebind(TupleWithSchema<String> childTuple) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected String getArguments() {
		return fieldMap.toString();
	}

	@Override
	public String getLogicalDescriptor() {
		return "rho: " + getArguments();
	}

	@Override
	public BasicSchema getAdditionalSchema() {
		return newItems;
	}
	
	public Map<String,String> getFieldMap() {
		return fieldMap;
	}
}

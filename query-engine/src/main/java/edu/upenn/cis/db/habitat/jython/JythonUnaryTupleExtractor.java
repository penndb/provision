/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.jython;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.python.core.PyCode;
import org.python.core.PyDictionary;
import org.python.core.PyList;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTupleExtractorWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

/**
 * Takes the fields in the tuple and sets them as Python variables.
 * Executes the python script.  Expects the returned output to be in the
 * variable result, which should evaluate to a Boolean.
 * 
 * @author zives
 *
 */
public class JythonUnaryTupleExtractor<S> extends UnaryTupleExtractorWithLookup<TupleWithSchema<String>,S> {
	
	PyCode pythonScript = null;
//	InputStream pythonStream = null;
	PythonInterpreter interp;
	BasicSchema outputSchema;
	String script;
	
	BasicSchema parameters;
	List<ArithmeticExpressionOperation<?>> parameterExpressions;
	
	public JythonUnaryTupleExtractor(BasicSchema outputSchema, String scriptName, String pythonScript, 
			BasicSchema parameters, List<ArithmeticExpressionOperation<?>> expressions) {
		String script = pythonScript.replace("\\n", "\n");
		String uniqueName = UUID.randomUUID().toString();
		interp = JythonGen.getPythonInterpreter(uniqueName);
		
		this.pythonScript = interp.compile(script);
		this.script = scriptName;
		
		this.outputSchema = outputSchema;
		this.parameters = parameters;
		this.parameterExpressions = expressions;
	}

	public JythonUnaryTupleExtractor(BasicSchema outputSchema, String scriptName, InputStream stream,
			BasicSchema parameters, List<ArithmeticExpressionOperation<?>> expressions) {
//		this.pythonStream = stream;
		String uniqueName = UUID.randomUUID().toString();
		interp = JythonGen.getPythonInterpreter(uniqueName);
		this.script = scriptName;
		
		pythonScript = interp.compile(new InputStreamReader(stream));
		
		this.outputSchema = outputSchema;
		this.parameters = parameters;
		this.parameterExpressions = expressions;
	}

	@Override
	public void bind(TupleWithSchema<String> tuple) {
		super.bind(tuple);
//		for (String key: tuple.getSchema().getKeys()) {
		for (int i = 0; i < parameters.getKeys().size(); i++) {
			String key = parameters.getKeyAt(i);
			ArithmeticExpressionOperation<?> op = parameterExpressions.get(i);
			op.bind(tuple);
			interp.set(key, op.getValue());//tuple.getValue(key));
		}
	}
	
	/**
	 * Take the position dictionary, and pull out (1) the field name and
	 * (2) the collection (list) of int indices.  Convert this into a ProvLocation.
	 * 
	 * @param loc Python dictionary
	 * @return
	 */
	ProvLocation getLocation(PyDictionary loc) {
		if (loc == null)
			return null;

		PyList list = (PyList) loc.get("position");

		if (list == null)
			return null;


		// Read the list of int items
		List<Integer> intList = new ArrayList<>();
		for (Object obj: list.asIterable())
			intList.add(((PyObject)obj).asInt());

		return new ProvLocation((String)loc.get("field"),
				intList);
	}
	
	/**
	 * Converts each result object (dictionary) from the Python code, into a provenance
	 * location and a tuple (which will ultimately have the provenance location embedded).
	 * 
	 * @param tup
	 * @param item
	 * @return
	 */
	boolean mapObject(BasicTuple tup, PyObject item) {
	     if (item.isMappingType()) {
	    	 PyDictionary dict = (PyDictionary)item;
	    	 
	    	 ProvLocation location = getLocation((PyDictionary)dict.get("location"));
	    	 if (location != null)
		    	 tup.setValue(ProvenanceApi.Provenance, location);

	    	 PyDictionary value = (PyDictionary)dict.get("value");
			 if (value != null) {
		    	 for (Object f: value.keySet()) {
		    		 String field = f.toString();
		    		 if (!outputSchema.getKeys().contains(field)) {
		    			 for (String sch: outputSchema.getKeys()) {
		    				 if (sch.endsWith("/" + field)) {
		    		    		 tup.setValue(sch, value.get(field));
		    		    		 return true;
		    				 }
		    			 }
		    			 throw new RuntimeException("Cannot output " + field);
		    		 }
		    		 tup.setValue(field, value.get(field));
		    	 }
			 }
//	    	 for (Object k: dict.keySet()) {
//	    		 String key = k.toString();
//	    		 
//	    		 if (key.equals("location")) {
//			    	 ProvLocation location = getLocation((PyDictionary)dict.get("location"));
//			    	 
//			    	 tup.setValue(SimpleProvAPI.Provenance, location);
//			    	 
//	    		 } else if (key.equals("value")) {
//			    	 PyDictionary loc = (PyDictionary)dict.get("value");
//	    			 
//			    	 for (Object f: loc.keySet()) {
//			    		 String field = f.toString();
//			    		 if (!outputSchema.getKeys().contains(field)) {
//			    			 throw new RuntimeException("Cannot output " + field);
//			    		 }
//			    		 tup.setValue(field, loc.get(field));
//			    	 }
//	    		 }
//	    		 
//	    	 }
	     } else {
	    	 throw new RuntimeException("Unexpected Python result: scalar instead of dictionary");
	     }
	     return true;
	}

	@Override
	public List<TupleWithSchema<String>> apply(TupleWithSchema<String> t) {
		List<TupleWithSchema<String>> ret = new ArrayList<>();

//		if (pythonScript != null)
			interp.exec(pythonScript);
//		else
//			interp.execfile(pythonStream);
		
		PyObject result = interp.get("result");

		if (result == null)
			throw new RuntimeException("Python result not found");
		
		if (result.isSequenceType()) {
			for (PyObject item : result.asIterable()) {
				BasicTuple tup = outputSchema.createTuple();
				if (mapObject(tup, item)) {
					ret.add(tup);
				}
			 }
		} else {
			BasicTuple tup = outputSchema.createTuple();
			if (mapObject(tup, result)) {
				ret.add(tup);
			}
		 }
		
		return ret;
	}

	@Override
	public String getName() {
		return script;
	}

}

/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators.materialize;

import java.io.File;
import java.util.Map;

import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.MaterializeOperator;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import htsjdk.samtools.fastq.AsyncFastqWriter;
import htsjdk.samtools.fastq.BasicFastqWriter;
import htsjdk.samtools.fastq.FastqRecord;
import htsjdk.samtools.fastq.FastqWriter;

public class FastQMaterializeOperator<T extends TableWithVariableSchema> extends MaterializeOperator<T> {
	
	FastqWriter writer;
	
	String filename;

	public FastQMaterializeOperator(QueryOperator<T> child,
			String filename,
			Map<String, String> fields, 
			ProvenanceWrapperApi provApi) {
		super(child, fields, provApi);
		
		this.filename = filename;
		
		for (String str: new String[] {
				"baseQualityHeader", "baseQualityString", "readHeader", "readString"}) {
			if (!fields.containsValue(str))
				throw new IllegalArgumentException("Must have base quality header + string and read header + string.  Missing " + str + " from " + fields.keySet());
		}
		
		writer = new AsyncFastqWriter(new BasicFastqWriter(new File(filename)), 100);
	}

	@Override
	public void recordWriter(BasicTuple tup) {
		FastqRecord rec = new FastqRecord(
				(String)tup.getValue("readHeader"),
				(String)tup.getValue("readString"),
				(String)tup.getValue("baseQualityHeader"),
				(String)tup.getValue("baseQualityString")
				);
		
		writer.write(rec);
	}

	@Override
	public boolean close() throws HabitatServiceException {
		writer.close();
		
		return super.close();
	}

	@Override
	protected String getArguments() {
		return filename;
	}

	@Override
	public String getLogicalDescriptor() {
		return getClass().getSimpleName() + ": " + getArguments();
	}
}

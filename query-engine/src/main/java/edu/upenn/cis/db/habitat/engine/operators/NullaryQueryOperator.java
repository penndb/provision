/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ID;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.type.InMemoryVariableSchemaTable;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public abstract class NullaryQueryOperator<T extends TableWithVariableSchema> 
	implements QueryOperator<T>, Supplier<T> {
	final ID id;
	
	protected final ProvenanceWrapperApi provApi;
	
	protected Path basePath;
	
	protected List<MutableTupleWithSchema<String>> recycled = new ArrayList<>();
	List<QueryOperator<T>> list = new ArrayList<>();
	
	public NullaryQueryOperator(ProvenanceWrapperApi provApi) {
		id = PlanGen.getID(this);
		this.provApi = provApi;
	}
	
	@Override
	public ProvenanceWrapperApi getProvApi() {
		return provApi;
	}
	
	@Override
	public ID getId() {
		return id;
	}

	@Override
	public boolean initialize() throws HabitatServiceException {
		return true;
	}

	@Override
	public boolean close() throws HabitatServiceException {
		return true;
	}
	
	@Override
	public T get() {
		@SuppressWarnings("unchecked")
		T ret = (T) new InMemoryVariableSchemaTable(getOutputSchemas());
		
		try {
			ret = execute();
		} catch (HabitatServiceException e) {
			
		}
		return ret;
	}

	@Override
	public T execute() throws HabitatServiceException {
		@SuppressWarnings("unchecked")
		T ret = (T) new InMemoryVariableSchemaTable(getOutputSchemas());
		
		initialize();
		while (getNextTuple(ret))
			;
		
		close();
		return ret;
	}

	public void recycle(Collection<MutableTupleWithSchema<String>> prevOutputs) {
		if (recycled.isEmpty()) {
			recycled.addAll(prevOutputs);
			prevOutputs.clear();
		}
	}

	public void recycle(MutableTupleWithSchema<String> prevOutput) {
		recycled.add(prevOutput);
	}

	public List<QueryOperator<T>> getChildren() {
		return Collections.unmodifiableList(list);
	}

	public Stream<QueryOperator<T>> flatten() {
		return Stream.of(this);
	}
	
	public Stream<QueryOperator<T>> flattenTo(Collection<QueryOperator<T>> stopAt) {
		return Stream.of(this);
	}

	protected abstract String getArguments();
	
	@Override
	public String toString() {
		return toString(0);

	}
	
	public String toString(int indent) {
		StringBuffer spaces = new StringBuffer();
		for (int i = 0; i < indent; i++)
			spaces.append(' ');
		
		return spaces.toString() + getClass().getSimpleName() + "[" + getId() + "," + getArguments() + "]";
	}
	
	public void setBasePath(Path path) {
		this.basePath = path;
	}

	@Override
	public BasicSchema getAdditionalSchema() {
		return getOutputSchemas().iterator().next();
	}
}

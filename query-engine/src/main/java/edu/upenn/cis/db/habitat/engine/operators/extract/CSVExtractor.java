/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators.extract;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.FileExtractOperator;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class CSVExtractor<T extends TableWithVariableSchema> extends FileExtractOperator<T> {
	Reader in;
	Iterator<CSVRecord> iter;
	ArrayList<String> header;
	int lineNo = 0;
	BasicSchema project;
	CSVFormat csvFileFormat;
	BasicSchema inputSchema;
//	CSVReader csvReader;
	
	String filename;

	public CSVExtractor(String filename, String schemaName, ProvenanceWrapperApi provApi) throws FileNotFoundException {
		super(provApi);
		this.filename = filename;

		project = new BasicSchema(schemaName);//filename);
	}
	
	public CSVExtractor(File fil, ProvenanceWrapperApi provApi) throws FileNotFoundException {
		super(provApi);
		
		this.filename = fil.getPath();
		
		project = new BasicSchema(fil.getName());
	}
	
	public CSVExtractor(String filename, ProvenanceWrapperApi provApi, BasicSchema inputSchema) throws FileNotFoundException {
		super(provApi);
		this.filename = filename;

		project = new BasicSchema(inputSchema.getName());
		this.inputSchema = inputSchema;
	}
	
	public CSVExtractor(File fil, ProvenanceWrapperApi provApi, BasicSchema inputSchema) throws FileNotFoundException {
		super(provApi);
		
		this.filename = fil.getName();
		
		project = new BasicSchema(fil.getName());
		this.inputSchema = inputSchema;
	}
	
	Character delimiter = ',';
	Character quoteCharacter;
	Character escape = '\\';
	Boolean hasHeader;
	
	/**
	 * Configure the CSV parser with custom delimiters
	 * 
	 * @param delimiter
	 * @param quoteCharacter
	 * @param escape
	 * @param hasHeader
	 */
	public void configure(
			Character delimiter, 
			Character quoteCharacter, 
			Character escape, 
			Boolean hasHeader) {
		if (delimiter != null)
			this.delimiter = delimiter;
		if (quoteCharacter != null)
			this.quoteCharacter = quoteCharacter;
		
		if (escape != null)
			this.escape = escape;
		
		if (hasHeader != null)
			this.hasHeader = hasHeader;
	}

	public boolean initialize() throws HabitatServiceException {
		try {
			in = new BufferedReader(new FileReader(filename));
			
			CSVFormat format = CSVFormat.newFormat(delimiter)
					.withQuote(quoteCharacter != null ? quoteCharacter : '"')
					.withEscape(escape);
			
			if (hasHeader)
					format = format.withFirstRecordAsHeader();
			
			CSVParser csvParser =
					format.parse(in);
			
			provApi.writeInputFile(filename);
			
			// Clear the project
			project = new BasicSchema(project.getName());
			project.addField(ProvenanceApi.Token, ProvToken.class);
			project.addField(ProvenanceApi.Provenance, ProvLocation.class);
			project.addField("lineNumber", Integer.class);
			
			Map<String,Integer> headerMap = csvParser.getHeaderMap();
			if (headerMap != null) {
				header = new ArrayList<String>();
				for (String field : headerMap.keySet()) {
					header.add(field);
					project.addField(field, String.class);
				}
			} else {
				for (int i = 0; i < inputSchema.getKeys().size(); i++) {
					project.addField(inputSchema.getKeyAt(i), inputSchema.getTypeAt(i));
				}
			}
			
			Iterable<CSVRecord> records = csvParser;
			iter = records.iterator();
			
			if (!iter.hasNext())
				return false;
			
			this.projectThese.add(project);
		} catch (IOException except) {
			throw new HabitatServiceException(except.getMessage());
		}
		return true;
	}
	
	@Override
	public boolean close() throws HabitatServiceException {
		try {
//			csvReader.close();
			in.close();
		} catch (IOException e) {
			throw new HabitatServiceException(e.getMessage());
		}
		
		return super.close();
	}
	
	int[] columnMap = null;


	@Override
	public boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		if (!iter.hasNext())
			return false;
		
		CSVRecord rec = iter.next();

		MutableTupleWithSchema<String> tup = (!recycled.isEmpty()) ? recycled.remove(0) : project.createTuple();
		ProvToken us = PlanGen.getToken(this);
		ProvLocation here = getInputPosition();
		
//		provApi.storeProvenanceNode("", us, here);
		
		tup.setValue(ProvenanceApi.Token, us);
		tup.setValue(ProvenanceApi.Provenance, here);
		tup.setValue("lineNumber", lineNo);
		
		if (header != null) {
			if (columnMap == null) {
				columnMap = new int[header.size()];
				for (int i = 0; i < header.size(); i++)
					columnMap[i] = tup.getKeys().indexOf(header.get(i));
			}
			for (int i = 0; i < columnMap.length; i++)
				tup.setValueAt(columnMap[i], rec.get(i));
		} else {
			if (columnMap == null) {
				columnMap = new int[inputSchema.getArity()];
				for (int i = 0; i < inputSchema.getArity(); i++)
					columnMap[i] = tup.getKeys().indexOf(inputSchema.getKeyAt(i));
			}

			for (int i = 0; i < columnMap.length; i++)
				if (inputSchema.getTypeAt(i) == String.class)
					tup.setValueAt(columnMap[i], rec.get(i));
				else if (inputSchema.getTypeAt(i) == Integer.class)
					tup.setValueAt(columnMap[i], Integer.valueOf(rec.get(i)));
				else if (inputSchema.getTypeAt(i) == Double.class)
					tup.setValueAt(columnMap[i], Double.valueOf(rec.get(i)));
				else if (inputSchema.getTypeAt(i) == Boolean.class)
					tup.setValueAt(columnMap[i], Boolean.valueOf(rec.get(i)));
				else if (inputSchema.getTypeAt(i) == Date.class)
					tup.setValueAt(columnMap[i], Date.valueOf(rec.get(i)));
				else
					throw new UnsupportedOperationException("Don't know how to cast to " + inputSchema.getTypeAt(i).getName());
		}
		
		provApi.writeInputRecord(us, here, filename, getId().toString(), tup);
		
		outputs.add(tup);
		lineNo++;
		return true;
	}

	@Override
	public ProvLocation getInputPosition() {
		Set<Integer> range = new HashSet<>();
		range.add(lineNo);
		return new ProvLocation(getId(), project.getName(), 
				range);
	}

	@Override
	protected String getArguments() {
		return filename;
	}

	@Override
	public String getLogicalDescriptor() {
		return getClass().getSimpleName() + ": " + getArguments();
	}
}

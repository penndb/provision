/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.UnaryOperator;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.repository.type.InMemoryVariableSchemaTable;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * A unary operator that changes its input schema(s).  Examples include projection,
 * augmentation, map, ... or even BLOCK or GROUP BY.  Note that Join and Cartesian
 * Product are counted differently since they are not unary.
 * 
 * @author zives
 *
 * @param <T>
 */
public abstract class SchemaModifyingOperator<T extends TableWithVariableSchema> 
extends UnaryQueryOperator<T> {
	Map<BasicSchema,BasicSchema> schemaMap = new HashMap<>();
	Map<BasicSchema,UnaryOperator<BasicSchema>> schemaFn = new HashMap<>();
	Set<BasicSchema> outputSchemas = new HashSet<>();

	public SchemaModifyingOperator(QueryOperator<T> child, ProvenanceWrapperApi provApi) {
		super(child, provApi);
	}
	
	@Override
	public boolean initialize() throws HabitatServiceException {
		if (!super.initialize())
			return false;

		Set<BasicSchema> inputs = child.getOutputSchemas();
		createMappings(inputs);
		
		// Apply projection mappings
		inputs.parallelStream().forEach(schema -> 
			{
				schemaMap.put(schema, schemaFn.get(schema).apply(schema)); 
			});
		outputSchemas.addAll(schemaMap.values());
		
		return true;
	}
	
	public abstract void createMappings(Set<BasicSchema> inputs);

	@Override
	public Set<BasicSchema> getOutputSchemas() {
		return outputSchemas;
	}
	
	@Override
	protected boolean needToRebind(TupleWithSchema<String> input) {
		return false;
	}
	
	T childOutputs = null;
	
	@Override
	public synchronized boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		//T childOutputs = (T) new InMemoryVariableSchemaTable(getOutputSchemas());
		if (childOutputs == null)
			childOutputs = (T) new InMemoryVariableSchemaTable(getOutputSchemas());
		else
			childOutputs.clear();

		boolean ok = child.getNextTuple(childOutputs);
		if (ok) {
			for (MutableTupleWithSchema<String> tup: childOutputs) {
				if (needToRebind(tup))
					rebind(tup);
				processTuple(tup, outputs);
				child.recycle(tup);
			}
		}
		return ok;
	}

}

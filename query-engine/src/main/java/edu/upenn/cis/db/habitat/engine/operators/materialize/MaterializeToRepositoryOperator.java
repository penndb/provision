/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators.materialize;

import java.util.Collection;
import java.util.Map;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.PassThroughOperator;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.repository.type.InMemoryVariableSchemaTable;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public abstract class MaterializeToRepositoryOperator<T extends TableWithVariableSchema> extends PassThroughOperator<T> {
	
	BasicSchema outputSchema;
	BasicTuple writeTuple;
	final Map<String,String> fields;
	final StorageApi<String,Object,?> storageSystem;

	public MaterializeToRepositoryOperator(QueryOperator<T> child, 
			Map<String,String> fields, StorageApi<String,Object,?> storage, 
			ProvenanceWrapperApi provApi) {
		super(child, provApi);
		this.fields = fields;
		this.storageSystem = storage;
	}
	
	public void recordWriter(BasicTuple tup) throws HabitatServiceException {
		storageSystem.addMetadata(tup.getSchema().getName(), 
				tup.getValue(ProvenanceApi.Token).toString(), tup);
	}
	
	public boolean initialize() throws HabitatServiceException {
		if (!super.initialize())
			return false;
		
		// Schema to write is a projection / rename of the child's schema
		BasicSchema childSchema = child.getOutputSchemas().iterator().next();
		outputSchema = new BasicSchema(childSchema.getName());
		for (String key: childSchema.getKeys())
			if (fields.containsKey(key))
				outputSchema.addField(fields.get(key), childSchema.getTypeAt(childSchema.indexOf(key)));
		
		writeTuple = outputSchema.createTuple();
		return true;
	}
	
	@Override
	public boolean processTuple(MutableTupleWithSchema<String> tuple, Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		boolean ret = super.processTuple(tuple, outputs);
		
		for (String key: fields.keySet()) {
			writeTuple.setValue(fields.get(key), tuple.getValue(key));
		}
		recordWriter(writeTuple);

		return ret;
	}

	@Override
	public boolean getNextTuple(Collection<MutableTupleWithSchema<String>> outputs) throws HabitatServiceException {
		@SuppressWarnings("unchecked")
		T childOutputs = (T) new InMemoryVariableSchemaTable(getOutputSchemas());

		boolean haveResult = false;
		boolean ok = child.getNextTuple(childOutputs);
		if (ok) {
			for (MutableTupleWithSchema<String> tup: childOutputs) {
				if (needToRebind(tup))
					rebind(tup);
				if (processTuple(tup, outputs))
					haveResult = true;
			}
		}
		return haveResult;
	}

	@Override
	public boolean isPassable(TupleWithSchema<String> tuple) {
		return true;
	}

	@Override
	public void rebind(TupleWithSchema<String> childTuple) {
		
	}

}

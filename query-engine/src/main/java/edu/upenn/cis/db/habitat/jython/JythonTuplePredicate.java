/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.jython;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.python.core.PyCode;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

import edu.upenn.cis.db.habitat.core.expressions.UnaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

/**
 * Takes the fields in the tuple and sets them as Python variables.
 * Executes the python script.  Expects the returned output to be in the
 * variable result, which should evaluate to a Boolean.
 * 
 * @author zives
 *
 */
public class JythonTuplePredicate extends UnaryTuplePredicateWithLookup {
	
	PyCode pythonScript = null;
//	InputStream pythonStream = null;
	PythonInterpreter interp;
	final Set<String> referencedFields;
	
	public JythonTuplePredicate(String pythonScript, Set<String> fields) {
		String script = pythonScript.replaceAll("\\n", "\n");
		String uniqueName = UUID.randomUUID().toString();
		interp = JythonGen.getPythonInterpreter(uniqueName);
		this.pythonScript = interp.compile(script);
		this.referencedFields = fields;
	}
	public JythonTuplePredicate(InputStream stream, Set<String> fields) {
//		this.pythonStream = stream;
		String uniqueName = UUID.randomUUID().toString();
		interp = JythonGen.getPythonInterpreter(uniqueName);
		this.referencedFields = fields;
		
		pythonScript = interp.compile(new InputStreamReader(stream));
	}

	@Override
	public boolean test(TupleWithSchema<String> t) {

//		if (pythonScript != null)
			interp.exec(pythonScript);
//		else
//			interp.execfile(pythonStream);
		
		PyObject ret = interp.get("result");
		
		if (ret == null)
			throw new RuntimeException("Python result not found");
		
		return ret.__nonzero__();
	}

	@Override
	public void bind(TupleWithSchema<String> tuple) {
		super.bind(tuple);
		for (String key: tuple.getSchema().getKeys()) {
			Object o = tuple.getValue(key);
			
			if (o == null)
				interp.set(key, null);
			else 
				interp.set(key, o);
		}
	}
	@Override
	public Set<String> getSymbolsReferenced() {
		return referencedFields;
	}
	@Override
	public void rename(Map<String, String> symbolTable) {
		for (String key: symbolTable.keySet()) {
			if (referencedFields.contains(key)) {
				referencedFields.add(symbolTable.get(key));
				referencedFields.remove(key);
			}
		}
	}

}

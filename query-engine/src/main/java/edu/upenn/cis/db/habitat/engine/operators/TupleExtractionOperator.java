/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.engine.operators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTupleExtractorWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.provenance.expressions.ProvenanceCombiner;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class TupleExtractionOperator<T extends TableWithVariableSchema,E extends TupleWithSchema,S> extends SchemaModifyingOperator<T> {
	final static Logger logger = LogManager.getLogger(TupleExtractionOperator.class);

	final UnaryTupleExtractorWithLookup<E,S> extractorFn;
	final ProvenanceCombiner combineProvenance;
	List<String> fields = new ArrayList<>();
	List<Class<? extends Object>> types = new ArrayList<>();
	
	List<String> args = new ArrayList<>();

	public TupleExtractionOperator(QueryOperator<T> child, 
			String newField,
			Class<? extends Object> fieldType,
			UnaryTupleExtractorWithLookup<E,S> extractorFn,
			ProvenanceCombiner combineProvenance,
			ProvenanceWrapperApi provApi) {
		super(child, provApi);
		
		fields.add(newField);
		types.add(fieldType);
		this.extractorFn = extractorFn;
		this.combineProvenance = combineProvenance;
	}
	
	public TupleExtractionOperator(QueryOperator<T> child, 
			List<String> newFields,
			List<Class<? extends Object>> fieldTypes,
			UnaryTupleExtractorWithLookup<E,S> extractorFn,
			ProvenanceCombiner combineProvenance,
			ProvenanceWrapperApi provApi) {
		super(child, provApi);
		
		fields.addAll(newFields);
		types.addAll(fieldTypes);
		this.extractorFn = extractorFn;
		this.combineProvenance = combineProvenance;
	}
	
	public TupleExtractionOperator(QueryOperator<T> child, 
			BasicSchema output,
			UnaryTupleExtractorWithLookup<E,S> extractorFn,
			ProvenanceCombiner combineProvenance,
			ProvenanceWrapperApi provApi) {
		super(child, provApi);
		
		fields.addAll(output.getKeys());
		types.addAll(output.getTypes());
		this.extractorFn = extractorFn;
		this.combineProvenance = combineProvenance;
	}
	
	@Override
	public void createMappings(Set<BasicSchema> inputs) {
		
		List<String> newFields = 
				fields.stream().filter(field -> !field.equals(ProvenanceApi.Provenance))
				.collect(Collectors.toList());
		
		List<Class<? extends Object>> newTypes = 
				types.stream().filter(type -> type != ProvSpecifier.class && type != ProvLocation.class)
				.collect(Collectors.toList());

		inputs.parallelStream().forEach(schema -> 
		{
			registerMappings(schema,
					// Schema map
					s -> s.augment(schema.getName() + "_" + getId(), newFields, newTypes));
		});
	}

	public void registerMappings(BasicSchema inputSchema,
			UnaryOperator<BasicSchema> schemaFn) {
		this.schemaFn.put(inputSchema, schemaFn);
	}

	@Override
	public boolean processTuple(MutableTupleWithSchema<String> tuple, Collection<MutableTupleWithSchema<String>> outputs) 
	throws HabitatServiceException {
		rebind(tuple);
		List<E> extractions = extractorFn.apply(tuple);
		
		List<MutableTupleWithSchema<String>> fromThis = new ArrayList<>();
		boolean ret = tuple.augment(
				schemaMap.get(tuple.getSchema()), 
				extractions, 
				args, // TODO: this should be the fn name + the fn in-field names
				fromThis, 
				() -> PlanGen.getToken(this),
				(BinaryOperator<ProvSpecifier>)combineProvenance,
				recycled);
		
		// Add provenance node for each derived result, then provenance edge
		// from source to derived
		fromThis.forEach(derived -> {
			try {
//				provApi.storeProvenanceNode("", (ProvToken)derived.getValue(ProvenanceApi.Token), 
//						(ProvSpecifier)derived.getValue(ProvenanceApi.Provenance));
//				provApi.storeProvenanceLink(
//						"",
//						(ProvToken)tuple.getValue(ProvenanceApi.Token), 
//						getId().toString(), 
//						(ProvToken)derived.getValue(ProvenanceApi.Token));
				provApi.writeTupleDerivation((ProvToken)derived.getValue(ProvenanceApi.Token), 
						(ProvSpecifier)derived.getValue(ProvenanceApi.Provenance), 
						(ProvToken)tuple.getValue(ProvenanceApi.Token), 
						getId().toString(), 
						derived);
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("Unable to store provenance for " + derived.getValue(ProvenanceApi.Token));
			}
			
		});
		
		outputs.addAll(fromThis);
		
		return ret;
	}
	
	@Override
	public void rebind(TupleWithSchema<String> childTuple) {
		extractorFn.bind(childTuple);
	}

	@Override
	protected String getArguments() {
		StringBuilder builder = new StringBuilder();
		
		for (int i = 0; i < fields.size(); i++) {
			if (i > 0)
				builder.append(",");
			builder.append(fields.get(i).toString() + ": " + types.get(i).getSimpleName());
		}

		return builder.toString();
	}

	@Override
	public String getLogicalDescriptor() {
		return getClass().getSimpleName() + ": " + getArguments();
	}
}

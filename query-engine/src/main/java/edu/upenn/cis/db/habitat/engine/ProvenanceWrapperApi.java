package edu.upenn.cis.db.habitat.engine;

import java.util.List;

import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvSpecifierModel;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

public class ProvenanceWrapperApi {
	final ProvenanceApi provApi;
	final ProvDmApi provDmApi;
	final String graph;
	
	public ProvenanceWrapperApi(ProvenanceApi api, ProvDmApi dm, String graph) {
		provApi = api;
		provDmApi = dm;
		
		if (graph == null || graph.isEmpty())
			graph = "graph";
		
		this.graph = graph;
	}
	
	
	/**
	 * Filescan or similar input operation
	 * 
	 * @param inputToken
	 * @param location
	 * @param tuple
	 * @throws HabitatServiceException
	 */
	public void writeInputRecord(ProvToken inputToken, ProvLocation location, String filename, 
			String operation, TupleWithSchema<String> tuple) 
			throws HabitatServiceException {
		if (provDmApi != null) {
			QualifiedName result = new QualifiedName(graph, inputToken.toString());
			QualifiedName input = new QualifiedName(graph, filename);
			QualifiedName relationId = new QualifiedName(graph, operation);

			provDmApi.entity(graph, result, ProvSpecifierModel.from(location));
			provDmApi.activity(graph, relationId, null, null, ProvSpecifierModel.from(location));
			provDmApi.used(graph, relationId, input, relationId, null);
			provDmApi.wasGeneratedBy(graph, result, relationId, null, null);
		} else {
			provApi.storeProvenanceNode(graph, inputToken, location);
			
			provApi.storeProvenanceLink(graph, new ProvStringToken(filename), operation, inputToken);
			
		}

//		provDmApi.wasDerivedFrom(graph, result, input, relationId, null, null);
	}
	
	public void writeInputFile(String filename) throws HabitatServiceException {
		if (provDmApi != null) {
			provDmApi.entity(graph, new QualifiedName(graph, filename), null);
		} else {
			provApi.storeProvenanceNode(graph, new ProvStringToken(filename), new ProvStringToken(filename));
		}
	}
	
	/**
	 * Join or Cartesian product: output consisting of the operation applied over the two inputs
	 * 
	 * @param outputToken
	 * @param outputSpecifier
	 * @param leftInputToken
	 * @param rightInputToken
	 * @param operation
	 * @param output
	 * @throws HabitatServiceException
	 */
	public void writeBinaryDerivation(ProvToken outputToken, ProvSpecifier outputSpecifier,
			ProvToken leftInputToken, ProvToken rightInputToken, String operation, 
			TupleWithSchema<String> output) throws HabitatServiceException {
		
		if (provDmApi == null) {
			provApi.storeProvenanceNode(
					graph,
					outputToken,
					outputSpecifier);
	
			provApi.storeProvenanceLink(
					graph,
					leftInputToken,
					rightInputToken,
					operation,
					outputToken);
		} else {		

			QualifiedName result = new QualifiedName(graph, outputToken.toString());
			QualifiedName leftInput = new QualifiedName(graph, leftInputToken.toString());
			QualifiedName rightInput = new QualifiedName(graph, rightInputToken.toString());
			QualifiedName relationId = new QualifiedName(graph, operation);
			provDmApi.entity(graph, result, ProvSpecifierModel.from(outputSpecifier));
			provDmApi.activity(graph, relationId, null, null, ProvSpecifierModel.from(outputSpecifier));
			provDmApi.used(graph, relationId, leftInput, relationId, null);
			provDmApi.used(graph, relationId, rightInput, relationId, null);
			provDmApi.wasGeneratedBy(graph, result, relationId, null, null);
	
	//		provDmApi.wasDerivedFrom(graph, result, leftInput, relationId, null, null);
	//		provDmApi.wasDerivedFrom(graph, result, rightInput, relationId, null, null);
		}
	}
	
	/**
	 * Group-by or similar aggregate computation
	 * 
	 * @param outputToken
	 * @param outputSpecifier
	 * @param inputTokens
	 * @param operation
	 * @param output
	 * @throws HabitatServiceException
	 */
	public void writeGroupDerivation(ProvToken outputToken, ProvSpecifier outputSpecifier,
			List<ProvToken> inputTokens, String operation, BasicTuple output) throws HabitatServiceException {
		if (provDmApi == null) {
			provApi.storeProvenanceNode(
					graph,
					outputToken, 
					outputSpecifier);
	
			provApi.storeProvenanceLink(
					graph,
					inputTokens,
					operation,
					outputToken);

		} else {
			QualifiedName result = new QualifiedName(graph, outputToken.toString());
			provDmApi.entity(graph, result, ProvSpecifierModel.from(outputSpecifier));
			QualifiedName relationId = new QualifiedName(graph, operation);
			provDmApi.activity(graph, relationId, null, null, ProvSpecifierModel.from(outputSpecifier));
			provDmApi.wasGeneratedBy(graph, result, relationId, null, null);
			
			for (ProvToken inputToken: inputTokens) {
				QualifiedName input = new QualifiedName(graph, inputToken.toString());
	//			provDmApi.wasDerivedFrom(graph, result, input, relationId, null, null);
				provDmApi.used(graph, relationId, input, relationId, null);
			}
		}
	}

	public void writeTupleDerivation(ProvToken outputToken, ProvSpecifier outputSpecifier,
			ProvToken inputToken, String operation, TupleWithSchema<String> output) throws HabitatServiceException {
		if (provDmApi == null) {
			provApi.storeProvenanceNode(
					graph,
					outputToken, 
					outputSpecifier);
	
			provApi.storeProvenanceLink(
					graph,
					inputToken,
					operation,
					outputToken);
		} else {
			QualifiedName result = new QualifiedName(graph, outputToken.toString());
			QualifiedName input = new QualifiedName(graph, inputToken.toString());
			QualifiedName relationId = new QualifiedName(graph, operation);
			provDmApi.entity(graph, result, ProvSpecifierModel.from(outputSpecifier));
	//		provDmApi.wasDerivedFrom(graph, result, input, relationId, null, null);
			provDmApi.activity(graph, relationId, null, null, ProvSpecifierModel.from(outputSpecifier));
			provDmApi.used(graph, relationId, input, relationId, null);
			provDmApi.wasGeneratedBy(graph, result, relationId, null, null);
		}
	}
}

SEQUENCE = 'seq'
QUALS = 'quals'
LENGTH = 'length'
PROVLOCATION = 'location'

# truncate 3' end of reads. Truncated reads don't count as having been
# trimmed as this impacts all reads.
# result format 
# result = {'location':{'field': vvv, 'position':vvv}, 'value': {v1:vvv, v2:vvv, ...}}
#import matplotlib

def cut3(readHeader, readString, baseQualityString, nCut):
    """
    Cuts nCut bases from 3' end of read.
    """
    header = readHeader
    seq = readString
    quals = baseQualityString
    length = len(seq)
    provLocation = list(range(0,length))
    # trim sequence
    seq = seq[:(length - nCut)]
   
    trimmedLocation = [i for i in range(length - nCut, length)]
    provLocation = [x for x in provLocation if x not in trimmedLocation]

    # need to trim quals in same way we trimmed sequence
    quals = quals[:(length - nCut)]

    location = dict([('field', 'readString'),('position',provLocation)])
    #value = dict([('readHeader', header),('readString',seq), ('baseQualityString', quals)])
    value = dict([('readString_trimmed',seq), ('baseQualityString_trimmed', quals)])
    row = dict([('location', location),('value',value)])
    result = [row]
    #print result
    return result

# test 
#read = {'seq':'abcd', 'quals':'1234', }
#print cut3(read, 1)
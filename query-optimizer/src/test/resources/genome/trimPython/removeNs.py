from asyncore import read
SEQUENCE = 'seq'
QUALS = 'quals'
LENGTH = 'length'
PROVLOCATION = 'provLocation'


# remove N's on either end of the read
# Input format:  {'seq':'Nbcd', 'quals':'1234'}
# Output format: {'quals': '234', 'length': 3, 'provLocation': [1, 2, 3], 'seq': 'bcd'}

def removeNs(read):
    """
    Removes any N's from the 3' and 5' ends of a sequence.
    """
    seq = read[SEQUENCE]
    quals = read[QUALS]
    length = len(seq)
    
    prov_location = list(range(0,len(seq)))
    trimmed_location = []
    # local flag for trimming having occurred. This is unnecessary
    # since removeNs() is first trimming to happen. However this
    # prevents possible problem in future if we add another trimming
    # option prior to removeNs()
    #wasTrimmed = False

    # trim N from beginning of read (5' end)
    if seq.startswith('N'):
        # trim sequence
        seq = seq.lstrip('N')

        trimmed_location.extend(list(range(0, len(quals) - len(seq))))
        #print seq, trimmed_location
        # need to trim quals in same way we trimmed sequence
        quals = quals[len(quals) - len(seq):]
        
        # delete location indexes of trimmed seq
        
        # flag read as having been trimmed
        #wasTrimmed = True
        
        #read["flags"].append('remove5N')

    # trim N from end of read (3' end)
    if seq.endswith('N'):
        # trim sequence
        seq = seq.rstrip('N')
        trimmed_location.extend(list(range(len(seq) - len(quals))))
        # need to trim quals in same way we trimmed sequence
        quals = quals[:len(seq)]
       
        # flag read as having been trimmed
        #wasTrimmed = True

        #read["flags"].append('remove3N')

    #if wasTrimmed:
    #    if DEBUG: read["debug"].append([read[SEQUENCE], length, seq, len(seq), 'removeNs', ''])

    read[SEQUENCE] = seq
    read[QUALS] = quals
    read[LENGTH] = len(seq)
    #read[TRIMMED] = True
    read[PROVLOCATION] = [x for x in prov_location if x not in trimmed_location]
    return read

#read = {'seq':'Nbcd', 'quals':'1234'}
#print removeNs(read)

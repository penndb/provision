/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat;

import static edu.upenn.cis.db.habitat.test.Util.getResourcePath;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.mapdb.DB;
import org.python.core.PySystemState;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import edu.upenn.cis.db.habitat.catalog.SystemCatalog;
import edu.upenn.cis.db.habitat.core.api.mocks.InMemoryProvAPI;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.optimizer.BitCombinationsHelper;
import edu.upenn.cis.db.habitat.optimizer.OptimizeExpression;
import edu.upenn.cis.db.habitat.optimizer.RelAlgebraHelper;
import edu.upenn.cis.db.habitat.optimizer.graph.QueryGraph;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.querygen.QueryGen;
import edu.upenn.cis.db.habitat.querygen.exceptions.QueryParseException;
import edu.upenn.cis.db.habitat.repository.type.InMemoryVariableSchemaTable;
import edu.upenn.cis.db.habitat.repository.type.StructuredCSVWriter;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import edu.upenn.cis.db.habitat.sql.SqlExpressionBuilder;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.statement.Statement;

public class TestExpressionEnumeration {
	static SqlExpressionBuilder builder = null;
	static String thePath = getResourcePath(TestExpressionEnumeration.class.getClassLoader(), ".");
	static DB db;

	static boolean didInit = false;
	
	OptimizeExpression<Serializable,TableWithVariableSchema> optimizer;
	
	@Before
	public void setUp() throws JSQLParserException, JsonProcessingException {
		if (File.separator.equals("\\") && thePath.charAt(2) == ':')
			thePath = thePath.substring(3);
		if (!didInit) {
			if (File.separator.equals("\\") && thePath.charAt(2) == ':')
				thePath = thePath.substring(3);
			if (File.separator.equals("\\") && thePath.charAt(2) == ':')
				thePath = thePath.substring(3);

			Properties props = PySystemState.getBaseProperties();
			String pythonPathProp = props.getProperty("python.path");
		    String ourPath;
		    
		    if (pythonPathProp==null) {
		        ourPath  = System.getProperty("user.dir") + java.io.File.pathSeparator;
		    } else {
		        ourPath = pythonPathProp +java.io.File.pathSeparator + 
		        		System.getProperty("user.dir") + java.io.File.pathSeparator;
		    }
		    props.setProperty("python.path", ourPath + thePath + File.pathSeparator + thePath + "genome/trimPython");

		    System.err.println("Path: " + ourPath + thePath + File.pathSeparator + thePath + "genome/trimPython");
			PySystemState.initialize(System.getProperties(), props, null);
			
			QueryGen.provAPI = new InMemoryProvAPI();
			QueryGen.provWrapperAPI = new ProvenanceWrapperApi(QueryGen.provAPI, null, "");

			builder = new SqlExpressionBuilder(thePath);
			String statement = "CREATE TABLE \"genome/test_1.fq\"(lineNumber int, readHeader varchar(255), readString varchar(255), baseQualityHeader varchar(255), baseQualityString varchar(255))";
			Statement st = builder.parseSQL(statement);
			builder.getDDLManager().process(statement);

			didInit = true;
		}
		optimizer = new OptimizeExpression<>(builder.getDDLManager().getCatalog());
//		optimizer = new OptimizeExpression<>(new SystemCatalog(Paths.get(thePath).toString()));
	}

	@Test
	public void testJoinPlan() throws JsonSyntaxException, JsonIOException, IOException, QueryParseException, HabitatServiceException {

		String planFile = "join_algebra_tree.json";

		StructuredCSVWriter result = new StructuredCSVWriter("output_module.txt");
		
		QueryOperator<TableWithVariableSchema> root = 
				PlanGen.getQueryOperator(PlanGen.getQueryPlan(thePath, planFile));

		System.out.println("Plan generation for:\n" + root.toString());

		// Make sure file I/O is relative to the right path
		root.setBasePath(Paths.get(thePath));
		
		if (root.initialize()) {
			root.close();
		
			// What are the nodes designating subplans?
			List<QueryOperator<TableWithVariableSchema>> leaves = RelAlgebraHelper.getLeaves(root, true);
			
			QueryGraph<TableWithVariableSchema> queryGraph = optimizer.getQueryGraph(root, leaves);
	
			List<QueryOperator<TableWithVariableSchema>> boundaries = optimizer.getSubBlocks(root);

			// source fields
			assertTrue(queryGraph.getNodes().size() == 16);
			// source relations
			assertTrue(queryGraph.getSources().size() == 5);
			// predicates, links, feed-forwards
			assertTrue(queryGraph.getEdges().size() == 5);

			try {
				QueryOperator<TableWithVariableSchema> newRoot =
						optimizer.getOptimalPlan(queryGraph);
				System.out.println(newRoot);
				
				if (newRoot.initialize()) {
					int count = 0;
					List<MutableTupleWithSchema<String>> buf
						= new ArrayList<>();
					while (newRoot.getNextTuple(buf)) {
						buf.clear();
						count++;
					}
					System.out.println("Produced " + count + " tuples");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void testGroupPlan() throws JsonSyntaxException, JsonIOException, IOException, QueryParseException, HabitatServiceException {

		String planFile = "group_algebra_tree.json";

		StructuredCSVWriter result = new StructuredCSVWriter("output_module.txt");
		
		QueryOperator<TableWithVariableSchema> root = 
				PlanGen.getQueryOperator(PlanGen.getQueryPlan(thePath, planFile));

		System.out.println("Plan generation for:\n" + root.toString());

		// Make sure file I/O is relative to the right path
		root.setBasePath(Paths.get(thePath));
		
		if (root.initialize()) {
			root.close();
		
			// What are the nodes designating subplans?
			List<QueryOperator<TableWithVariableSchema>> leaves = RelAlgebraHelper.getLeaves(root, false);
			
			QueryGraph<TableWithVariableSchema> queryGraph = optimizer.getQueryGraph(root, leaves);
	
			List<QueryOperator<TableWithVariableSchema>> boundaries = optimizer.getSubBlocks(root);
			
			// 5 source fields plus we use them in grouping
			assertTrue(queryGraph.getNodes().size() == 6);
			// One source relation plus we have a virtual group
			assertTrue(queryGraph.getSources().size() == 2);
			// Predicates
			assertTrue(queryGraph.getEdges().size() == 3);
			// No subqueries
			System.out.println("Subqueries: " + boundaries);
			System.out.println("Leaves: " + leaves);
			assertTrue(boundaries.size() == 0);

			try {
				QueryOperator<TableWithVariableSchema> newRoot =
						optimizer.getOptimalPlan(queryGraph);
				
				System.out.println(newRoot);
				
				if (newRoot.initialize()) {
					int count = 0;
					List<MutableTupleWithSchema<String>> buf
						= new ArrayList<>();
					while (newRoot.getNextTuple(buf)) {
						buf.clear();
						count++;
					}
					System.out.println("Produced " + count + " tuples");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void testSelPlan() throws JsonSyntaxException, JsonIOException, IOException, QueryParseException, HabitatServiceException {

		String planFile = "select_algebra_tree.json";

		StructuredCSVWriter result = new StructuredCSVWriter("output_module.txt");
		
		QueryOperator<TableWithVariableSchema> root = 
				PlanGen.getQueryOperator(PlanGen.getQueryPlan(thePath, planFile));

		System.out.println("Plan generation for:\n" + root.toString());

		// Make sure file I/O is relative to the right path
		root.setBasePath(Paths.get(thePath));
		
		if (root.initialize()) {
			root.close();
			// What are the nodes designating subplans?
			List<QueryOperator<TableWithVariableSchema>> leaves = RelAlgebraHelper.getLeaves(root, true);
			
			QueryGraph<TableWithVariableSchema> queryGraph = optimizer.getQueryGraph(root, leaves);
	
			List<QueryOperator<TableWithVariableSchema>> boundaries = optimizer.getSubBlocks(root);

			// 5 source fields
			assertTrue(queryGraph.getNodes().size() == 5);
			// One source relation
			assertTrue(queryGraph.getSources().size() == 1);
			// One predicate
			assertTrue(queryGraph.getEdges().size() == 2);
			
			try {
				QueryOperator<TableWithVariableSchema> newRoot =
						optimizer.getOptimalPlan(queryGraph);
				System.out.println(newRoot);
				
				if (newRoot.initialize()) {
					int count = 0;
					List<MutableTupleWithSchema<String>> buf
						= new ArrayList<>();
					while (newRoot.getNextTuple(buf)) {
						buf.clear();
						count++;
					}
					System.out.println("Produced " + count + " tuples");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Test
	public void testSubsets() {
		BitSet b = new BitSet(5);
		
		b.flip(0, 5);
		
		System.out.println(b);

		int count = 0;
		for (BitSet bs: BitCombinationsHelper.getAllSubsets(b)) {
//			System.out.println(bs);
			count++;
		}
		assertTrue(count == 30);
		
		count = 0;
		b.flip(4);
		for (BitSet bs: BitCombinationsHelper.getAllSubsets(b)) {
//			System.out.println(bs);
			count++;
		}
		System.out.println(count);
		assertTrue(count == 14);
	}
}

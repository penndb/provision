package edu.upenn.cis.db.habitat.sql;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.statement.Statement;

public class SqlDdlTest {
	SqlDDLManager ddl = new SqlDDLManager(".");
	
	@Test
	public void testCreateTable() throws JSQLParserException, JsonProcessingException {
		String statement = "CREATE TABLE \"genome/test_1.fq\"(lineNumber int, readHeader varchar(255), readString varchar(255), baseQualityHeader varchar(255), baseQualityString varchar(255))";

		Statement st = SqlExpressionBuilder.parseSQL(statement);
		
		ddl.process(statement);
	}

	/**
	 * TODO next:
	 * 
	 * - We should leverage CREATE INDEX to (1) create a data profile
	 *   with number of uniques, min, max, histogram
	 *   
	 * - Create a B+ Tree from BasicTuple to its byte offset?
	 */
	
}

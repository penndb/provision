/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat;

import static edu.upenn.cis.db.habitat.test.Util.getResourcePath;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Paths;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.python.core.PySystemState;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import edu.upenn.cis.db.habitat.catalog.SystemCatalog;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.optimizer.OptimizeExpression;
import edu.upenn.cis.db.habitat.provenance.MapDBProvAPI;
import edu.upenn.cis.db.habitat.querygen.PlanGen;
import edu.upenn.cis.db.habitat.querygen.QueryGen;
import edu.upenn.cis.db.habitat.querygen.exceptions.QueryParseException;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

public class TestPlanParsing {
	static String thePath = getResourcePath(TestExpressionEnumeration.class.getClassLoader(), ".");
	
	OptimizeExpression<Serializable,TableWithVariableSchema> optimizer;
	
	static boolean didInit = false;
	static SamReaderFactory factory;
	static DB db;
	
	@Before
	public void setUp() {
		optimizer = new OptimizeExpression<>(new SystemCatalog("."));
		if (!didInit) {
			if (File.separator.equals("\\") && thePath.charAt(2) == ':')
				thePath = thePath.substring(3);
			if (File.separator.equals("\\") && thePath.charAt(2) == ':')
				thePath = thePath.substring(3);
			factory =
			          SamReaderFactory.makeDefault()
		              .enable(SamReaderFactory.Option.INCLUDE_SOURCE_IN_RECORDS, SamReaderFactory.Option.VALIDATE_CRC_CHECKSUMS)
		              .validationStringency(ValidationStringency.SILENT);

			Properties props = PySystemState.getBaseProperties();
			String pythonPathProp = props.getProperty("python.path");
		    String ourPath;
		    
		    if (pythonPathProp==null) {
		        ourPath  = System.getProperty("user.dir") + java.io.File.pathSeparator;
		    } else {
		        ourPath = pythonPathProp +java.io.File.pathSeparator + 
		        		System.getProperty("user.dir") + java.io.File.pathSeparator;
		    }
		    props.setProperty("python.path", ourPath + thePath + File.pathSeparator + thePath + "genome/trimPython");

		    System.err.println("Path: " + ourPath + thePath + File.pathSeparator + thePath + "genome/trimPython");
			PySystemState.initialize(System.getProperties(), props, null);

			// If we use MapDB
			db = DBMaker.fileDB("file.db")
					.fileMmapEnable()
					.checksumHeaderBypass()
					.closeOnJvmShutdown()
//					.cleanerHackEnable()
					.make();

//			Injector injector = Guice.createInjector(
//					// Storage / repository services (by default PostgreSQL and Neo4J)
//					new StorageModule(),
//					// Provenance services
//					new ProvStorageModule()
//					);
//			
//			provApi = injector.getInstance(ProvenanceApi.class);
			QueryGen.provAPI = new MapDBProvAPI(db);
			QueryGen.provWrapperAPI = new ProvenanceWrapperApi(QueryGen.provAPI, null, "");
			
			didInit = true;
		}
	}

	@Test
	public void testSelectPlan() throws JsonSyntaxException, JsonIOException, IOException, QueryParseException, HabitatServiceException {

		String planFile = "select_algebra_tree.json";

		QueryOperator<TableWithVariableSchema> root = 
				PlanGen.getQueryOperator(PlanGen.getQueryPlan(thePath, planFile));

		System.out.println(root.toString());
		
		root.setBasePath(Paths.get(thePath));
		TableWithVariableSchema answer = root.execute();

		System.out.println(answer);
	}

	@Test
	public void testGroupPlan() throws JsonSyntaxException, JsonIOException, IOException, QueryParseException, HabitatServiceException {

		String planFile = "group_algebra_tree.json";

		QueryOperator<TableWithVariableSchema> root = 
				PlanGen.getQueryOperator(PlanGen.getQueryPlan(thePath, planFile));

		System.out.println(root.toString());

		root.setBasePath(Paths.get(thePath));
		TableWithVariableSchema answer = root.execute();
		
		System.out.println(answer);
	}

	// This is not a complete plan!
//	@Test
	public void testExtractPlan() throws JsonSyntaxException, JsonIOException, IOException, QueryParseException, HabitatServiceException {

		String planFile = "trim_algebra_tree.json";

		QueryOperator<TableWithVariableSchema> root = 
				PlanGen.getQueryOperator(PlanGen.getQueryPlan(thePath, planFile));

		System.out.println(root.toString());
		
		root.setBasePath(Paths.get(thePath));
		TableWithVariableSchema answer = root.execute();

		System.out.println(answer);
	}


	@Test
	public void testJoinPlan() throws JsonSyntaxException, JsonIOException, IOException, QueryParseException, HabitatServiceException {

		String planFile = "join_algebra_tree.json";

		QueryOperator<TableWithVariableSchema> root = 
				PlanGen.getQueryOperator(PlanGen.getQueryPlan(thePath, planFile));

		System.out.println(root.toString());

		root.setBasePath(Paths.get(thePath));
		TableWithVariableSchema answer = root.execute();

		System.out.println(answer);
	}

}

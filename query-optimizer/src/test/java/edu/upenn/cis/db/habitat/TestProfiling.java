package edu.upenn.cis.db.habitat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import edu.upenn.cis.db.habitat.optimizer.stats.BucketProfile;

public class TestProfiling {

	@Test
	public void testBucketProfiles() {
		
		List<Integer> values = new ArrayList<Integer>();
		values.add(0);
		values.add(8000);
		values.add(1000);
		values.add(5000);
		values.add(10000);
		BucketProfile<Integer> test = new BucketProfile<>(3, Integer.class, values);
		
		
		System.out.println(test);
		
		assertTrue(test.getCount() == values.size());
		
		assertTrue(test.getMax() == values.get(values.size()-1));
		assertTrue(test.getMin() == values.get(0));

		assertEquals(test.getCountInRange(Integer.valueOf(0), Integer.valueOf(5000)), 2);
		assertTrue(test.getCountInRange(Integer.valueOf(0), Integer.valueOf(8000)) > 2);
		assertEquals(test.getCountInRange(Integer.valueOf(0), Integer.valueOf(10000)), 5);
		assertEquals(test.getCountInRange(Integer.valueOf(0), Integer.valueOf(12000)),5);
		
		assertEquals(test.getCountInRange(Integer.valueOf(1000), Integer.valueOf(5000)), 1);
		assertEquals(test.getCountInRange(Integer.valueOf(1000), Integer.valueOf(8000)), 2);
		assertEquals(test.getCountInRange(Integer.valueOf(1000), Integer.valueOf(10000)), 4);
		assertEquals(test.getCountInRange(Integer.valueOf(1000), Integer.valueOf(12000)), 4);

		assertEquals(test.getCountInRange(Integer.valueOf(-1000), Integer.valueOf(5000)), 2);
		assertEquals(test.getCountInRange(Integer.valueOf(-1000), Integer.valueOf(12000)), 5);
	}
}

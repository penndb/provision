package edu.upenn.cis.db.habitat.sql;

import static edu.upenn.cis.db.habitat.test.Util.getResourcePath;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.python.core.PySystemState;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import edu.upenn.cis.db.habitat.core.api.mocks.NullProvAPI;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionLiteral;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.PythonUdf;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.ProvenanceWrapperApi;
import edu.upenn.cis.db.habitat.engine.operators.GroupByOperator;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.optimizer.OptimizeExpression;
import edu.upenn.cis.db.habitat.optimizer.RelAlgebraHelper;
import edu.upenn.cis.db.habitat.optimizer.graph.QueryGraph;
import edu.upenn.cis.db.habitat.querygen.QueryGen;
import edu.upenn.cis.db.habitat.querygen.exceptions.QueryParseException;
import edu.upenn.cis.db.habitat.repository.type.StructuredCSVWriter;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import edu.upenn.cis.db.habitat.sql.exceptions.TableNotFoundException;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.Select;

public class SqlDmlTest {
	static SqlExpressionBuilder builder = null;
//	OptimizeExpression<Serializable, TableWithVariableSchema> optimizer;
	static String thePath = getResourcePath(SqlDmlTest.class.getClassLoader(), ".");
	
	static boolean didInit = false;
	
	static void setupTpch() throws JSQLParserException, IOException {
		String dfn = "CREATE TABLE \"tpc-h-100/customer.tbl\"(c_custkey int, c_name varchar(255), c_address varchar(255), c_nationkey int, c_phone varchar(15), c_acctbal double, c_mktsegment varchar(10), c_comment varchar(255))";
		builder.ddl.process(dfn);
		List<String> cKeys = new ArrayList<>(2);
		cKeys.add("c_custkey");
		builder.ddl.setPrimaryKey("tpc-h-100/customer.tbl", cKeys);
		cKeys.clear();
		cKeys.add("c_mktsegment");
		builder.ddl.collectStatistics("tpc-h-100/customer.tbl", cKeys);

		dfn = "CREATE TABLE \"tpc-h-100/orders.tbl\"(o_orderkey int, o_custkey int, o_orderstatus varchar(1), o_totalprice double, o_orderdate varchar(12), o_orderpriority varchar(10), o_clerk varchar(20), o_shippriority int, o_comment varchar(255))";
		builder.ddl.process(dfn);
		List<String> oKeys = new ArrayList<>(2);
		oKeys.add("o_orderkey");
		builder.ddl.setPrimaryKey("tpc-h-100/orders.tbl", oKeys);
		dfn = "CREATE TABLE \"tpc-h-100/lineitem.tbl\"(l_orderkey int, l_partkey int, l_suppkey int, l_linenumber int, l_quantity int, l_extendedprice double, l_discount double, l_tax double, l_returnflag varchar(1), l_linestatus varchar(1), l_shipdate varchar(12), l_commitdate varchar(12), l_shipinstruct varchar(20), l_shipmode varchar(20), l_comment varchar(255))";
		builder.ddl.process(dfn);
		List<String> liKeys = new ArrayList<>(2);
		liKeys.add("l_orderkey");
		liKeys.add("l_partkey");
		builder.ddl.setPrimaryKey("tpc-h-100/lineitem.tbl", liKeys);
		liKeys.clear();
		liKeys.add("l_returnflag");
		liKeys.add("l_linestatus");
		builder.ddl.collectStatistics("tpc-h-100/lineitem.tbl", liKeys);
		dfn = "CREATE TABLE \"tpc-h-100/supplier.tbl\" (" + 
				"s_suppkey int, s_name varchar(25), s_address varchar(40), s_nationkey int, s_phone varchar(15),"
				+ " s_acctbal double, s_comment varchar(101));"; 
		builder.ddl.process(dfn);
		
		List<String> sKeys = new ArrayList<>(2);
		sKeys.add("s_suppkey");
		sKeys.add("s_name");
		builder.ddl.setPrimaryKey("tpc-h-100/supplier.tbl", sKeys);
				
		dfn = "CREATE TABLE \"tpc-h-100/nation.tbl\" (" + 
				"n_nationkey int, n_name varchar(25), n_regionkey int, n_comment varchar(152));"; 
		builder.ddl.process(dfn);
		List<String> nKeys = new ArrayList<>(2);
		nKeys.add("n_nationkey");
		builder.ddl.setPrimaryKey("tpc-h-100/nation.tbl", nKeys);
		dfn = "CREATE TABLE \"tpc-h-100/region.tbl\" (" + 
				"r_regionkey int, r_name varchar(25), r_comment varchar(152));";
		builder.ddl.process(dfn);
		List<String> rKeys = new ArrayList<>(2);
		rKeys.add("r_regionkey");
		builder.ddl.setPrimaryKey("tpc-h-100/region.tbl", rKeys);
		
	}

	@Before
	public void setUp() throws JSQLParserException, IOException {
		if (File.separator.equals("\\") && thePath.charAt(2) == ':')
			thePath = thePath.substring(3);
		if (!didInit) {
			if (File.separator.equals("\\") && thePath.charAt(2) == ':')
				thePath = thePath.substring(3);
			if (File.separator.equals("\\") && thePath.charAt(2) == ':')
				thePath = thePath.substring(3);

			Properties props = PySystemState.getBaseProperties();
			String pythonPathProp = props.getProperty("python.path");
		    String ourPath;
		    
		    if (pythonPathProp==null) {
		        ourPath  = System.getProperty("user.dir") + java.io.File.pathSeparator;
		    } else {
		        ourPath = pythonPathProp +java.io.File.pathSeparator + 
		        		System.getProperty("user.dir") + java.io.File.pathSeparator;
		    }
		    props.setProperty("python.path", ourPath + thePath + File.pathSeparator + thePath + "genome/trimPython");

		    System.err.println("Path: " + ourPath + thePath + File.pathSeparator + thePath + "genome/trimPython");
			PySystemState.initialize(System.getProperties(), props, null);
			
			builder = new SqlExpressionBuilder(thePath);
			
			QueryGen.provAPI = new NullProvAPI();//InMemoryProvAPI();
			QueryGen.provWrapperAPI = new ProvenanceWrapperApi(QueryGen.provAPI, null, "");

			didInit = true;
			
			List<ArithmeticExpressionOperation<?>> parameters = new ArrayList<>();
			parameters.add(new ArithmeticExpressionLiteral<String>(String.class, "readHeader"));
			parameters.add(new ArithmeticExpressionLiteral<String>(String.class, "readString"));
			parameters.add(new ArithmeticExpressionLiteral<String>(String.class, "baseQualityString"));
			BasicSchema inSch = new BasicSchema("cut3-in");
			inSch.addField("readHeader", String.class);
			inSch.addField("readString", String.class);
			inSch.addField("baseQualityString", String.class);
			BasicSchema outSch = new BasicSchema("cut3");
			builder.ddl.catalog.writeMetadata(new PythonUdf("cut3",
					inSch,
					parameters,
					outSch,
					"from cut3 import cut3\r\nresult = cut3(readHeader, readString, baseQualityString,2)"));
			
			setupTpch();
		}
	}
	

	
	public void exec(QueryOperator<TableWithVariableSchema> op, int max) throws HabitatServiceException {
		op.initialize();

		List<MutableTupleWithSchema<String>> results = new ArrayList<>();
		int count = 0;
		while (op.getNextTuple(results) && count < max)
			count++;
		
		//System.out.println("Query produced " + count + " results");
		op.close();
	}
	
	public void exec(QueryOperator<TableWithVariableSchema> op, String outputFileName) throws HabitatServiceException, IOException {
		op.initialize();

		List<MutableTupleWithSchema<String>> results = new ArrayList<>();
		int count = 0;
		while (op.getNextTuple(results))
			count++;
		op.close();

		StructuredCSVWriter output = new StructuredCSVWriter(outputFileName);
		output.addAll(results);
		output.close();
	}
	
	@Test
	public void testQueryStar() throws JSQLParserException, IOException, TableNotFoundException, HabitatServiceException, QueryParseException {
		String statement = "SELECT * FROM \"genome/test_1.fq\" T";

		Statement st = builder.parseSQL(statement);
		System.out.println(st.toString());
		
		QueryOperator<TableWithVariableSchema> op = builder.getQueryExpression((Select) st);
		if (st instanceof Select) {
			System.out.println(op);
		} else
			assertTrue(false);
		
		exec(op, Integer.MAX_VALUE);
	}

	@Test
	public void testQuerySelectProject() throws JSQLParserException, IOException, TableNotFoundException, HabitatServiceException, QueryParseException {
		String statement = "SELECT lineNumber, readString FROM \"genome/test_1.fq\" WHERE lineNumber < 10 and readString <> 'abc'";

		Statement st = builder.parseSQL(statement);
		System.out.println(st.toString());
		
		QueryOperator<TableWithVariableSchema> op = builder.getQueryExpression((Select) st);
		
		if (st instanceof Select) {
			System.out.println(op);
		} else
			assertTrue(false);
		
		exec(op, Integer.MAX_VALUE);

	}

	@Test
	public void testQuerySelectProjectAliased() throws JSQLParserException, IOException, TableNotFoundException, HabitatServiceException, QueryParseException {
		String statement = "SELECT T.lineNumber, T.readString FROM \"genome/test_1.fq\" T WHERE T.lineNumber < 10 and readString <> 'abc'";

		Statement st = builder.parseSQL(statement);
		System.out.println(st.toString());
		
		QueryOperator<TableWithVariableSchema> op = builder.getQueryExpression((Select) st);
		
		if (st instanceof Select) {
			System.out.println(op);
		} else
			assertTrue(false);
		
		exec(op, Integer.MAX_VALUE);
	}

	@Test
	public void testQuerySelectProjectOptimized() throws JSQLParserException, IOException, TableNotFoundException, HabitatServiceException, QueryParseException {
		String statement = "SELECT lineNumber, readString FROM \"genome/test_1.fq\" WHERE lineNumber < 10 and readString <> 'abc'";

		Statement st = builder.parseSQL(statement);
		System.out.println(st.toString());
		
		QueryOperator<TableWithVariableSchema> root = builder.getQueryExpression((Select) st);
		
		if (st instanceof Select) {
			System.out.println(root);
		} else
			assertTrue(false);
		
		OptimizeExpression<Serializable, TableWithVariableSchema> optimizer = new OptimizeExpression<>(builder.getDDLManager().getCatalog());
//		exec(op);
		if (root.initialize()) {
			// What are the nodes designating subplans?
			List<QueryOperator<TableWithVariableSchema>> leaves = RelAlgebraHelper.getLeaves(root, true);
			
			QueryGraph<TableWithVariableSchema> queryGraph = optimizer.getQueryGraph(root, leaves);
	
			List<QueryOperator<TableWithVariableSchema>> boundaries = optimizer.getSubBlocks(root);
			
			System.out.println("Root: " + boundaries);
			root.close();
		}
	}
	
	@Test
	public void testQueryUdf() throws JSQLParserException, JsonParseException, JsonMappingException, TableNotFoundException, IOException, QueryParseException {
		String statement = "select cut3(readHeader,readString,baseQualityString,2)\r\n" + 
				"from \"genome/test_1.fq\"";

		Statement st = builder.parseSQL(statement);
		System.out.println(st.toString());
		QueryOperator<TableWithVariableSchema> op = builder.getQueryExpression((Select) st);
	}

	@Test
	public void testQueryJoin() throws JSQLParserException, JsonParseException, JsonMappingException, TableNotFoundException, IOException, QueryParseException {
		String statement = "select *\r\n" + 
				"from \"genome/test_1.fq\" g1, \"genome/test_2.fq\" g2\r\n" +
				"where g1.readHeader = g2.readHeader";

		Statement st = builder.parseSQL(statement);
		QueryOperator<TableWithVariableSchema> op = builder.getQueryExpression((Select) st);
		
		System.out.println(op.toString());
	}
	
	@Test
	public void testQueryJoinNewStyle() throws JSQLParserException, JsonParseException, JsonMappingException, TableNotFoundException, IOException, QueryParseException {
		String statement = "select *\r\n" + 
				"from \"genome/test_1.fq\" g1 join \"genome/test_2.fq\" g2\r\n" +
				"on g1.readHeader = g2.readHeader";

		Statement st = builder.parseSQL(statement);
		System.out.println(st.toString());
		QueryOperator<TableWithVariableSchema> op = builder.getQueryExpression((Select) st);

		System.out.println(op.toString());
	}
	
	@Test
	public void testQueryJoinUdfs() throws JSQLParserException {
		String statement = "select *\r\n" + 
				"from\r\n" + 
				"(select cut3(readHeader,readString,baseQualityString,2)\r\n" + 
				"from \"genome/test_1.fq\") as g1, \r\n" + 
				"(select cut3(readHeader,readString,baseQualityString,2)\r\n" + 
				"from \"genome/test_1.fq\") as g2\r\n" + 
				"where g1.readString_trimmed = g2.readString_trimmed";
		
		System.out.println(builder.parseSQL(statement));
	}

	@Test
	public void testTpcQ1() throws JSQLParserException, JsonParseException, JsonMappingException, TableNotFoundException, IOException, QueryParseException, HabitatServiceException {		
		String statement = "SELECT\r\n" + 
				"    l_returnflag,\r\n" + 
				"    l_linestatus,\r\n" + 
//				"    sum(l_quantity) as sum_qty,\r\n" + 
//				"    sum_int(l_quantity) as sum_qty,\r\n" + 
				"    sum(l_extendedprice) as sum_base_price,\r\n" + 
//				"    sum(l_extendedprice * (1 - l_discount)) as sum_disc_price,\r\n" + 
//				"    sum(l_extendedprice * (1 - l_discount) * (1 + l_tax)) as sum_charge,\r\n" + 
//				"    avg(l_quantity) as avg_qty,\r\n" + 
//				"    avg(l_extendedprice) as avg_price,\r\n" + 
//				"    avg(l_discount) as avg_disc,\r\n" + 
				"    count(*) as count_order\r\n" + 
				"FROM\r\n" + 
				"    \"tpc-h-100/lineitem.tbl\"\r\n"  + 

				"WHERE\r\n" + 
				"    l_shipdate <= '1998-09-03'\r\n" + 
				"GROUP BY\r\n" + 
				"    l_returnflag,\r\n" + 
				"    l_linestatus\r\n" + 
				"ORDER BY\r\n" + 
				"    l_returnflag,\r\n" + 
				"    l_linestatus;";
		System.out.println(builder.parseSQL(statement));
		Statement st = builder.parseSQL(statement);
		
		QueryOperator<TableWithVariableSchema> op = builder.getQueryExpression((Select) st);
		System.out.println(op.toString());
		
		OptimizeExpression<Serializable, TableWithVariableSchema> optimizer = new OptimizeExpression<>(builder.getDDLManager().getCatalog());
		if (op.initialize()) {
			op.close();
		
			// What are the nodes designating subplans?
			List<QueryOperator<TableWithVariableSchema>> leaves = RelAlgebraHelper.getLeaves(op, true);
			
			QueryGraph<TableWithVariableSchema> queryGraph = optimizer.getQueryGraph(op, leaves);
			try {
				QueryOperator<TableWithVariableSchema> newOp =
						optimizer.getOptimalPlan(queryGraph);
				
				op = RelAlgebraHelper.cloneGroup(newOp, (GroupByOperator)op, queryGraph);  
				System.out.println(op);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
		long startTime = System.nanoTime();
		exec(op, Integer.MAX_VALUE);
		long finishTime = System.nanoTime();
		System.out.println((finishTime-startTime)/1000000 + "ms");
	}

	@Test
	public void testTpcQ3() throws JSQLParserException, JsonParseException, JsonMappingException, TableNotFoundException, IOException, QueryParseException, HabitatServiceException {
		String statement = "select\r\n" + 
				"        l_orderkey,\r\n" + 
				"        sum(l_extendedprice * (1 - l_discount)) as revenue,\r\n" + 
				"        o_orderdate,\r\n" + 
				"        o_shippriority\r\n" + 
				"from\r\n" + 
				"        \"tpc-h-100/customer.tbl\",\r\n" + 
				"        \"tpc-h-100/orders.tbl\",\r\n" + 
				"        \"tpc-h-100/lineitem.tbl\"\r\n" + 
				"where\r\n" + 
				"        c_mktsegment = 'HOUSEHOLD'\r\n" + 
				"        and c_custkey = o_custkey\r\n" + 
				"        and l_orderkey = o_orderkey\r\n" + 
				"        and o_orderdate < '1995-03-16'\r\n" + 
				"        and l_shipdate > '1995-03-16'\r\n" + 
				"group by\r\n" + 
				"        l_orderkey,\r\n" + 
				"        o_orderdate,\r\n" + 
				"        o_shippriority\r\n" + 
				"order by\r\n" + 
				"        revenue desc,\r\n" + 
				"        o_orderdate;";
		System.out.println(builder.parseSQL(statement));
		Statement st = builder.parseSQL(statement);
		
		QueryOperator<TableWithVariableSchema> op = builder.getQueryExpression((Select) st);
		System.out.println(op.toString());

		OptimizeExpression<Serializable, TableWithVariableSchema> optimizer = new OptimizeExpression<>(builder.getDDLManager().getCatalog());
		if (op.initialize()) {
			op.close();
		
			// What are the nodes designating subplans?
			List<QueryOperator<TableWithVariableSchema>> leaves = RelAlgebraHelper.getLeaves(op, true);
			
			QueryGraph<TableWithVariableSchema> queryGraph = optimizer.getQueryGraph(op, leaves);
			try {
				QueryOperator<TableWithVariableSchema> newOp =
						optimizer.getOptimalPlan(queryGraph);
				
				op = RelAlgebraHelper.cloneGroup(newOp, (GroupByOperator)op, queryGraph);  
				System.out.println(op);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
		long startTime = System.nanoTime();
		exec(op, Integer.MAX_VALUE);
		long finishTime = System.nanoTime();
		System.out.println((finishTime-startTime)/1000000 + "ms");
	}
	
	@Test
	public void testTpcQ5() throws JSQLParserException, JsonParseException, JsonMappingException, TableNotFoundException, IOException, QueryParseException, HabitatServiceException {
		String statement = "SELECT\r\n" + 
				"    n_name,\r\n" + 
				"    sum(l_extendedprice * (1 - l_discount)) as revenue\r\n" + 
				"FROM\r\n" + 
				"    \"tpc-h-100/customer.tbl\",\r\n" + 
				"    \"tpc-h-100/orders.tbl\",\r\n" + 
				"    \"tpc-h-100/lineitem.tbl\",\r\n" + 
				"    \"tpc-h-100/supplier.tbl\",\r\n" + 
				"    \"tpc-h-100/nation.tbl\",\r\n" + 
				"    \"tpc-h-100/region.tbl\"\r\n" + 
				"WHERE\r\n" + 
				"    c_custkey = o_custkey\r\n" + 
				"    AND l_orderkey = o_orderkey\r\n" + 
				"    AND l_suppkey = s_suppkey\r\n" + 
				"    AND c_nationkey = s_nationkey\r\n" + 
				"    AND s_nationkey = n_nationkey\r\n" + 
				"    AND n_regionkey = r_regionkey\r\n" + 
				"    AND r_name = 'ASIA'\r\n" + 
				"    AND o_orderdate >= '1994-01-01'\r\n" + 
				"    AND o_orderdate < '1995-01-01'\r\n" + 
				"GROUP BY\r\n" + 
				"    n_name\r\n" + 
				"ORDER BY\r\n" + 
				"    revenue desc;";
		System.out.println(builder.parseSQL(statement));
		Statement st = builder.parseSQL(statement);
		
		QueryOperator<TableWithVariableSchema> op = builder.getQueryExpression((Select) st);
		
		OptimizeExpression<Serializable, TableWithVariableSchema> optimizer = new OptimizeExpression<>(builder.getDDLManager().getCatalog());
		System.out.println(op.toString());
		if (op.initialize()) {
			op.close();
		
			// What are the nodes designating subplans?
			List<QueryOperator<TableWithVariableSchema>> leaves = RelAlgebraHelper.getLeaves(op, true);
			
			QueryGraph<TableWithVariableSchema> queryGraph = optimizer.getQueryGraph(op, leaves);
			try {
				QueryOperator<TableWithVariableSchema> newOp =
						optimizer.getOptimalPlan(queryGraph);
				
				op = RelAlgebraHelper.cloneGroup(newOp, (GroupByOperator)op, queryGraph);  
				System.out.println(op);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
		long startTime = System.nanoTime();
		exec(op, Integer.MAX_VALUE);
		long finishTime = System.nanoTime();
		System.out.println((finishTime-startTime)/1000000 + "ms");
	}
}

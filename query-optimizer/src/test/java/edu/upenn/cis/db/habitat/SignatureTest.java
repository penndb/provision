/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat;

import java.util.BitSet;

import org.junit.Test;

import edu.upenn.cis.db.habitat.optimizer.memo.QueryGraphSignature;
import edu.upenn.cis.db.habitat.optimizer.memo.QueryGraphSignatureIterator;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class SignatureTest {

	@Test
	public void testOne() {
		BitSet bits = new BitSet(5);
		
		bits.set(0, 5);
		QueryGraphSignature<TableWithVariableSchema> sig = new QueryGraphSignature<>(bits);
		System.out.println(sig + " / one:");
		
		QueryGraphSignatureIterator<TableWithVariableSchema> iter = new QueryGraphSignatureIterator<>(sig.getBits(), 4, 1);
		
		while (iter.hasNext()) {
			QueryGraphSignature<TableWithVariableSchema> bits2 = iter.next();
			System.out.println(bits2);
		}
	}

	@Test
	public void testTwo() {
		BitSet bits = new BitSet(5);
		
		bits.set(0, 5);
		QueryGraphSignature<TableWithVariableSchema> sig = new QueryGraphSignature<>(bits);
		System.out.println(sig + " / two:");
		
		QueryGraphSignatureIterator<TableWithVariableSchema> iter = new QueryGraphSignatureIterator<>(sig.getBits(), 4, 2);
		
		while (iter.hasNext()) {
			QueryGraphSignature<TableWithVariableSchema> bits2 = iter.next();
			System.out.println(bits2);
		}
	}

	@Test
	public void testThree() {
		BitSet bits = new BitSet(5);
		
		bits.set(0, 5);
		QueryGraphSignature<TableWithVariableSchema> sig = new QueryGraphSignature<>(bits);
		System.out.println(sig + " / three:");
		
		QueryGraphSignatureIterator<TableWithVariableSchema> iter = new QueryGraphSignatureIterator<>(sig.getBits(), 4, 3);
		
		while (iter.hasNext()) {
			QueryGraphSignature<TableWithVariableSchema> bits2 = iter.next();
			System.out.println(bits2);
		}
	}

	@Test
	public void testFour() {
		BitSet bits = new BitSet(5);
		
		bits.set(0, 5);
		QueryGraphSignature<TableWithVariableSchema> sig = new QueryGraphSignature<>(bits);
		System.out.println(sig + " / four:");
		
		QueryGraphSignatureIterator<TableWithVariableSchema> iter = new QueryGraphSignatureIterator<>(sig.getBits(), 4, 4);
		
		while (iter.hasNext()) {
			QueryGraphSignature<TableWithVariableSchema> bits2 = iter.next();
			System.out.println(bits2);
		}
	}

	@Test
	public void testFive() {
		BitSet bits = new BitSet(5);
		
		bits.set(0, 5);
		QueryGraphSignature<TableWithVariableSchema> sig = new QueryGraphSignature<>(bits);
		System.out.println(sig + " / five:");
		
		QueryGraphSignatureIterator<TableWithVariableSchema> iter = new QueryGraphSignatureIterator<>(sig.getBits(), 4, 5);
		
		while (iter.hasNext()) {
			QueryGraphSignature<TableWithVariableSchema> bits2 = iter.next();
			System.out.println(bits2);
		}
	}

	@Test
	public void testTwoSubset() {
		BitSet bits = new BitSet(5);
		
		bits.set(0, 2);
		bits.set(3, 4);
		QueryGraphSignature<TableWithVariableSchema> sig = new QueryGraphSignature<>(bits);
		System.out.println(sig + " / two set:");
		
		QueryGraphSignatureIterator<TableWithVariableSchema> iter = new QueryGraphSignatureIterator<>(sig.getBits(), 4, 2);
		
		while (iter.hasNext()) {
			QueryGraphSignature<TableWithVariableSchema> bits2 = iter.next();
			System.out.println(bits2);
		}
	}
}

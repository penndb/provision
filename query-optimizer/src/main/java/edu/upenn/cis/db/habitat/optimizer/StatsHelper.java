package edu.upenn.cis.db.habitat.optimizer;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.catalog.TableStatistics;
import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations.BooleanOperation;
import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations.ComparisonOperation;
import edu.upenn.cis.db.habitat.core.expressions.UnaryFieldComparisonPredicate;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTupleBooleanExpression;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTuplePredicate;
import edu.upenn.cis.db.habitat.jython.JythonTuplePredicate;
import edu.upenn.cis.db.habitat.optimizer.stats.BucketProfile;

public class StatsHelper {
	static Logger logger = LogManager.getLogger(StatsHelper.class);

	public static TableStatistics estimateJoinStatistics(TableStatistics statistics, TableStatistics statistics2,
			boolean isJoin, List<String> leftFields, List<String> rightFields) {
		
		if (!isJoin)
			return statistics;
		
		// Cartesian product
		// TODO: reason about predicates, k-fk
		TableStatistics two = new TableStatistics();

		if (leftFields.size() == 0)					
			two.setCardinality(statistics.getCardinality() * statistics2.getCardinality());
		else if (statistics.getCardinality() > statistics2.getCardinality()) {
			two.setCardinality(statistics.getCardinality());

			 if (statistics.getValueProfiles().containsKey(leftFields.get(0)) ||
					 statistics2.getValueProfiles().containsKey(rightFields.get(0))) {
				 
				 BucketProfile bp = (BucketProfile) statistics.getValueProfiles().get(leftFields.get(0));
				 if (bp == null) {
					 bp = (BucketProfile) statistics2.getValueProfiles().get(rightFields.get(0));
					 two.setCardinality((int)(two.getCardinality() * 
							 bp.getScaleFactor() * bp.getDuplicateFactor()));
				 } else {
					 two.setCardinality((int)(two.getCardinality() * 
							 bp.getScaleFactor() * bp.getDuplicateFactor()));
				 }
				 logger.debug("Have stats on join field");
			 }
			 float scaleFactor = ((float)two.getCardinality()) / statistics.getCardinality();
			 
			 if (statistics.getValueProfiles().containsKey(leftFields.get(0)) ||
					 statistics2.getValueProfiles().containsKey(rightFields.get(0))) {
				 logger.debug("Have stats on join field");
			 }

			 for (String rp: statistics2.getValueProfiles().keySet()) {
				BucketProfile bp = (BucketProfile)statistics2.getValueProfiles().get(rp);
				
				if (scaleFactor > 1)
					two.getValueProfiles().put(rp, bp.duplicateBy(scaleFactor));
				else
					two.getValueProfiles().put(rp, bp.filterBy(scaleFactor));
			 }
			 for (String rp: statistics.getValueProfiles().keySet()) {
				BucketProfile bp = (BucketProfile)statistics.getValueProfiles().get(rp);
				
				two.getValueProfiles().put(rp, bp);
			 }
		} else {
			 two.setCardinality(statistics2.getCardinality());
			 
			 if (statistics.getValueProfiles().containsKey(leftFields.get(0)) ||
					 statistics2.getValueProfiles().containsKey(rightFields.get(0))) {
				 
				 BucketProfile bp = (BucketProfile) statistics.getValueProfiles().get(leftFields.get(0));
				 if (bp == null) {
					 bp = (BucketProfile) statistics2.getValueProfiles().get(rightFields.get(0));
					 two.setCardinality((int)(two.getCardinality() * 
							 bp.getScaleFactor() * bp.getDuplicateFactor()));
				 } else {
					 two.setCardinality((int)(two.getCardinality() * 
							 bp.getScaleFactor() * bp.getDuplicateFactor()));
				 }
				 logger.debug("Have stats on join field");
			 }
			 float scaleFactor = ((float)two.getCardinality()) / statistics.getCardinality();
			 
			 for (String rp: statistics.getValueProfiles().keySet()) {
				BucketProfile bp = (BucketProfile)statistics.getValueProfiles().get(rp);
				
				if (scaleFactor > 1)
					two.getValueProfiles().put(rp, bp.duplicateBy(scaleFactor));
				else
					two.getValueProfiles().put(rp, bp.filterBy(scaleFactor));
			 }
			 for (String rp: statistics2.getValueProfiles().keySet()) {
				BucketProfile bp = (BucketProfile)statistics2.getValueProfiles().get(rp);
				
				two.getValueProfiles().put(rp, bp);
			 }
		}
		return two;
	}
	
	static double max(double one, double two) {
		return (one > two) ? one : two;
	}
	
	static double getSelectivity(TableStatistics statistics,UnaryTuplePredicate pred) {
		if (pred instanceof JythonTuplePredicate)
			return 0.1;
		else if (pred instanceof UnaryTupleBooleanExpression) {
			UnaryTupleBooleanExpression b = (UnaryTupleBooleanExpression)pred;
			if (b.getOperation().equals(BooleanOperation.Not))
				return getSelectivity(statistics, b.getLeft());
			else if (b.getOperation().equals(BooleanOperation.And))
				return max(getSelectivity(statistics, b.getLeft()), 
						getSelectivity(statistics, b.getRight()));
			else if (b.getOperation().equals(BooleanOperation.And))
				return max(getSelectivity(statistics, b.getLeft()) + 
						getSelectivity(statistics, b.getRight()), 1);
			else
				throw new UnsupportedOperationException();
		} else if (pred instanceof UnaryFieldComparisonPredicate) {
			UnaryFieldComparisonPredicate fc = (UnaryFieldComparisonPredicate)pred;
			
			if (fc.getOp().equals(ComparisonOperation.Equal) ||
					fc.getOp().equals(ComparisonOperation.IsNull)) {
				
				// Get first variable
				String var = fc.getSymbolsReferenced().iterator().next();
				if (statistics.getValueProfiles().containsKey(var)) {
					BucketProfile bp = (BucketProfile)statistics.getValueProfiles().get(var);
					
//					return (int)(((float)bp.getCount()) / bp.getUniqueCount());
					return 1.0 / bp.getUniqueCount();
				} else
					return 0.1;
			} else if (fc.getOp().equals(ComparisonOperation.NotEqual) || 
					fc.getOp().equals(ComparisonOperation.NotNull)) {
				return 1.0;
			} else {
				return 0.3;
			}
		} else
			return 1.0;
	}
	
	/**
	 * Estimate a selection predicate
	 * @param statistics
	 * @param pred
	 * @return
	 */
	public static TableStatistics estimateSelStatistics(TableStatistics statistics, UnaryTuplePredicate pred) {
		TableStatistics two = new TableStatistics();
		
		if (pred != null) {
			
			double sel = getSelectivity(statistics, pred);
			two.setCardinality((int)(statistics.getCardinality() * sel));
			
			for (String rp: statistics.getValueProfiles().keySet()) {
				BucketProfile bp = (BucketProfile)statistics.getValueProfiles().get(rp);
				
				two.getValueProfiles().put(rp, bp.filterBy(sel));
			}
		}
		
		if (two.getCardinality() < 1)
			two.setCardinality(1);
		
		return two;
	}
}

/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.optimizer.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.collections.impl.set.sorted.mutable.TreeSortedSet;

import com.google.common.collect.HashMultimap;

import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.optimizer.memo.PlanSubgraph;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * A query graph captures the lists of fields (with common sources) and
 * the predicates among them.
 * 
 * @author ZacharyIves
 *
 * @param <T>
 */
public class QueryGraph<T extends TableWithVariableSchema> {
	LinkedHashSet<HyperEdge<T>> edges = new LinkedHashSet<>();
	SortedSet<Node<T>> nodes = new TreeSet<>();
	HashMultimap<String, Node<T>> fieldMap = HashMultimap.create();
	
	Map<String,Node<T>> nodesByName = new HashMap<>();
	
	HashMultimap<Node<T>,HyperEdge<T>> outgoing = HashMultimap.create();
	HashMultimap<Node<T>,HyperEdge<T>> incoming = HashMultimap.create();
	
	QueryOperator<T> blockRoot;
	String name;

	Map<String, String> symbolTable;
	
	PlanSubgraph<?,T> optimal = null;
	
	public LinkedHashSet<HyperEdge<T>> getEdges() {
		return edges;
	}
	public void setEdges(Set<HyperEdge<T>> edges) {
		for (HyperEdge<T> edge: edges)
			addEdge(edge);
	}
	public void addEdge(HyperEdge<T> edge) {
		edges.add(edge);
		
		for (Node<T> node: edge.getFrom())
			outgoing.put(node, edge);
		
		for (Node<T> node: edge.getTo())
			incoming.put(node, edge);
	}
	
	public Set<String> getSources() {
		return fieldMap.keySet();
	}
	
	public SortedSet<Node<T>> getNodes() {
		return nodes;
	}
	public void setNodes(Set<Node<T>> nodes) {
		this.nodes.clear();
		fieldMap = HashMultimap.create();
		
		for (Node<T> n: nodes)
			addNode(n);
	}
	public void addNode(Node<T> node) {
		nodes.add(node);
		fieldMap.put(node.getSource().getId().toString(), node);
		nodesByName.put(node.toString(), node);
	}
	
	/**
	 * All graph nodes corresponding to fields of a single source
	 * 
	 * @param source
	 * @return
	 */
	public Set<Node<T>> getNodesForSource(QueryOperator<T> source) {
		return fieldMap.get(source.getId().toString());
	}
	
	/**
	 * There is assumed to be one query graph per "block". The
	 * root of the block corresponds to a particular operator.
	 * 
	 * @return
	 */
	public QueryOperator<T> getBlockRoot() {
		return blockRoot;
	}
	/**
	 * There is assumed to be one query graph per "block". The
	 * root of the block corresponds to a particular operator.
	 */
	public void setBlockRoot(QueryOperator<T> blockRoot) {
		this.blockRoot = blockRoot;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Assert that these two nodes are equivalent, e.g.,
	 * because one is a rename of another, or because they
	 * are constrained to be equal through a join / equality
	 * constraint.
	 * 
	 * @param n1
	 * @param n2
	 */
	public void makeEquivalent(Node<T> n1, Node<T> n2) {
		for (Node<T> eq : n1.getEquivalents())
			eq.addEquivalent(n2);
		n1.addEquivalent(n2);
		for (Node<T> eq : n2.getEquivalents())
			eq.addEquivalent(n1);
		n2.addEquivalent(n1);
	}

	/**
	 * Get all edges originating from a node (or its equivalents)
	 * 
	 * @param n
	 * @return
	 */
	public Set<HyperEdge<T>> getEdgesFrom(Node<T> n) {
		Set<HyperEdge<T>> ret = new HashSet<>();
		
		ret.addAll(getEdgesExactlyFrom(n));
		for (Node<T> eq: n.getEquivalents())
			ret.addAll(getEdgesExactlyFrom(eq));
		
		return ret;
	}
	
	public Set<HyperEdge<T>> getEdgesExactlyFrom(Node<T> node) {
		return outgoing.get(node);
	}
	
	/**
	 * Get all edges ending at a node (or its equivalents)
	 * 
	 * @param n
	 * @return
	 */
	public Set<HyperEdge<T>> getEdgesTo(Node<T> n) {
		Set<HyperEdge<T>> ret = new HashSet<>();
		
		if (n != null) {
			ret.addAll(getEdgesExactlyTo(n));
			for (Node<T> eq: n.getEquivalents())
				ret.addAll(getEdgesExactlyTo(eq));
		}
		
		return ret;
	}

	public Set<HyperEdge<T>> getEdgesExactlyTo(Node<T> node) {
		return incoming.get(node);
	}

	public QueryGraph(Set<HyperEdge<T>> edges, SortedSet<Node<T>> nodes, QueryOperator<T> blockRoot, String name) {
		super();
		this.edges.addAll(edges);
		this.nodes = nodes;
		this.blockRoot = blockRoot;
		this.name = name;
	}
	
	public QueryGraph(QueryOperator<T> blockRoot, String name) {
		this(new TreeSortedSet<>(), new TreeSortedSet<>(), blockRoot, name);
	}
	public Node<T> getNode(String nw) {
		return nodesByName.get(nw);
	}
	
	public void setSymbolTable(Map<String, String> symbolTable) {
		this.symbolTable = symbolTable;
	}
	
	public Map<String,String> getSymbolTable() {
		return this.symbolTable;
	}
	
	public void setPlanGraph(PlanSubgraph<?, T> planSubgraph) {
		optimal = planSubgraph;
	}
	
	public PlanSubgraph<?,T> getPlanGraph() {
		return optimal;
	}
}

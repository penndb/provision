/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.optimizer.graph;

import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class Node<T extends TableWithVariableSchema> implements Comparable<Node<T>> {
	QueryOperator<T> source;
	
	String fieldName;
	Set<Node<T>> equivalents = new HashSet<>();
	
	public QueryOperator<T> getSource() {
		return source;
	}

	public void setSource(QueryOperator<T> source) {
		this.source = source;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Node(QueryOperator<T> source, String fieldName) {
		super();
		this.source = source;
		this.fieldName = fieldName;
	}
	
	
	@Override
	public int hashCode() {
		return fieldName.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Node))
			return false;
		
		Node<T> o2 = (Node<T>)o;
		return fieldName.equals(o2.fieldName) && source.getId().equals(o2.source.getId());
	}
	
	public void addEquivalent(Node<T> second) {
		equivalents.add(second);
	}
	
	public Set<Node<T>> getEquivalents() {
		if (equivalents != null)
			return equivalents;
		else
			return new HashSet<Node<T>>();
	}
	
	@Override
	public String toString() {
		return source.getId().toString() + "/" + fieldName; 
	}

	@Override
	public int compareTo(Node<T> arg0) {
		return toString().compareTo(arg0.toString());
	}
}

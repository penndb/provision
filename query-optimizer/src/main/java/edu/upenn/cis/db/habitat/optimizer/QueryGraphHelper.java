package edu.upenn.cis.db.habitat.optimizer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.python.google.common.collect.Sets;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTupleBooleanExpression;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTupleBooleanExpression;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.engine.operators.BlockingOperator;
import edu.upenn.cis.db.habitat.engine.operators.CartesianOperator;
import edu.upenn.cis.db.habitat.engine.operators.FieldExtractionOperator;
import edu.upenn.cis.db.habitat.engine.operators.GroupByOperator;
import edu.upenn.cis.db.habitat.engine.operators.JoinOperator;
import edu.upenn.cis.db.habitat.engine.operators.MaterializeOperator;
import edu.upenn.cis.db.habitat.engine.operators.ProjectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.engine.operators.RenameOperator;
import edu.upenn.cis.db.habitat.engine.operators.SelectionOperator;
import edu.upenn.cis.db.habitat.optimizer.graph.HyperEdge;
import edu.upenn.cis.db.habitat.optimizer.graph.Node;
import edu.upenn.cis.db.habitat.optimizer.graph.QueryGraph;
import edu.upenn.cis.db.habitat.optimizer.memo.PlanSubgraph;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class QueryGraphHelper {
	static Logger logger = LogManager.getLogger(QueryGraphHelper.class);

	/**
	 * Which column symbols are referenced?
	 * 
	 * @param <P>
	 * @param <T>
	 * @param symTab
	 * @param node
	 * @param g
	 * @return
	 */
	public static <P, T extends TableWithVariableSchema> List<String> 
		getReferencedSymbols(Map<String,String> symTab, PlanSubgraph<P,T> node, 
			QueryGraph<T> g) {
		BasicSchema schema = node.getSchema();
		
		Set<Node<T>> refd = new HashSet<>();
		
		Set<Node<T>> schemaNodes = new HashSet<>();
		
		for (String sch: schema.getKeys()) {
			Node<T> n = g.getNode(sch);
			if (n != null) {
				schemaNodes.add(n);
				schemaNodes.addAll(n.getEquivalents());
			}
		}
		
		// Expand for any symbols in the symbol table that don't map directly to fully qualified names
		for (String key: g.getSymbolTable().keySet()) {
			if (g.getNode(key) != null && schemaNodes.contains(g.getNode(g.getSymbolTable().get(key)))) {
				schemaNodes.add(g.getNode(key));
			}
		}

		// Let's see if any of the variables participates in an expression that is outside
		Set<Node<T>> refdNodes = new HashSet<>();
		for (Node<T> n: schemaNodes) {
//			Node<T> n = g.getNode(name);
			boolean added = false;
			if (n != null) {
				// n is needed for field extraction and the like?
				Set<SortedSet<Node<T>>> predNodes =
						g.getEdgesFrom(n)
						.stream().map(he -> he.getTo()).collect(Collectors.toSet());
				
				for (SortedSet<Node<T>> n2: predNodes)
					if (!schemaNodes.containsAll(n2) && !refd.contains(n.getFieldName())) {
						refd.add(n);//.getFieldName());
						added = true;
						break;
					}

				if (added)
					continue;
				
				predNodes =
						g.getEdgesTo(n)
						.stream().map(he -> he.getFrom()).collect(Collectors.toSet());
				
				for (SortedSet<Node<T>> n2: predNodes)
					if (!schemaNodes.containsAll(n2) && !refd.contains(n.getFieldName())) {
						refd.add(n);//.getFieldName());
						added = true;
						break;
					}
				
				if (added)
					continue;
				
				if (symTab.values().contains(n.getFieldName()) && !refd.contains(n.getFieldName()))
					refd.add(n);//.getFieldName());
			}
		}
		
		List<String> names = new ArrayList<>();
		for (String sch: schema.getKeys())
			if (refd.contains(g.getNode(sch)))
				names.add(sch);

		return names;
	}


	public static <T extends TableWithVariableSchema> Map<String,String> 
	addEdges(
			QueryOperator<T> operator, 
			Collection<QueryOperator<T>> boundaries, 
			QueryGraph<T> graph) {
		Map<String,String> ret = new HashMap<>();
	
		// Get the symbol table from the child
		List<Map<String,String>> childList = new ArrayList<>();
		if (!boundaries.contains(operator) || (operator instanceof RenameOperator) || 
				(operator instanceof FieldExtractionOperator)) {
			for (QueryOperator<T> child: operator.getChildren()) {
				childList.add(addEdges(child, boundaries, graph));
			}
		}
//		if (!childList.isEmpty())
		for (Map<String,String> childSymbols: childList)
			ret.putAll(childSymbols);

		// Add a mapping for each symbol that comes from a subquery 
		if (boundaries.contains(operator)) {
			for (String attr: operator.getAdditionalSchema().getKeys()) {
				if (attr.equals(ProvenanceApi.Provenance) || attr.equals(ProvenanceApi.Token))
					continue;
				if (!ret.containsKey(attr))
					ret.put(attr, operator.getId().toString() + "/" + attr);
			}
		}
		
		// Now start adding
		
		if (operator instanceof RenameOperator) {
			RenameOperator<T> ren = (RenameOperator<T>)operator;
			
			Map<String,String> map = ren.getFieldMap();
			for (String attr: map.keySet()) {
				if (attr.equals(ProvenanceApi.Provenance) || attr.equals(ProvenanceApi.Token))
					continue;
				
				String old = ret.get(attr);
				String nw = ren.getId() + "/" + map.get(attr);
				
				logger.debug("Linking " + old + " to " + nw);
				graph.addEdge(new HyperEdge<T>(graph.getNode(old), graph.getNode(nw), false));
				
				graph.getNode(old).addEquivalent(graph.getNode(nw));
				graph.getNode(nw).addEquivalent(graph.getNode(old));
			}
		} else if (operator instanceof FieldExtractionOperator) {
			@SuppressWarnings("unchecked")
			FieldExtractionOperator<T,?,?> fe = (FieldExtractionOperator<T,?,?>)operator;
			
			// Right now we assume we depend on EVERYTHING
			Set<Node<T>> dependsOn = new HashSet<>();
			for (String nodeId: ret.values()) {
				if (nodeId.endsWith("/" + ProvenanceApi.Provenance) || nodeId.endsWith("/" + ProvenanceApi.Token))
					continue;
				if (nodeId.startsWith(operator.getId().toString()))
					continue;
				dependsOn.add(graph.getNode(nodeId));
			}
			
			Set<Node<T>> newSymbols = Sets.newHashSet();
			for (String newAttr: fe.getAdditionalSchema().getKeys()) {
				if (newAttr.equals(ProvenanceApi.Provenance) || newAttr.equals(ProvenanceApi.Token))
					continue;
				
				newSymbols.add(graph.getNode(fe.getId() + "/" + newAttr));
			}
			
			logger.debug("Feeding " + dependsOn + " to " + newSymbols);
			graph.addEdge(new HyperEdge<T>(dependsOn, newSymbols, false, null));
		} else if (operator instanceof JoinOperator) {
			JoinOperator<T> join = (JoinOperator<T>)operator;
			
			// Get the left and right mappings
			for (int i = 0; i < join.getLeftJoinKeys().size(); i++) {
				String lf = childList.get(0).get(join.getLeftJoinKeys().get(i));
				String rt = childList.get(1).get(join.getRightJoinKeys().get(i));

				logger.debug("Joining " + lf + " to " + rt);
				graph.addEdge(new HyperEdge<T>(graph.getNode(lf), graph.getNode(rt), true));
			}
			
			addJoinPredicateEdges(join.getPredicate(), childList.get(0), childList.get(1), graph);
			ret.putAll(childList.get(0));
			ret.putAll(childList.get(1));
		} else if (operator instanceof CartesianOperator) {
			ret.putAll(childList.get(1));
		} else if (operator instanceof SelectionOperator) {
			SelectionOperator<T> sel = (SelectionOperator<T>)operator;
			
			addSelectionPredicateEdges(sel.getPredicate(), ret, graph);
			ret.putAll(childList.get(0));
		} else if (operator instanceof ProjectionOperator) {
			ProjectionOperator<T> proj = (ProjectionOperator<T>)operator;
			
			List<String> projected = proj.getOutputSchemas().iterator().next().getKeys(); 

//			Map<String,String> ret2 = new HashMap<>();
//			for (String key: ret.keySet())
//				if (projected.contains(key))
//					ret2.put(key, ret.get(key));
//			
//			ret = ret2;
				
		} 

		return ret;
	}
	
	/**
	 * Create edges, etc for selection predicates
	 * 
	 * @param pred
	 * @param nameMappings Symbol table  (attr -> node)
	 * @param graph
	 */
	public static <T extends TableWithVariableSchema> void addSelectionPredicateEdges(UnaryTuplePredicateWithLookup pred, Map<String,String> nameMappings, QueryGraph<T> graph) {
		if (pred instanceof UnaryTupleBooleanExpression && 
				((UnaryTupleBooleanExpression)pred).getOperation() == PredicateOperations.BooleanOperation.And) {
			addSelectionPredicateEdges(((UnaryTupleBooleanExpression)pred).getLeft(), nameMappings, graph);
			addSelectionPredicateEdges(((UnaryTupleBooleanExpression)pred).getRight(), nameMappings, graph);
		} else
			addSelectionPredicateEdgesConjunct(pred, nameMappings, graph);
	}

	public static <T extends TableWithVariableSchema> void addSelectionPredicateEdgesConjunct(UnaryTuplePredicateWithLookup pred, Map<String,String> nameMappings, QueryGraph<T> graph) {
		Set<String> symbolsRefd = pred.getSymbolsReferenced();
		
		// TODO: if it's a conjunction at the top level, we can break it into separate predicates!
		
		Set<Node<T>> nodes = Sets.newHashSet();
		for (String sym: symbolsRefd) {
			nodes.add(graph.getNode(nameMappings.get(sym)));
		}
		
		if (!nodes.isEmpty()) {
			logger.debug("Selection Predicate edge: " + nodes.toString() + ": " + pred.toString());
			graph.addEdge(new HyperEdge<T>(nodes, pred));
		}
	}
	
	/**
	 * Create edges for join predicates
	 * @param pred
	 * @param leftMappings Left-child symbol table (attr -> node)
	 * @param rightMappings Right-child symbol table (attr -> node)
	 * @param graph
	 */
	public static <T extends TableWithVariableSchema> void addJoinPredicateEdges(BinaryTuplePredicateWithLookup pred, Map<String,String> leftMappings, 
			Map<String,String> rightMappings, QueryGraph<T> graph) {
		if (pred instanceof BinaryTupleBooleanExpression && 
				((BinaryTupleBooleanExpression)pred).getOperation() == PredicateOperations.BooleanOperation.And) {
			addJoinPredicateEdges(((BinaryTupleBooleanExpression)pred).getLeft(), leftMappings, rightMappings, graph);
			addJoinPredicateEdges(((BinaryTupleBooleanExpression)pred).getRight(), leftMappings, rightMappings, graph);
		} else
			addJoinPredicateEdgesConjunct(pred, leftMappings, rightMappings, graph);
	}

	public static <T extends TableWithVariableSchema> void 
	addJoinPredicateEdgesConjunct(
			BinaryTuplePredicateWithLookup pred, Map<String,String> leftMappings, 
			Map<String,String> rightMappings, QueryGraph<T> graph) {
		Set<String> lSymbolsRefd = pred.getSymbolsReferencedLeft();
		Set<String> rSymbolsRefd = pred.getSymbolsReferencedRight();
		
		// TODO: if it's a conjunction at the top level, we can break it into separate predicates!
		
		Set<Node<T>> lNodes = Sets.newHashSet();
		Set<Node<T>> rNodes = Sets.newHashSet();
		for (String sym: lSymbolsRefd) {
			lNodes.add(graph.getNode(leftMappings.get(sym)));
		}
		for (String sym: rSymbolsRefd) {
			rNodes.add(graph.getNode(rightMappings.get(sym)));
		}
		
		if (!lNodes.isEmpty() || !rNodes.isEmpty()) {
			logger.debug("Join Predicate edge: " + lNodes.toString() + " to " + rNodes.toString() + ": " + pred.toString());
			graph.addEdge(new HyperEdge<T>(lNodes, rNodes, false, pred));
		}
	}
	
}

package edu.upenn.cis.db.habitat.sql.udfs;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Stream;

import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.JavaUdf;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class AvgUdf<T extends Number> extends JavaUdf<TableWithVariableSchema, TupleWithSchema<String>, T>  {
	private static final long serialVersionUID = 1L;
	
	Function<TupleWithSchema<String>, T> valueToAggregate;
	Class<?> typ;

	public AvgUdf(Class<T> cl, ArithmeticExpressionOperation<T> op) {
		super("avg", new BasicSchema("avg"), op, new BasicSchema("avg"));
		super.getArguments().addField("x", cl);
		
		super.getOutputSchema().addField("avg", cl);
		typ = cl;
		valueToAggregate = new GetExpressionFromTuple<T>(op);
	}

	public AvgUdf() {
		super("avg", new BasicSchema("avg"), new ArrayList<>(), new BasicSchema("vg"));
		super.getArguments().addField("x", Double.class);
		
		typ = Double.class;
		super.getOutputSchema().addField("avg", Double.class);
	}

	@Override
	public Function computeInput() {
		return valueToAggregate;
	}

	@Override
	public TupleWithSchema<String> apply(TableWithVariableSchema t) {
		Stream<T> str = t.stream().map(computeInput());
		
		
		T sum = str.reduce((T)Integer.valueOf(0), new BinaryOperator<T>() {

			@Override
			public T apply(T t, T u) {
				if (t instanceof Double || u instanceof Double)
					return (T)Double.valueOf(((Double) t).doubleValue() + ((Double) u).doubleValue());
				else
					return (T)Integer.valueOf(((Double) t).intValue() + ((Double) u).intValue());
			}
			
		});
		
		MutableTupleWithSchema<String> tup = super.getOutputSchema().createTuple();
		
		tup.setValue("avg", sum.doubleValue()/t.size());
		return tup;
	}

	@Override
	public AvgUdf<T> instantiate(List<ArithmeticExpressionOperation<T>> op) {
		return new AvgUdf<T>((Class<T>) typ, op.get(0));
	}

	public Function<TupleWithSchema<String>, T> getValueToAggregate() {
		return valueToAggregate;
	}

	public void setValueToAggregate(Function<TupleWithSchema<String>, T> valueToAggregate) {
		this.valueToAggregate = valueToAggregate;
	}

	
}

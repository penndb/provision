/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.optimizer.stats;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Simple equi-depth histogram
 * 
 * @author zives
 *
 * @param <T>
 */
public class BucketProfile<T extends Number & Comparable<T>> implements ResultProfile<T> {
	
	int[] counts;
	Class<T> type;
	List<T> bounds;
	
	double scaleFactor = 1.0;
	double duplicateFactor = 1.0;
	
	public BucketProfile() {}
	
	public BucketProfile(int numBuckets, Class<T> type, List<T> values) {
		counts = new int[numBuckets];
		this.type = type;
		bounds = new ArrayList<>(numBuckets+1);
		
		populateBuckets(values);
	}
	
	public BucketProfile(BucketProfile<T> two) {
		counts = new int[two.counts.length];
		
		scaleFactor = two.scaleFactor;
		duplicateFactor = two.duplicateFactor;
		
		for (int i = 0; i < counts.length; i++)
			counts[i] = two.counts[i];
		
		this.type = two.type;
		bounds = new ArrayList<>(counts.length + 1);
		
		for (int i = 0; i < counts.length+1; i++) {
			bounds.add(two.bounds.get(i));
		}
	}
	
	public void populateBuckets(List<T> values) {
		Collections.sort(values);
		
		bounds.clear();
		
		bounds.add(values.get(0));
		
		float interval = (float)values.size() / counts.length;
		
		int soFar = 0;
		for (int i = 1; i < counts.length; i++) {
			int pos = Math.round(i * interval);
			
			bounds.add(values.get(pos));
			counts[i-1] = pos - Math.round((i-1) * interval);
			soFar += counts[i-1];
		}
		if (counts.length > 1)
			counts[counts.length-1] = values.size() - soFar;
		else
			counts[counts.length-1] = values.size();
		bounds.add(values.get(values.size() - 1));
	}

	@Override
	@JsonIgnore
	public Class<T> getDataType() {
		return type;
	}

	@Override
	@JsonIgnore
	public int getCount() {
		return (int)Math.round(Arrays.stream(counts).sum() * scaleFactor * duplicateFactor);
	}
	
	@JsonIgnore
	public int getUniqueCount() {
		return (int)Math.round(Arrays.stream(counts).sum() * scaleFactor);
	}
	
	int interpolateLeft(Double val, Double min, Double max, int count) {
		double slices = (max.doubleValue() - min.doubleValue()) / count;
		int sliceCount = 0;
		
		for (int i = 0; i < count; i++) {
			double initialValue = slices * i;
			
			if (initialValue >= val)
				sliceCount++;
		}
		
		return sliceCount;
	}

	int interpolateRight(Double val, Double min, Double max, int count) {
		double slices = (max.doubleValue() - min.doubleValue()) / count;
		int sliceCount = 0;
		
		for (int i = 0; i < count; i++) {
			double initialValue = slices * i;
			
			if (initialValue <= val)
				sliceCount++;
		}
		
		return sliceCount;
	}

	int interpolateLeft(Float val, Float min, Float max, int count) {
		float slices = (max.floatValue() - min.floatValue()) / (float)count;
		int sliceCount = 0;
		
		for (int i = 0; i < count; i++) {
			float initialValue = slices * i;
			
			if (initialValue >= val)
				sliceCount++;
		}
		
		return sliceCount;
	}

	int interpolateRight(Float val, Float min, Float max, int count) {
		float slices = (max.floatValue() - min.floatValue()) / (float)count;
		int sliceCount = 0;
		
		for (int i = 0; i < count; i++) {
			float initialValue = slices * i;
			
			if (initialValue <= val)
				sliceCount++;
		}
		
		return sliceCount;
	}

	int interpolateLeft(Integer val, Integer min, Integer max, int count) {
		float slices = (max.intValue() - min.intValue()) / (float)count;
		int sliceCount = 0;
		
		for (int i = 0; i < count; i++) {
			int initialValue = Math.round(slices * i);
			
			if (initialValue >= val)
				sliceCount++;
		}
		
		return sliceCount;
	}

	int interpolateRight(Integer val, Integer min, Integer max, int count) {
		float slices = (max.intValue() - min.intValue()) / (float)count;
		int sliceCount = 0;
		
		for (int i = 0; i < count; i++) {
			int initialValue = Math.round(slices * i);
			
			if (initialValue <= val)
				sliceCount++;
		}
		
		return sliceCount;
	}
	
	int interpolateLeft(Long val, Long min, Long max, int count) {
		double slices = (max.longValue() - min.longValue()) / (double)count;
		int sliceCount = 0;
		
		for (int i = 0; i < count; i++) {
			long initialValue = Math.round(slices * i);
			
			if (initialValue >= val)
				sliceCount++;
		}
		
		return sliceCount;
	}

	int interpolateRight(Long val, Long min, Long max, int count) {
		double slices = (max.longValue() - min.longValue()) / (double)count;
		int sliceCount = 0;
		
		for (int i = 0; i < count; i++) {
			long initialValue = Math.round(slices * i);
			
			if (initialValue <= val)
				sliceCount++;
		}
		
		return sliceCount;
	}
	
	@Override
	@JsonIgnore
	public int getCountInRange(T start, T end) {

		// Go through each bucket
		
		int bucket = 0;
		
		int matches = 0;
		
		// Advance through item list until we are at least at a match for 
		// left of the histogram
		while (bucket +1 < bounds.size() && start.compareTo(bounds.get(bucket+1)) > 0)
			bucket++;
		
		// TODO: Interpolate leading bucket
		if (bucket < bounds.size() && bounds.get(bucket+1).compareTo(start) > 0) {
			if (start instanceof Integer)
				matches += interpolateLeft((Integer)start, (Integer)bounds.get(bucket), (Integer)bounds.get(bucket+1), counts[bucket]);
			else if (start instanceof Long)
				matches += interpolateLeft((Long)start, (Long)bounds.get(bucket), (Long)bounds.get(bucket+1), counts[bucket]);
			else if (start instanceof Double)
				matches += interpolateLeft((Double)start, (Double)bounds.get(bucket), (Double)bounds.get(bucket+1), counts[bucket]);
			else if (start instanceof Float)
				matches += interpolateLeft((Float)start, (Float)bounds.get(bucket), (Float)bounds.get(bucket+1), counts[bucket]);
			bucket++;
		}

		while (bucket < counts.length && bounds.get(bucket).compareTo(end) < 0) {
			matches += counts[bucket];
			bucket++;
		}

		// TODO: Interpolate trailing bucket
		if (bucket < bounds.size() && bounds.get(bucket).compareTo(end) > 0
				&& (bucket > 0 && bounds.get(bucket-1).compareTo(end) < 0)) {
			if (start instanceof Integer)
				matches += interpolateRight((Integer)end, (Integer)bounds.get(bucket-1), (Integer)bounds.get(bucket), counts[bucket-1]);
			else if (start instanceof Long)
				matches += interpolateRight((Long)end, (Long)bounds.get(bucket-1), (Long)bounds.get(bucket), counts[bucket-1]);
			else if (start instanceof Double)
				matches += interpolateRight((Double)end, (Double)bounds.get(bucket-1), (Double)bounds.get(bucket), counts[bucket-1]);
			else if (start instanceof Float)
				matches += interpolateRight((Float)end, (Float)bounds.get(bucket-1), (Float)bounds.get(bucket), counts[bucket-1]);
		}
		
		return (int)(matches * scaleFactor * duplicateFactor);
	}

	@Override
	@JsonIgnore
	public int getCountMatching(Collection<T> items) {
		List<T> itemList = new ArrayList<T>(items.size());
		itemList.addAll(items);
		Collections.sort(itemList);
		
		// Go through each bucket
		
		int j = 0;
		
		int matches = 0;
		
		// Advance through item list until we are at least at a match for 
		// left of the histogram
		while (itemList.get(j).compareTo(bounds.get(0)) < 0 && j < itemList.size())
			j++;
		for (int i = 0; i < counts.length; i++) {
			int count = 0;
			
			// We are going to do a super-conservative thing, which is to count each probe
			// within a bucket as likely matching the value.
			//
			//
			// If we instead focus on integers ONLY, we could compute the probability that the bucket
			// contains an entry at each point.
			while (itemList.get(j).compareTo(bounds.get(i+1)) < 0 && j < itemList.size()) {
				j++;
				count++;
			}
			if (count < counts[i])
				matches = count;
			else
				matches = counts[i];
		}
		
		return (int)(matches * scaleFactor * duplicateFactor);
	}

	@JsonIgnore
	public T getMax() {
		return bounds.get(bounds.size()-1);
	}

	@JsonIgnore
	public T getMin() {
		return bounds.get(0);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(Double.toString(scaleFactor) + "*" + Double.toString(duplicateFactor) + "*");
		for (int i = 0; i < counts.length; i++) {
			sb.append("[" + bounds.get(i) + "..(" + counts[i] + ")");
			if (i < counts.length - 1)
				sb.append(']');
		}
		sb.append(bounds.get(counts.length) + "]");
		
		return sb.toString();
	}
	
	/**
	 * Scale down each bucket count by a ratio
	 * @param factor
	 * @return
	 */
	public BucketProfile<T> filterBy(double factor) {
		BucketProfile<T> scaled = new BucketProfile<>(this);
		
		scaled.scaleFactor = this.scaleFactor * factor;
		return scaled;
	}

	/**
	 * Scale up each bucket count by a ratio
	 * @param factor
	 * @return
	 */
	public BucketProfile<T> duplicateBy(double factor) {
		BucketProfile<T> scaled = new BucketProfile<>(this);
		
		scaled.duplicateFactor = this.duplicateFactor * factor;
		return scaled;
	}

	public int[] getCounts() {
		return counts;
	}

	public void setCounts(int[] counts) {
		this.counts = counts;
	}

	public Class<T> getType() {
		return type;
	}

	public void setType(Class<T> type) {
		this.type = type;
	}

	public List<T> getBounds() {
		return bounds;
	}

	public void setBounds(List<T> bounds) {
		this.bounds = bounds;
	}

	public double getScaleFactor() {
		return scaleFactor;
	}

	public void setScaleFactor(double scaleFactor) {
		this.scaleFactor = scaleFactor;
	}

	public double getDuplicateFactor() {
		return duplicateFactor;
	}

	public void setDuplicateFactor(double duplicateFactor) {
		this.duplicateFactor = duplicateFactor;
	}
	
	
}

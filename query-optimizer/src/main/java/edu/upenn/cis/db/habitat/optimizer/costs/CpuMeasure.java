package edu.upenn.cis.db.habitat.optimizer.costs;

public class CpuMeasure implements Measure {
	
	double value = 0;
	
	public CpuMeasure(double pctCpu) {
		value = pctCpu;
	}

	@Override
	public double getValue() {
		return value;
	}

	@Override
	public void setValue(double value) {
		this.value = value;
	}

}

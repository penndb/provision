package edu.upenn.cis.db.habitat.sql;

import java.io.IOException;
import java.io.PrintStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import edu.upenn.cis.db.habitat.catalog.SystemCatalog;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.Metadata;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.alter.Alter;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;

public class SqlDDLManager {
	SystemCatalog catalog = null;
	PrintStream output = System.out;
	
	public SqlDDLManager(String path, PrintStream output) {
		catalog = new SystemCatalog(path);
		this.output = output;
	}

	public SqlDDLManager(String path) {
		catalog = new SystemCatalog(path);
	}
	
	public SystemCatalog getCatalog() {
		return catalog;
	}
	
	public int process(String sql) throws JSQLParserException, JsonProcessingException {
		Statement st = SqlExpressionBuilder.parseSQL(sql);

		return process(st);
	}
	
	public int process(Statement statement) throws JsonProcessingException {
		
		if (statement instanceof CreateTable) {
			CreateTable ct = (CreateTable)statement;
			
			String tableName = ct.getTable().getFullyQualifiedName();
			List<String> fields = new ArrayList<>();
			List<Class<? extends Object>> types = new ArrayList<>();
			
			for (ColumnDefinition col: ct.getColumnDefinitions()) {
				fields.add(col.getColumnName());
				
				String typ = col.getColDataType().getDataType().toLowerCase();
				if (typ.startsWith("varchar")) {
					types.add(String.class);
				} else if (typ.equals("double")) {
					types.add(Double.class);
				} else if (typ.equals("int") || typ.equals("integer")) {
					types.add(Integer.class);
				} else if (typ.equals("bool") || typ.equals("boolean")) {
					types.add(Boolean.class);
				} else if (typ.equals("date") || typ.equals("datetime")) {
					types.add(Date.class);
				} else
					throw new UnsupportedOperationException("No support for type: " + typ);
				
			}
			if (tableName.startsWith("\"") && tableName.endsWith("\""))
				tableName = tableName.substring(1, tableName.length() - 1);
			else if (tableName.startsWith("'") && tableName.endsWith("'"))
				tableName = tableName.substring(1, tableName.length() - 1);
			else if (tableName.startsWith("[") && tableName.endsWith("]"))
				tableName = tableName.substring(1, tableName.length() - 1);
			
			BasicSchema schema = new BasicSchema(tableName,
					fields,
					types);
			
			catalog.writeSchema(schema);
			output.println("CREATE TABLE: " + schema.toString());
		} else if (statement instanceof Alter) {
			
		}
		return 0;
	}
	
	/**
	 * Return metadata from the catalog
	 * 
	 * @param name
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public Metadata getMetadata(String name) throws JsonParseException, JsonMappingException, IOException {
		return catalog.getMetadata(name);
	}

	/**
	 * Updates a schema entry with a primary key
	 * 
	 * @param name
	 * @param keys
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public void setPrimaryKey(String name, List<String> keys) throws JsonParseException, JsonMappingException, IOException {
		BasicSchema sch = catalog.getSchema(name);
		
		sch.setLookupKeys(keys);
		
		catalog.writeSchema(sch);
	}

	/**
	 * Updates a schema entry with a primary key
	 * 
	 * @param name
	 * @param keys
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public void collectStatistics(String name, List<String> keys) throws JsonParseException, JsonMappingException, IOException {
		BasicSchema sch = catalog.getSchema(name);
		
		for (String str: keys)
			sch.addLookupKey(str);
		
		catalog.writeSchema(sch);
	}
}

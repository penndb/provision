package edu.upenn.cis.db.habitat.sql.udfs;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Stream;

import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.JavaUdf;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class SumUdf<T extends Number> extends JavaUdf<TableWithVariableSchema, TupleWithSchema<String>, T> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Function<TupleWithSchema<String>, T> valueToAggregate;
	Class<?> typ;
	
	public SumUdf(Class<T> cl, ArithmeticExpressionOperation<T> op) {
		super("sum", new BasicSchema("sum"), op, new BasicSchema("sum"));
		super.getArguments().addField("x", cl);
		
		super.getOutputSchema().addField("sum", cl);
		typ = cl;
		valueToAggregate = new GetExpressionFromTuple<T>(op);
	}
	
	public SumUdf() {
		super("sum", new BasicSchema("sum"), new ArrayList<>(), new BasicSchema("sum"));
		super.getArguments().addField("x", Double.class);
		
		typ = Double.class;
		super.getOutputSchema().addField("sum", Double.class);
	}
	
	@Override
	public SumUdf<T> instantiate(List<ArithmeticExpressionOperation<T>> op) {
		return new SumUdf<T>((Class<T>)typ, op.get(0));
	}

	@Override
	public TupleWithSchema<String> apply(TableWithVariableSchema t) {
		Stream<T> str = t.stream().map(computeInput());
		
		
		T sum = str.reduce((T)Integer.valueOf(0), new BinaryOperator<T>() {

			@Override
			public T apply(T t, T u) {
				if (t instanceof Double || u instanceof Double)
					return (T)Double.valueOf(t.doubleValue() + u.doubleValue());
				else
					return (T)Integer.valueOf(t.intValue() + u.intValue());
			}
			
		});
		
		MutableTupleWithSchema<String> tup = super.getOutputSchema().createTuple();
		
		tup.setValue("sum", sum);
		return tup;
	}

	@Override
	public Function<TupleWithSchema<String>, T> computeInput() {
		return valueToAggregate;
	}

	public Function<TupleWithSchema<String>, T> getValueToAggregate() {
		return valueToAggregate;
	}

	public void setValueToAggregate(Function<TupleWithSchema<String>, T> valueToAggregate) {
		this.valueToAggregate = valueToAggregate;
	}

}

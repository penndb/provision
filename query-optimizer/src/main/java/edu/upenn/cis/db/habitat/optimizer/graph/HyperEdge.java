/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.optimizer.graph;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import org.eclipse.collections.impl.set.sorted.mutable.TreeSortedSet;

import com.google.common.collect.Lists;

import edu.upenn.cis.db.habitat.core.expressions.BinaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class HyperEdge<T extends TableWithVariableSchema> {
	SortedSet<Node<T>> from = new TreeSortedSet<>();
	SortedSet<Node<T>> to = new TreeSortedSet<>();
	boolean symmetric;
	
	BinaryTuplePredicateWithLookup predicate;
	UnaryTuplePredicateWithLookup unary;

	public SortedSet<Node<T>> getFrom() {
		return from;
	}

	public void setFrom(Set<Node<T>> from) {
		this.from.clear();
		this.from.addAll(from);
	}

	public SortedSet<Node<T>> getTo() {
		return to;
	}

	public void setTo(Set<Node<T>> to) {
		this.to.clear();
		this.to.addAll(to);
	}

	public boolean isSymmetric() {
		return symmetric;
	}

	public void setSymmetric(boolean symmetric) {
		this.symmetric = symmetric;
	}

	public BinaryTuplePredicateWithLookup getPredicate() {
		return predicate;
	}

	public void setPredicate(BinaryTuplePredicateWithLookup predicate) {
		this.predicate = predicate;
	}
	
	public UnaryTuplePredicateWithLookup getUnaryPredicate() {
		return unary;
	}

	public HyperEdge(Collection<Node<T>> from, Collection<Node<T>> to, boolean symmetric,
			BinaryTuplePredicateWithLookup predicate) {
		super();
		this.from.addAll(from);
		this.to.addAll(to);
		this.symmetric = symmetric;
		this.predicate = predicate;
	}
	
	public HyperEdge(Node<T> from, Node<T> to, boolean symmetric) {
		super();
		this.from.add(from);
		this.to.add(to);
		this.symmetric = symmetric;
		this.predicate = null;
	}

	public HyperEdge(Collection<Node<T>> symbolsRefd, UnaryTuplePredicateWithLookup pred) {
		super();
		this.from = new TreeSortedSet<>();//Lists.newArrayList();
		this.from.addAll(symbolsRefd);
		
		this.to = new TreeSortedSet<>();
		this.symmetric = false;
		this.unary = pred;
	}
	
	public String toString() {
		if (predicate != null)
			return predicate.toString();
		else if (unary != null)
			return unary.toString();
		else
			return from.toString() + ((symmetric) ? "<->" : " -> ") + to.toString();
	}
}

/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.optimizer.rewrites;

import java.util.Iterator;

import edu.upenn.cis.db.habitat.optimizer.memo.PlanSubgraph;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class CommutativeRule<P,T extends TableWithVariableSchema> extends RewriteRule<P,T> {

	@Override
	public Iterator<PlanSubgraph<P,T>> getAlternatives(PlanSubgraph<P,T> currentPlan) {
		// TODO Auto-generated method stub
		return null;
	}

}

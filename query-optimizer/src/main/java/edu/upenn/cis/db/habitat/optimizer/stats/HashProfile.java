/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.optimizer.stats;

import java.util.Collection;

@Deprecated
public class HashProfile<T> implements ResultProfile<T> {

	@Override
	public Class<T> getDataType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getCountInRange(Object start, Object end) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getCountMatching(Collection<T> items) {
		// TODO Auto-generated method stub
		return 0;
	}

}

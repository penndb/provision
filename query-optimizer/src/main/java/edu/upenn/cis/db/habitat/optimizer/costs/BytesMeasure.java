package edu.upenn.cis.db.habitat.optimizer.costs;

public class BytesMeasure implements Measure {
	
	double value = 0;
	
	public BytesMeasure(double bytes) {
		value = bytes;
	}

	@Override
	public double getValue() {
		return value;
	}

	@Override
	public void setValue(double value) {
		this.value = value;
	}

}

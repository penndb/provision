package edu.upenn.cis.db.habitat.sql;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.collect.Sets;

import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionLiteral;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation.ArithmeticOperation;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionVariable;
import edu.upenn.cis.db.habitat.core.expressions.BinaryArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.expressions.BinaryFieldComparisonPredicate;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTupleBooleanExpression;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations;
import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations.BooleanOperation;
import edu.upenn.cis.db.habitat.core.expressions.UnaryFieldComparisonPredicate;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTupleBooleanExpression;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.JavaUdf;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.Udf;
import edu.upenn.cis.db.habitat.engine.operators.CartesianOperator;
import edu.upenn.cis.db.habitat.engine.operators.GroupByOperator;
import edu.upenn.cis.db.habitat.engine.operators.JoinOperator;
import edu.upenn.cis.db.habitat.engine.operators.ProjectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.engine.operators.RenameOperator;
import edu.upenn.cis.db.habitat.engine.operators.SelectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.extract.CSVExtractor;
import edu.upenn.cis.db.habitat.engine.operators.extract.FastQExtractor;
import edu.upenn.cis.db.habitat.provenance.expressions.AggregateProvenance;
import edu.upenn.cis.db.habitat.querygen.QueryGen;
import edu.upenn.cis.db.habitat.querygen.exceptions.QueryParseException;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import edu.upenn.cis.db.habitat.sql.exceptions.TableNotFoundException;
import edu.upenn.cis.db.habitat.sql.parsing.TableAliasFinder;
import edu.upenn.cis.db.habitat.sql.udfs.RegisterBuiltInUdfs;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitorAdapter;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.ComparisonOperator;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.GroupByElement;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.util.TablesNamesFinder;

public class SqlExpressionBuilder {
	SqlDDLManager ddl;

	public SqlExpressionBuilder(String path) {
		ddl = new SqlDDLManager(path);
		RegisterBuiltInUdfs.registerAll(ddl.catalog);
	}

	public static Statement parseSQL(String sql) throws JSQLParserException {
		return CCJSqlParserUtil.parse(sql);
	}
	
	public SqlDDLManager getDDLManager() {
		return ddl;
	}
	
	public static String cleanTableName(String tableName) {
		if (tableName.startsWith("\"") && tableName.endsWith("\""))
			tableName = tableName.substring(1, tableName.length() - 1);
		else if (tableName.startsWith("'") && tableName.endsWith("'"))
			tableName = tableName.substring(1, tableName.length() - 1);
		else if (tableName.startsWith("[") && tableName.endsWith("]"))
			tableName = tableName.substring(1, tableName.length() - 1);
		
		return tableName;
	}
	
	boolean isConstOrLiteral(String term) {
		if (term == null || term.isEmpty())
			return false;
		
		if (term.startsWith("'") && term.endsWith("'"))
			return true;
		
		boolean isNum = true;
		int decimals = 0;
		for (int i = 0; i < term.length(); i++) {
			if (i == 0 && term.charAt(i) == '-')
				;			// Negative number
			else if (term.charAt(i) == '.' && decimals == 0)
				isNum &= (decimals++ == 0);
			else if (term.charAt(i) < '0' || term.charAt(i) > '9')
				isNum = false;
		}
		return isNum;
	}
	
	boolean isNumber(String term) {
		if (term == null || term.isEmpty())
			return false;
		
		boolean isNum = true;
		int decimals = 0;
		for (int i = 0; i < term.length(); i++) {
			if (i == 0 && term.charAt(i) == '-')
				;			// Negative number
			else if (term.charAt(i) == '.' && decimals == 0)
				isNum &= (decimals++ == 0);
			else if (term.charAt(i) < '0' || term.charAt(i) > '9')
				isNum = false;
		}
		return isNum;
	}

	/**
	 * Gets the table alias from a qualified field name
	 * 
	 * @param name
	 * @return
	 */
	public static String getAliasFromQualifiedName(String name, Map<String,Object> symbolTable) {
		if (symbolTable.get(name) != null && symbolTable.get(name) instanceof String)
			name = (String)symbolTable.get(name);
		
		if (name.lastIndexOf('.') >= 0) {
			return name.substring(0, name.lastIndexOf('.'));
		} else
			return null;
	}
	
	/**
	 * Gets the table alias from a qualified field name
	 * 
	 * @param name
	 * @return
	 */
	public static String getAttributeFromQualifiedName(String name, Map<String,Object> symbolTable) {
		if (symbolTable.get(name) != null && symbolTable.get(name) instanceof String)
			name = (String)symbolTable.get(name);
		
		if (name.lastIndexOf('.') >= 0) {
			return name.substring(name.lastIndexOf('.'));
		} else
			return null;
	}

	public static String getTableFromQualifiedName(String name, Map<String,Object> symbolTable) {
		if (symbolTable.get(name) != null && symbolTable.get(name) instanceof String)
			name = (String)symbolTable.get(name);
		
		if (name.lastIndexOf('.') >= 0) {
			return name.substring(0, name.lastIndexOf('.'));
		} else
			return null;
	}
	
	public static String getQualifiedName(String name, Map<String,Object> symbolTable) {
		if (symbolTable.get(name) != null && symbolTable.get(name) instanceof String)
			name = (String)symbolTable.get(name);

		return name;
	}
	
	public static Class getClassFromName(String name, Map<String,Object> symbolTable) {
		if (symbolTable.get(name) != null && symbolTable.get(name) instanceof String)
			name = (String)symbolTable.get(name);

		return (Class)symbolTable.get(name);
	}

	/**
	 * Get a list of RA expressions, one for each table source in the FROM clause.
	 * The list is in the order of the FROM clause.
	 * The sourceMap is between table alias and the expression.
	 * All fields are renamed to qualified (alias.field) form.
	 * 
	 * @param sel SQL select statement
	 * @param symbolTable Out parameter, unqualified to qualified names, qualified names -> null
	 * @param sourceMap Out parameter, table alias to op
	 * @return
	 * @throws TableNotFoundException
	 * @throws IOException
	 */
	List<QueryOperator<TableWithVariableSchema>> getTableExpressions(Select sel,
			Map<String,Object> symbolTable,
			Map<String,QueryOperator<TableWithVariableSchema>> sourceMap
			) throws TableNotFoundException, IOException {
		QueryOperator<TableWithVariableSchema> ret = null;
		TablesNamesFinder tablesNamesFinder = new TablesNamesFinder();
		TableAliasFinder aliases = new TableAliasFinder();
		
		// The names of tables and their aliases
		List<String> tableList = tablesNamesFinder.getTableList(sel);
		List<String> tableAliases = aliases.getTableList(sel);
		List<QueryOperator<TableWithVariableSchema>> ops = new ArrayList<>();		

		// Iterate over each aliased table and create a scan operator
		// and then set the sourceMap to point to it
		for (int i = 0; i < tableList.size(); i++) {
			String tableName = cleanTableName(tableList.get(i));
			String tableAlias = cleanTableName(tableAliases.get(i));
			
			BasicSchema sch = (BasicSchema) ddl.getMetadata(tableName);
			System.out.println(tableName + ": " + sch);
			
			// FastQ, always has a prebuilt schema
			if (tableName.endsWith(".fq")) {
				try {
					ddl.process(SqlExpressionBuilder.parseSQL("CREATE TABLE IF NOT EXISTS \"" + tableName + 
							"\"(lineNumber int, readHeader varchar(255), readString varchar(255), baseQualityHeader varchar(255), baseQualityString varchar(255))"));
				} catch (JSQLParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				sch = (BasicSchema) ddl.getMetadata(tableName);
				
				ret = new FastQExtractor<TableWithVariableSchema>(ddl.catalog.getBasePath() + tableName, tableName, QueryGen.provWrapperAPI);
			} else if (tableName.endsWith(".csv")) {
				if (sch == null)
					throw new TableNotFoundException(tableName);
				ret = new CSVExtractor<TableWithVariableSchema>(ddl.catalog.getBasePath() + tableName, tableName, QueryGen.provWrapperAPI);
			} else if (tableName.endsWith(".tbl")) {
				if (sch == null)
					throw new TableNotFoundException(tableName);
				ret = new CSVExtractor<TableWithVariableSchema>(ddl.catalog.getBasePath() + tableName, QueryGen.provWrapperAPI, sch);
				((CSVExtractor<?>)ret).configure('|', '"', '\\', false);
			} else
				throw new UnsupportedOperationException("Unsupported input table type");
			
			// rename to qualified names!

			Map<String,String> fieldMap = new HashMap<>();
			ret = new RenameOperator<TableWithVariableSchema>(ret, fieldMap, QueryGen.provWrapperAPI);
			
			// Aliases
			for (String field: sch.getKeys()) {
				// Qualified name, using the table alias
				symbolTable.put(tableAlias + "." + field, sch.getTypeAt(sch.getKeys().indexOf(field)));
				
				fieldMap.put(field, tableAlias + "." + field);
				
				// And also set an unqualified name to remap to the qualified name
				if (!symbolTable.containsKey(field))
					symbolTable.put(field, tableAlias + "." + field);
			}
			
			ops.add(ret);
			sourceMap.put(tableAlias, ret);
		}
		
		return ops;
	}
	
	UnaryTuplePredicateWithLookup getSelectionPredicate(String fieldName, 
			PredicateOperations.ComparisonOperation comparison, String literal,
			Map<String,Object> symbolTable,
			Map<String,QueryOperator<TableWithVariableSchema>> sourceMap,
			Map<QueryOperator<TableWithVariableSchema>,List<UnaryTuplePredicateWithLookup>> predicates
			) {
		UnaryTuplePredicateWithLookup utp = null;
		try {
			if (isNumber(literal)) {
				utp = new UnaryFieldComparisonPredicate(fieldName, comparison, 
						(literal.indexOf('.') >= 0) ? Double.valueOf(literal) : Integer.valueOf(literal)); 
			} else
				utp = new UnaryFieldComparisonPredicate(fieldName, comparison, 
						literal, true); 

			// Which operator do we attach the predicate to?
			QueryOperator<TableWithVariableSchema> op = sourceMap.get(getAliasFromQualifiedName(fieldName, 
					symbolTable));
			List<UnaryTuplePredicateWithLookup> preds = predicates.get(op);
			if (preds == null) {
				preds = new ArrayList<>();
				predicates.put(op,  preds);
			}
			
			preds.add(utp);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return utp;
	}

	/**
	 * Find WHERE predicates and turn them into selection or join predicates
	 * 
	 * @param sel SQL select
	 * @param ops List of query expression roots for each input table (updated)
	 * @param sourceMap Map from alias name to query expression root (updated)
	 * @param symbolTable Map from unqualified to qualified names
	 * @return A map of join table aliases (in lexicographic order) and the predicates
	 * that are posed over them 
	 */
	public Map<List<String>,List<BinaryTuplePredicateWithLookup>> getPredicates(Select sel,
			List<QueryOperator<TableWithVariableSchema>> ops,
			Map<String,QueryOperator<TableWithVariableSchema>> sourceMap,
			Map<String,Object> symbolTable
			) {
		PlainSelect ps = (PlainSelect)sel.getSelectBody();
		QueryOperator<TableWithVariableSchema> ret = ops.get(0);

        // Go over the WHERE clause and create predicates associated with the
        // relevant sources
        Expression expr = ps.getWhere();
    	final Map<List<String>,List<BinaryTuplePredicateWithLookup>> joinPredicates = new HashMap<>();
        if (expr != null) {
        	final Map<QueryOperator<TableWithVariableSchema>,List<UnaryTuplePredicateWithLookup>> predicates = new HashMap<>();
	        expr.accept(new ExpressionVisitorAdapter() {
	
	            @Override
	            protected void visitBinaryExpression(BinaryExpression expr) {
	                if (expr instanceof ComparisonOperator) {
	        			PredicateOperations.ComparisonOperation comparison;
        				comparison = PredicateOperations.getComparison(expr.getStringExpression());
	        			
        				// Comparison with a literal on the left
	        			if (isConstOrLiteral(expr.getLeftExpression().toString())) {
	        				getSelectionPredicate(getQualifiedName(expr.getRightExpression().toString(), symbolTable),
	        						comparison,
	        						expr.getLeftExpression().toString(),
	        						symbolTable,
	        						sourceMap,
	        						predicates);
        					
        				// Comparison with a literal on the right
	        			} else if (isConstOrLiteral(expr.getRightExpression().toString())) {
	        				getSelectionPredicate(getQualifiedName(expr.getLeftExpression().toString(), symbolTable),
	        						comparison,
	        						expr.getRightExpression().toString(),
	        						symbolTable,
	        						sourceMap,
	        						predicates);

        				// Join
	        			} else {
	        				List<String> neededTables = new ArrayList<>();
	        				neededTables.add(getAliasFromQualifiedName(expr.getLeftExpression().toString(), symbolTable));
	        				neededTables.add(getAliasFromQualifiedName(expr.getRightExpression().toString(), symbolTable));
	        				System.out.println(neededTables);
	        				Collections.sort(neededTables);
	        				if (!joinPredicates.containsKey(neededTables)) {
	        					joinPredicates.put(neededTables, new ArrayList<>());
	        				}
	        				try {
	        					String left = getQualifiedName(expr.getLeftExpression().toString(), symbolTable);
	        					String right = getQualifiedName(expr.getRightExpression().toString(), symbolTable); 
								joinPredicates.get(neededTables).add(new BinaryFieldComparisonPredicate(
										left, comparison,
										right));
								
								System.out.println("Adding " + joinPredicates.get(neededTables));
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	        			}

	                } else if (expr instanceof AndExpression) {
	                    super.visitBinaryExpression(expr); 
	                }
	            }
	        });
	        
	        // Take lists of predicates and turn them into a conjunction, then add them to each source
	        addPredicatesToExpressions(ops, predicates, sourceMap);
        }
        
        List<Join> joins = ps.getJoins();
        
        if (joins != null) {
	        for (Join j: joins) {
	        	Expression expr2 = j.getOnExpression();
	        	if (expr2 != null)
			        expr2.accept(new ExpressionVisitorAdapter() {
			        	
			            @Override
			            protected void visitBinaryExpression(BinaryExpression expr) {
			                if (expr instanceof ComparisonOperator) {
			        			PredicateOperations.ComparisonOperation comparison;
		        				comparison = PredicateOperations.getComparison(expr.getStringExpression());
			        			
			        				List<String> neededTables = new ArrayList<>();
			        				neededTables.add(getAliasFromQualifiedName(expr.getLeftExpression().toString(), symbolTable));
			        				neededTables.add(getAliasFromQualifiedName(expr.getRightExpression().toString(), symbolTable));
			        				Collections.sort(neededTables);
			        				if (!joinPredicates.containsKey(neededTables)) {
			        					joinPredicates.put(neededTables, new ArrayList<>());
			        				}
			        				try {
										joinPredicates.get(neededTables).add(new BinaryFieldComparisonPredicate(
												getQualifiedName(expr.getLeftExpression().toString(), symbolTable), comparison,
												getQualifiedName(expr.getRightExpression().toString(), symbolTable)));
										
										System.out.println("Adding " + joinPredicates.get(neededTables));
									} catch (UnsupportedEncodingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
		
			                } else if (expr instanceof AndExpression) {
			                    super.visitBinaryExpression(expr); 
			                }
			            }
			        });
	        	
	        	if (j.isFull()) {
	        		
	        	} else if (j.isLeft()) {
	        		
	        	} else if (j.isRight()) {
	        		
	        	}
	        }
        
        }
        
        return joinPredicates;
	}
	
	private List<BinaryTuplePredicateWithLookup> getMatches(Map<List<String>,List<BinaryTuplePredicateWithLookup>> joinPredicates, 
			List<String> expression) {
		List<BinaryTuplePredicateWithLookup> ret = new ArrayList<>();
		
		Set<List<String>> remove = new HashSet<>();
		for (List<String> k: joinPredicates.keySet()) {
			if (k.equals(expression) || expression.containsAll(k)) {
				ret.addAll(joinPredicates.get(k));
				remove.add(k);
			}
		}
		for (List<String> k: remove)
			joinPredicates.remove(k);
		
		return ret;
	}

	/**
	 * Joins together all expressions into a single rooted expression.
	 * 
	 * @param ops
	 * @param joinPredicates
	 * @param symbolTable
	 * @param sourceMap
	 * @return
	 */
	private QueryOperator<TableWithVariableSchema> joinExpressions(
			List<QueryOperator<TableWithVariableSchema>> ops,
			Map<List<String>,List<BinaryTuplePredicateWithLookup>> joinPredicates,
			Map<String, Object> symbolTable, 
			Map<String, QueryOperator<TableWithVariableSchema>> sourceMap) {
    	List<String> aliasesSoFar = new ArrayList<String>();
    	Map<QueryOperator<TableWithVariableSchema>,String> inverseSourceMap = new HashMap<>();
    	for (String key: sourceMap.keySet())
    		inverseSourceMap.put(sourceMap.get(key), key);
    	
    	aliasesSoFar.add(inverseSourceMap.get(ops.get(0)));
        while (ops.size() > 1) {
        	List<String> aliasesLeft = new ArrayList<>();
        	aliasesLeft.addAll(aliasesSoFar);
        	aliasesSoFar.add(inverseSourceMap.get(ops.get(1)));
        	QueryOperator<TableWithVariableSchema> second = ops.get(1);
        	ops.remove(1);
        	
        	// See if there are join predicates
        	Collections.sort(aliasesSoFar);
        	
        	List<BinaryTuplePredicateWithLookup> matches = getMatches(joinPredicates, aliasesSoFar);
        	System.out.println(aliasesSoFar + " matches " + matches);
        	if (!matches.isEmpty()) {
            	List<String> leftEquijoin = new ArrayList<>();
            	List<String> rightEquijoin = new ArrayList<>();
            	int predIndex = 0;
            	BinaryTuplePredicateWithLookup lastPred = null;
            	
        		while (!matches.isEmpty()) {
	        		BinaryTuplePredicateWithLookup pred = matches.get(predIndex);
	        		matches.remove(0);
	        		
	        		if (pred instanceof BinaryFieldComparisonPredicate) {
	        			BinaryFieldComparisonPredicate bcp = (BinaryFieldComparisonPredicate)pred;
	        			
	        			String table = getTableFromQualifiedName(bcp.getLeft(), symbolTable);
	        			if (aliasesLeft.contains(table)) {
	        				leftEquijoin.add(bcp.getLeft());
	        				rightEquijoin.add(bcp.getRight());
	        				
	        			} else {
	        				leftEquijoin.add(bcp.getRight());
	        				rightEquijoin.add(bcp.getLeft());
	        				
	        			}
	        		} else {
	        			if (lastPred == null)
	        				lastPred = pred;
	        			else
		        			lastPred = new BinaryTupleBooleanExpression(BooleanOperation.And, lastPred, pred);
	        		}
        		}
        		if (lastPred == null)
        			lastPred = 	new BinaryTuplePredicateWithLookup() {

        			@Override
        			public boolean test(TupleWithSchema<String> t, TupleWithSchema<String> u) {
        				return true;
        			}

        			@Override
        			public Set<String> getSymbolsReferencedLeft() {
        				return Sets.newHashSet();
        			}

        			@Override
        			public Set<String> getSymbolsReferencedRight() {
        				return Sets.newHashSet();
        			}

        			@Override
        			public void swapLeftAndRight() {
        				// TODO Auto-generated method stub

        			}
        		};

        		ops.set(0, new JoinOperator<TableWithVariableSchema>(ops.get(0), second,
        				leftEquijoin, rightEquijoin, lastPred, QueryGen.provWrapperAPI));
        	} else {
        		ops.set(0, new CartesianOperator<TableWithVariableSchema>(ops.get(0), second, 
        				QueryGen.provWrapperAPI));
        	}
        }
        return ops.get(0);
	}
	
	/**
	 * Finds relevant source expressions and adds any selection predicates (creating a conjunction if
	 * there are multiple such predicates).
	 * 
	 * @param ops
	 * @param predicates
	 * @param sourceMap
	 */
	void addPredicatesToExpressions(List<QueryOperator<TableWithVariableSchema>> ops,
			Map<QueryOperator<TableWithVariableSchema>,List<UnaryTuplePredicateWithLookup>> predicates,
			Map<String,QueryOperator<TableWithVariableSchema>> sourceMap
			) {
        // Take lists of predicates and turn them into a conjunction, then add them to each source
        //for (QueryOperator<TableWithVariableSchema> op: ops) {
		for (int x = 0; x < ops.size(); x++) {
			final QueryOperator<TableWithVariableSchema> op = ops.get(x);
        	if (predicates.get(op) != null) {
        		UnaryTuplePredicateWithLookup pred = predicates.get(op).get(0);
        		for (int i = 1; i < predicates.get(op).size(); i++)
					pred = new UnaryTupleBooleanExpression(PredicateOperations.BooleanOperation.And, pred, predicates.get(op).get(i));
        		final SelectionOperator<TableWithVariableSchema> ret2 = 
        				new SelectionOperator<TableWithVariableSchema>(op, pred, QueryGen.provWrapperAPI);
        		
            	ops.set(x,  ret2);
            	sourceMap.replaceAll((key,o) -> { if (o == op) return ret2; else return o; });
        	}
        }
	}
	
	private QueryOperator<TableWithVariableSchema> addFinalProjections(Select sel,
			List<QueryOperator<TableWithVariableSchema>> ops,
			Map<String,QueryOperator<TableWithVariableSchema>> sourceMap,
			Map<String, Object> symbolTable) {
		PlainSelect ps = (PlainSelect)sel.getSelectBody();
		List<String> columns = new ArrayList<>();
        List<SelectItem> selectitems = ps.getSelectItems();
        selectitems.stream().forEach(selectItem -> 
        	columns.add(getQualifiedName(selectItem.toString(), symbolTable)));
		
        if (!(columns.size() == 1 && columns.get(0).contentEquals("*"))) {
        	final QueryOperator<TableWithVariableSchema> ret3 = ops.get(0);
        	final ProjectionOperator<TableWithVariableSchema> ret2 = new ProjectionOperator<>(ops.get(0), 
        			columns, QueryGen.provWrapperAPI);
        	ops.replaceAll(op -> { if (op == ret3) return ret2; else return ret3; } );
        	sourceMap.replaceAll((key,op) -> { if (op == ret3) return ret2; else return ret3; });
        }
        return ops.get(0);
	}

	/**
	 * Find the output type based on two inputs
	 * 
	 * @param left
	 * @param right
	 * @param op
	 * @return
	 */
	private Class getType(Class left, Class right, ArithmeticOperation op) {
		if (left == String.class || right == String.class)
			return String.class;
		
		if (left == Double.class || right == Double.class)
			return Double.class;
		
		return Integer.class;
	}


	/**
	 * Get an RA expression representing the query
	 * 
	 * @param statement
	 * @return
	 * @throws TableNotFoundException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws QueryParseException
	 */
	public QueryOperator<TableWithVariableSchema> getQueryExpression(Select statement) throws TableNotFoundException, JsonParseException, JsonMappingException, IOException, QueryParseException {
		
		// Symbol table is used to track (and map) qualified field names
		Map<String,Object> symbolTable = new HashMap<>();
		Select sel = (Select)statement;
		
		// Table aliases to source expressions (pre-join)
		Map<String,QueryOperator<TableWithVariableSchema>> sourceMap = new HashMap<>();
		
		// The operators we are creating
		List<QueryOperator<TableWithVariableSchema>> ops = getTableExpressions(sel, symbolTable, sourceMap);

		Map<List<String>,List<BinaryTuplePredicateWithLookup>> joinPredicates =
				getPredicates(sel, ops, sourceMap, symbolTable);
		
		// The root to return by default
		QueryOperator<TableWithVariableSchema> ret = ops.get(0);

		if (ops.size() > 1) {
			ret = joinExpressions(ops, joinPredicates, symbolTable, sourceMap);
		}
        
        // TODO: final group-by and select-list
        // and any UDFs
		
		

		// TODO: any final renames
		PlainSelect ps = (PlainSelect)sel.getSelectBody();
		GroupByElement grouping = ps.getGroupBy();
		if (grouping != null) {
			System.out.println("Group by " + grouping.getGroupByExpressions());
			List<String> groupingFields = new ArrayList<>();
			
			for (Expression expr: grouping.getGroupByExpressions())
				groupingFields.add(getQualifiedName(expr.toString(), symbolTable));
			
			List<Function<TableWithVariableSchema,BasicTuple>> aggFunctions = new ArrayList<>();
			List<BasicSchema> aggSchemas = new ArrayList<>();
			
			for (SelectItem item: ps.getSelectItems()) {
				if (((SelectExpressionItem)item).getExpression() instanceof net.sf.jsqlparser.expression.Function) {
					net.sf.jsqlparser.expression.Function f = 
							(net.sf.jsqlparser.expression.Function)((SelectExpressionItem)item).getExpression();
					
					// TODO: look up the function, figure out the parameters
					if (f.getParameters() != null) {

						if (f.getParameters().getExpressions().size() != 1)
							throw new UnsupportedOperationException("No support for multi-argument SQL functions");
						
						// There should be one expression
						Expression expr = f.getParameters().getExpressions().get(0);
						
						final List<ArithmeticExpressionOperation<?>> expressionStack = new ArrayList<>();

				        expr.accept(new ExpressionVisitorAdapter() {
				        	public void visit(Multiplication mult) {
				        		mult.getLeftExpression();
				        		mult.getRightExpression();
				        		super.visitBinaryExpression(mult);
				        		ArithmeticExpressionOperation<?> right = 
				        				expressionStack.remove(expressionStack.size() - 1);
				        		ArithmeticExpressionOperation<?> left = 
				        				expressionStack.remove(expressionStack.size() - 1);
				        		
				        		expressionStack.add(new BinaryArithmeticExpressionOperation(
				        				getType(left.getType(), right.getType(), ArithmeticOperation.Multiply), 
				        				ArithmeticOperation.Multiply, 
				        				left, right));
				        	}
				        	
				        	public void visit(Subtraction sub) {
				        		sub.getLeftExpression();
				        		sub.getRightExpression();
				        		super.visit(sub);
				        		ArithmeticExpressionOperation<?> right = 
				        				expressionStack.remove(expressionStack.size() - 1);
				        		ArithmeticExpressionOperation<?> left = 
				        				expressionStack.remove(expressionStack.size() - 1);
				        		
				        		expressionStack.add(new BinaryArithmeticExpressionOperation(
				        				getType(left.getType(), right.getType(), ArithmeticOperation.Subtract), 
				        				ArithmeticOperation.Subtract, 
				        				left, right));
				        	}

				        	public void visit(Addition add) {
				        		add.getLeftExpression();
				        		add.getRightExpression();
				        		super.visit(add);
				        		ArithmeticExpressionOperation<?> right = 
				        				expressionStack.remove(expressionStack.size() - 1);
				        		ArithmeticExpressionOperation<?> left = 
				        				expressionStack.remove(expressionStack.size() - 1);
				        		
				        		expressionStack.add(new BinaryArithmeticExpressionOperation(
				        				getType(left.getType(), right.getType(), ArithmeticOperation.Add), 
				        				ArithmeticOperation.Add, 
				        				left, right));
				        	}
				        	
				        	public void visit(Division div) {
				        		div.getLeftExpression();
				        		div.getRightExpression();
				        		super.visit(div);
				        		ArithmeticExpressionOperation<?> right = 
				        				expressionStack.remove(expressionStack.size() - 1);
				        		ArithmeticExpressionOperation<?> left = 
				        				expressionStack.remove(expressionStack.size() - 1);
				        		
				        		expressionStack.add(new BinaryArithmeticExpressionOperation(
				        				getType(left.getType(), right.getType(), ArithmeticOperation.Divide), ArithmeticOperation.Divide, 
				        				left, right));
				        	}

				            @Override
				            public void visit(DoubleValue value) {
				            	expressionStack.add(new ArithmeticExpressionLiteral<Double>(Double.class, value.getValue()));
				            }

				            @Override
				            public void visit(LongValue value) {
				            	expressionStack.add(new ArithmeticExpressionLiteral<Long>(Long.class, value.getValue()));
				            }
				            
				            @Override
				            public void visit(StringValue value) {
				            	expressionStack.add(new ArithmeticExpressionLiteral<String>(String.class, value.toString()));
				            }
				            
				            public void visit(Column column) {
				            	Class c = getClassFromName(column.toString(), symbolTable);
				            	expressionStack.add(new ArithmeticExpressionVariable(c, 
				            			getQualifiedName(column.toString(), symbolTable)));
				            }
				        }
						        );
				        
						Function<TableWithVariableSchema,BasicTuple> aggFunction = (Function<TableWithVariableSchema, BasicTuple>) 
								ddl.catalog.getFunction(f.getName());
						Udf functionInfo = (Udf) ddl.catalog.getMetadata(f.getName());
						
						aggSchemas.add(functionInfo.getOutputSchema());
						
						JavaUdf udf = functionInfo.getFunction().instantiate(expressionStack);

						System.out.println("Aggregate " + udf.toString());
						
						aggFunctions.add(udf);
					}
					
				}
			}

			
			ret = new GroupByOperator<TableWithVariableSchema>(ret, groupingFields, 
					aggFunctions, aggSchemas, 
					new AggregateProvenance(), QueryGen.provWrapperAPI);
		} else
			ret = addFinalProjections(sel, ops, sourceMap, symbolTable);
        
		if (ops.size() != 1)
			throw new UnsupportedOperationException("Currently limited to single expressions!");
		
		return ret;
	}

}

package edu.upenn.cis.db.habitat.sql.udfs;

import java.util.function.Function;

import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

public class GetExpressionFromTuple<T> implements Function<TupleWithSchema<String>, T> {
	ArithmeticExpressionOperation<T> getResult;
	
	public GetExpressionFromTuple(ArithmeticExpressionOperation<T> getResult) {
		this.getResult = getResult;
	}

	@Override
	public T apply(TupleWithSchema<String> t) {
		getResult.bind(t);
		
		return getResult.getValue();
	}
		
}

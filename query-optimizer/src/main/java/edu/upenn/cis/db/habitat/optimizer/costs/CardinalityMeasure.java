package edu.upenn.cis.db.habitat.optimizer.costs;

public class CardinalityMeasure implements Measure {
	
	double value = 0;
	
	public CardinalityMeasure(double tuples) {
		value = tuples;
	}

	@Override
	public double getValue() {
		return value;
	}

	@Override
	public void setValue(double value) {
		this.value = value;
	}

}

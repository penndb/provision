package edu.upenn.cis.db.habitat.sql.udfs;

import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.upenn.cis.db.habitat.catalog.SystemCatalog;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.JavaUdf;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.Udf;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class RegisterBuiltInUdfs {
	public static void registerAll(SystemCatalog catalog) {
		try {
			registerAverage(catalog);
			registerSumDouble(catalog);
			registerSumInt(catalog);
			registerCount(catalog);
			registerMin(catalog);
			registerMax(catalog);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void registerAverage(SystemCatalog catalog) throws JsonProcessingException {
		final BasicSchema aggInputs = new BasicSchema("Agg");
		aggInputs.addField("input", Integer.class);

		final BasicSchema aggSchema = new BasicSchema("Avg");
		aggSchema.addField("avg", Double.class);

		JavaUdf udf = new AvgUdf<Double>();
		udf.setArguments(aggInputs);
		udf.setExpressions(new ArrayList<>());
		udf.setOutputSchema(aggSchema);
		catalog.writeMetadata(
				new Udf<TableWithVariableSchema,TupleWithSchema<String>,Double>("avg", 
						aggInputs, 
				new ArrayList<ArithmeticExpressionOperation<Double>>(), aggSchema, udf));
	}
	
	public static void registerSumDouble(SystemCatalog catalog) throws JsonProcessingException {
		final BasicSchema aggInputs = new BasicSchema("Agg");
		aggInputs.addField("input", Integer.class);

		final BasicSchema aggSchema = new BasicSchema("Sum");
		aggSchema.addField("sum", Double.class);

		JavaUdf udf = new SumUdf<Double>();
		udf.setArguments(aggInputs);
		udf.setExpressions(new ArrayList<>());
		udf.setOutputSchema(aggSchema);
		catalog.writeMetadata(
				new Udf<TableWithVariableSchema,TupleWithSchema<String>,Double>("sum", 
						aggInputs, 
				new ArrayList<ArithmeticExpressionOperation<Double>>(), aggSchema, udf));
//		catalog.storeFunction("count", aggFunction);
	}

	public static void registerSumInt(SystemCatalog catalog) throws JsonProcessingException {
		final BasicSchema aggInputs = new BasicSchema("Agg");
		aggInputs.addField("input", Integer.class);

		final BasicSchema aggSchema = new BasicSchema("Sum");
		aggSchema.addField("sum", Integer.class);

		JavaUdf udf = new SumUdf<Integer>();
		udf.setArguments(aggInputs);
		udf.setExpressions(new ArrayList<>());
		udf.setOutputSchema(aggSchema);
		catalog.writeMetadata(
				new Udf<TableWithVariableSchema,TupleWithSchema<String>,Integer>("sum_int", 
						aggInputs, 
				new ArrayList<ArithmeticExpressionOperation<Integer>>(), aggSchema, udf));
//		catalog.storeFunction("count", aggFunction);
	}

	public static void registerCount(SystemCatalog catalog) throws JsonProcessingException {
		final BasicSchema aggInputs = new BasicSchema("Agg");

		final BasicSchema aggSchema = new BasicSchema("Count");
		aggSchema.addField("count", Integer.class);
				
		JavaUdf udf = new CountUdf();
		udf.setArguments(aggInputs);
		udf.setExpressions(new ArrayList<>());
		udf.setOutputSchema(aggSchema);
		catalog.writeMetadata(new Udf("count", aggInputs, new ArrayList<>(), aggSchema, udf));
//		catalog.storeFunction("count", aggFunction);
	}

	public static void registerMin(SystemCatalog catalog) {
		
	}

	public static void registerMax(SystemCatalog catalog) {
		
	}
}

/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.optimizer.memo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import edu.upenn.cis.db.habitat.catalog.TableStatistics;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.optimizer.costs.ResourceVector;
import edu.upenn.cis.db.habitat.optimizer.graph.HyperEdge;
import edu.upenn.cis.db.habitat.optimizer.rewrites.ResultProperties;
import edu.upenn.cis.db.habitat.optimizer.stats.ResultProfile;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * Query plan subgraph.
 * 
 * @author ZacharyIves
 *
 * @param <P> Type of data (Object, Serializable, etc.)
 * @param <T> Type of table (TableWithVariableSchema)
 */
public abstract class PlanSubgraph<P,T extends TableWithVariableSchema> {
	List<PlanSubgraph<P,T>> children = new ArrayList<>();
	ResultProperties properties;
	ResultProfile<P> profile;
	ResourceVector resources;
	
	Set<HyperEdge<T>> covered = new HashSet<>();
	
	QueryOperator<T> operator;
	
	BasicSchema schema;
	
	TableStatistics statistics;
	
	public void addSubplan(PlanSubgraph<P,T> plan) {
		children.add(plan);
	}
	
	public QueryOperator<T> getOperator() {
		return operator;
	}
	
	/**
	 * Histogram, sketch, bounds, etc.
	 * 
	 * @return
	 */
	public ResultProfile<P> getProfile() {
		return profile;
	}
	
	/**
	 * Cost
	 * @return
	 */
	public ResourceVector getResourcesUsed() {
		return resources;
	}
	
	/**
	 * Sort order(s), partitions, etc.
	 * @return
	 */
	public ResultProperties getResultProperties() {
		return properties;
	}
	
	/**
	 * Get child expression
	 * 
	 * @param index
	 * @return
	 */
	public PlanSubgraph<P,T> getSubplan(int index) {
		return children.get(index);
	}
	
	/**
	 * Number of children
	 * 
	 * @return
	 */
	public int getSubplanCount() {
		return children.size();
	}
	
	public void setOperator(QueryOperator<T> operator) {
		this.operator = operator;
	}

	public void setResources(ResourceVector resources) {
		this.resources = resources;
	}

	/**
	 * Histogram, sketch, bounds, etc.
	 */
	public void setProfile(ResultProfile<P> profile) {
		this.profile = profile;
	}

	public void setResultProperties(ResultProperties properties) {
		this.properties = properties;
	}
	
	public List<PlanSubgraph<P,T>> getChildren() {
        return Collections.unmodifiableList(children);
    }
 
    public Stream<PlanSubgraph<P,T>> flattened() {
        return Stream.concat(
                Stream.of(this),
                children.stream().flatMap(PlanSubgraph<P,T>::flattened));
    }

	public abstract PlanSubgraph<P,T> getBest(Function<ResourceVector,Integer> costFn);

	public TableStatistics getStatistics() {
		return statistics;
	}

	public void setStatistics(TableStatistics statistics) {
		this.statistics = statistics;
	}

	public BasicSchema getSchema() {
		return schema;
	}

	public void setSchema(BasicSchema schema) {
		this.schema = schema;
	}

	public Set<HyperEdge<T>> getCovered() {
		return covered;
	}

	public void setCovered(Set<HyperEdge<T>> covered) {
		this.covered = covered;
	}

	
}

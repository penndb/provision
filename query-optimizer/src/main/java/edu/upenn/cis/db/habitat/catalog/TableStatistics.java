package edu.upenn.cis.db.habitat.catalog;

import java.util.HashMap;
import java.util.Map;

import edu.upenn.cis.db.habitat.core.type.Metadata;
import edu.upenn.cis.db.habitat.optimizer.stats.ResultProfile;

public class TableStatistics implements Metadata {
	String name;
	int cardinality;
	Map<String,ResultProfile> profiles = new HashMap<>();
	
	public TableStatistics() {
		
	}
	
	public TableStatistics(String name, int card) {
		this.name = name;
		this.cardinality = card;
	}
	
	public Map<String, ResultProfile> getValueProfiles() {
		return profiles;
	}

	public void setValueProfiles(Map<String, ResultProfile> uniqueValues) {
		this.profiles = uniqueValues;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCardinality(int cardinality) {
		this.cardinality = cardinality;
	}

	
	public int getCardinality() {
		return cardinality;
	}

	@Override
	public String getName() {
		return name;
	}

}

package edu.upenn.cis.db.habitat.catalog;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sleepycat.je.Cursor;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.Metadata;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.Udf;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.engine.operators.extract.CSVExtractor;
import edu.upenn.cis.db.habitat.engine.operators.extract.FastQExtractor;
import edu.upenn.cis.db.habitat.optimizer.stats.BucketProfile;
import edu.upenn.cis.db.habitat.querygen.QueryGen;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import edu.upenn.cis.db.habitat.sql.exceptions.TableNotFoundException;

/**
 * A BerkeleyDB catalog manager for storing metadata
 * 
 * @author zives
 *
 */
public class SystemCatalog {
    Environment environment = null;
    Database catalog = null;
    ObjectMapper objectMapper = new ObjectMapper()
			.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
    
    String basePath;
    
    Map<String,Udf<?,?,?>> functionDefs = new HashMap<>();

    public SystemCatalog(String path) throws DatabaseException {
    	if (path == null)
    		path = ".";
    	
    	if (!path.endsWith("/"))
    		path = path + "/";
    	
    	basePath = path;
    	
        // Open the environment, creating one if it does not exist
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        envConfig.setTransactional(true);
        File dir = new File(path + "habitat_catalog");
        
        dir.mkdirs();
        
        environment = new Environment(dir,
                                          envConfig);

        // Open the database, creating one if it does not exist
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setAllowCreate(true);
        dbConfig.setTransactional(true);
        catalog = environment.openDatabase(null, "SystemCatalog", dbConfig);
    }
    
    public String getBasePath() {
    	return basePath;
    }
    
    
    public Database getCatalog() {
    	return catalog;
    }
    
    public void writeSchema(BasicSchema schema) throws JsonProcessingException {
    	writeMetadata(schema);
    }
    
    public BasicSchema getSchema(String name) throws JsonParseException, JsonMappingException, IOException {
    	return (BasicSchema)getMetadata(name);
    }
    
    /**
     * Write a metadata object to the catalog
     * 
     * @param md
     * @throws JsonProcessingException
     */
    public void writeMetadata(Metadata md) throws JsonProcessingException {
    	String mdJson = objectMapper.writeValueAsString(md);
    	writeEntry(md.getName(), mdJson);
    }
    
    public List<Metadata> getMetadataWithPrefix(String prefix) {
    	List<Metadata> ret = new ArrayList<>();
    			
    	
    	Cursor myCursor = null;
    	 
    	try {
    	    myCursor = catalog.openCursor(null, null);
    	 
    	    // Cursors returns records as pairs of DatabaseEntry objects
    	    DatabaseEntry foundKey = new DatabaseEntry();
    	    DatabaseEntry foundData = new DatabaseEntry();
    	 
    	    // Retrieve records with calls to getNext() until the
    	    // return status is not OperationStatus.SUCCESS
    	    while (myCursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
    	        OperationStatus.SUCCESS) {
    	        String keyString = new String(foundKey.getData(), "UTF-8");
    	        
    	        if (keyString.startsWith(prefix)) {
	    	        String dataString = new String(foundData.getData(), "UTF-8");
    	        
	    	        try {
						Metadata md = objectMapper.readValue(dataString, Metadata.class);
					} catch (JsonParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JsonMappingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    	        }    	        
    	    }
    	} catch (DatabaseException de) {
    	    System.err.println("Error reading from database: " + de);
    	} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
    	    try {
    	        if (myCursor != null) {
    	            myCursor.close();
    	        }
    	    } catch(DatabaseException dbe) {
    	        System.err.println("Error closing cursor: " + dbe.toString());
    	    }
    	}
    	
    	return ret;
    }
    
    /**
     * Read a metadata object from the catalog
     * 
     * @param name
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    public Metadata getMetadata(String name) throws JsonParseException, JsonMappingException, IOException {
    	String mdJson = getEntry(name);
    	
    	if (mdJson != null)
    		return objectMapper.readValue(mdJson, Metadata.class);
    	else
    		return null;
    }
    public void writeEntry(String key, String value) {
    	try {
	        DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
	        DatabaseEntry theData = new DatabaseEntry(value.getBytes("UTF-8"));
	     
	        catalog.put(null, theKey, theData);
    	} catch (UnsupportedEncodingException uee) {
    		throw new UnsupportedOperationException("Fatal error decoding UTF!!!"); 
    	}
    }
    
    public String getEntry(String key) {
    	try {
	        DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
	        DatabaseEntry theData = new DatabaseEntry();
	     
	        // Call get() to query the database
	        if (catalog.get(null, theKey, theData, LockMode.DEFAULT) ==
	            OperationStatus.SUCCESS) {
	
	        	return new String(theData.getData(), "UTF-8");
	        } else {
	            return null;
	        }
    	} catch (UnsupportedEncodingException uee) {
    		throw new UnsupportedOperationException("Fatal error decoding UTF!!!"); 
    	}
    }
    
    public void storeFunction(String name, Udf<?,?,?> function) {
    	functionDefs.put(name, function);
    }
    
    public Udf<?,?,?> getFunction(String name) {
    	return functionDefs.get(name);
    }
    
    /**
     * Read statistics from the system catalog ... or else scan the file
     * and store these statistics
     * 
     * @param name
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws TableNotFoundException
     * @throws HabitatServiceException
     */
    public TableStatistics getStatistics(String name) throws JsonParseException, JsonMappingException, IOException, TableNotFoundException, HabitatServiceException {
    	if (getMetadata(name + "_stat") != null)
    		return (TableStatistics)getMetadata(name + "_stat");

    	BasicSchema sch = (BasicSchema)getMetadata(name);
    	QueryOperator<?> ret;
		if (sch == null)
			throw new TableNotFoundException(name);
    	
		// FastQ, always has a prebuilt schema
		if (name.endsWith(".fq")) {
			
			ret = new FastQExtractor<TableWithVariableSchema>(getBasePath() + name, name, QueryGen.provWrapperAPI);
		} else if (name.endsWith(".csv")) {
			ret = new CSVExtractor<TableWithVariableSchema>(getBasePath() + name, name, QueryGen.provWrapperAPI);
		} else if (name.endsWith(".tbl")) {
			ret = new CSVExtractor<TableWithVariableSchema>(getBasePath() + name, QueryGen.provWrapperAPI, sch);
			((CSVExtractor<?>)ret).configure('|', '"', '\\', false);
		} else
			throw new UnsupportedOperationException("Unsupported input table type");
		

		Map<String, Set> stats = new HashMap<>();
		
		// Compute a map of lists for each lookup key
		for (String statsOn: sch.getLookupKeys())
			stats.put(statsOn, new HashSet());
		
    	// Profile
		int count = 0;
		if (ret.initialize()) {
			List<MutableTupleWithSchema<String>> temp = new ArrayList<>();
			while (ret.getNextTuple(temp)) {
				for (String statsOn: sch.getLookupKeys())
					for (MutableTupleWithSchema<String> tup: temp)
						stats.get(statsOn).add(tup.getValue(statsOn));
				temp.clear();
				count++;
			}
			
			TableStatistics stat = new TableStatistics(name + "_stat", count);
			
			for (String str: sch.getLookupKeys()) {
				int buckets = 100;
				if (stats.get(str).size() > 0 && (stats.get(str).iterator().next() instanceof Integer)) {
					Set<Integer> intSet = stats.get(str);
					if (buckets > intSet.size())
						buckets = intSet.size();
					BucketProfile<Integer> profile = new BucketProfile<Integer>(
							buckets, Integer.class, 
							intSet.stream().collect(Collectors.toList()));
					profile.setDuplicateFactor(((double)count) / intSet.size());
					stat.getValueProfiles().put(str,  profile);
				} else if (stats.get(str).size() > 0 && (stats.get(str).iterator().next() instanceof String)) {
					Set<String> strSet = stats.get(str);
					if (buckets > strSet.size())
						buckets = strSet.size();
					BucketProfile<Integer> profile = new BucketProfile<Integer>(
							buckets, Integer.class, 
							strSet.stream().map(st -> st.hashCode()).collect(Collectors.toList()));
					profile.setDuplicateFactor(((double)count) / strSet.size());
					stat.getValueProfiles().put(str,  profile);
				}
			}
			
			this.writeMetadata(stat);
			return stat;
		}
		return new TableStatistics("dummy", 1);
    }
}

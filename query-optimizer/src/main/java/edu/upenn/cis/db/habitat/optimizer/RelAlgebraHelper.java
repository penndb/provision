package edu.upenn.cis.db.habitat.optimizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import edu.upenn.cis.db.habitat.catalog.SystemCatalog;
import edu.upenn.cis.db.habitat.catalog.TableStatistics;
import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTupleBooleanExpression;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations.BooleanOperation;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.JavaUdf;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.Udf;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.operators.BlockingOperator;
import edu.upenn.cis.db.habitat.engine.operators.FieldExtractionOperator;
import edu.upenn.cis.db.habitat.engine.operators.GroupByOperator;
import edu.upenn.cis.db.habitat.engine.operators.MaterializeOperator;
import edu.upenn.cis.db.habitat.engine.operators.NullaryQueryOperator;
import edu.upenn.cis.db.habitat.engine.operators.ProjectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.engine.operators.RenameOperator;
import edu.upenn.cis.db.habitat.engine.operators.SelectionOperator;
import edu.upenn.cis.db.habitat.optimizer.costs.CardinalityMeasure;
import edu.upenn.cis.db.habitat.optimizer.costs.ResourceVector;
import edu.upenn.cis.db.habitat.optimizer.graph.HyperEdge;
import edu.upenn.cis.db.habitat.optimizer.graph.Node;
import edu.upenn.cis.db.habitat.optimizer.graph.QueryGraph;
import edu.upenn.cis.db.habitat.optimizer.memo.AlternatePlanSubgraph;
import edu.upenn.cis.db.habitat.optimizer.memo.JointPlanSubgraph;
import edu.upenn.cis.db.habitat.optimizer.memo.PlanSubgraph;
import edu.upenn.cis.db.habitat.optimizer.stats.ResultProfile;
import edu.upenn.cis.db.habitat.querygen.QueryGen;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import edu.upenn.cis.db.habitat.sql.exceptions.TableNotFoundException;

public class RelAlgebraHelper {
	static Logger logger = LogManager.getLogger(RelAlgebraHelper.class);

	public static <T extends TableWithVariableSchema> 
		boolean isValidJoin(BitSet left, BitSet right, List<QueryOperator<T>> leaves) {
		//  - Disallow Cartesian products (zero predicates between left + right)
		//    unless there are no more predicates

		// FieldExpression must be on the right
		if (left.cardinality() == 1 && (getSourceOp(leaves.get(left.nextSetBit(0)))
				instanceof FieldExtractionOperator))
			return false;
		
		return true;
	}
	
	
	public static <P, T extends TableWithVariableSchema> boolean 
	isValidExtraction(Map<BitSet, PlanSubgraph<P,T>> memoTable, QueryGraph<T> g, BitSet left, BitSet right, List<QueryOperator<T>> leaves) {
		//  - Disallow FieldExpression if it doesn't have all of its inputs

		// FieldExpression must be on the right
		if (left.cardinality() == 1 && (RelAlgebraHelper.getSourceOp(leaves.get(left.nextSetBit(0)))
				instanceof FieldExtractionOperator))
			return false;
		
		if (right.cardinality() == 1 && (RelAlgebraHelper.getSourceOp(leaves.get(right.nextSetBit(0)))
				instanceof FieldExtractionOperator)) {
			BasicSchema source = memoTable.get(left).getSchema();
			
			if (source == null || source.getKeys() == null)
				logger.error("Illegal state");
			
			Set<Node<T>> leftNodes = source.getKeys().stream().map(key -> g.getNode(key)).collect(Collectors.toSet());
			
			leftNodes.remove(null);
			
			Set<Node<T>> addThese = new HashSet<>();
			
			for (Node<T> n: leftNodes)
				addThese.addAll(n.getEquivalents());
			
			leftNodes.addAll(addThese);
			addThese.clear();
//			logger.debug("Looking at extraction, given fields: {}", leftNodes);

			BasicSchema extracted = memoTable.get(right).getSchema();
			Set<Node<T>> extractionNodes = extracted.getKeys().
					stream().map(key -> g.getNode(key)).collect(Collectors.toSet());
			
			// In case there was a null
			extractionNodes.remove(null);
			
			for (Node<T> n: extractionNodes)
				addThese.addAll(n.getEquivalents());
			
//			extractionNodes.addAll(addThese);

			int overallMatches = 0;
			// If any of the extraction variables relies on a symbol that isn't in the left,
			// we have an invalid case
			for (Node<T> n: extractionNodes) {
				Set<HyperEdge<T>> edges = g.getEdgesTo(n);
				Set<SortedSet<Node<T>>> nodesRefd = edges.stream().map(e -> e.getFrom()).collect(Collectors.toSet());
				
				boolean hasMatch = false;
				for (SortedSet<Node<T>> ss: nodesRefd) {
//					logger.debug("Extraction uses fields: {}", ss);
					if (leftNodes.containsAll(ss)) {
						hasMatch = true;
					} else {
//						logger.debug("FieldExtraction relies on undefined symbol in {}", ss);
					}
				}
				if (hasMatch)
					overallMatches++;
			}
			
			logger.debug("Found potential extraction for fields: {}", extractionNodes);

			// Each variable has a matched edge
			return overallMatches == extractionNodes.size();
		} else
			return false;
		
	}


	public static <T extends TableWithVariableSchema>
	QueryOperator<T> getSourceOp(QueryOperator<T> op) {
		if (op instanceof SelectionOperator ||
				op instanceof ProjectionOperator ||
				op instanceof RenameOperator)
			return getSourceOp(op.getChildren().get(0));
		else {
//			logger.debug("Source {}", op);
			return op;
		}
	}


	public static <T extends TableWithVariableSchema>
	String getSource(QueryOperator<T> root) {
		if (root instanceof NullaryQueryOperator) {
			return ((NullaryQueryOperator<T>)root).getAdditionalSchema().getName();
		} else {
			for (QueryOperator<T> child: root.getChildren()) {
				String ret = getSource(child);
				if (ret != null)
					return ret;
			}
		}
		return null;
	}
	
	/**
	 * Clones the relevant parts of a FieldExtractOperator
	 * @param g
	 * @param right
	 * @param queryOperator
	 * @return
	 */
	public static <T extends TableWithVariableSchema>
	FieldExtractionOperator<T,?,?> cloneExtraction(
			QueryOperator<T> extractInput, FieldExtractionOperator<T,?,?> queryOperator) {

		return new FieldExtractionOperator(extractInput, 
				queryOperator.getAdditionalSchema(),
				queryOperator.getExtractorFunction(), 
				queryOperator.getProvenanceCombiner(), 
				queryOperator.getProvApi());
	}

	/**
	 * Clones the relevant parts of a FieldExtractOperator
	 * @param g
	 * @param right
	 * @param queryOperator
	 * @return
	 */
	public static <P,T extends TableWithVariableSchema>
		GroupByOperator<T> cloneGroup(
			QueryOperator<T> newInput, GroupByOperator<T> queryOperator,
			QueryGraph<T> graph) {
		
		BasicSchema finalSchema = graph.getPlanGraph().getSchema();
		
		Set<Node<T>> existingNodes = new HashSet<>();
		Map<Node<T>, String> names = new HashMap<>();
		for (String sch: finalSchema.getKeys()) {
			Node<T> graphNode = graph.getNode(sch);
			if (graphNode != null) {
				names.put(graphNode, sch);
				existingNodes.add(graphNode);
			}
		}
			
		
		List<String> newGroups = new ArrayList<>();

		for (String gf: queryOperator.getGroupingFields()) {
			gf = graph.getSymbolTable().get(gf);
			Node<T> nod = graph.getNode(gf); 
			if (nod != null && existingNodes.contains(nod)) {
				newGroups.add(names.get(nod));
			} else if (nod != null) {
				for (Node<T> nod2: nod.getEquivalents()) {
					if (existingNodes.contains(nod2)) {
						newGroups.add(names.get(nod2));
					}
				}
				
			}
		}
		
		List<Function<T,BasicTuple>> aggFunctions = new ArrayList<>();
		for (Function f: queryOperator.getAggFunctions()) {
			if (f instanceof JavaUdf) {
				JavaUdf<TableWithVariableSchema, TupleWithSchema<String>, ?> fn = (JavaUdf) f;
				
				for (int i = 0; i < fn.getExpressions().size(); i++) {
					Set<String> symbols = fn.getExpressions().get(i).getSymbolsReferenced();
					Map<String,String> replace = new HashMap<>();

					for (String gf1: symbols) {
						String gf = graph.getSymbolTable().get(gf1);
						Node<T> nod = graph.getNode(gf); 
						if (nod != null && existingNodes.contains(nod)) {
							newGroups.add(names.get(nod));
						} else if (nod != null) {
							for (Node<T> nod2: nod.getEquivalents()) {
								if (existingNodes.contains(nod2)) {
									replace.put(gf1, names.get(nod2));
								}
							}
							
						}
					}
					fn.getExpressions().get(i).rename(replace);
				}
			}
			aggFunctions.add(f);
		}
		
		return new GroupByOperator<T>(newInput, 
				newGroups,
				aggFunctions, 
				queryOperator.getAggOutput(), 
				queryOperator.getProvenanceAggregator(),
				queryOperator.getProvApi());
	}

	/**
	 * Create a leaf node, usually a table scan
	 * 
	 * @param bs
	 * @param leaves
	 * @param g
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws TableNotFoundException
	 * @throws HabitatServiceException
	 */
	public static <P, T extends TableWithVariableSchema> void 
	createLeaf(
			Map<BitSet, PlanSubgraph<P,T>> memoTable, 
			BitSet bs, List<QueryOperator<T>> leaves, 
			SystemCatalog catalog,
			QueryGraph<T> g) 
					throws JsonParseException, JsonMappingException, IOException, TableNotFoundException, HabitatServiceException {
		QueryOperator<T> leaf = leaves.get(bs.nextSetBit(0)); 
		final String leafName = leaf.getId().toString() + "/";

		Map<String,String> fieldMap = new HashMap<>();

		for (String key: leaf.getAdditionalSchema().getKeys()) {
			if (!key.equals(ProvenanceApi.Provenance) && !key.equals(ProvenanceApi.Token))
				fieldMap.put(key, leafName + key);
		}

		// Rename the leaf symbols to be unique in the query plan
		RenameOperator<T> rename = new RenameOperator<T>(leaf, 
				fieldMap, QueryGen.provWrapperAPI);
		
		PlanSubgraph<P,T> node = new JointPlanSubgraph<P,T>(rename);
		BasicSchema sourceSchema;
		
		if (leaf instanceof NullaryQueryOperator)
			sourceSchema = catalog.getSchema(RelAlgebraHelper.getSource(leaf));
		else
			sourceSchema = leaf.getAdditionalSchema();
		
		if (sourceSchema == null)
			throw new RuntimeException("Cannot find schema!");
		
		for (int inx = 0; inx < sourceSchema.getArity(); inx++) {
			if (!sourceSchema.getKeyAt(inx).equals(ProvenanceApi.Provenance) &&
					!sourceSchema.getKeyAt(inx).equals(ProvenanceApi.Token))
				sourceSchema.setKeyAt(inx, leafName + sourceSchema.getKeyAt(inx));
		}
		
		// Base schema
		node.setSchema(sourceSchema);
		
		logger.debug("Created source {} with schema {}", RelAlgebraHelper.getSource(leaf), node.getSchema());
		
		// Profile should include number of unique values of the key,
		// any histograms,
		
//		node.setProfile(profile);
//
//		// Properties includes any sort order
//		node.setResultProperties(properties);
		TableStatistics sourceStats = catalog.getStatistics(RelAlgebraHelper.getSource(leaf));

		// Update attribute symbols to be namespaced with the leaf operator
		Map<String,ResultProfile> ren = new HashMap<>();
		for (String var: sourceStats.getValueProfiles().keySet()) {
			ren.put(leaf.getId().toString() + "/" + var, sourceStats.getValueProfiles().get(var));
		}
		sourceStats.getValueProfiles().clear();
		for (String var: ren.keySet()) {
			sourceStats.getValueProfiles().put(var, ren.get(var));
		}
		
		
		TableStatistics newStats = pushdown(g, node, sourceStats); 
		int estCard = sourceStats.getCardinality();
//		
		// Resources should currently include tuples used
		node.setResources(new ResourceVector("cardinality", 
				new CardinalityMeasure(estCard)));

		node.setStatistics(newStats);
		
		// Parent node
		PlanSubgraph<P,T> pNode = new AlternatePlanSubgraph<P, T>(node);
		pNode.setStatistics(newStats);
		pNode.setResources(new ResourceVector("cardinality", 
				new CardinalityMeasure(estCard)));
		pNode.setSchema(sourceSchema);
		pNode.setOperator(node.getOperator());
		pNode.getCovered().addAll(node.getCovered());
		
		memoTable.put(bs, pNode);
		
	}

	public static <P,T extends TableWithVariableSchema> BinaryTuplePredicateWithLookup createJoin(QueryGraph<T> g, PlanSubgraph<P, T> leftNode,
			PlanSubgraph<P, T> rightNode, List<String> leftFields, List<String> rightFields) {
		
		
		Set<Node<T>> leftNodes = leftNode.getSchema().getKeys().stream().map(key -> g.getNode(key)).collect(Collectors.toSet());
		leftNodes.remove(null);
		
		Set<Node<T>> addThese = new HashSet<>();
		for (Node<T> n: leftNodes)
			addThese.addAll(n.getEquivalents());
		leftNodes.addAll(addThese);
		addThese.clear();
		Set<Node<T>> rightNodes = rightNode.getSchema().getKeys().stream().map(key -> g.getNode(key)).collect(Collectors.toSet());
		rightNodes.remove(null);
		for (Node<T> n: rightNodes)
			addThese.addAll(n.getEquivalents());
		rightNodes.addAll(addThese);
		
//		Set<Node<T>> allNodes = new HashSet<>();
//		allNodes.addAll(leftNodes);
//		allNodes.addAll(rightNodes);
		
//		logger.debug("Left {}", leftNodes);
//		logger.debug("Right {}", rightNodes);
		
		Set<HyperEdge<T>> edges = new HashSet<>();
		for (Node<T> start: leftNodes) {
			Set<HyperEdge<T>> radiatingFrom = g.getEdgesFrom(start);
			for (HyperEdge<T> edge: radiatingFrom) {
				
				// This isn't a join predicate
				if (edge.getFrom().isEmpty() || edge.getTo().isEmpty())
					continue;
				
				if (leftNodes.containsAll(edge.getFrom()) && rightNodes.containsAll(edge.getTo()) &&
						!edges.contains(edge)) {
					edges.add(edge);
					logger.debug("Join hyperedge: {}", edge);
					
					for (Node<T> n: edge.getFrom()) {
						if (leftNodes.contains(n)) {
							Set<Node<T>> existingNodes = 
									leftNode.getSchema().getKeys().stream().map(key -> g.getNode(key)).collect(Collectors.toSet());

							if (existingNodes.contains(n)) {
								logger.debug("Found left symbol: {}", n);
								leftFields.add(n.toString());//leftNode.getOperator().getId() + "/" + n.getFieldName());
							} else
							for (Node<T> alias: n.getEquivalents())
								if (existingNodes.contains(alias)) {
									logger.debug("Found left symbol: {}", alias);
									leftFields.add(alias.toString());//leftNode.getOperator().getId() + "/" + alias.getFieldName());
									break;
								}
						}
					}
					for (Node<T> n: edge.getTo()) {
						if (rightNodes.contains(n)) {
							Set<Node<T>> existingNodes = 
									rightNode.getSchema().getKeys().stream().map(key -> g.getNode(key)).collect(Collectors.toSet());
							
							if (existingNodes.contains(n)) {
								logger.debug("Found right symbol: {}", n);
								rightFields.add(n.toString());//rightNode.getOperator().getId() + "/" + n.getFieldName());
							} else
							for (Node<T> alias: n.getEquivalents())
								if (existingNodes.contains(alias)) {
									rightFields.add(alias.toString());//rightNode.getOperator().getId() + "/" + alias.getFieldName());
									logger.debug("Found right symbol: {}", alias);
									break;
								}
						}
					}
					
					// TODO: get the current name, using equivalents
					
				} else if (rightNodes.containsAll(edge.getFrom()) && leftNodes.containsAll(edge.getTo())) {
					edges.add(edge);
					logger.debug("Join hyperedge: {}", edge);
					
				} 
			}
		}
		
		// TODO Auto-generated method stub
		return null;
	}

	public static <P, T  extends TableWithVariableSchema> 
	TableStatistics pushdown(QueryGraph<T> g, 
			PlanSubgraph<P, T> node, 
			TableStatistics sourceStats) {

		Map<String,String> symTab = g.getSymbolTable();

		// Look up graph nodes that correspond to our schema elements
		List<Node<T>> graphNodes = new ArrayList<>();
		Map<Node<T>, String> revMap = new HashMap<>();
		for (String sym: node.getSchema().getKeys()) {
			Node<T> nod = g.getNode(sym);
			if (nod != null) {
				graphNodes.add(nod);
				revMap.put(nod, sym);
			}
		}
		
		List<UnaryTuplePredicateWithLookup> predicates = new ArrayList<>();
		for (Node<T> refd: graphNodes) {
			Set<HyperEdge<T>> edges = g.getEdgesFrom(refd);
			edges.addAll(g.getEdgesExactlyTo(refd));
			
			for (HyperEdge<T> edge: edges) {
				if (edge.getUnaryPredicate() != null && !node.getCovered().contains(edge)) {
					node.getCovered().add(edge);
					predicates.add(edge.getUnaryPredicate());
				}
			}
		}
		
		// Build a conjunction of unary tuple expressions
		UnaryTuplePredicateWithLookup main = null;
		while (!predicates.isEmpty()) {
			if (main == null) {
				main = predicates.get(0);
				Set<String> vars = main.getSymbolsReferenced();
				Map<String, String> replace = new HashMap<>();
				for (String var: vars) {
					String sym = g.getSymbolTable().get(var);
					Set<Node<T>> nodes = new HashSet<>();
					Node<T> nod = g.getNode(sym);
					nodes.add(nod);
					if (nod != null) {
						nodes.addAll(nod.getEquivalents());
						for (Node<T> nod2: nodes)
							if (graphNodes.contains(nod2))
								replace.put(var, revMap.get(nod2));
					}
				}
				main.rename(replace);
			} else {
				UnaryTuplePredicateWithLookup main2 = predicates.get(0);
				Set<String> vars = main2.getSymbolsReferenced();
//				main2.rename(g.getSymbolTable());
				Map<String, String> replace = new HashMap<>();
				for (String var: vars) {
					String sym = g.getSymbolTable().get(var);
					Set<Node<T>> nodes = new HashSet<>();
					Node<T> nod = g.getNode(sym);
					nodes.add(nod);
					if (nod != null) {
						nodes.addAll(nod.getEquivalents());
						for (Node<T> nod2: nodes)
							if (graphNodes.contains(nod2))
								replace.put(var, revMap.get(nod2));
					}
				}
				main2.rename(replace);
				main = new UnaryTupleBooleanExpression(BooleanOperation.And, main, main2);
			}
			predicates.remove(0);
		}
		if (main != null) {
			logger.debug("Selecting on {}", main);
			node.setOperator(new SelectionOperator<T>(node.getOperator(), main, QueryGen.provWrapperAPI));

			List<String> toProject = 
					QueryGraphHelper.getReferencedSymbols(symTab, node, g);
				
			logger.debug("Symbols used: {}", toProject);
			if (toProject.size() > 0 && toProject.size() < node.getSchema().getArity()) {
				logger.debug("Projecting down to symbols: {}" + toProject);
				node.setOperator(new ProjectionOperator<T>(node.getOperator(), 
						toProject, QueryGen.provWrapperAPI));
			}

			
			return StatsHelper.estimateSelStatistics(sourceStats, main);
		} else {
			List<String> toProject = 
					QueryGraphHelper.getReferencedSymbols(symTab, node, g);
				
			logger.debug("Symbols used: {}", toProject);
			if (toProject.size() > 0 && toProject.size() < node.getSchema().getArity()) {
				logger.debug("Projecting down to symbols: {}" + toProject);
				node.setOperator(new ProjectionOperator<T>(node.getOperator(), 
						toProject, QueryGen.provWrapperAPI));
			}

			return sourceStats;
		}
	}

	/**
	 * Recursively collect all nodes at the leaf level.  We will count blocking, grouping, or materialization
	 * as leaves for the current query block
	 * 
	 * @param root
	 * @param leaves
	 */
	public static <T extends TableWithVariableSchema> void 
	collectLeaves(
			QueryOperator<T> root, List<QueryOperator<T>> leaves, boolean addRenames) {
		if (root.getChildren().isEmpty() || root instanceof GroupByOperator || root instanceof BlockingOperator ||
				root instanceof MaterializeOperator)
			leaves.add(root);
		else {
			// If we have an extraction, that will count as a leaf because it's really joining with something
			// Ditto for a rename because it introduces new fields (equal to the old ones)
			if (root instanceof FieldExtractionOperator || (addRenames && root instanceof RenameOperator))
				leaves.add(root);
			
			// Recursively add descendents
			for (QueryOperator<T> child: root.getChildren())
				collectLeaves(child, leaves, addRenames);
		}
	}
	
	/**
	 * Iterate through the plan and collect all leaves.
	 * 
	 * @param root
	 * @return
	 */
	public static <T extends TableWithVariableSchema> List<QueryOperator<T>> 
	getLeaves(QueryOperator<T> root, boolean addRenames) {
		List<QueryOperator<T>> ret = new ArrayList<>();

		// By default we'll try to ignore the root node,
		// and just work on its subtrees
		for (QueryOperator<T> child: root.getChildren())
			collectLeaves(child, ret, addRenames);
		
		// If it's empty, the root must itself be a leaf
		if (root.getChildren().isEmpty())
			ret.add(root);
		
		return ret;
	}
	
}

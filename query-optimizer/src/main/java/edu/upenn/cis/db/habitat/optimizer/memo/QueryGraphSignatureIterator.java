/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.optimizer.memo;

import java.util.BitSet;
import java.util.Iterator;

import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * An iterator that finds all subsets of a query graph signature / bitset
 * 
 * @author ZacharyIves
 *
 * @param <T>
 */
public class QueryGraphSignatureIterator<T extends TableWithVariableSchema> implements Iterator<QueryGraphSignature<T>> {
	final int mustSubset;
	final BitSet mainSet;
	Iterator<QueryGraphSignature<T>> subIterator = null;
	int pos;
	boolean first = false;
	
	public QueryGraphSignatureIterator(BitSet mainSet, int start, int mustSubset) {
		this.mustSubset = mustSubset;
		this.mainSet = mainSet;
		
		// Find the first bit that is actually set in the main set
		pos = start;
		while (!mainSet.get(pos) && pos >= 0)
			pos--;
		
		if (pos < 0)
			throw new IllegalArgumentException("Cannot subset a bitset with too few set bits");
		
		if (mustSubset > 1) {
			subIterator = new QueryGraphSignatureIterator<T>(mainSet, pos - 1, mustSubset - 1);
		}
		first = (mustSubset == 1);
	}

	@Override
	public boolean hasNext() {
		// If we can get another sub-iterator 
		if (pos >= (mustSubset-1) && (subIterator != null && subIterator.hasNext()))
			return true;
		else if (first && pos >= 0)
			return true;

		// Are there mustSubset bits available if we move?
		int count = 0;
		for (int i = 0; i < pos; i++)
			if (mainSet.get(i))
				count++;
		
		return count >= mustSubset;
	}

	@Override
	public QueryGraphSignature<T> next() {
		// Choose 1 bit from pos, plus whatever is in the child
		QueryGraphSignature<T> child = null;
		if (first) {
			first = false;
//			System.out.println(pos + " no child");
		} else if ((subIterator != null && subIterator.hasNext())) {
//			System.out.println(pos + " read child");
			child = subIterator.next();
		} else {
			do {
				pos--;
			} while (pos >= 0 && !mainSet.get(pos));
//			System.out.println("Decrementing parent to " + pos + ", subset " + mustSubset);
			if (mustSubset > 1 && pos > 0) {
//				System.out.println(pos + " plus new child");
				subIterator = new QueryGraphSignatureIterator<T>(mainSet, pos - 1, mustSubset - 1);
				
				if (subIterator != null && subIterator.hasNext())
					child = subIterator.next();
			} else {
//				System.out.println("Return " + pos);
				subIterator = null;
			}
		}
		
		BitSet ret = new BitSet(mainSet.length());
		ret.set(pos);
		if (child != null)
			ret.or(child.getBits());
		else if (mustSubset > 1)
			throw new RuntimeException("Underflow");
		
		return new QueryGraphSignature<T>(ret);
	}
}

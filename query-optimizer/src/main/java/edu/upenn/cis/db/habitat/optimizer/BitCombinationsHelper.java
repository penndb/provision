package edu.upenn.cis.db.habitat.optimizer;

import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BitCombinationsHelper {
	static Map<BitSet,Set<BitSet>> bitSubsets = new HashMap<>();
	
	public static Set<BitSet> getAllSubsets(BitSet bs) {
		
		// Take these from a cache if available
		Set<BitSet> allSubsets = bitSubsets.get(bs); 
				
//		if (allSubsets != null)
//			return allSubsets;
		
		// TODO: go from 
		
		Set<BitSet> ret = new HashSet<>();

		int pos = bs.nextSetBit(0);
		int max = bs.length();
		
		while (pos >= 0 && pos < max) {
			// Initialize entries with this bit set + clear
//			System.out.println(pos);
			if (ret.isEmpty()) {
				BitSet set = new BitSet(max);
				set.set(pos);
				ret.add(set);
				set = new BitSet(max);
//				set.clear(pos);
				ret.add(set);
			} else {
				Set<BitSet> working = new HashSet<>();
				for (BitSet set: ret) {
					BitSet set2 = new BitSet(max);
					set2.or(set);
					set2.set(pos);
					working.add(set2);
//					bitSubsets.put(set2, ret);
					set2 = new BitSet(max);
					set2.or(set);
					set2.clear(pos);
					working.add(set2);
//					bitSubsets.put(set2, ret);
				}
				ret = working;
			}
			pos = bs.nextSetBit(pos+1);
		}
		
//		// Remove the all-zeros case
		BitSet blank = new BitSet(bs.size());
		ret.remove(blank);
		ret.remove(bs);
		
		return ret;
	}


}

package edu.upenn.cis.db.habitat.sql.parsing;

import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.util.TablesNamesFinder;

public class TableAliasFinder extends TablesNamesFinder {

	@Override
    protected String extractTableName(Table table) {
        Alias ali = table.getAlias();
        
        if (ali == null)
        	return table.getFullyQualifiedName();
        else
        	return ali.getName();
    }

}

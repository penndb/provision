/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.optimizer.memo;

import java.util.BitSet;

import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

/**
 * A query graph signature is a concise description of a query fragment.
 * We use a bit vector based on a subset of the canonically ordered set
 * of source nodes in the query graph.
 * 
 * e.g., If we have 3 nodes A, B, C that get joined in a query graph,
 * then the signature [110] indicates A \join B.  Also, [110] subsumes
 * [010] and [100] (and itself).
 * 
 * @author ZacharyIves
 *
 * @param <T>
 */
public class QueryGraphSignature<T extends TableWithVariableSchema> {
	final BitSet sourcesPresent;

	public QueryGraphSignature(BitSet sourcesPresent) {
		this.sourcesPresent = sourcesPresent;
	}

	/**
	 * If we subsume the other bit set, then anding with us won't change
	 * anything in it.
	 * 
	 * @param o
	 * @return
	 */
	public boolean subsumes(QueryGraphSignature<T> o) {
		BitSet workingSet = (BitSet) o.sourcesPresent.clone();
		workingSet.and(sourcesPresent);
		
		return (workingSet.equals(o.sourcesPresent));
	}

	public boolean subsumedBy(QueryGraphSignature<T> o) {
		return o.subsumedBy(this);
	}
	
	public int hashCode() {
		return sourcesPresent.hashCode();
	}

	public BitSet getBits() {
		return sourcesPresent;
	}
	
	@Override
	public String toString() {
		return sourcesPresent.toString();
	}
}

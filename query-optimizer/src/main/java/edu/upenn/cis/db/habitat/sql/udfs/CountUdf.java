package edu.upenn.cis.db.habitat.sql.udfs;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.JavaUdf;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class CountUdf extends JavaUdf<TableWithVariableSchema, BasicTuple, Integer> {
	
	public CountUdf() {
		super("count", new BasicSchema("count"), new ArrayList<>(), new BasicSchema("count"));
		
		super.getOutputSchema().addField("count", Integer.class);
	}
	
	@Override
	public BasicTuple apply(TableWithVariableSchema t) {
		BasicTuple tup = super.getOutputSchema().createTuple();
		
		tup.setValue("count", t.size());
		return tup;
	}

	@Override
	public Function<BasicTuple, Integer> computeInput() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JavaUdf<TableWithVariableSchema, BasicTuple, Integer> instantiate(
			List<ArithmeticExpressionOperation<Integer>> op) {
		return new CountUdf();
	}
}

/************************************************
 * Copyright 2019, 20 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.optimizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.python.google.common.collect.Sets;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import edu.upenn.cis.db.habitat.catalog.SystemCatalog;
import edu.upenn.cis.db.habitat.catalog.TableStatistics;
import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTupleBooleanExpression;
import edu.upenn.cis.db.habitat.core.expressions.BinaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations;
import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations.BooleanOperation;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTupleBooleanExpression;
import edu.upenn.cis.db.habitat.core.expressions.UnaryTuplePredicateWithLookup;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.JavaUdf;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.engine.operators.BlockingOperator;
import edu.upenn.cis.db.habitat.engine.operators.CartesianOperator;
import edu.upenn.cis.db.habitat.engine.operators.FieldExtractionOperator;
import edu.upenn.cis.db.habitat.engine.operators.GroupByOperator;
import edu.upenn.cis.db.habitat.engine.operators.JoinOperator;
import edu.upenn.cis.db.habitat.engine.operators.MaterializeOperator;
import edu.upenn.cis.db.habitat.engine.operators.NullaryQueryOperator;
import edu.upenn.cis.db.habitat.engine.operators.ProjectionOperator;
import edu.upenn.cis.db.habitat.engine.operators.QueryOperator;
import edu.upenn.cis.db.habitat.engine.operators.RenameOperator;
import edu.upenn.cis.db.habitat.engine.operators.SelectionOperator;
import edu.upenn.cis.db.habitat.optimizer.costs.CardinalityMeasure;
import edu.upenn.cis.db.habitat.optimizer.costs.ResourceVector;
import edu.upenn.cis.db.habitat.optimizer.graph.HyperEdge;
import edu.upenn.cis.db.habitat.optimizer.graph.Node;
import edu.upenn.cis.db.habitat.optimizer.graph.QueryGraph;
import edu.upenn.cis.db.habitat.optimizer.memo.AlternatePlanSubgraph;
import edu.upenn.cis.db.habitat.optimizer.memo.JointPlanSubgraph;
import edu.upenn.cis.db.habitat.optimizer.memo.PlanSubgraph;
import edu.upenn.cis.db.habitat.querygen.QueryGen;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;
import edu.upenn.cis.db.habitat.sql.exceptions.TableNotFoundException;

/**
 * Main query optimizer
 * 
 * @author zives
 *
 * @param <P>
 * @param <T>
 */
public class OptimizeExpression<P,T extends TableWithVariableSchema> {
	Map<BitSet, PlanSubgraph<P,T>> memoTable = new HashMap<>();
	
	static Logger logger = LogManager.getLogger(OptimizeExpression.class);
	
	SystemCatalog catalog;
	Function<ResourceVector,Integer> costFunction;
	
	public OptimizeExpression(SystemCatalog catalog, Function<ResourceVector,Integer> costFunction) {
		this.catalog = catalog;
		this.costFunction = costFunction;
	}
	
	public OptimizeExpression(SystemCatalog catalog) {
		this.catalog = catalog;
		
		// Default cost function is based on cardinality only
		this.costFunction = 
				((ResourceVector rv) -> 
				Integer.valueOf((int)((CardinalityMeasure)rv.get("cardinality")).getValue()));
	}

	/**
	 * Get the roots of any query plans that aren't true leaves 
	 * 
	 * @param root
	 * @return
	 */
	public List<QueryOperator<T>> getSubBlocks(QueryOperator<T> root) {
		List<QueryOperator<T>> ret = RelAlgebraHelper.getLeaves(root, true);
		
		ret.removeIf(op -> (op instanceof NullaryQueryOperator));
		
		return ret;
	}

	public List<QueryGraph<T>> getQueryGraphsForExpression(QueryOperator<T> root) {
		List<QueryGraph<T>> ret = new ArrayList<>();
		List<QueryOperator<T>> leaves = RelAlgebraHelper.getLeaves(root, true);
		
		QueryGraph<T> graph = getQueryGraph(root, leaves);
		ret.add(graph);
		
		// Recursively expand out the child plans
		for (QueryOperator<T> childPlan: leaves) {
			if (!(childPlan instanceof NullaryQueryOperator)) {
				ret.addAll(getQueryGraphsForExpression(childPlan));
			}
		}
		
		return ret;
	}
	
	/**
	 * Create a graph as follows.  One node per source
	 * or function.  Explode into fields.  One (hyper)edge per 2+-ary predicate.
	 * 
	 * TODO: Add transitive hyperedges.
	 */
	public QueryGraph<T> getQueryGraph(QueryOperator<T> root, Collection<QueryOperator<T>> leaves) {
		QueryGraph<T> ret = new QueryGraph<>(root, root.getId().toString());
		
		logger.info("Optimizing " + root.getLogicalDescriptor());
		for (QueryOperator<T> leaf: leaves) {
			logger.debug("** " + leaf.getClass().getSimpleName() + " **");
			BasicSchema schema = leaf.getAdditionalSchema();
			
			for (String field: schema.getKeys()) {
				if (!field.equals(ProvenanceApi.Provenance) && !field.equals(ProvenanceApi.Token)) {
					logger.debug("Adding node " + leaf.getId().toString() + "/" + field);
					ret.addNode(new Node<T>(leaf, field));
				}
			}
		}
		
		logger.info(" Adding predicates and causal dependencies for " + root.getLogicalDescriptor());
		Map<String,String> symbolTable = QueryGraphHelper.addEdges(root, leaves, ret);
		
		ret.setSymbolTable(symbolTable);
		
		logger.debug("Symbol table: {}", symbolTable);
		
		// If there's a terminal group-by, make all of its referenced symbols linked by edges in the graph
		if (root instanceof GroupByOperator) {
			GroupByOperator<T> gby = (GroupByOperator<T>)root;
			
			Node<T> node = new Node<T>(gby, "group");
			ret.addNode(node);
			
			for (String grouper: gby.getGroupingFields()) {
				grouper = ret.getSymbolTable().get(grouper);
				if (ret.getNode(grouper) != null)
					ret.addEdge(new HyperEdge<T>(ret.getNode(grouper), node, false));
			}
			
			for (Function<T, BasicTuple> f: gby.getAggFunctions()) {
				if (f instanceof JavaUdf) {
					JavaUdf<TableWithVariableSchema, TupleWithSchema<String>, ?> j = (JavaUdf)f;
					
					for (ArithmeticExpressionOperation<?> exp: j.getExpressions()) { 
						for (String str: exp.getSymbolsReferenced()) {
							str = ret.getSymbolTable().get(str);
							if (ret.getNode(str) != null)
								ret.addEdge(new HyperEdge<T>(ret.getNode(str), node, false));
						}
					}
				}
			}
		}
		
		
		return ret;
	}
	
	/**
	 * Optimize the query graph
	 * 
	 * @param g
	 * @return
	 */
	public QueryOperator<T> getOptimalPlan(QueryGraph<T> g) throws Exception {
		List<QueryOperator<T>> leaves = RelAlgebraHelper.getLeaves(g.getBlockRoot(), false);
		
		// Enumerate full expression (all bits set = all relations joined)
		BitSet bs = new BitSet(leaves.size()); 
		bs.flip(0, leaves.size());
		
		g.setBlockRoot(enumerate(g, leaves, bs).getOperator());
		g.setPlanGraph(memoTable.get(bs));

		return g.getBlockRoot();
	}

	/**
	 * Recursive plan enumeration
	 * 
	 * @param g Query graph
	 * @param leaves Operators delineating this query block from subqueries
	 * @param bs Bit-set describing expression signature to enumerate in top-down fashion
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws TableNotFoundException
	 * @throws HabitatServiceException
	 */
	PlanSubgraph<P,T> enumerate(QueryGraph<T> g, List<QueryOperator<T>> leaves, BitSet bs) throws JsonParseException, JsonMappingException, IOException, TableNotFoundException, HabitatServiceException {
		PlanSubgraph<P,T> node = memoTable.get(bs);

		if (node != null) {
			logger.debug("Existing plan from memoTable: " + node.getOperator());
			return node;
		}
		
		// Leaf node
		if (bs.cardinality() == 1) {
			RelAlgebraHelper.createLeaf(memoTable, bs, leaves, catalog, g);
			
			return memoTable.get(bs).getBest(costFunction);
		} else {
			Set<BitSet> allSubsets = BitCombinationsHelper.getAllSubsets(bs);
			
			for (BitSet left: allSubsets) {
				if (left.cardinality() == 0)
					continue;
					
				BitSet right = new BitSet(leaves.size());
				
				// right = bs and not left
				right.or(bs);
				right.andNot(left);
				
				// Can't do left-linear only
				if (right.cardinality() == 0)// || right.cardinality() > 1)
					continue;
				
				if (left.cardinality() + right.cardinality() != bs.cardinality())
					logger.error("Mismatch: {}, {} != {}", left, right, bs);
				
//				logger.debug("Combination: {} x {}", left, right);
				
				// This operation is invalid
				if (!RelAlgebraHelper.isValidJoin(left, right, leaves))
					continue;

				PlanSubgraph<P,T> leftNode = memoTable.get(left);
				if (leftNode == null)
					leftNode = enumerate(g, leaves, left);
				
				PlanSubgraph<P,T> rightNode = memoTable.get(right);
				if (rightNode == null)
					rightNode = enumerate(g, leaves, right);
				
				// One of the subplans is invalid
				if (leftNode == null || rightNode == null || isBushy(rightNode))
					continue;

				JointPlanSubgraph<P,T> firstOption = new JointPlanSubgraph<>(null);
				boolean join = false;
				List<String> leftFields = new ArrayList<>();
				List<String> rightFields = new ArrayList<>();
				
				// Is it actually a field extraction UDF operator, which is modeled as a
				// dependent join? 
				if (right.cardinality() == 1 && 
						RelAlgebraHelper.getSourceOp(rightNode.getOperator()) instanceof FieldExtractionOperator) {

					// This operation is invalid
					if (!RelAlgebraHelper.isValidExtraction(memoTable, g, left, right, leaves))
						continue;

					FieldExtractionOperator<T,?,?> op = RelAlgebraHelper.cloneExtraction( 
							leftNode.getOperator(), 
							(FieldExtractionOperator<T,?,?>)leaves.get(right.nextSetBit(0)));
					
					firstOption.setOperator(op);

					logger.debug("Creating UDF extraction {}", op.getId());
					BasicSchema jointSchema = new BasicSchema(firstOption.getOperator().getId().toString());
					for (int inx = 0; inx < leftNode.getSchema().getArity(); inx++)
						jointSchema.addField(leftNode.getSchema().getKeyAt(inx), 
								leftNode.getSchema().getTypeAt(inx));
					for (int inx = 0; inx < op.getAdditionalSchema().getArity(); inx++)
						if (!op.getAdditionalSchema().getKeyAt(inx).equals(ProvenanceApi.Provenance) && 
								!op.getAdditionalSchema().getKeyAt(inx).equals(ProvenanceApi.Token)) {
							jointSchema.addField(op.getAdditionalSchema().getKeyAt(inx), op.getAdditionalSchema().getTypeAt(inx));
						}
					firstOption.setSchema(jointSchema);
					
				// Or a true join?
				} else {
					join = true;
					
					BinaryTuplePredicateWithLookup predicate = RelAlgebraHelper.createJoin(g, leftNode, rightNode,
							leftFields, rightFields);
					
					if (leftNode.getOperator() == null || rightNode.getOperator() == null) {
						logger.error("Child without operator!");
						throw new RuntimeException("Error in optimizer logic");
					}
					
					// No Cartesian products
					if (leftFields.isEmpty())
						continue;

					JoinOperator<T> op = new JoinOperator<T>(leftNode.getOperator(), 
							rightNode.getOperator(), leftFields, rightFields, predicate, QueryGen.provWrapperAPI);
					
					logger.debug("Creating join {}", op.getId());
					
					// TODO: derive properties, profile etc.
	
					// TODO: explore alternatives, also branch + bound
					firstOption.setOperator(op);
					BasicSchema jointSchema = new BasicSchema(firstOption.getOperator().getId().toString());
					for (int inx = 0; inx < leftNode.getSchema().getArity(); inx++)
						jointSchema.addField(leftNode.getSchema().getKeyAt(inx), leftNode.getSchema().getTypeAt(inx));
					for (int inx = 0; inx < rightNode.getSchema().getArity(); inx++)
						jointSchema.addField(rightNode.getSchema().getKeyAt(inx), rightNode.getSchema().getTypeAt(inx));
					firstOption.setSchema(jointSchema);
				}
				
				TableStatistics joinStats = StatsHelper.estimateJoinStatistics(leftNode.getStatistics(),
						rightNode.getStatistics(), join, leftFields, rightFields);
				int estCard = joinStats.getCardinality();
//				
				// Resources should currently include tuples used
				firstOption.setResources(new ResourceVector("cardinality", 
						new CardinalityMeasure(estCard)));
				firstOption.getCovered().addAll(leftNode.getCovered());
				firstOption.getCovered().addAll(rightNode.getCovered());
				
				firstOption.setStatistics(
						RelAlgebraHelper.pushdown(g, firstOption, joinStats)
						);

				if (memoTable.get(bs) == null) {
					memoTable.put(bs, new AlternatePlanSubgraph<P,T>(firstOption));
					memoTable.get(bs).setResources(memoTable.get(bs).getChildren().get(0).getResourcesUsed());
					memoTable.get(bs).setStatistics(memoTable.get(bs).getChildren().get(0).getStatistics());
					memoTable.get(bs).setSchema(memoTable.get(bs).getChildren().get(0).getSchema());
					memoTable.get(bs).setOperator(memoTable.get(bs).getChildren().get(0).getOperator());
					memoTable.get(bs).getCovered().addAll(memoTable.get(bs).getChildren().get(0).getCovered());
					
					logger.debug("New AlternatePlanSubgraph for {}: {} x {} with {}", bs, left ,right, memoTable.get(bs).getStatistics().getCardinality());					
				} else {
					memoTable.get(bs).addSubplan(firstOption);
					logger.debug("Adding to AlternatePlanSubgraph for {}: {} x {} with {}", bs, left,right, memoTable.get(bs).getStatistics().getCardinality());					
					memoTable.get(bs).setResources(memoTable.get(bs).getBest(costFunction).getResourcesUsed());
					memoTable.get(bs).setStatistics(memoTable.get(bs).getBest(costFunction).getStatistics());
					memoTable.get(bs).setSchema(memoTable.get(bs).getBest(costFunction).getSchema());
					memoTable.get(bs).setOperator(memoTable.get(bs).getBest(costFunction).getOperator());
					memoTable.get(bs).getCovered().addAll(memoTable.get(bs).getBest(costFunction).getCovered());
				}
			}
		}
		
		// Was there a valid entry?
		if (memoTable.containsKey(bs))
			return memoTable.get(bs).getBest(costFunction);
		else
			return null;
	}

	private boolean isBushy(PlanSubgraph<P, T> rightNode) {
		return rightNode.getOperator().flatten().filter(op -> (op instanceof JoinOperator))
			.count() > 1;
	}

}

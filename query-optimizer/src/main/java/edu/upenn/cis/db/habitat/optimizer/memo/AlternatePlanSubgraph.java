/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.optimizer.memo;

import java.util.function.Function;

import edu.upenn.cis.db.habitat.optimizer.costs.ResourceVector;
import edu.upenn.cis.db.habitat.repository.type.TableWithVariableSchema;

public class AlternatePlanSubgraph<P,T extends TableWithVariableSchema> extends 
	PlanSubgraph<P,T> {
	
	public AlternatePlanSubgraph( 
			PlanSubgraph<P, T> op) {
		addSubplan(op);
	}
	
	/**
	 * Find the min-cost subplan
	 * 
	 * @param costFn
	 * @return
	 */
	@Override
	public PlanSubgraph<P,T> getBest(Function<ResourceVector,Integer> costFn) {
		int minCost = costFn.apply(getSubplan(0).getResourcesUsed());
		PlanSubgraph<P,T> subplan = this.getSubplan(0);
		for (int i = 1; i < this.getSubplanCount(); i++) {
			int newCost = costFn.apply(getSubplan(i).getResourcesUsed());
			if (newCost < minCost) {
				minCost = newCost;
				subplan = this.getSubplan(i);
			}
		}
		return subplan;
	}
}

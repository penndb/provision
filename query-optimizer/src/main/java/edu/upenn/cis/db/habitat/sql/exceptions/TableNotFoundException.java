package edu.upenn.cis.db.habitat.sql.exceptions;

public class TableNotFoundException extends Exception {

	public TableNotFoundException(String error) {
		super(error);
	}
}

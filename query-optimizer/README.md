# Query Optimizer for PROVision System / Habitat Framework

## Overview and Example Usage

The Habitat optimizer is a *rewriting* optimizer: it takes an existing query plan (generated manually or via the SQL parser), breaks it into **blocks**, and uses dynamic programming to separately optimize each block.  Internally, the optimizer makes use of a **query plan graph** capturing relationships among variables and a **memo table** to map between plan expressions and the optimum (known) plan.

Simply running an SQL query over a data file (in the `src/test/resources` directory) looks like the following (where `builder` is an `SQLExpressionBuilder`):

```
String statement = "SELECT * FROM \"genome/test_1.fq\" T";

Statement st = builder.parseSQL(statement);
		
QueryOperator<TableWithVariableSchema> op = builder.getQueryExpression((Select) st);
		
///// Exec the plan
if (op.initialize()) {
  List<MutableTupleWithSchema<String>> results = new ArrayList<>();
  int count = 0;
  while (op.getNextTuple(results))
    count++;
      
  System.out.println("Query produced " + count + " results");
  op.close();
}
```

Note that the above does not trigger the optimizer, but only the query plan generator.  To optimize, we actually need to initialize the default query plan in order to read the schema from the source file (`test_1.fq` above), then close that plan and replace it:

```
String statement = "SELECT * FROM \"genome/test_1.fq\" T";

Statement st = builder.parseSQL(statement);
		
QueryOperator<TableWithVariableSchema> op = builder.getQueryExpression((Select) st);
		
op.initialize();

OptimizeExpression<Serializable, TableWithVariableSchema> optimizer = new OptimizeExpression<>(builder.getDDLManager().getCatalog());
if (op.initialize()) {
  op.close();
		
  // What are the nodes designating subplans?
  List<QueryOperator<TableWithVariableSchema>> leaves = RelAlgebraHelper.getLeaves(op, true);
			
  // Abstracts the predicates in the query for the optimizer
  QueryGraph<TableWithVariableSchema> queryGraph = optimizer.getQueryGraph(op, leaves);

  // Actual optimization, which currently optimizes the SPJ part of the query but not any final grouping
  QueryOperator<TableWithVariableSchema> newOp = optimizer.getOptimalPlan(queryGraph);
      
  // Now create a new group-by based on the newOp and the original group
  op = RelAlgebraHelper.cloneGroup(newOp, (GroupByOperator)op, queryGraph);  

  ///// Exec the plan
  if (op.initialize()) {
    List<MutableTupleWithSchema<String>> results = new ArrayList<>();
    int count = 0;
    while (op.getNextTuple(results))
      count++;
        
    System.out.println("Query produced " + count + " results");
    op.close();
  }
}
```

You can look at `SqlDmlTest` for example test cases of the query parser and optimizer applied to SQL queries, and `TestExpressionEnumeration` for example test cases of the core parts of the optimizer search.

## SQL Parsing and Plan Generation

The `SqlExpressionBuilder` leverages the open-source [JSQLParser](https://github.com/JSQLParser/JSqlParser) to parse standard SQL query blocks.

An individual SQL statement is parsed via `parseSQL` with the SQL string, which returns a jsql `Statement`.

This `Statement` is fed into `getQueryExpression`, which in turn results in a query expression tree (query plan) consisting of a series of `QueryOperator`s.  The plan will be generated in left-linear form based on the sequence of tables in the `FROM` clause of the SQL query, and is quite likely to be sub-optimal.

## Rewriting a Query Plan

The main class responsible for query rewriting is `OptimizeExpression`.  Rewriting takes the SQL query plan, converts it back into a *query graph*, and explores equivalent plans. We use dynamic programming to consider all (legal) join orders and combinations of joins and user-defined functions (`FieldExtraction` operators).  Cost estimation requires a general-purpose abstraction for cost parameters, information about data distributions, and pruning strategies.

### Determining the Schema via the Default Plan

Habitat often is used to query over "external" data sources that are not registered in the system catalog.  Thus, before we optimize, we run `initialize` on the query plan from the plan generator.  This is enough to open the source files, read their headers, determine schemas, and propagate them through the query plan.  The initial plan is then immediately closed.  The optimizer will generate a replacement plan (which shares the operators used as leaves, but replaces all others).

### The System Catalog

The Habitat `SystemCatalog` contains information about schemas (by their path name), statistics on tables, statistics on fields, and UDF definitions.  This is consulted by the query optimizer.

Tables in PROVision are uniquely named using a file path (as necessary, delimited in SQL using double-quotes around the pathname).

For different input files, different levels of catalog information are required for correct program execution.  Certain input files (e.g., FASTQ) have fixed schemas, and thus do not need to be manually registered in the system catalog.  Most input files should be defined in the system catalog using SQL DDL `CREATE TABLE` constructs.

### Enumeration and Memoization

The optimizer, in the tradition of Volcano and its successors, uses *top-down* enumeration, i.e., it starts with the full query and recursively explores potential subqueries.

We start by generating a **query graph** from the default plan.  This allows us to establish all input relations and their attributes (each of these becomes a `Node`); predicates (each of these becomes a bidirectional `Edge`); and dependent dataflow in user-defined functions (directed `Edge`s).

To generate an optimal plan, we capture each query expression via a *signature*, which is stored as a Java `BitSet`.  Each bit represents a source relation (or the application of a UDF).  Given a signature, we take the set of relations and UDFs, and link them all via joins and UDF invocations (`FieldExtraction`s).  We maximally apply all predicates (i.e., all edges between the nodes represented by the signature).  This means:

* Selection `UnaryTuplePredicate`s are pushed down (expensive selections should be modeled as `FieldExtraction`s)
* Join `BinaryTuplePredicate`s are also applied between all sources.  If the predicates are equality-based, these are typically converted into hash keys for hash-based `JoinOperator`s.  Any additional predicates are applied to the results of any hash probe.
* `ProjectionOperator`s are maximally applied after each expression, based on whether the attributes are used elsewhere in the query. (This is indicated by `Edges` to other nodes outside the query expression.) **Currently there is limited logic for determining SELECT-fields, but GROUP BY keys are captured using edges.**

#### And-Or Graphs and Plan Enumeration

Enumeration creates a memo table, which maps between `BitSet` *signatures* and `PlanSubgraphs`.  There are two kinds of `PlanSubgraphs`: `AlternatePlanSubgraphs` or OR nodes, and `JointPlanSubgraphs` or AND nodes.  In reality, each memo table entry points from a signature to an `AlternatePlanSubgraph`; in turn each `AlternatePlanSubgraph` has one or more `JointPlanSubgraph`s as children.  This is sometimes termed an AND-OR graph structure because we layer AND (joint) and OR (alternate) nodes. (It would additionally be a *tree* if we disallow subexpression sharing)

A `PlanSubgraph` points to an algebraic `QueryOperator` representing the root of a query subexpression.  This represents the actual query plan corresponding to the expression.

Each subgraph has zero or more `PlanSubgraph` children, representing input query subexpressions.  An `AlternatePlanSubgraph` represents a choice among several possible plan subexpressions (e.g., a join `RST` could be `(RS)T`, `(RT)S`, `R(ST)`, `T(RS)`, etc).  The `AlternatePlanSubgraph` typically chooses from among its children, based on the `getBest` function and a **cost estimator**.

### Cost Estimation

* `TableStatistics` keeps (1) a name and cardinality of an overall query expression, and (2) a map between field names and estimated numbers of unique values.
* `ResultProfile` represents a summary structure on a key:
  * `BucketProfile` represents a histogram bucket.
  * `HashProfile` represents a hash-based sketch.
  * **Learned indices** would be ideal here
* `ResourceVector` is intended to capture the amount of named resources used by the query.  Bandwidth, CPU, disk I/O can be tracked separately.  If there are multiple CPU cores these can be tracked separately.
* `ResultProperties` is a general dictionary that allows us to capture, e.g., sort order on a key, parallelism of some form, or hash partitioning on a key.

### Pruning Heuristics

Our initial implementation creates *left-linear* plans with respect to joins: no join has as its input a right-child that includes a join, i.e., the right child of each join is typically a base table.

However, given that UDFs may require more complex dataflow that is not amenable to bushy plans, the `FieldExtractor` portion of the query plan may be "bushy."  Thus, either the left or right child of a join or a UDF may include the output of another UDF.

*Branch-and-bound*


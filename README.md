# README

PROVIsion source code. Depends on https://bitbucket.org/penndb/hab-repo for Habitat repository back-end and client interfaces.

## PROVision query services:

This project allows us to **reconstruct** provenance from various scientific workflows using "semantic descriptors".
Unlike many provenance systems, our novelty is in supporting **fine-grained** provenance tracing, for a wide variety
of matching and ETL operations.  See https://ieeexplore.ieee.org/abstract/document/8731460/ for our paper describing 
the details.

# Setup

1. Install Eclipse, Maven, and Docker.

1. Ensure that Postgres and Neo4J are set up.  To do this, clone https://bitbucket.org/penndb/hab-repo and follow the Setup instructions there.

1. Run ``mvn clean install`` to build the various JARs.  For the final PROVision project (in **graybox**), we build a "fat jar" for the final result, which is large but nicely self-contained.  

Run ``java -jar graybox/target/graybox-0.0.1-SNAPSHOT.jar`` to launch the Web app.  It needs Postgres and Neo4J to be installed, as per above.  

You can go to ``localhost:8088`` in your browser to log in via Google OAuth.  

Once you log in, you'll see a screen with a series of example use cases, a link to Neo4J to see the provenance graph, and more.

